/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-12-07 16:57:23 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 *
 */
#ifndef UVES_REDUCE_MFLAT_COMBINE_H
#define UVES_REDUCE_MFLAT_COMBINE_H

#include <cpl.h>

#include <stdbool.h>

int
uves_mflat_combine_define_parameters_body(cpl_parameterlist *parameters, 
                  const char *recipe_id);

void
uves_mflat_combine_exe_body(cpl_frameset *frames, 
            const cpl_parameterlist *parameters,
            const char *starttime,
            const char *recipe_id);

extern const char * const uves_mflat_combine_desc;

#endif  /* UVES_REDUCE_MFLAT_COMBINE_H */
