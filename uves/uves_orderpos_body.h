/*                                                                              *
 *   This file is part of the ESO UVES  Pipeline                                *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:04 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.2  2006/10/26 14:03:31  jmlarsen
 * Fixed position of const modifier
 *
 * Revision 1.1  2006/10/24 14:03:00  jmlarsen
 * Factored out common UVES/FLAMES code
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.10  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_ORDERPOS_BODY_H
#define UVES_ORDERPOS_BODY_H

#include <cpl.h>
#include <stdbool.h>

int
uves_orderpos_define_parameters_body(cpl_parameterlist *parameters, 
                     const char *recipe_id);

void
uves_orderpos_exe_body(cpl_frameset *frames,
               bool flames,
               const char *recipe_id,
               const cpl_parameterlist *parameters,
               const char *starttime);

extern const char * const uves_orderpos_desc_short;
extern const char * const uves_orderpos_desc;

#endif /* UVES_ORDERPOS_BODY_H */
