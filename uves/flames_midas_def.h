/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 13:56:55 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2010/09/24 09:32:02  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.1  2009/04/14 07:01:07  amodigli
 * added to CVS (moded from flames tree)
 *
 * Revision 1.19  2007/10/01 17:19:50  amodigli
 * added scdprs
 *
 * Revision 1.18  2007/06/22 14:52:18  jmlarsen
 * Exported dtype_to_cpltype function
 *
 * Revision 1.17  2007/06/08 15:38:08  jmlarsen
 * Added option to not automatically propagate NAXIS
 *
 * Revision 1.16  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.15  2007/06/04 11:26:40  jmlarsen
 * Added SCKGETC_fsp for cpl_frameset pointers
 *
 * Revision 1.14  2007/05/30 12:06:03  amodigli
 * #include<fitsio.h>
 *
 * Revision 1.13  2007/05/29 14:39:50  jmlarsen
 * Readded 3d table functions but map them to simple (2d) table functions
 *
 * Revision 1.12  2007/05/29 13:46:23  jmlarsen
 * Removed 3d table column stubs which were not needed for FLAMES
 *
 * Revision 1.11  2007/04/10 07:31:13  jmlarsen
 * Added TCTID
 *
 * Revision 1.10  2007/04/03 11:05:33  jmlarsen
 * Implemented table module
 *
 * Revision 1.9  2007/03/26 12:40:52  jmlarsen
 * Converted SCTDIS calls
 *
 * Revision 1.8  2007/03/23 13:44:37  jmlarsen
 * Implemented SCKWR- functions
 *
 * Revision 1.7  2007/03/23 10:10:15  jmlarsen
 * Implemented catalog interface
 *
 * Revision 1.6  2007/03/05 09:40:31  jmlarsen
 * Added SCDCOP
 *
 * Revision 1.5  2007/02/27 14:09:31  jmlarsen
 * Extended interface of uves_find_property
 *
 * Revision 1.4  2007/01/31 13:14:57  jmlarsen
 * Initial implementation of SCFGET
 *
 * Revision 1.3  2007/01/29 13:09:57  jmlarsen
 * Work on conversion to CPL
 *
 * Revision 1.2  2007/01/15 14:00:23  jmlarsen
 * Imported FLAMES sources from MIDAS pipeline
 *
 * Revision 1.1  2007/01/10 08:06:10  jmlarsen
 * Added source files
 *
 * Revision 1.1  2006/10/20 06:42:09  jmlarsen
 * Moved FLAMES source to flames directory
 *
 * Revision 1.78  2006/10/12 11:37:28  jmlarsen
 * Temporarily disabled FLAMES code generation
 *
 */
#ifndef FLAMES_MIDAS_DEF_H
#define FLAMES_MIDAS_DEF_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <uves_cpl_size.h>
/*-----------------------------------------------------------------------------
                    Includes
 -----------------------------------------------------------------------------*/
#include <fitsio.h>
#include <cpl.h>
/*-----------------------------------------------------------------------------
                                   Typedefs
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                             Defines
 -----------------------------------------------------------------------------*/
/* Definitions not used by FLAMES are commented out */
/* from flames_uves.h */
#define MAREMMA 2

/* Data types */
#define D_OLD_FORMAT    0
#define D_I1_FORMAT     1               /* I*1 = 1 byte                 */
#define D_I2_FORMAT     2               /* I*2 = 16 bit integer         */
#define D_UI2_FORMAT  102               /* I*2 = 16 bit unsigned integer */
#define D_I4_FORMAT     4               /* I*4 = 32 bit integer         */
#define D_R4_FORMAT    10               /* R*4 = 32 bit floating point  */
#define D_R8_FORMAT    18               /* R*8 = 64 bit floating point  */
//#define D_L1_FORMAT    21               /* L*1 = 1 byte logical         */
//#define D_L2_FORMAT    22               /* L*2 = 16 bit logical         */
#define D_L4_FORMAT    24               /* L*4 = 32 bit logical         */
#define D_C_FORMAT     30               /* 1 byte character             */
//#define D_X_FORMAT     40               /* 1 byte flags                 */
//#define D_P_FORMAT     50               /* pointers                     */



/*  Filetypes */
//#define F_OLD_TYPE      0               /* use old type of files        */
#define F_IMA_TYPE      1               /* type no. for image files     */
//#define F_ASC_TYPE      2               /* type no. for ASCII files     */
#define F_TBL_TYPE      3               /* type no. for table files     */
//#define F_FIT_TYPE      4               /* type no. for fit files       */
//#define F_FIMA_TYPE     11              /* type no. for FITS images     */
//#define F_FTBL_TYPE     13              /* type no. for FITS tables     */
//#define F_FFIT_TYPE     14              /* type no. for FITS fit files  */



/* Opening modes */
//#define F_TRANS      0          /* table transposed format */
//#define F_RECORD     1          /* table record format */
#define F_I_MODE     0          /* map file for input only    */
#define F_O_MODE     1          /* map file for output        */
#define F_IO_MODE    2          /* map file for updating      */
//#define F_U_MODE     2          /* map file for updating      */
//#define F_X_MODE     9          /* create/map virtual memory  */
//#define F_H_MODE     7          /* create header file only */
//#define F_FO_MODE    11         /* map FITS file for output   */

/* May be combined with */
//#define F_MAP_FORCE 0x10        /* Force the mapping (modifier)         */
//#define F_EIO_FORCE 0x20        /* Force the elementary i/o (modifier)*/
//#define F_ALL_FORCE 0x40        /* Force the allocated values   */

/* Table i/o modes */
#define F_D_MODE     2          /* map for descriptors only */

/* From midas_def.h */
#define TEXT_LEN      84





/* init, end */
#define SCSPRO(name) flames_midas_scspro(name)
#define SCSEPI() flames_midas_scsepi()

/* error handling */

/* display text */
#define SCTPUT(msg) flames_midas_sctput(msg, __func__, __FILE__, __LINE__)

/* access keywords */
#define SCKGETC(key, felem, maxvals, actvals, values) flames_midas_sckgetc(key, felem, maxvals, actvals, values)
#define SCKGETC_fs(key, felem, maxvals, actvals, values)  flames_midas_sckgetc_fs(key, felem, maxvals, actvals, values)
#define SCKGETC_fsp(key, felem, maxvals, actvals, values) flames_midas_sckgetc_fsp(key, felem, maxvals, actvals, values)
#define SCKFND(key, type, noelem, bytelem) flames_midas_sckfnd(key, type, noelem, bytelem)
#define SCKFND_double(key, type, noelem, bytelem) flames_midas_sckfnd_double(key, type, noelem, bytelem)
#define SCKFND_float(key, type, noelem, bytelem) flames_midas_sckfnd_float(key, type, noelem, bytelem)
#define SCKFND_int(key, type, noelem, bytelem) flames_midas_sckfnd_int(key, type, noelem, bytelem)
#define SCKFND_string(key, type, noelem, bytelem) flames_midas_sckfnd_string(key, type, noelem, bytelem)

#define SCKRDD(key, felem, maxvals, actvals, values, unit, null) flames_midas_sckrdd(key, felem, maxvals, actvals, values, unit, null)
#define SCKRDC(key, noelem, felem, maxvals, actvals, values, unit, null) flames_midas_sckrdc(key, noelem, felem, maxvals, actvals, values, unit, null)
#define SCKRDI(key, felem, maxvals, actvals, values, unit, null) flames_midas_sckrdi(key, felem, maxvals, actvals, values, unit, null)
#define SCKRDR(key, felem, maxvals, actvals, values, unit, null) flames_midas_sckrdr(key, felem, maxvals, actvals, values, unit, null)

#define SCKWRC(key, noelem, values, felem, maxval, unit) flames_midas_sckwrc(key, noelem, values, felem, maxval, unit)
#define SCKWRD(key, values, felem, maxval, unit) flames_midas_sckwrd(key, values, felem, maxval, unit)
#define SCKWRI(key, values, felem, maxval, unit) flames_midas_sckwri(key, values, felem, maxval, unit)

/* manipulate catalogues */
#define SCCADD(catfile, name, ident) flames_midas_sccadd(catfile, name, ident)
#define SCCCRE(catfile, type, flag)  flames_midas_scccre(catfile, type, flag)
#define SCCFND(catfile, frmno, frame) flames_midas_sccfnd(catfile, frmno, frame)
#define SCCGET(catfile, flag, name, ident, no) flames_midas_sccget(catfile, flag, name, ident, no)
#define SCCSHO(catfile, noent, last) flames_midas_sccsho(catfile, noent, last)

/* access bulk data frames (bdf) */
#define SCFINF(name, fno, ibuf) flames_midas_scfinf(name, fno, ibuf)
#define SCFCRE(name, dattype, iomode, filtype, size, imno) flames_midas_scfcre(name, dattype, iomode, filtype, size, imno)
#define SCFOPN(name, dattype, newopn, filtype, imno) flames_midas_scfopn(name, dattype, newopn, filtype, imno)
#define SCFCLO(imno) flames_midas_scfclo(imno)
#define SCFPUT(imno, felem, size, bufadr) flames_midas_scfput(imno, felem, size, bufadr)
#define SCFGET(imno, felem, size, actsize, bufadr) flames_midas_scfget(imno, felem, size, actsize, bufadr)

/* access descriptors */
#define SCDFND(imno, descr, type, noelem, bytelem) flames_midas_scdfnd(imno, descr, type, noelem, bytelem) 
#define SCDPRS(imno, descr, type, noelem, bytelem) flames_midas_scdprs(imno, descr, type, noelem, bytelem) 
#define SCDRDI(imno, descr, felem, maxvals, actvals, values, unit, null) flames_midas_scdrdi(imno, descr, felem, maxvals, actvals, values, unit, null)
#define SCDRDD(imno, descr, felem, maxvals, actvals, values, unit, null) flames_midas_scdrdd(imno, descr, felem, maxvals, actvals, values, unit, null)
#define SCDWRI(imno, descr, values, felem, nval, unit) flames_midas_scdwri(imno, descr, values, felem, nval, unit)
#define SCDWRD(imno, descr, values, felem, nval, unit) flames_midas_scdwrd(imno, descr, values, felem, nval, unit)
#define SCDWRR(imno, descr, values, felem, nval, unit) flames_midas_scdwrr(imno, descr, values, felem, nval, unit)
#define SCDWRC(imno, descr, noelm, values, felem, nval, unit) flames_midas_scdwrc(imno, descr, noelm, values, felem, nval, unit)
#define SCDCOP(from, to, mask) flames_midas_scdcop(from, to, mask)
#define SCDCOP_nonaxis(from, to, mask) flames_midas_scdcop_nonaxis(from, to, mask)
#define SCDDEL(imno, descr) flames_midas_scddel(imno, descr)
#define SCDRDR(imno, descr, felem, maxvals, actvals, values, unit, null) flames_midas_scdrdr(imno, descr, felem, maxvals, actvals, values, unit, null)
#define SCDRDC(imno, descr, noelem, felem, maxvals, actvals, values, unit, null) flames_midas_scdrdc(imno, descr, noelem, felem, maxvals, actvals, values, unit, null)
/* note: SCDGETC is same as SCDRDC with noelem = number of bytes per data value = 1 */
#define SCDGETC(imno, descr, felem, maxvals, actvals, values) SCDRDC(imno, descr, 1, felem, maxvals, actvals, values, NULL, NULL)

/* table access */
#define TCTOPN(name, mode, tid) flames_midas_tctopn(name, mode, -1, tid)
#define TCTCLO(tid) flames_midas_tctclo(tid)
#define TCCSER(tid, colref, column) flames_midas_tccser(tid, colref, column)
#define TCIGET(tid, column, row) flames_midas_tciget(tid, column, row)

#define TCERDD(tid, row, column, value, null) flames_midas_tcerdd(tid, row, column, value, null)

#define TCBGET(tid, column, dtype, items, bytes) flames_midas_tcbget(tid, column, dtype, items, bytes)

#define TCCINI(tid, dtype, alen, form, unit, label, column) flames_midas_tccini(tid, dtype, alen, form, unit, label, column)
#define TCDGET(tid, store) flames_midas_tcdget(tid, store)

#define TCERDC(tid, row, column, values, null) flames_midas_tcerdc(tid, row, column, values, null)
#define TCERDI(tid, row, column, values, null) flames_midas_tcerdi(tid, row, column, values, null)
#define TCERDR(tid, row, column, values, null) flames_midas_tcerdr(tid, row, column, values, null)
#define TCEWRC(tid, row, column, value) flames_midas_tcewrc(tid, row, column, value)
#define TCEWRD(tid, row, column, value) flames_midas_tcewrd(tid, row, column, value)
#define TCEWRI(tid, row, column, value) flames_midas_tcewri(tid, row, column, value)
#define TCEWRR(tid, row, column, value) flames_midas_tcewrr(tid, row, column, value)

#define TCARDC(tid, row, col, index, items, value) flames_midas_tcardc(tid, row, col, index, items, value)
#define TCARDD(tid, row, col, index, items, value) flames_midas_tcardd(tid, row, col, index, items, value)
#define TCARDI(tid, row, col, index, items, value) flames_midas_tcardi(tid, row, col, index, items, value)
#define TCARDR(tid, row, col, index, items, value) flames_midas_tcardr(tid, row, col, index, items, value)
#define TCAWRC(tid, row, col, index, items, value) flames_midas_tcawrc(tid, row, col, index, items, value)
#define TCAWRD(tid, row, col, index, items, value) flames_midas_tcawrd(tid, row, col, index, items, value)
#define TCAWRI(tid, row, col, index, items, value) flames_midas_tcawri(tid, row, col, index, items, value)
#define TCAWRR(tid, row, col, index, items, value) flames_midas_tcawrr(tid, row, col, index, items, value)

#define TCFGET(tid, column, form, dtype) flames_midas_tcfget(tid, column, form, dtype)
#define TCLGET(tid, column, label) flames_midas_tclget(tid, column, label)
#define TCLSER(tid, label, column) flames_midas_tclser(tid, label, column)
#define TCSGET(tid, row, value) flames_midas_tcsget(tid, row, value)
#define TCSPUT(tid, row, value) flames_midas_tcsput(tid, row, value)

/* TCTINI  -->  TCTOPN */
#define TCTINI(name, mode, allrow, tid) flames_midas_tctopn(name, mode, allrow, tid)

#define TCUGET(tid, column, unit) flames_midas_tcuget(tid, column, unit)

#define flames_midas_fail() flames_midas_fail_macro(__FILE__, __func__, __LINE__)
#define flames_midas_error(status) flames_midas_error_macro(__FILE__, __func__, __LINE__, status)

/*-----------------------------------------------------------------------------
                                   Prototypes
 -----------------------------------------------------------------------------*/

/* init, end */
int flames_midas_scspro(const char *name);
int flames_midas_scsepi(void);
int flames_midas_fail_macro(const char *file, const char *function, int line);
int flames_midas_error_macro(const char *file, const char *function, int line,
                 int status);

/* error handling */


/* access keywords */
int flames_midas_sckgetc(const char *key, 
             int felem, int maxvals, 
             int *actvals, char *values);
int flames_midas_sckgetc_fs(const cpl_frameset *key, 
             int felem, int maxvals, 
             int *actvals, const cpl_frameset **values);

int flames_midas_sckgetc_fsp(cpl_frameset **key, 
             int felem, int maxvals, 
             int *actvals, cpl_frameset ***values);

int flames_midas_sckrdd(const double *key, int felem, int maxvals, 
            int *actvals, double *values, int *unit, int *null);
int flames_midas_sckrdi(const int *key, int felem, int maxvals, 
            int *actvals, int *values, int *unit, int *null);
int flames_midas_sckrdr(const float *key, int felem, int maxvals, 
            int *actvals, float *values, int *unit, int *null);
int flames_midas_sckrdc(const char *key, int noelem, int felem, int maxvals, 
            int *actvals, char *values, int *unit, int *null);

int flames_midas_sckwri(int *key, const int *values, 
            int felem, int maxval, int *unit);
int flames_midas_sckwrd(double *key, const double *values, 
            int felem, int maxval, int *unit);
int flames_midas_sckwrc(char *key, int noelem,
            const char *values, int felem, int maxvals, int *unit);

int flames_midas_sckfnd_double(const double *key, char *type, int *noelem, int *bytelem);
int flames_midas_sckfnd_float(const float *key, char *type, int *noelem, int *bytelem);
int flames_midas_sckfnd_int(const int *key, char *type, int *noelem, int *bytelem);
int flames_midas_sckfnd_string(const char *key, char *type, int *noelem, int *bytelem);


/* display text */
int flames_midas_sctput(const char *msg, 
            const char *func, const char *file, int line);

/* access bulk data frames (bdf) */
int flames_midas_scfopn(const char *name, int dattype, int newopn, int filtype, int *imno);
int flames_midas_scfclo(int imno);
int flames_midas_scfcre(const char *name, int dattype, int iomode, int filtype, 
            int size, int *imno);
int flames_midas_scfinf(const char *name, int fno, int *ibuf);
int flames_midas_scfget(int imno, int felem, int size, int *actsize, char *bufadr);
int flames_midas_scfput(int imno, int felem, int size, const char *bufadr);

/* access descriptors */
int flames_midas_scdfnd(int imno, const char *descr, 
            char *type, int *noelem, int *bytelem);

int flames_midas_scdprs(int imno, const char *descr, 
            char *type, int *noelem, int *bytelem);

int flames_midas_scdrdi(int imno, const char *descr, 
            int felem, int maxvals,
            int *actvals, int *values,
            int *unit, int *null);
int flames_midas_scdrdd(int imno, const char *descr, 
            int felem, int maxvals,
            int *actvals, double *values,
            int *unit, int *null);
int flames_midas_scdrdr(int imno, const char *descr, 
            int felem, int maxvals,
            int *actvals, float *values,
            int *unit, int *null);
int flames_midas_scdrdc(int imno, const char *descr, 
            int noelem,
            int felem, int maxvals,
            int *actvals, char *values,
            int *unit, int *null);

int flames_midas_scdwri(int imno, const char *descr, const int *values, 
            int felem, int nval, const int *unit);
int flames_midas_scdwrd(int imno, const char *descr, const double *values, 
            int felem, int nval, const int *unit);

int flames_midas_scdwrr(int imno, const char *descr, const float *values, 
            int felem, int nval, const int *unit);
int flames_midas_scdwrc(int imno, const char *descr, int noelm, const char *values, 
            int felem, int nval, const int *unit);

int flames_midas_scdcop(int from, int to, int mask);
int flames_midas_scdcop_nonaxis(int from, int to, int mask);

int flames_midas_scddel(int imno, const char *descr);

/* tables */
int flames_midas_tctopn(const char *name, int mode, int allrow, int *tid);
int flames_midas_tctclo(int tid);
int flames_midas_tccser(int tid, const char *colref, int *column);
int flames_midas_tciget(int tid, int *column, int *row);

int flames_midas_tcardc(int tid, int row, int col, int index, int items, char *value);
int flames_midas_tcardd(int tid, int row, int col, int index, int items, double *value);
int flames_midas_tcardi(int tid, int row, int col, int index, int items, int *value);
int flames_midas_tcardr(int tid, int row, int col, int index, int items, float *value);
int flames_midas_tcawrc(int tid, int row, int col, int index, int items, const char *value);
int flames_midas_tcawrd(int tid, int row, int col, int index, int items, const double *value);
int flames_midas_tcawri(int tid, int row, int col, int index, int items, const int *value);
int flames_midas_tcawrr(int tid, int row, int col, int index, int items, const float *value);
int flames_midas_tcbget(int tid, int column, int *dtype, int *items, int *bytes);
int flames_midas_tcdget(int tid, int *store);
int flames_midas_tcfget(int tid, int column, char *form, int *dtype);
int flames_midas_tclget(int tid, int column, char *label);
int flames_midas_tcuget(int tid, int column, char *unit);
int flames_midas_tclser(int tid, const char *label, int *column);
int flames_midas_tccini(int tid, int dtype, int alen,
                        const char *form, const char *unit, const char *label,
                        int *column);
int flames_midas_tcerdc(int tid, int row, int column, char *values, int *null);
int flames_midas_tcerdi(int tid, int row, int column, int *values, int *null);
int flames_midas_tcerdr(int tid, int row, int column, float *values, int *null);
int flames_midas_tcerdd(int tid, int row, int column,
            double *value, int *null);
int flames_midas_tcewrc(int tid, int row, int column, const char *value);
int flames_midas_tcewri(int tid, int row, int column, const int *value);
int flames_midas_tcewrr(int tid, int row, int column, const float *value);
int flames_midas_tcewrd(int tid, int row, int column, const double *value);
int flames_midas_tcsget(int tid, int row, int *value);
int flames_midas_tcsput(int tid, int row, const int *value);

/* catalogs */
int flames_midas_sccsho(const cpl_frameset *catfile,
                        int *noent, 
                        int *last);

int flames_midas_sccget(const cpl_frameset *catfile,
                        int flag,
                        char *name, char *ident, 
                        int *no);

int flames_midas_sccfnd(const cpl_frameset *catfile, 
                        int frmno, 
                        char *frame);
int flames_midas_scccre(cpl_frameset **catfile, 
                        int type,
                        int flag);
int flames_midas_sccadd(cpl_frameset *catfile,
            const char *name,
            const char *ident);

/* Other */
cpl_type
flames_midas_image_dtype_to_cpltype(int dtype);


#endif
