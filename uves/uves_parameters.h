/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:06 $
 * $Revision: 1.19 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.17  2010/06/09 07:17:14  amodigli
 * renamed parameter definition function used by response step in redchain recipe to make them more explicit
 *
 * Revision 1.16  2010/06/08 16:29:23  amodigli
 * Fixed problems in parameter definition for reduction chain recipe
 *
 * Revision 1.15  2010/05/18 11:35:18  amodigli
 * added uves_master_flat_define_parameters
 *
 * Revision 1.14  2010/04/28 08:46:17  amodigli
 * add uves_define_efficiency_parameters()
 *
 * Revision 1.13  2010/03/22 15:55:33  amodigli
 * added uves_master_stack_define_parameters
 *
 * Revision 1.12  2008/03/04 15:21:36  amodigli
 * fixed redchain problem with clean_traps parameter
 *
 * Revision 1.11  2008/03/03 16:35:07  amodigli
 * added parameter to control trap column correction
 *
 * Revision 1.10  2007/06/26 13:34:57  jmlarsen
 * Exported function for FLAMES
 *
 * Revision 1.9  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.8  2007/03/15 12:34:08  jmlarsen
 * Added missing include directive
 *
 * Revision 1.7  2006/10/24 14:04:46  jmlarsen
 * Defined macros to provide minimal syntax for parameter creation
 *
 * Revision 1.6  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.5  2006/08/07 11:35:35  jmlarsen
 * Disabled parameter environment variable mode
 *
 * Revision 1.4  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_PARAMETERS_H
#define UVES_PARAMETERS_H

/*-----------------------------------------------------------------------------
                    Includes
 -----------------------------------------------------------------------------*/

#include <uves_utils_wrappers.h>

#include <cpl.h>
/*-----------------------------------------------------------------------------
                            Typedefs
 -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                             Defines
 -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                                   Prototypes
 -----------------------------------------------------------------------------*/

cpl_error_code
uves_define_global_parameters(cpl_parameterlist *parlist);

cpl_error_code
uves_define_efficiency_parameters(cpl_parameterlist *parlist);


cpl_error_code
uves_define_rebin_for_response_chain_parameters(cpl_parameterlist *parlist);

cpl_error_code
uves_define_reduce_for_response_chain_parameters(cpl_parameterlist *parlist);

cpl_error_code
uves_define_efficiency_for_response_chain_parameters(cpl_parameterlist *parlist);

cpl_error_code
uves_define_extract_for_response_chain_parameters(cpl_parameterlist *parlist);

cpl_error_code
uves_define_background_for_response_chain_parameters(cpl_parameterlist *parlist);

cpl_error_code
uves_master_stack_define_parameters(cpl_parameterlist *parlist, const char *recipe_id);
cpl_error_code
uves_master_flat_define_parameters(cpl_parameterlist *parlist, const char *recipe_id);

cpl_error_code
uves_fpn_define_parameters(cpl_parameterlist *parlist, const char *recipe_id);
int
uves_exec_recipe(int (*get_info)(cpl_pluginlist *), 
                 const char *recipe_domain,
                 const cpl_parameterlist *parameters, 
                 cpl_frameset *frames,
                 const char *caller_id, const char *context);

int
uves_invoke_recipe(const char *recipe_id, const cpl_parameterlist *parameters, 
		   cpl_frameset *frames,
		   const char *caller_id, const char *context);

int
uves_propagate_parameters_step(const char *step_id,
                   cpl_parameterlist *parameters,
                   const char *recipe_id, const char *context);

int
uves_prop_par(int (*get_info)(cpl_pluginlist *),
	      cpl_parameterlist *parameters,
	      const char *recipe_id, const char *context);

int
uves_propagate_parameters(const char *subrecipe_id, 
              cpl_parameterlist *parameters,
              const char *recipe_id, const char *context);

cpl_error_code
uves_corr_traps_define_parameters(cpl_parameterlist * parameters,
                                  const char *recipe_id);


/*----------------------------------------------------------------------------*/
/**
   @brief    Create a new parameter and disable environment variable mode
   @param    p              Parameter variable
   @param    name           See cpl_parameter_new_value()
   @param    type           See cpl_parameter_new_value()
   @param    description    See cpl_parameter_new_value()
   @param    context        See cpl_parameter_new_value()
   @param    default        See cpl_parameter_new_value()
*/
/*----------------------------------------------------------------------------*/
#define uves_parameter_new_value(p, name, type, description, context, default) \
      do {                                                                     \
            p = cpl_parameter_new_value(                                       \
                         name, type, description, context, default);           \
            cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);                  \
      } while(false)

/*----------------------------------------------------------------------------*/
/**
   @brief    Create a new parameter and disable environment variable mode
   @param    p              Parameter variable
   @param    name           See cpl_parameter_new_range()
   @param    type           See cpl_parameter_new_range()
   @param    description    See cpl_parameter_new_range()
   @param    context        See cpl_parameter_new_range()
*/
/*----------------------------------------------------------------------------*/
#define uves_parameter_new_range(p, name, type, description, context,          \
                 default, min, max)                            \
      do {                                                                     \
            p = cpl_parameter_new_range(                                       \
                         name, type, description, context, default, min, max); \
            cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);                  \
      } while(false)

/*----------------------------------------------------------------------------*/
/**
   @brief    Create a new parameter and disable environment variable mode
   @param    p              Parameter variable
   @param    name           See cpl_parameter_new_enum()
   @param    type           See cpl_parameter_new_enum()
   @param    description    See cpl_parameter_new_enum()
   @param    context        See cpl_parameter_new_enum()
   @param    nopt           See cpl_parameter_new_enum()
*/
/*----------------------------------------------------------------------------*/
#define uves_parameter_new_enum(p, name, type, description, context, nopt, ...)\
      do {                                                                     \
            p = cpl_parameter_new_enum(                                        \
                         name, type, description, context, nopt, __VA_ARGS__); \
            cpl_parameter_disable(p, CPL_PARAMETER_MODE_ENV);                  \
      } while(false)



/*----------------------------------------------------------------------------*/
/**
   @brief    Minimal syntax for creating new value parameter
   @param    name           See cpl_parameter_new_range()
   @param    type           See cpl_parameter_new_range()
   @param    description    See cpl_parameter_new_range()
   @param    default        See cpl_parameter_new_range()

   Assumes existence of local variables
   recipe_id, subcontext, parameters
*/
/*----------------------------------------------------------------------------*/
#define uves_par_new_value(name, type, description, default)               \
    do {                                                                   \
        cpl_parameter *p;                                                  \
        char *context   = uves_sprintf("%s%s%s", recipe_id,                \
                                       subcontext != NULL ? "." : "",      \
                                       subcontext != NULL ? subcontext: "");\
        char *full_name = uves_sprintf("%s.%s", context, name);            \
        uves_parameter_new_value(p, full_name,                             \
                 type,                                     \
                 description,                              \
                                 context,                                  \
                 default);                                 \
       cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, name);           \
       cpl_parameterlist_append(parameters, p);                            \
       cpl_free(context);                                                  \
       cpl_free(full_name);                                                \
    } while(false)

/*----------------------------------------------------------------------------*/
/**
   @brief    Minimal syntax for creating new range parameter
   @param    name           See cpl_parameter_new_range()
   @param    type           See cpl_parameter_new_range()
   @param    description    See cpl_parameter_new_range()
   @param    default        See cpl_parameter_new_range()
   @param    min            See cpl_parameter_new_range()
   @param    max            See cpl_parameter_new_range()

   Assumes existence of local variables
   recipe_id, subcontext, parameters
*/
/*----------------------------------------------------------------------------*/
#define uves_par_new_range(name, type, description, default, min, max)     \
    do {                                                                   \
        cpl_parameter *p;                                                  \
        char *context   = uves_sprintf("%s%s%s", recipe_id,                \
                                       subcontext != NULL ? "." : "",      \
                                       subcontext != NULL ? subcontext: "");\
        char *full_name = uves_sprintf("%s.%s", context, name);            \
        uves_parameter_new_range(p, full_name,                             \
                 type,                                     \
                 description,                              \
                                 context,                                  \
                 default, min, max);                       \
       cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, name);           \
       cpl_parameterlist_append(parameters, p);                            \
       cpl_free(context);                                                  \
       cpl_free(full_name);                                                \
    } while(false)


/*----------------------------------------------------------------------------*/
/**
   @brief    Minimal syntax for creating new enumeration parameter
   @param    name           See cpl_parameter_new_range()
   @param    type           See cpl_parameter_new_range()
   @param    description    See cpl_parameter_new_range()
   @param    default        See cpl_parameter_new_range()
   @param    nopt           See cpl_parameter_new_range()

   Assumes existence of local variables
   recipe_id, subcontext, parameters
*/
/*----------------------------------------------------------------------------*/
#define uves_par_new_enum(name, type, description, default, nopt, ...)       \
    do {                                                                     \
        cpl_parameter *p;                                                    \
        char *context   = uves_sprintf("%s%s%s", recipe_id,                \
                                       subcontext != NULL ? "." : "",      \
                                       subcontext != NULL ? subcontext: "");\
        char *full_name = uves_sprintf("%s.%s", context, name);              \
        uves_parameter_new_enum(p, full_name,                                \
                 type,                                       \
                 description,                                \
                                 context,                                    \
                 default, nopt, __VA_ARGS__);                \
       cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, name);             \
       cpl_parameterlist_append(parameters, p);                              \
       cpl_free(context);                                                    \
       cpl_free(full_name);                                                  \
    } while(false)




#endif  /* UVES_PARAMETERS_H */
