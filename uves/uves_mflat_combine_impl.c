/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston MA 02110-1301 USA          *
 */
 
/*
 * $Author: amodigli $
 * $Date: 2012-04-16 06:24:26 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_mflat  Recipe: Master Flat
 *
 * This recipe calculates the master flat frame.
 * See man-page for details.
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_reduce_mflat_combine.h>

#include <uves_parameters.h>
#include <uves_recipe.h>
#include <uves.h>
#include <uves_error.h>

#include <cpl.h>

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/

static int uves_mflat_combine_define_parameters(cpl_parameterlist *parameters);

/*-----------------------------------------------------------------------------
                            Recipe standard code
 -----------------------------------------------------------------------------*/
#define cpl_plugin_get_info uves_mflat_combine_get_info
UVES_RECIPE_DEFINE(
    UVES_MFLAT_COMBINE_ID, UVES_MFLAT_COMBINE_DOM, uves_mflat_combine_define_parameters,
    "Andrea Modigliani", "cpl@eso.org",
    "Combines the master flat field and the master dflat frames",
    uves_mflat_combine_desc);

/**@{*/
/*-----------------------------------------------------------------------------
                              Functions code
  ----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    parameters        the parameterlist to fill
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int
uves_mflat_combine_define_parameters(cpl_parameterlist *parameters)
{
   /* TODO define params */   
   if (uves_define_global_parameters(parameters) != CPL_ERROR_NONE)
   {
      return -1;
   }







    {
    const char *context = "uves";
    const char* name = "order_threshold";
    char* full_name = uves_sprintf("%s.%s", context, name);
    cpl_parameter *p;
    uves_parameter_new_range(p, full_name,
                    CPL_TYPE_INT,
                    "Order where master flats are joined ",
                    context,
                    7,5,9);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, name);
    cpl_parameterlist_append(parameters, p);
    cpl_free(full_name);
    }

 return 0;
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Execute the data reduction
   @param    frames      the frames list
   @param    parameters  the parameters list
   @param    starttime   start of execution
   @return   CPL_ERROR_NONE if everything is ok

*/
/*----------------------------------------------------------------------------*/
static void
UVES_CONCAT2X(UVES_MFLAT_COMBINE_ID,exe)(cpl_frameset *frames, 
                   const cpl_parameterlist *parameters,
                   const char *starttime)
{
    uves_mflat_combine_exe_body(frames, parameters, starttime, make_str(UVES_MFLAT_COMBINE_ID));
    return;
}

/**@}*/
