/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.15 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.13  2008/03/28 08:56:19  amodigli
 * IRPLIB_RECIPE_DEFINE-->UVES_IRPLIB_RECIPE_DEFINE IRPLIB_CONCAT2X UVES_CONCAT2X irplib_plugin-->uves_plugin
 *
 * Revision 1.12  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.11  2007/02/09 15:10:42  jmlarsen
 * Show recipe name correctly
 *
 * Revision 1.10  2007/02/09 13:40:11  jmlarsen
 * Added year 2007
 *
 * Revision 1.9  2007/01/10 12:39:44  jmlarsen
 * Replace UVES_TFLAT_ID with recipe name parameter
 *
 * Revision 1.8  2006/11/16 14:10:49  jmlarsen
 * Fixed usage of recipe name string in error message
 *
 * Revision 1.7  2006/11/15 15:02:15  jmlarsen
 * Implemented const safe workarounds for CPL functions
 *
 * Revision 1.5  2006/11/15 14:04:08  jmlarsen
 * Removed non-const version of parameterlist_get_first/last/next which is already
 * in CPL, added const-safe wrapper, unwrapper and deallocator functions
 *
 * Revision 1.4  2006/10/24 14:01:21  jmlarsen
 * include uves_msg.h
 *
 * Revision 1.3  2006/10/17 12:33:08  jmlarsen
 * Added semicolon at UVES_RECIPE_DEFINE invocation
 *
 * Revision 1.2  2006/10/10 14:43:48  jmlarsen
 * Add semicolon at invocation of IRPLIB_RECIPE_DEFINE
 *
 * Revision 1.1  2006/10/09 13:01:33  jmlarsen
 * Use macro to define recipe interface functions
 *
 * Revision 1.3  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */

#ifndef UVES_RECIPE_H
#define UVES_RECIPE_H

#include <uves_msg.h>
#include <uves_error.h>
#include <uves_plugin.h>
#include <cpl.h>

/*-----------------------------------------------------------------------------
                                   Defines
 -----------------------------------------------------------------------------*/
#define UVES_RECIPE_DEFINE(NAME, MSG_DOMAIN, FILL_PARAMS_FUNC,                 \
               AUTHOR, AUTHOR_EMAIL,                               \
               SYNOPSIS, DESCRIPTION)                              \
     UVES_IRPLIB_RECIPE_DEFINE(NAME, UVES_BINARY_VERSION,                           \
     FILL_PARAMS_FUNC(recipe->parameters),                                     \
     AUTHOR, AUTHOR_EMAIL, "2004, 2005, 2006, 2007", SYNOPSIS, DESCRIPTION);   \
                                                                               \
static void UVES_CONCAT2X(NAME,exe)(cpl_frameset *,                          \
                     const cpl_parameterlist *,                \
                                     const char *starttime);                   \
                                                                               \
/* This is the UVES pipeline specific (but common to all recipes)              \
   code to run before/after executing each recipe. It calls the function       \
   uves_rrrecipe_exe(frames, parameters, starttime) declared above. */         \
                                                                               \
static int NAME(cpl_frameset *frames,                                          \
        const cpl_parameterlist *parameters)                           \
{                                                                              \
    char *starttime = NULL;                                                    \
                                                                               \
    /* Error handling (irplib) was already initialized */                      \
                                                                               \
    /* Initialize messaging */                                                 \
    uves_msg_init(-1, MSG_DOMAIN);        /* -1 = max level */                 \
                                                                               \
    /* Initialize recipe */                                                    \
    check( (uves_msg_louder(),                                                 \
            starttime = uves_initialize(frames, parameters, make_str(NAME),    \
                       SYNOPSIS),                              \
            uves_msg_softer()),                                                \
       "Initialization failed");                                           \
                                                                               \
    /* Recipe specific actions */                                              \
    check( (uves_msg_louder(),                                                 \
            UVES_CONCAT2X(NAME,exe)(frames, parameters, starttime),          \
            uves_msg_softer()),                                                \
        MSG_DOMAIN " execution failed");                                   \
                                                                               \
    /* Terminate recipe */                                                     \
    check( uves_end(make_str(NAME), frames), "Termination failed");            \
                                                                               \
  cleanup:                                                                     \
    cpl_free(starttime);                                                       \
                                           \
    return (cpl_error_get_code() == CPL_ERROR_NONE) ? 0 : 1;                   \
}                                                                              \
  /* This dummy declaration requires the macro to be invoked as if it was      \
     a kind of function declaration, with a terminating ; */                   \
extern int uves_recipe_end


#endif
