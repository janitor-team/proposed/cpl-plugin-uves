/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:02 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.2  2007/08/30 07:56:54  amodigli
 * fixed some doxygen warnings
 *
 * Revision 1.1  2007/01/18 07:43:22  jmlarsen
 * Add chip type
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_chip_type    CCD chip abstract type
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_chip_type.h>

/*-----------------------------------------------------------------------------
                            Type definitions
 -----------------------------------------------------------------------------*/

struct _uves_chip 
{
    char whatever;
};


/*-----------------------------------------------------------------------------
                            Implementation
 -----------------------------------------------------------------------------*/
static struct _uves_chip blue;
static struct _uves_chip redl;
static struct _uves_chip redu;
static struct _uves_chip invalid;

const uves_chip UVES_CHIP_BLUE = &blue;
const uves_chip UVES_CHIP_REDL = &redl;
const uves_chip UVES_CHIP_REDU = &redu;
const uves_chip UVES_CHIP_INVALID = &invalid;
/**@{*/

/**@}*/
