/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2010-04-08 10:47:27 $
 * $Revision: 1.26 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_tflat  Recipe: TFlat Reduction
 *
 * See man-page for details.
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_reduce_mflat.h>
#include <uves_reduce_scired.h>
#include <uves_parameters.h>
#include <uves_utils_wrappers.h>
#include <uves_dfs.h>
#include <uves_recipe.h>
#include <uves.h>
#include <uves_error.h>
#include <uves_msg.h>

#include <cpl.h>
#include <string.h>
/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/
static int
uves_tflat_define_parameters(cpl_parameterlist *parameters);

/*-----------------------------------------------------------------------------
                            Recipe standard code
 -----------------------------------------------------------------------------*/
#define cpl_plugin_get_info uves_tflat_get_info
UVES_RECIPE_DEFINE(
    UVES_TFLAT_ID, UVES_TFLAT_DOM, uves_tflat_define_parameters,
    "Jonas M. Larsen", "cpl@eso.org",
    "Reduces a TFLAT frame",
    "This recipe reduces a TFLAT_xxx frame (xxx = BLUE,RED). This is\n"
    "achieved by\n"
    "1) combining all provided TFLAT frames to a MASTER_TFLAT frame, then\n"
    "2) doing a normal science reduction on the first input TFLAT frame\n"
    "Input frames are raw TFLAT_xxx  frames, and: \n"
    "order table(s) for each chip, ORDER_TABLE_xxxx (where xxxx=BLUE, REDL, REDU),\n"
    "line table(s) for each chip, LINE_TABLE_xxxx, a master bias frame,\n"
    "MASTER_BIAS_xxxx, a master flat, MASTER_FLAT_xxxx, \n");

/**@{*/
/*-----------------------------------------------------------------------------
                              Functions code
 -----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    parameters        the parameterlist to fill
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int
uves_tflat_define_parameters(cpl_parameterlist *parameters)
{

   uves_mflat_define_parameters_body(parameters,"uves_cal_tflat" );

    /*******************
     *  Reduce.        *
     ******************/
    if (uves_propagate_parameters_step(UVES_REDUCE_ID, parameters,
                                       make_str(UVES_TFLAT_ID), NULL) != 0)
        {
            return -1;
        }

    /* For TFLAT reduction: average extraction, no sky subtraction */
    {
    const char *param = "average";
    bool bool_param;

    if (uves_set_parameter_default(parameters, 
                       make_str(UVES_TFLAT_ID), "reduce.extract.method",
                       CPL_TYPE_STRING, &param) != CPL_ERROR_NONE)
        {
        return -1;
        }

    bool_param = false;
    if (uves_set_parameter_default(parameters, 
                       make_str(UVES_TFLAT_ID), "reduce.skysub",
                       CPL_TYPE_BOOL, &bool_param) != CPL_ERROR_NONE)
        {
        return -1;
        }
    }
    
    return (cpl_error_get_code() != CPL_ERROR_NONE);
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Get the command line options and execute the data reduction
  @param    parameters  the parameters list
  @param    frames      the frames list
  @return   CPL_ERROR_NONE if everything is ok
 */
/*----------------------------------------------------------------------------*/
static void
UVES_CONCAT2X(UVES_TFLAT_ID,exe)(cpl_frameset *frames,
                   const cpl_parameterlist *parameters, 
                   const char *starttime)
{
    uves_msg("Creating master tflat");
    check_nomsg( uves_mflat_exe_body(frames, parameters, 
                     starttime,
                     make_str(UVES_TFLAT_ID)) );

    uves_msg("Reducing first raw tflat");
    check_nomsg( uves_reduce_scired(frames, parameters, make_str(UVES_TFLAT_ID), starttime) );

    /* The MASTER_TFLAT was reclassified as CALIB by the reduction step.
       But it is actually a product of this recipe, so retag it */
    {
      enum uves_chip chip;
      int blue;
      const char* PROCESS_CHIP=NULL;
      cpl_frame *f = NULL;
      check( uves_get_parameter(parameters, NULL, "uves", "process_chip", CPL_TYPE_STRING, &PROCESS_CHIP),
	     "Could not read parameter");
    uves_string_toupper((char*)PROCESS_CHIP);

      /* Loop over all possible MASTER_TFLATs */
      for (blue = 0; blue <= 1; blue++)
        {
	  for (chip = uves_chip_get_first(blue);
	       chip != UVES_CHIP_INVALID;
	       chip = uves_chip_get_next(chip))
            {

	      if(strcmp(PROCESS_CHIP,"REDU") == 0) {
		chip = uves_chip_get_next(chip);
	      }

	      f = cpl_frameset_find(frames, UVES_MASTER_TFLAT(chip));
	      if (f != NULL)
                {
		  cpl_frame_set_group(f, CPL_FRAME_GROUP_PRODUCT);
                }
            
	      f = cpl_frameset_find(frames, UVES_BKG_FLAT(chip));
	      if (f != NULL)
                {
		  cpl_frame_set_group(f, CPL_FRAME_GROUP_PRODUCT);
                }
            }

	  if(strcmp(PROCESS_CHIP,"REDL") == 0) {
	    chip = uves_chip_get_next(chip);
	  }

        }
    }

  cleanup:
    return;
}

/**@}*/
