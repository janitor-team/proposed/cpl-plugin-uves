/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.44 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.42  2008/03/28 08:54:39  amodigli
 * IRPLIB_CONCAT2X-->UVES_CONCAT2X
 *
 * Revision 1.41  2007/06/11 13:28:26  jmlarsen
 * Changed recipe contact address to cpl at eso.org
 *
 * Revision 1.40  2007/06/08 13:06:16  jmlarsen
 * Send bug reports to Andrea
 *
 * Revision 1.39  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.38  2007/02/09 13:44:15  jmlarsen
 * Moved code to reduce_scired to enable calling from uves_obs_spatred
 *
 * Revision 1.37  2006/11/06 15:19:42  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.36  2006/10/17 12:33:02  jmlarsen
 * Added semicolon at UVES_RECIPE_DEFINE invocation
 *
 * Revision 1.35  2006/10/09 13:01:13  jmlarsen
 * Use macro to define recipe interface functions
 *
 * Revision 1.34  2006/09/27 15:08:45  jmlarsen
 * Fixed doc. bug
 *
 * Revision 1.33  2006/09/27 13:19:00  jmlarsen
 * Factored out scired body to enable calling it from other recipes
 *
 * Revision 1.32  2006/09/20 15:42:18  jmlarsen
 * Implemented MASTER_RESPONSE support
 *
 * Revision 1.31  2006/09/20 12:53:57  jmlarsen
 * Replaced stringcat functions with uves_sprintf()
 *
 * Revision 1.30  2006/09/20 07:26:43  jmlarsen
 * Shortened max line length
 *
 * Revision 1.29  2006/09/19 14:25:26  jmlarsen
 * Propagate FITS keywords from master flat, not science, to WCALIB_FLAT_OBJ
 *
 * Revision 1.28  2006/09/19 06:55:31  jmlarsen
 * Changed interface of uves_frameset to optionally write image statistics kewwords
 *
 * Revision 1.27  2006/09/06 14:47:05  jmlarsen
 * Added commented out code to skip one chip
 *
 * Revision 1.26  2006/08/24 11:36:37  jmlarsen
 * Write recipe start/stop time to header
 *
 * Revision 1.25  2006/08/22 15:13:15  amodigli
 * fix typo
 *
 * Revision 1.24  2006/08/22 14:18:59  amodigli
 * new format for SCI QC key
 *
 * Revision 1.23  2006/08/18 13:35:42  jmlarsen
 * Fixed/changed QC parameter formats
 *
 * Revision 1.22  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.21  2006/08/17 09:19:19  jmlarsen
 * Removed CPL2 code
 *
 * Revision 1.20  2006/08/11 14:56:05  amodigli
 * removed Doxygen warnings
 *
 * Revision 1.19  2006/08/11 09:00:21  jmlarsen
 * Take into account the different meanings of line table 'Y' column
 *
 * Revision 1.18  2006/08/10 12:35:15  amodigli
 * added QC log
 *
 * Revision 1.17  2006/08/07 14:42:02  jmlarsen
 * Implemented on-the-fly correction of a line table when its order numbering
 * is inconsistent with the order table (DFS02694)
 *
 * Revision 1.16  2006/07/14 12:30:34  jmlarsen
 * Compute PRO CATG depending on DO CATG
 *
 * Revision 1.15  2006/07/03 12:46:34  amodigli
 * updated description
 *
 * Revision 1.14  2006/06/16 08:25:45  jmlarsen
 * Manually propagate ESO.DET. keywords from 1st/2nd input header
 *
 * Revision 1.13  2006/06/13 11:57:02  jmlarsen
 * Check that calibration frames are from the same chip ID
 *
 * Revision 1.12  2006/06/06 08:40:10  jmlarsen
 * Shortened max line length
 *
 * Revision 1.11  2006/05/16 12:13:07  amodigli
 * added QC log
 *
 * Revision 1.10  2006/05/12 15:11:15  jmlarsen
 * Implemented bad pixel propagation for flux calibration
 *
 * Revision 1.9  2006/04/24 09:25:34  jmlarsen
 * Use FITS convention for coordinates in QC calculation
 *
 * Revision 1.8  2006/04/20 10:47:39  amodigli
 * added qclog
 *
 * Revision 1.7  2006/04/06 09:48:15  amodigli
 * changed uves_frameset_insert interface to have QC log
 *
 * Revision 1.6  2006/04/06 08:51:04  jmlarsen
 * Added flux-calibrated science products
 *
 * Revision 1.5  2006/03/24 14:46:39  jmlarsen
 * Doc. bugfix
 *
 * Revision 1.4  2006/03/24 13:51:50  jmlarsen
 * Changed meaning of VARIANCE_SCIENCE to match MIDAS
 *
 * Revision 1.3  2006/03/06 09:22:43  jmlarsen
 * Added support for reading MIDAS line tables with MIDAS tags
 *
 * Revision 1.2  2006/03/03 13:54:11  jmlarsen
 * Changed syntax of check macro
 *
 * Revision 1.1  2006/02/03 07:51:04  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.48  2006/01/19 08:47:24  jmlarsen
 * Inserted missing doxygen end tag
 *
 * Revision 1.47  2005/12/20 16:10:32  jmlarsen
 * Added some documentation
 *
 * Revision 1.46  2005/12/20 10:33:20  jmlarsen
 * Added some doxygen doc.
 *
 * Revision 1.45  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_scired  Recipe: Science Reduction
 *
 * This recipe reduces a science frame.
 * See man-page for details.
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_reduce_scired.h>
#include <uves_parameters.h>
#include <uves_recipe.h>
#include <uves.h>
#include <uves_error.h>
#include <uves_msg.h>

#include <cpl.h>

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/
static int
uves_scired_define_parameters(cpl_parameterlist *parameters);

/*-----------------------------------------------------------------------------
                            Recipe standard code
 -----------------------------------------------------------------------------*/
#define cpl_plugin_get_info uves_scired_get_info
UVES_RECIPE_DEFINE(
    UVES_SCIRED_ID, UVES_SCIRED_DOM, uves_scired_define_parameters,
    "Jonas M. Larsen", "cpl@eso.org",
    uves_scired_desc_short,
    uves_scired_desc);

/**@{*/
/*-----------------------------------------------------------------------------
                              Functions code
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    parameters        the parameterlist to fill
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int
uves_scired_define_parameters(cpl_parameterlist *parameters)
{
    return uves_scired_define_parameters_body(parameters, make_str(UVES_SCIRED_ID));
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Get the command line options and execute the data reduction
  @param    parameters  the parameters list
  @param    frames      the frames list
  @return   CPL_ERROR_NONE if everything is ok
 */
/*----------------------------------------------------------------------------*/
static void
UVES_CONCAT2X(UVES_SCIRED_ID,exe)(cpl_frameset *frames,
        const cpl_parameterlist *parameters,
        const char *starttime)
{
    /* Do science reduction while pretending to be 'make_str(UVES_SCIRED_ID)' */
    uves_reduce_scired(frames, parameters, make_str(UVES_SCIRED_ID), starttime);

    return;
}


/**@}*/
