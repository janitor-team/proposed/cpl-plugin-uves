/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:02 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.7  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.6  2007/01/16 10:26:19  jmlarsen
 * Removed obsolete declaration
 *
 * Revision 1.5  2007/01/10 12:35:31  jmlarsen
 * Added uves_chip_tochar
 *
 * Revision 1.4  2006/08/17 13:56:52  jmlarsen
 * Reduced max line length
 *
 * Revision 1.3  2006/02/28 09:15:22  jmlarsen
 * Minor update
 *
 * Revision 1.2  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_CHIP_H
#define UVES_CHIP_H

#include <cpl.h>
#include <stdbool.h>

enum uves_chip { UVES_CHIP_BLUE = 43542, 
         UVES_CHIP_REDL = 6813,
         UVES_CHIP_REDU = 11280, 
         UVES_CHIP_INVALID = 3243};
/* Decrease the chance that the user erroneously uses the integer values (which
   the user should not) by defining 'random' enumeration values */

enum uves_chip uves_chip_get_first(bool blue);
enum uves_chip uves_chip_get_next(enum uves_chip chip);

int uves_chip_get_index(enum uves_chip chip);

const char *uves_chip_tostring_lower(enum uves_chip chip);
const char *uves_chip_tostring_upper(enum uves_chip chip);

const char *uves_chip_get_det(enum uves_chip chip);
char uves_chip_tochar(enum uves_chip chip);

#endif  /* UVES_CHIP_H */
