/* 
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_CORVEL_H
#define FLAMES_CORVEL_H

int flames_corvel(const char *IN_A,
                  const char *IN_B,
                  const int  IN_N,
                  const char *OU_A,
                  const char *OU_B,
                  const char *OU_C,
                  const double rv_ccf_min,
                  const double rv_ccf_max,
                  const double rv_ccf_step);

#endif
