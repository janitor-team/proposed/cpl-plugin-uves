/*
 * This file is part of the UVES Pipeline
 * Copyright (C) 2002, 2003, 2004, 2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 13:55:51 $
 * $Revision: 1.148 $
 * $Name: not supported by cvs2svn $
 *
 */

#ifndef UVES_DFS_H
#define UVES_DFS_H

/*-----------------------------------------------------------------------------
                                Includes
  -----------------------------------------------------------------------------*/
#include <uves_cpl_size.h>
#include <uves_propertylist.h>
#include <uves_utils_polynomial.h>
#include <uves_chip.h>
#include <uves_utils.h>
#include <uves_globals.h>
#include <cpl.h>

#include <stdbool.h>
#include <math.h>
/*-----------------------------------------------------------------------------
                Defines
 -----------------------------------------------------------------------------*/
/* FLAMES */
/*
 * reduce
 */
#define RAW_IMA       "RAW_IMA"
#define FLAMES_SCI_RED       "FIB_SCI_RED"
#define FLAMES_SCI_SIM_RED   "FIB_SCI_SIM_RED"
#define FLAMES_SCI_COM_RED   "FIB_SCI_COM_RED"
#define FLAMES_CORVEL_MASK   "CORVEL_MASK"

#define FLAMES_INFO_TABLE(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_SCI_INFO_TAB_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_SCI_INFO_TAB_REDU" : "???")

#define FLAMES_LINE_TABLE(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_LINE_TABLE_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_LINE_TABLE_REDU" : "???")

#define FLAMES_LINE_TABLE_MIDAS(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_LINE_TABLE_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_LINE_TABLE_REDU" : "???")

#define FLAMES_SLIT_FF_DT1(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_DT1_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_DT1_REDU" : "???")

#define FLAMES_SLIT_FF_DT2(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_DT2_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_DT2_REDU" : "???")

#define FLAMES_SLIT_FF_DT3(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_DT3_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_DT3_REDU" : "???")

#define FLAMES_SLIT_FF_DTC(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_DTC_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_DTC_REDU" : "???")

#define FLAMES_SLIT_FF_DT(it, chip) ((it) == 1 ? FLAMES_SLIT_FF_DT1(chip) : \
                                     (it) == 2 ? FLAMES_SLIT_FF_DT2(chip) : \
                                     (it) == 3 ? FLAMES_SLIT_FF_DT3(chip) : "???")

#define FLAMES_SLIT_FF_BP1(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_BP1_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_BP1_REDU" : "???")

#define FLAMES_SLIT_FF_BP2(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_BP2_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_BP2_REDU" : "???")

#define FLAMES_SLIT_FF_BP3(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_BP3_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_BP3_REDU" : "???")

#define FLAMES_SLIT_FF_BPC(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_BPC_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_BPC_REDU" : "???")

#define FLAMES_SLIT_FF_BP(it, chip) ((it) == 1 ? FLAMES_SLIT_FF_BP1(chip) : \
                                     (it) == 2 ? FLAMES_SLIT_FF_BP2(chip) : \
                                     (it) == 3 ? FLAMES_SLIT_FF_BP3(chip) : "???")


#define FLAMES_SLIT_FF_BN1(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_BN1_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_BN1_REDU" : "???")

#define FLAMES_SLIT_FF_BN2(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_BN2_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_BN2_REDU" : "???")

#define FLAMES_SLIT_FF_BN3(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_BN3_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_BN3_REDU" : "???")

#define FLAMES_SLIT_FF_BN4(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_BN4_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_BN4_REDU" : "???")

#define FLAMES_SLIT_FF_BNC(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_BNC_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_BNC_REDU" : "???")

#define FLAMES_SLIT_FF_BN(it, chip) ((it) == 1 ? FLAMES_SLIT_FF_BN1(chip) : \
                                     (it) == 2 ? FLAMES_SLIT_FF_BN2(chip) : \
                                     (it) == 3 ? FLAMES_SLIT_FF_BN3(chip) : \
                                     (it) == 4 ? FLAMES_SLIT_FF_BN4(chip) : "???")


#define FLAMES_SLIT_FF_SG1(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_SG1_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_SG1_REDU" : "???")

#define FLAMES_SLIT_FF_SG2(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_SG2_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_SG2_REDU" : "???")

#define FLAMES_SLIT_FF_SG3(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_SG3_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_SG3_REDU" : "???")

#define FLAMES_SLIT_FF_SGC(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_SGC_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_SGC_REDU" : "???")


#define FLAMES_SLIT_FF_SG(it, chip) ((it) == 1 ? FLAMES_SLIT_FF_SG1(chip) : \
                                     (it) == 2 ? FLAMES_SLIT_FF_SG2(chip) : \
                                     (it) == 3 ? FLAMES_SLIT_FF_SG3(chip) : "???")

#define FLAMES_SLIT_FF_COM(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_COM_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_COM_REDU" : "???")

#define FLAMES_SLIT_FF_NOR(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_NOR_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_NOR_REDU" : "???")

#define FLAMES_SLIT_FF_NSG(chip)  ((chip) == UVES_CHIP_REDL ? "SLIT_FF_NSG_REDL" : \
                       (chip) == UVES_CHIP_REDU ? "SLIT_FF_NSG_REDU" : "???")



#define FLAMES_FIB_FF_DT1(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_DT1_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_DT1_REDU" : "???")

#define FLAMES_FIB_FF_DT2(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_DT2_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_DT2_REDU" : "???")

#define FLAMES_FIB_FF_DT3(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_DT3_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_DT3_REDU" : "???")

#define FLAMES_FIB_FF_DTC(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_DTC_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_DTC_REDU" : "???")


#define FLAMES_FIB_FF_DT(it, chip) ((it) == 1 ? FLAMES_FIB_FF_DT1(chip) : \
                                     (it) == 2 ? FLAMES_FIB_FF_DT2(chip) : \
                                     (it) == 3 ? FLAMES_FIB_FF_DT3(chip) : "???")



#define FLAMES_FIB_FF_BP1(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_BP1_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_BP1_REDU" : "???")

#define FLAMES_FIB_FF_BP2(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_BP2_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_BP2_REDU" : "???")

#define FLAMES_FIB_FF_BP3(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_BP3_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_BP3_REDU" : "???")

#define FLAMES_FIB_FF_BPC(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_BPC_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_BPC_REDU" : "???")

#define FLAMES_FIB_FF_BP(it, chip) ((it) == 1 ? FLAMES_FIB_FF_BP1(chip) : \
                                     (it) == 2 ? FLAMES_FIB_FF_BP2(chip) : \
                                     (it) == 3 ? FLAMES_FIB_FF_BP3(chip) : "???")



#define FLAMES_FIB_FF_BN1(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_BN1_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_BN1_REDU" : "???")

#define FLAMES_FIB_FF_BN2(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_BN2_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_BN2_REDU" : "???")

#define FLAMES_FIB_FF_BN3(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_BN3_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_BN3_REDU" : "???")
#define FLAMES_FIB_FF_BNC(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_BNC_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_BNC_REDU" : "???")


#define FLAMES_FIB_FF_SG1(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_SG1_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_SG1_REDU" : "???")

#define FLAMES_FIB_FF_SG2(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_SG2_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_SG2_REDU" : "???")

#define FLAMES_FIB_FF_SG3(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_SG3_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_SG3_REDU" : "???")

#define FLAMES_FIB_FF_SGC(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_SGC_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_SGC_REDU" : "???")

#define FLAMES_FIB_FF_SG(it, chip) ((it) == 1 ? FLAMES_FIB_FF_SG1(chip) : \
                                     (it) == 2 ? FLAMES_FIB_FF_SG2(chip) : \
                                     (it) == 3 ? FLAMES_FIB_FF_SG3(chip) : "???")


#define FLAMES_FIB_FF_COM(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_COM_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_COM_REDU" : "???")

#define FLAMES_FIB_FF_NOR(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_NOR_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_NOR_REDU" : "???")

#define FLAMES_FIB_FF_NSG(chip)  ((chip) == UVES_CHIP_REDL ? "FIB_FF_NSG_REDL" : \
                      (chip) == UVES_CHIP_REDU ? "FIB_FF_NSG_REDU" : "???")


#define FIB_FF_ODD_INFO_TAB "FIB_FF_ODD_INFO_TAB"
#define FIB_FF_EVEN_INFO_TAB "FIB_FF_EVEN_INFO_TAB"
#define FIB_FF_ALL_INFO_TAB "FIB_FF_ALL_INFO_TAB"


/*
 * prep_sff_ofpos
 */
#define FLAMES_FIB_FF_ALL   "FIB_FF_ALL_RED"
#define FLAMES_FIB_FF_ODD   "FIB_FF_ODD_RED"
#define FLAMES_FIB_FF_EVEN  "FIB_FF_EVEN_RED"

#define FLAMES_CORVEL(chip)  ((chip) == UVES_CHIP_REDL ? "CORVEL_TAB_REDL" : \
                  (chip) == UVES_CHIP_REDU ? "CORVEL_TAB_REDU" : "???")

/*
 * wavecal
 */
#define FLAMES_FIB_SCI_SIM "FIB_SCI_SIM_RED"


#define FLAMES_ORDEF(flames, chip)  ((flames) ?                                       \
                     (((chip) == UVES_CHIP_REDU) ? "FIB_ORDEF_REDU" : \
                      ((chip) == UVES_CHIP_REDL) ? "FIB_ORDEF_REDL" : \
                      "???") : "??UVES??")
#define FLAMES_ORDEF_EXTENSION(flames, chip)  0


/* UVES + FLAMES */

/* Narrow arc lamp frames */
#define UVES_FORMATCHECK(flames,blue) ((flames) ? "FIB_ARC_LAMP_FORM_RED" : \
                                      ((blue) ? "ARC_LAMP_FORM_BLUE" : "ARC_LAMP_FORM_RED"))

/* Narrow flat field frames */
#define UVES_ORDER_FLAT(flames, blue) ((flames) ? "FIB_ORDEF_RED" : \
                       ((blue) ? "ORDER_FLAT_BLUE" : "ORDER_FLAT_RED"))

/* Bias raw frames */
#define UVES_BIAS(blue)           ((blue) ? "BIAS_BLUE"      : "BIAS_RED")

/* Dark raw frames */
#define UVES_DARK(blue)           ((blue) ? "DARK_BLUE"      : "DARK_RED")
#define UVES_PDARK(blue)          ((blue) ? "PDARK_BLUE"     : "PDARK_RED")

/* Flatfield raw frames */
#define UVES_FLAT(blue)           ((blue) ? "FLAT_BLUE"      : "FLAT_RED")
#define UVES_IFLAT(blue)          ((blue) ? "IFLAT_BLUE"     : "IFLAT_RED")
#define UVES_DFLAT(blue)          ((blue) ? "DFLAT_BLUE"     : "DFLAT_RED")
#define UVES_SFLAT(blue)          ((blue) ? "SFLAT_BLUE"     : "SFLAT_RED")
#define UVES_TFLAT(blue)          ((blue) ? "TFLAT_BLUE"     : "TFLAT_RED")
#define UVES_SCREEN_FLAT(blue)          ((blue) ? "SCREEN_FLAT_BLUE"     : "SCREEN_FLAT_RED")

/* Arc lamp */
#define UVES_ARC_LAMP(flames,blue)     ((flames) ? "FIB_ARC_LAMP_RED" : \
                                       ((blue) ? "ARC_LAMP_BLUE" : "ARC_LAMP_RED"))

#define UVES_ECH_ARC_LAMP(blue)       ((blue) ? "ECH_ARC_LAMP_BLUE" : "ECH_ARC_LAMP_RED")

/* CD align */
#define UVES_CD_ALIGN(blue)       ((blue) ? "CD_ALIGN_BLUE" : "CD_ALIGN_RED")

/* STD Star */
#define UVES_STD_STAR(blue)           ((blue) ? "STANDARD_BLUE" : "STANDARD_RED")

/* Science */
#define UVES_SCIENCE(blue)         ((blue) ? "SCIENCE_BLUE"    : "SCIENCE_RED")
#define UVES_SCI_EXTND(blue)       ((blue) ? "SCI_EXTND_BLUE"  : "SCI_EXTND_RED")
#define UVES_SCI_POINT(blue)       ((blue) ? "SCI_POINT_BLUE"  : "SCI_POINT_RED")
#define UVES_SCI_SLICER(blue)      ((blue) ? "SCI_SLICER_BLUE" : "SCI_SLICER_RED")

/* DRS setup table (for backwards compatibility) */
#define UVES_DRS_SETUP(flames, chip) ((flames) ?                                            \
                      (((chip) == UVES_CHIP_REDU) ? "FIB_DRS_REDU" :        \
                       ((chip) == UVES_CHIP_REDL) ? "FIB_DRS_REDL" : "???") \
                      :                                                     \
                      (((chip) == UVES_CHIP_BLUE) ? "DRS_SETUP_BLUE" :      \
                       ((chip) == UVES_CHIP_REDU) ? "DRS_SETUP_REDU" :      \
                       ((chip) == UVES_CHIP_REDL) ? "DRS_SETUP_REDL" : "???"))
    


#define UVES_DRS_SETUP_EXTENSION(chip)  1

/* Order table (written by physmod + flames_cal_orderpos) */
#define UVES_GUESS_ORDER_TABLE(flames,chip) ((flames) ?                                  \
                                (((chip) == UVES_CHIP_REDU) ? "FIB_ORD_GUE_REDU" :       \
                                 ((chip) == UVES_CHIP_REDL) ? "FIB_ORD_GUE_REDL" : "???")\
                                 :                                                       \
                                 (((chip) == UVES_CHIP_BLUE) ? "ORDER_GUESS_TAB_BLUE" :  \
                                 ((chip) == UVES_CHIP_REDU) ? "ORDER_GUESS_TAB_REDU" :   \
                 ((chip) == UVES_CHIP_REDL) ? "ORDER_GUESS_TAB_REDL" :   \
                 "???"))


/* Order table (written by physmod + flames_cal_orderpos) */
#define UVES_ORD_TAB(flames,chip) ((flames) ?                                  \
                                (((chip) == UVES_CHIP_REDU) ? "FIB_ORD_TAB_REDU" :       \
                                 ((chip) == UVES_CHIP_REDL) ? "FIB_ORD_TAB_REDL" : "???")\
                                 :                                                       \
                                 (((chip) == UVES_CHIP_BLUE) ? "ORDER_TABLE_BLUE" :  \
                                 ((chip) == UVES_CHIP_REDU) ? "ORDER_TABLE_REDU" :   \
                 ((chip) == UVES_CHIP_REDL) ? "ORDER_TABLE_REDL" :   \
                 "???"))

#define UVES_GUESS_ORDER_TABLE_EXTENSION(flames,chip)  1


/* Order table (written by uves_orderpos / flames_cal_prep_sff_ofpos) */
#define UVES_ORDER_TABLE(flames,chip)  ((flames) ?                                             \
                                        (((chip) == UVES_CHIP_REDU) ? "FIB_ORDEF_TABLE_REDU" : \
                         ((chip) == UVES_CHIP_REDL) ? "FIB_ORDEF_TABLE_REDL" : \
                                         "???")                                                \
                    :                                                      \
                    (((chip) == UVES_CHIP_BLUE) ? "ORDER_TABLE_BLUE" :     \
                     ((chip) == UVES_CHIP_REDU) ? "ORDER_TABLE_REDU" :     \
                     ((chip) == UVES_CHIP_REDL) ? "ORDER_TABLE_REDL" :     \
                     "???"))
#define UVES_ORDER_TABLE_EXTENSION       1
#define UVES_ORDER_TABLE_EXTENSION_POLY  2
#define UVES_ORDER_TABLE_EXTENSION_FIBRE 3

#define RESPONSE_WINDOWS		           "RESPONSE_WINDOWS"
#define QUALITY_AREAS		               "QUALITY_AREAS"
#define FIT_AREAS		                   "FIT_AREAS"

/* Flame science products
FIB_SCI_INFO_TAB
MWXB_SCI_REDL
ERR_MWXB_SCI_REDL
XB_SCI_REDL

MWXB_SCI_RAW_REDL
ERR_MWXB_SCI_RAW_REDL
XB_SCI_RAW_REDL

MWXB_SCI_REDU
ERR_MWXB_SCI_REDU
XB_SCI_REDU
MWXB_SCI_RAW_REDU
ERR_MWXB_SCI_RAW_REDU
XB_SCI_RAW_REDU
*/
#define FLAMES_SCI_INFO_TAB "FIB_SCI_INFO_TAB"

#define FLAMES_MWXB_SCI(chip) ( \
              ((chip) == UVES_CHIP_REDU) ? "MWXB_SCI_REDU" : \
              ((chip) == UVES_CHIP_REDL) ? "MWXB_SCI_REDL" : \
                              "???")

#define FLAMES_ERR_MWXB_SCI(chip) ( \
              ((chip) == UVES_CHIP_REDU) ? "ERR_MWXB_SCI_REDU" : \
              ((chip) == UVES_CHIP_REDL) ? "ERR_MWXB_SCI_REDL" : \
                              "???")


#define FLAMES_XB_SCI(chip) ( \
              ((chip) == UVES_CHIP_REDU) ? "XB_SCI_REDU" : \
              ((chip) == UVES_CHIP_REDL) ? "XB_SCI_REDL" : \
                              "???")

#define FLAMES_ERR_XB_SCI(chip) ( \
              ((chip) == UVES_CHIP_REDU) ? "ERR_XB_SCI_REDU" : \
              ((chip) == UVES_CHIP_REDL) ? "ERR_XB_SCI_REDL" : \
                              "???")

#define FLAMES_WXB_SCI(chip) ( \
              ((chip) == UVES_CHIP_REDU) ? "WXB_SCI_REDU" : \
              ((chip) == UVES_CHIP_REDL) ? "WXB_SCI_REDL" : \
                              "???")

#define FLAMES_ERR_WXB_SCI(chip) ( \
              ((chip) == UVES_CHIP_REDU) ? "ERR_WXB_SCI_REDU" : \
              ((chip) == UVES_CHIP_REDL) ? "ERR_WXB_SCI_REDL" : \
                              "???")


#define FLAMES_MWXB_SCI_RAW(chip) ( \
              ((chip) == UVES_CHIP_REDU) ? "MWXB_SCI_RAW_REDU" : \
              ((chip) == UVES_CHIP_REDL) ? "MWXB_SCI_RAW_REDL" : \
                              "???")

#define FLAMES_ERR_MWXB_SCI_RAW(chip) ( \
             ((chip) == UVES_CHIP_REDU) ? "ERR_MWXB_SCI_RAW_REDU" : \
             ((chip) == UVES_CHIP_REDL) ? "ERR_MWXB_SCI_RAW_REDL" : \
                              "???")


#define FLAMES_WXB_SCI_RAW(chip) ( \
              ((chip) == UVES_CHIP_REDU) ? "WXB_SCI_RAW_REDU" : \
              ((chip) == UVES_CHIP_REDL) ? "WXB_SCI_RAW_REDL" : \
                              "???")

#define FLAMES_ERR_WXB_SCI_RAW(chip) ( \
              ((chip) == UVES_CHIP_REDU) ? "ERR_WXB_SCI_RAW_REDU" : \
              ((chip) == UVES_CHIP_REDL) ? "ERR_WXB_SCI_RAW_REDL" : \
                              "???")


#define FLAMES_XB_SCI_RAW(chip) ( \
                          ((chip) == UVES_CHIP_REDU) ? "XB_SCI_RAW_REDU" : \
              ((chip) == UVES_CHIP_REDL) ? "XB_SCI_RAW_REDL" : \
                              "???")

#define FLAMES_ERR_XB_SCI_RAW(chip) ( \
              ((chip) == UVES_CHIP_REDU) ? "ERR_XB_SCI_RAW_REDU" : \
              ((chip) == UVES_CHIP_REDL) ? "ERR_XB_SCI_RAW_REDL" : \
                              "???")



/* Background tables are used only to tell
   the user that they are actually *not* used for processing */
#define UVES_BACKGR_TABLE(chip) (((chip) == UVES_CHIP_BLUE) ? "BACKGR_TABLE_BLUE" :    \
                 ((chip) == UVES_CHIP_REDU) ? "BACKGR_TABLE_REDU" :    \
                 ((chip) == UVES_CHIP_REDL) ? "BACKGR_TABLE_REDL" : "???")

/* Master bias */
#define UVES_MASTER_BIAS(chip)           (((chip) == UVES_CHIP_BLUE) ? "MASTER_BIAS_BLUE" :  \
                      ((chip) == UVES_CHIP_REDU) ? "MASTER_BIAS_REDU" :  \
                      ((chip) == UVES_CHIP_REDL) ? "MASTER_BIAS_REDL" : "???")
#define UVES_MASTER_BIAS_EXTENSION(chip) 0

#define UVES_BIAS_PD(chip)           (((chip) == UVES_CHIP_BLUE) ? "BIAS_PD_BLUE" :  \
                      ((chip) == UVES_CHIP_REDU) ? "BIAS_PD_REDU" :  \
                      ((chip) == UVES_CHIP_REDL) ? "BIAS_PD_REDL" : "???")

#define UVES_BIAS_PD_MASK(chip)           (((chip) == UVES_CHIP_BLUE) ? "BIAS_PD_MASK_BLUE" :  \
                      ((chip) == UVES_CHIP_REDU) ? "BIAS_PD_MASK_REDU" :  \
                      ((chip) == UVES_CHIP_REDL) ? "BIAS_PD_MASK_REDL" : "???")



/* Master Formatcheck */
#define UVES_MASTER_ARC_FORM(chip) (((chip) == UVES_CHIP_BLUE) ? "MASTER_FORM_BLUE" : \
                                    ((chip) == UVES_CHIP_REDU) ? "MASTER_FORM_REDU" : \
                                    ((chip) == UVES_CHIP_REDL) ? "MASTER_FORM_REDL" : "???")

#define UVES_MASTER_ARC_FORM_EXTENSION(chip) 0

/* Master dark */
#define UVES_MASTER_DARK(chip)   (((chip) == UVES_CHIP_BLUE) ? "MASTER_DARK_BLUE" : \
                                  ((chip) == UVES_CHIP_REDU) ? "MASTER_DARK_REDU" : \
                                  ((chip) == UVES_CHIP_REDL) ? "MASTER_DARK_REDL" : "???")

#define UVES_MASTER_PDARK(chip)  (((chip) == UVES_CHIP_BLUE) ? "MASTER_PDARK_BLUE": \
                  ((chip) == UVES_CHIP_REDU) ? "MASTER_PDARK_REDU": \
                  ((chip) == UVES_CHIP_REDL) ? "MASTER_PDARK_REDL": "???")

#define UVES_MASTER_DARK_EXTENSION(chip) 0

/* Master flat */
#define UVES_MASTER_FLAT(chip)  (((chip) == UVES_CHIP_BLUE) ? "MASTER_FLAT_BLUE" : \
                                 ((chip) == UVES_CHIP_REDU) ? "MASTER_FLAT_REDU" : \
                                 ((chip) == UVES_CHIP_REDL) ? "MASTER_FLAT_REDL" : "???")
#define UVES_MASTER_DFLAT(chip) (((chip) == UVES_CHIP_BLUE) ? "MASTER_DFLAT_BLUE" : \
                                 ((chip) == UVES_CHIP_REDU) ? "MASTER_DFLAT_REDU" : \
                                 ((chip) == UVES_CHIP_REDL) ? "MASTER_DFLAT_REDL" : "???")
#define UVES_MASTER_SFLAT(chip) (((chip) == UVES_CHIP_BLUE) ? "MASTER_SFLAT_BLUE" : \
                                 ((chip) == UVES_CHIP_REDU) ? "MASTER_SFLAT_REDU" : \
                                 ((chip) == UVES_CHIP_REDL) ? "MASTER_SFLAT_REDL" : "???")
#define UVES_MASTER_IFLAT(chip) (((chip) == UVES_CHIP_BLUE) ? "MASTER_IFLAT_BLUE" : \
                                 ((chip) == UVES_CHIP_REDU) ? "MASTER_IFLAT_REDU" : \
                                 ((chip) == UVES_CHIP_REDL) ? "MASTER_IFLAT_REDL" : "???")
#define UVES_MASTER_TFLAT(chip) (((chip) == UVES_CHIP_BLUE) ? "MASTER_TFLAT_BLUE" : \
                                 ((chip) == UVES_CHIP_REDU) ? "MASTER_TFLAT_REDU" : \
                                 ((chip) == UVES_CHIP_REDL) ? "MASTER_TFLAT_REDL" : "???")
#define UVES_REF_TFLAT(chip) (((chip) == UVES_CHIP_BLUE) ? "REF_TFLAT_BLUE" : \
                                 ((chip) == UVES_CHIP_REDU) ? "REF_TFLAT_REDU" : \
                                 ((chip) == UVES_CHIP_REDL) ? "REF_TFLAT_REDL" : "???")
#define UVES_MASTER_SCREEN_FLAT(chip) (((chip) == UVES_CHIP_BLUE) ? "MASTER_SCREEN_FLAT_BLUE" : \
                                       ((chip) == UVES_CHIP_REDU) ? "MASTER_SCREEN_FLAT_REDU" : \
                                       ((chip) == UVES_CHIP_REDL) ? "MASTER_SCREEN_FLAT_REDL" : \
                       "???")
#define UVES_MASTER_FLAT_EXTENSION(chip) 0
#define UVES_BKG_FLAT(chip)              (((chip) == UVES_CHIP_BLUE) ? "BKG_FLAT_BLUE" : \
                                          ((chip) == UVES_CHIP_REDU) ? "BKG_FLAT_REDU" : \
                                          ((chip) == UVES_CHIP_REDL) ? "BKG_FLAT_REDL" : "???" )
#define UVES_RATIO_TFLAT(chip)              (((chip) == UVES_CHIP_BLUE) ? "RATIO_TFLAT_BLUE" : \
                                          ((chip) == UVES_CHIP_REDU) ? "RATIO_TFLAT_REDU" : \
                                          ((chip) == UVES_CHIP_REDL) ? "RATIO_TFLAT_REDL" : "???" )

#define UVES_WEIGHTS(chip)          ((chip) == UVES_CHIP_REDU ? "WEIGHTS_REDU" : \
                                     (chip) == UVES_CHIP_REDL ? "WEIGHTS_REDL" : \
                                     (chip) == UVES_CHIP_BLUE ? "WEIGHTS_BLUE" : \
                                     "???")
/* Line table */
#define UVES_LINE_TABLE(flames,chip)   ((flames) ?                                           \
                                 (((chip) == UVES_CHIP_REDU) ? "FIB_LINE_TABLE_REDU" :       \
                                  ((chip) == UVES_CHIP_REDL) ? "FIB_LINE_TABLE_REDL" : "???")\
                                  :                                                          \
                                  (((chip) == UVES_CHIP_BLUE) ? "LINE_TABLE_BLUE" :          \
                                   ((chip) == UVES_CHIP_REDU) ? "LINE_TABLE_REDU" :          \
                                   ((chip) == UVES_CHIP_REDL) ? "LINE_TABLE_REDL" : "???"))

/* Guess line table */
#define UVES_GUESS_LINE_TABLE(flames,chip) ((flames) ?                                    \
                                 (((chip) == UVES_CHIP_REDU) ? "FIB_LIN_GUE_REDU" :       \
                                  ((chip) == UVES_CHIP_REDL) ? "FIB_LIN_GUE_REDL" : "???")\
                                  :                                                       \
                                  (((chip) == UVES_CHIP_BLUE) ? "LINE_GUESS_TAB_BLUE" :   \
                                  ((chip) == UVES_CHIP_REDU) ? "LINE_GUESS_TAB_REDU" :    \
                   ((chip) == UVES_CHIP_REDL) ? "LINE_GUESS_TAB_REDL" : "???"))

#define UVES_LINE_TABLE_EXTENSION            1
#define UVES_LINE_TABLE_EXTENSION_DISPERSION 2
#define UVES_LINE_TABLE_EXTENSION_ABSORDER   3

/* For backwards compatibility */
#define UVES_LINE_TABLE_MIDAS_BLUE(window) (((window)==1) ? "LINE_TABLE_BLUE1" : \
                                            ((window)==2) ? "LINE_TABLE_BLUE2" : \
                                            ((window)==3) ? "LINE_TABLE_BLUE3" : \
                        "LINE_TABLE_BLUEx")
#define UVES_LINE_TABLE_MIDAS_REDL(window) (((window)==1) ? "LINE_TABLE_REDL1" : \
                                            ((window)==2) ? "LINE_TABLE_REDL2" : \
                                            ((window)==3) ? "LINE_TABLE_REDL3" : \
                                            "LINE_TABLE_REDLx")
#define UVES_LINE_TABLE_MIDAS_REDU(window) (((window)==1) ? "LINE_TABLE_REDU1" : \
                                            ((window)==2) ? "LINE_TABLE_REDU2" : \
                                            ((window)==3) ? "LINE_TABLE_REDU3" : \
                        "LINE_TABLE_REDUx")
#define UVES_LINE_TABLE_MIDAS(chip,window) ( ((chip) == UVES_CHIP_BLUE) ?        \
                        UVES_LINE_TABLE_MIDAS_BLUE(window) : \
                                             ((chip) == UVES_CHIP_REDU) ?        \
                        UVES_LINE_TABLE_MIDAS_REDU(window) : \
                                             ((chip) == UVES_CHIP_REDL) ?        \
                        UVES_LINE_TABLE_MIDAS_REDL(window) : "???")

/* Response curves et al. */
#define UVES_INSTR_RESPONSE(chip) (((chip) == UVES_CHIP_BLUE) ? "INSTR_RESPONSE_BLUE" : \
                                   ((chip) == UVES_CHIP_REDU) ? "INSTR_RESPONSE_REDU" : \
                                   ((chip) == UVES_CHIP_REDL) ? "INSTR_RESPONSE_REDL" : "???")


#define UVES_INSTR_RESPONSE_FINE(chip) (((chip) == UVES_CHIP_BLUE) ? "INSTR_RESPONSE_FINE_BLUE" : \
                                   ((chip) == UVES_CHIP_REDU) ? "INSTR_RESPONSE_FINE_REDU" : \
                                   ((chip) == UVES_CHIP_REDL) ? "INSTR_RESPONSE_FINE_REDL" : "???")
#define UVES_INSTR_RESPONSE_EXTENSION(chip) 0

#define UVES_MASTER_RESPONSE(chip) (((chip) == UVES_CHIP_BLUE) ? "MASTER_RESPONSE_BLUE" : \
                                    ((chip) == UVES_CHIP_REDU) ? "MASTER_RESPONSE_REDU" : \
                                    ((chip) == UVES_CHIP_REDL) ? "MASTER_RESPONSE_REDL" : "???")
#define UVES_MASTER_RESPONSE_EXTENSION(chip) 0

#define UVES_WCALIB_FF_RESPONSE(chip) (((chip) == UVES_CHIP_BLUE) ? "WCALIB_FF_RESPONSE_BLUE" : \
                                       ((chip) == UVES_CHIP_REDU) ? "WCALIB_FF_RESPONSE_REDU" : \
                                       ((chip) == UVES_CHIP_REDL) ? "WCALIB_FF_RESPONSE_REDL" : \
                       "???")
#define UVES_RED_STD(chip)               (((chip) == UVES_CHIP_BLUE) ? "RED_STD_BLUE" : \
                                          ((chip) == UVES_CHIP_REDU) ? "RED_STD_REDU" : \
                                          ((chip) == UVES_CHIP_REDL) ? "RED_STD_REDL" : "???")

#define UVES_RED_NOAPPEND_STD(chip)       (((chip) == UVES_CHIP_BLUE) ? "RED_NONMERGED_STD_BLUE" : \
                                          ((chip) == UVES_CHIP_REDU) ? "RED_NONMERGED_STD_REDU" : \
                                          ((chip) == UVES_CHIP_REDL) ? "RED_NONMERGED_STD_REDL" : "???")

#define UVES_BKG_STD(chip)               (((chip) == UVES_CHIP_BLUE) ? "BKG_STD_BLUE" : \
                                          ((chip) == UVES_CHIP_REDU) ? "BKG_STD_REDU" : \
                                          ((chip) == UVES_CHIP_REDL) ? "BKG_STD_REDL" : "???")

#define UVES_ORDER_EXTRACT_QC(chip)               (((chip) == UVES_CHIP_BLUE) ? "ORDER_EXTRACT_QC_BLUE" : \
                                          ((chip) == UVES_CHIP_REDU) ? "ORDER_EXTRACT_QC_REDU" : \
                                          ((chip) == UVES_CHIP_REDL) ? "ORDER_EXTRACT_QC_REDL" : "???")

#define UVES_EFFICIENCY_TABLE(chip) (((chip) == UVES_CHIP_BLUE) ? "EFFICIENCY_TABLE_BLUE" : \
                                     ((chip) == UVES_CHIP_REDU) ? "EFFICIENCY_TABLE_REDU" : \
                                     ((chip) == UVES_CHIP_REDL) ? "EFFICIENCY_TABLE_REDL" : "???")

/* CD align table */
#define UVES_CD_ALIGN_TABLE(blue)       ((blue) ? "CD_ALIGN_TABLE_BLUE" : "CD_ALIGN_TABLE_RED")

/* 
 * Reference CALIB tags
 */

/* Line catalogue */
#define UVES_LINE_REFER_TABLE           "LINE_REFER_TABLE"
#define UVES_LINE_REFER_TABLE_EXTENSION 1

#define UVES_LINE_INTMON_TABLE           "LINE_INTMON_TABLE"
#define UVES_LINE_INTMON_TABLE_EXTENSION 1
#define UVES_RESPONSE_TABLE_EXTENSION 1

/* Standard star flux */
#define UVES_FLUX_STD_TABLE           "FLUX_STD_TABLE"
#define UVES_FLUX_STD_TABLE_EXTENSION 1

/* Extinction coefficients */
#define UVES_EXTCOEFF_TABLE           "EXTCOEFF_TABLE"
#define UVES_EXTCOEFF_TABLE_EXTENSION 1


#define FLUX_STD_TABLE                     "FLUX_STD_TABLE"
#define FLUX_STD_CATALOG                   "FLUX_STD_CATALOG"
#define TELL_MOD_CATALOG                   "TELL_MOD_CATALOG"
#define RESP_FIT_POINTS_CAT                "RESP_FIT_POINTS_CATALOG"

/*
 *  Image statistics
 */

#define UVES_ALL_STATS (CPL_STATS_MEAN | CPL_STATS_STDEV | CPL_STATS_MEDIAN | \
            CPL_STATS_MIN  | CPL_STATS_MAX)
#define DICTIONARY "PRO-1.15"

/*-----------------------------------------------------------------------------
                                Functions prototypes
 -----------------------------------------------------------------------------*/
int uves_contains_frames_kind(cpl_frameset * sof, 
                                 cpl_frameset* raw,
                                 const char*         type);

char *
uves_local_filename(const char *prefix, enum uves_chip chip, int trace, int window);

void
uves_copy_if_possible(uves_propertylist *to, const uves_propertylist *from,
                      const char *name);

void
uves_warn_if_chip_names_dont_match(const uves_propertylist *calib_header, 
                   const char *raw_chip_name, enum uves_chip chip);

cpl_error_code
uves_extract_frames_group_type(const cpl_frameset * set, cpl_frameset** ext, 
                   cpl_frame_group type);
cpl_error_code
uves_frameset_merge(cpl_frameset * set1, const cpl_frameset* set2);
cpl_error_code
uves_sflats_get_encoder_steps(const cpl_frameset * set, cpl_table** encoder_tbl, int* nset);

cpl_error_code uves_dfs_set_groups(cpl_frameset *);
char *
uves_scired_hdrl_fluxcal_science_filename(enum uves_chip chip);
cpl_image *
uves_crop_and_rotate(const cpl_image *image, const uves_propertylist *header,
             enum uves_chip chip,
             const uves_propertylist *redl_header, 
             bool new_format, uves_propertylist **out_header);

void *uves_read_midas_array(const uves_propertylist *plist, const char *name, int *length,
                cpl_type *type, int *nkeys);
polynomial *
uves_polynomial_convert_from_plist_midas(const uves_propertylist *plist, 
					 const char *regression_name,
                                         const int index);

/* Save */
void
uves_dfs_write_statistics(const cpl_image *image, uves_propertylist *header,
			  unsigned stats_mask);

cpl_error_code
uves_frameset_insert(cpl_frameset *frames, 
                     void *object, 
             cpl_frame_group group, 
                     cpl_frame_type type, 
                     cpl_frame_level level,
             const char *filename, 
                     const char *tag, 
             const uves_propertylist *raw_header,
                     const uves_propertylist *primary_header, 
             const uves_propertylist *table_header, 
                     const cpl_parameterlist *parameters, 
             const char *recipe, 
                     const char *pipeline,
                     cpl_table **qc,
             const char *start_time,
             bool dump_paf,
             unsigned stats_mask);

int uves_check_rec_status(const int val) ;

cpl_error_code
uves_save_image_local(const char *description, const char *filename_prefix, 
                      const cpl_image *image, 
                      enum uves_chip chip, int trace, int window,
                      const uves_propertylist *plist,
                      bool use_bitpix16_for_int);

cpl_error_code uves_save_table_local(const char *description, const char *filename_prefix,
                     const cpl_table *table,
                     enum uves_chip chip, int trace, int window,
                     const uves_propertylist *pheader, 
                     const uves_propertylist *eheader);
cpl_error_code uves_save_polynomial(polynomial *p, const char *filename,
                    const uves_propertylist *header);

void uves_save_image(const cpl_image *image, const char *filename, const uves_propertylist *plist,
		     bool use_bitpix16_for_int, bool save1d);

void uves_save_imagelist(const cpl_imagelist *iml, const char *filename, const uves_propertylist *plist);

cpl_image *uves_load_image(const cpl_frame *f,
    int plane,
    int extension,
    uves_propertylist **header);

cpl_image *uves_load_image_file(const char *filename,
    int plane,
    int extension,
    uves_propertylist **header);
    

/* Load raw */
cpl_error_code
uves_load_master_formatcheck(const cpl_frameset *frames, const char *chip_name,
        const char **mform_filename, 
        cpl_image **mform, uves_propertylist **mform_header, enum uves_chip chip);

cpl_error_code uves_load_formatcheck(const cpl_frameset *frames,
                     bool flames,
                     const char **raw_filename,
                     cpl_image *raw_image[2],
                     uves_propertylist *raw_header[2], 
                     uves_propertylist *rotated_header[2], bool *blue);

cpl_error_code uves_load_orderpos(const cpl_frameset *frames,
                  bool flames,
                  const char **raw_filename,
                  cpl_image *raw_image[2],
                  uves_propertylist *raw_header[2], 
                  uves_propertylist *rotated_header[2], 
                                  bool *blue);

void uves_load_arclamp(const cpl_frameset *frames,
               bool flames,
               const char **raw_filename, 
               cpl_image *raw_image[2], uves_propertylist *raw_header[2],
               uves_propertylist *rotated_header[2], 
               bool *blue, bool *sim_cal);

cpl_error_code uves_load_science(const cpl_frameset *frames, const char **raw_filename, 
                 cpl_image *raw_image[2], 
                 uves_propertylist *raw_header[2], 
                 uves_propertylist *rotated_header[2], 
                 bool *blue,
                 const char **sci_type);
cpl_error_code uves_load_standard(const cpl_frameset *frames, const char **raw_filename,
                  cpl_image *raw_image[2], uves_propertylist *raw_header[2], 
                  uves_propertylist *rotated_header[2], bool *blue);
cpl_error_code uves_load_raw_imagelist(const cpl_frameset *frames, 
                       bool flames,
                       const char *blue_tag, 
                       const char *red_tag, cpl_type type, 
                       cpl_imagelist *images[2], uves_propertylist **raw_headers[2],
                       uves_propertylist *rotated_header[2],
                       bool *blue);

/* Load calibration */
cpl_error_code uves_load_drs(const cpl_frameset *frames, 
                 bool flames,
                 const char *chip_name,
                 const char **drs_filename, 
                 uves_propertylist **drs_header,
                 enum uves_chip chip);
cpl_error_code uves_load_mbias(const cpl_frameset *frames, const char *chip_id,
                   const char **mbias_filename,
                   cpl_image **mbias, uves_propertylist **mbias_header, 
                   enum uves_chip chip);
cpl_error_code uves_load_mdark(const cpl_frameset *frames, const char *chip_id,
                   const char **mdark_filename,
                   cpl_image **mdark, uves_propertylist **mdark_header,
                   enum uves_chip chip);
cpl_error_code uves_load_mflat_const(const cpl_frameset *frames, const char *chip_name,
                     const char **mflat_filename, 
                     cpl_image **mflat, uves_propertylist **mflat_header, 
                     enum uves_chip chip,
                     const cpl_frame **mflat_frame);
cpl_error_code uves_load_mflat(cpl_frameset *frames, const char *chip_name,
                   const char **mflat_filename, 
                   cpl_image **mflat, uves_propertylist **mflat_header, 
                   enum uves_chip chip,
                   cpl_frame **mflat_frame);
cpl_image *
uves_load_weights(const cpl_frameset *frames, const char **weights_filename,
                  enum uves_chip chip);

void uves_load_ref_flat(const cpl_frameset *frames, const char *chip_name,
            const char **filename, cpl_image **rflat,
            uves_propertylist **rflat_header, enum uves_chip chip);

cpl_error_code uves_load_ordertable(const cpl_frameset *frames, 
                    bool flames,
                    const char *chip_id,
                    const char **ordertable_filename,
                    cpl_table **ordertable, 
                    uves_propertylist **ordertable_header, 
                    uves_propertylist **ordertable_xheader, 
                    polynomial **order_locations, 
                                    cpl_table **traces,
                    int *tab_in_out_oshift,
                    double *tab_in_out_yshift,
                                    int ** fibre_mask,
                                    double ** fibre_pos,
                    enum uves_chip chip,
                    bool guess_table);

void uves_load_linetable_const(const cpl_frameset *frames, 
                   bool flames,
                   const char *chip_name,
                   const polynomial *order_locations, 
                   int minorder, int maxorder,
                   const char **linetable_filename,
                   const cpl_table **linetable,
                   const uves_propertylist **linetable_header,
                   const polynomial **dispersion_relation,
                   polynomial **absolute_order,
                   enum uves_chip chip, int trace_id, int window);

void uves_load_linetable(const cpl_frameset *frames, 
             bool flames,
             const char *chip_name,
             const polynomial *order_locations, 
             int minorder, int maxorder,
             const char **linetable_filename,
             cpl_table **linetable,
             uves_propertylist **linetable_header,
             polynomial **dispersion_relation,
             polynomial **absolute_order,
             enum uves_chip chip, int trace_id, int window);

cpl_error_code uves_load_response_curve(const cpl_frameset *frames, const char *chip_name,
                    const char **response_filename, 
                    cpl_image **response_curve,
                    cpl_table **master_response,
                    uves_propertylist **response_header, enum uves_chip chip);

void uves_load_corvel(const cpl_frameset *frames,
              cpl_table **corvel,
              uves_propertylist **corvel_header,
              const char **corvel_filename);
            
void uves_load_cd_align(const cpl_frameset *frames,
            const char **raw_filename1,
            const char **raw_filename2,
            cpl_image *raw_image1[2],
            cpl_image *raw_image2[2],
            uves_propertylist *raw_header1[2], 
            uves_propertylist *raw_header2[2], 
            uves_propertylist *rotated_header1[2], 
            uves_propertylist *rotated_header2[2], 
            bool *blue);

/* Static calibration */
cpl_error_code uves_load_linerefertable(const cpl_frameset *frames, 
                    const char **line_refer_filename, 
                    cpl_table **line_refer, 
                    uves_propertylist **line_refer_header);

cpl_error_code uves_load_lineintmon(const cpl_frameset *frames, 
                    const char **line_intmonr_filename, 
                    cpl_table **line_intmon);

cpl_error_code uves_load_flux_table(const cpl_frameset *frames, const char **flux_table_filename, 
                    cpl_table **flux_table);
    
cpl_error_code uves_load_atmo_ext(const cpl_frameset *frames, 
                  const char **atmext_table_filename, 
                  cpl_table **atmext_table);

/* Product filenames */

/* Mbias */
char *uves_masterbias_filename(enum uves_chip chip);
char *uves_fpn_filename(enum uves_chip chip);
char *uves_fpn_mask_filename(enum uves_chip chip);
/* Mdark */
char *uves_masterdark_filename(enum uves_chip chip);

/* Mflat */
char *uves_masterflat_filename(enum uves_chip chip);
char *uves_masterflat_bkg_filename(enum uves_chip chip);

/* Orderpos */
char *uves_ordef_filename(enum uves_chip chip);
char *uves_order_table_filename(enum uves_chip chip);
char *uves_guess_order_table_filename(enum uves_chip chip);
char *uves_guess_line_table_filename(enum uves_chip chip);

/* Wavecal */
char *uves_line_table_filename(enum uves_chip chip);
char *uves_line_table_filename_paf(enum uves_chip chip);

/* Response */
char *uves_response_hdrl_filename(enum uves_chip chip);
char *uves_response_curve_filename(enum uves_chip chip);
char *uves_response_curve_2d_filename(enum uves_chip chip);
char *uves_response_red_standard_filename(enum uves_chip chip);
char *uves_response_red_noappend_standard_filename(enum uves_chip chip);
char *uves_response_efficiency_filename(enum uves_chip chip);
char *uves_response_bkg_standard_filename(enum uves_chip chip);

char *uves_order_extract_qc_standard_filename(enum uves_chip chip);

/* Tflat */
char *uves_flat_ratio_filename(enum uves_chip chip);

/* CDAlign */
char *uves_cd_align_filename(enum uves_chip chip);

/* Scired */
char *uves_scired_red_science_filename(enum uves_chip chip);
char *uves_scired_red_noappend_science_filename(enum uves_chip chip);
char *uves_scired_red_2d_science_filename(enum uves_chip chip);
char *uves_scired_red_error_filename(enum uves_chip chip);
char *uves_scired_red_noappend_error_filename(enum uves_chip chip);
char *uves_scired_red_2d_error_filename(enum uves_chip chip);
char *uves_scired_fluxcal_science_filename(enum uves_chip chip);
char *uves_scired_fluxcal_science_noappend_filename(enum uves_chip chip);
char *uves_scired_fluxcal_science_2d_filename(enum uves_chip chip);
char *uves_scired_fluxcal_error_filename(enum uves_chip chip);
char *uves_scired_fluxcal_error_noappend_filename(enum uves_chip chip);
char *uves_scired_fluxcal_error_2d_filename(enum uves_chip chip);
char *uves_scired_ff_variance_filename(enum uves_chip chip);
char *uves_scired_ff_variance_2d_filename(enum uves_chip chip);
char *uves_scired_background_filename(enum uves_chip chip);
char *uves_scired_merged_sky_filename(enum uves_chip chip);
char *uves_scired_merged_science_filename(enum uves_chip chip);
char *uves_scired_merged_2d_science_filename(enum uves_chip chip);
char *uves_scired_resampled_filename(enum uves_chip chip);
char *uves_scired_resampled_2d_filename(enum uves_chip chip);
char *uves_scired_resampledmf_filename(enum uves_chip chip);
char *uves_scired_rebinned_filename(enum uves_chip chip);
char *uves_scired_rebinned_error_filename(enum uves_chip chip);
char *uves_scired_rebinned_2d_filename(enum uves_chip chip);
char *uves_scired_rebinned_2d_error_filename(enum uves_chip chip);
char *uves_scired_ordertrace_filename(enum uves_chip chip);
char *uves_scired_wmap_filename(enum uves_chip chip);
char *uves_scired_crmask_filename(enum uves_chip chip);
char *uves_scired_ext2d_filename(enum uves_chip chip);
char *uves_scired_ff2d_filename(enum uves_chip chip);
cpl_image*
uves_vector_to_image(const cpl_vector* vector,cpl_type type);


cpl_error_code
uves_check_if_format_is_midas(uves_propertylist* header, bool* format_is_midas);


#endif
