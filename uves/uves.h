/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-12-07 16:56:13 $
 * $Revision: 1.26 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.25  2010/09/24 09:32:02  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.23  2007/10/24 09:34:01  amodigli
 * removed FLAMES_MKMASTER_DOM FLAMES_MKMASTER_ID
 *
 * Revision 1.22  2007/10/24 09:30:40  amodigli
 * UVES_MKMASTER_SFLAT-->FLAMES_MKMASTER
 *
 * Revision 1.21  2007/08/17 10:06:16  amodigli
 * added UVES_QCDARK_ID
 *
 * Revision 1.20  2007/02/16 15:32:50  amodigli
 * renamed uves_cal_mkmaster_tflat uves_cal_tflat
 *
 * Revision 1.19  2007/02/09 13:35:39  jmlarsen
 * Renamed recipes
 *
 * Revision 1.18  2007/02/08 07:31:48  jmlarsen
 * Added cd_align recipe
 *
 * Revision 1.17  2006/09/27 13:07:46  jmlarsen
 * Added uves_tflat
 *
 * Revision 1.16  2006/06/26 07:53:35  amodigli
 * removed flames function
 *
 * Revision 1.15  2006/06/22 12:25:44  amodigli
 * fixed compilation warnings
 *
 * Revision 1.14  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.3  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */

#ifndef UVES_H
#define UVES_H

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                   Defines
 -----------------------------------------------------------------------------*/

#define UVES_PHYSMOD_ID  uves_cal_predict
#define UVES_ORDERPOS_ID uves_cal_orderpos
#define UVES_MBIAS_ID    uves_cal_mbias
#define UVES_MDARK_ID    uves_cal_mdark
#define UVES_MFLAT_ID    uves_cal_mflat
#define UVES_MFLAT_COMBINE_ID    uves_cal_mflat_combine
#define UVES_MKMASTER_ID uves_cal_mkmaster
#define UVES_WAVECAL_ID  uves_cal_wavecal
#define UVES_RESPONSE_ID uves_cal_response
#define UVES_TFLAT_ID    uves_cal_tflat
#define UVES_CD_ALIGN_ID uves_cal_cd_align
#define UVES_SCIRED_ID   uves_obs_scired
#define UVES_SPATRED_ID  uves_obs_spatred
#define UVES_REDCHAIN_ID uves_obs_redchain

#define UVES_REDUCE_ID  "reduce"
#define UVES_BACKSUB_ID "backsub"
#define UVES_QCDARK_ID  "qc_dark"
#define UVES_EXTRACT_ID "extract"
#define UVES_REBIN_ID   "rebin"

/* Message domains */
#define UVES_PHYSMOD_DOM  "Predict"
#define UVES_ORDERPOS_DOM "OrderPos"
#define UVES_MBIAS_DOM    "MBias"
#define UVES_MDARK_DOM    "MDark"
#define UVES_MFLAT_DOM    "MFlat"
#define UVES_MFLAT_COMBINE_DOM    "MFlatCombine"
#define UVES_MKMASTER_DOM "MkMaster"
#define UVES_WAVECAL_DOM  "WaveCal"
#define UVES_RESPONSE_DOM "Response"
#define UVES_TFLAT_DOM    "TFlat"
#define UVES_CD_ALIGN_DOM "CDAlign"
#define UVES_SCIRED_DOM   "SciRed"
#define UVES_SPATRED_DOM   "SpatRed"
#define UVES_REDCHAIN_DOM "RedChain"


/*-----------------------------------------------------------------------------
                                   Prototypes
 -----------------------------------------------------------------------------*/

int uves_physmod_get_info(cpl_pluginlist *);
int uves_orderpos_get_info(cpl_pluginlist *);
int uves_mbias_get_info(cpl_pluginlist *);
int uves_mdark_get_info(cpl_pluginlist *);
int uves_mflat_get_info(cpl_pluginlist *);
int uves_mflat_combine_get_info(cpl_pluginlist *);
int uves_cal_mkmaster_get_info(cpl_pluginlist *);
int uves_cal_mkmaster_sflat_get_info(cpl_pluginlist *);
int uves_wavecal_get_info(cpl_pluginlist *);
int uves_response_get_info(cpl_pluginlist *);
int uves_scired_get_info(cpl_pluginlist *);
int uves_obs_spatred_get_info(cpl_pluginlist *);
int uves_tflat_get_info(cpl_pluginlist *);
int uves_cal_cd_align_get_info(cpl_pluginlist *);
int uves_redchain_get_info(cpl_pluginlist *);
#endif /* UVES_H */
