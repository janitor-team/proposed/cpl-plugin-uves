/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 13:56:00 $
 * $Revision: 1.11 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.10  2010/09/24 09:32:07  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.8  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.7  2007/05/22 11:31:35  jmlarsen
 * Removed image plotting functionality
 *
 * Revision 1.6  2006/08/23 15:09:25  jmlarsen
 * Added uves_plot_bivectors
 *
 * Revision 1.5  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.4  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_PLOT_H
#define UVES_PLOT_H
#include <uves_cpl_size.h>
#include <cpl.h>

cpl_error_code
uves_plot_initialize(const char *plotter_command);

cpl_error_code
uves_plot_table(const cpl_table *table, const char *colx, const char *coly,
        const char *format, ...)
#ifdef __GNUC__
__attribute__((format (printf, 4, 5)))
#endif
;

cpl_error_code
uves_plot_image_rows(const cpl_image *image, int first_row, int last_row, int step, 
             const char *xtitle, const char *ytitle, const char *format, ...)
#ifdef __GNUC__
__attribute__((format (printf, 7, 8)))
#endif
;

void
uves_plot_bivectors(cpl_bivector **bivectors, char **titles, 
            int N, const char *xtitle,
            const char *ytitle);

cpl_error_code
uves_plot_image_columns(const cpl_image *image, int first_column, int last_column, int step, 
            const char *xtitle, const char *ytitle, const char *format, ...)
#ifdef __GNUC__
__attribute__((format (printf, 7, 8)))
#endif
;

#endif
