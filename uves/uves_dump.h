/*                                                                              *
 *   This file is part of the ESO UVES  Pipeline                                *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:03 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.2  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_DUMP_H
#define UVES_DUMP_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <uves_propertylist.h>
#include <cpl.h>

cpl_error_code uves_print_uves_propertylist(const uves_propertylist *pl, long low, long high);
cpl_error_code uves_print_cpl_property(const cpl_property *);

cpl_error_code uves_print_cpl_frameset(const cpl_frameset *);
cpl_error_code uves_print_cpl_frame(const cpl_frame *);

const char *uves_tostring_cpl_type(cpl_type t);
const char *uves_tostring_cpl_frame_type(cpl_frame_type);
const char *uves_tostring_cpl_frame_group(cpl_frame_group);
const char *uves_tostring_cpl_frame_level(cpl_frame_level);

#endif /* UVES_DUMP_H */
