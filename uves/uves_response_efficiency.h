/*                                                                              *
 *   This file is part of the ESO UVES  Pipeline                                *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2013-08-08 13:36:46 $
 * $Revision: 1.14 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.13  2013/07/01 15:35:42  amodigli
 * Rename DEBUG to debug_mode to remove compiler error on some platforms (that name is reserved to special compiler options)
 *
 * Revision 1.12  2011/12/08 14:02:01  amodigli
 * include uves_cpl_size.h for CPL6
 *
 * Revision 1.11  2010/09/24 09:32:07  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.9  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.8  2007/05/22 11:44:34  jmlarsen
 * Removed MIDAS flag for good
 *
 * Revision 1.7  2007/05/03 15:24:58  jmlarsen
 * Fixed const bugs
 *
 * Revision 1.6  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.5  2007/01/29 12:13:29  jmlarsen
 * Calculate extraction slit length
 *
 * Revision 1.4  2006/11/06 15:19:42  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.3  2006/11/03 15:01:21  jmlarsen
 * Killed UVES 3d table module and use CPL 3d tables
 *
 * Revision 1.2  2006/05/16 12:13:07  amodigli
 * added QC log
 *
 * Revision 1.1  2006/02/03 07:51:04  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.7  2005/12/20 08:11:44  jmlarsen
 * Added CVS  entry
 *
 */
#ifndef UVES_RESPONSE_EFFICIENCY_H
#define UVES_RESPONSE_EFFICIENCY_H
#include <uves_cpl_size.h>
#include <uves_utils_polynomial.h>
#include <uves_chip.h>

#include <cpl.h>

#include <stdbool.h>

cpl_error_code
uves_response_efficiency(const cpl_image *raw_image, 
                         const uves_propertylist *raw_header, 
                         const uves_propertylist *rotated_header,
             const cpl_image *master_bias,
             const uves_propertylist *mbias_header,
             const cpl_image *master_dark, 
                         const uves_propertylist *mdark_header, 
             const cpl_table *ordertable, 
                         const polynomial *order_locations,
             const cpl_table *linetable[3], 
                         const uves_propertylist *linetable_header[3], 
                         const polynomial *dispersion_relation[3],
             const cpl_table *flux_table,
             const cpl_table *atm_extinction,
             enum uves_chip chip,
                         bool debug_mode,
             const cpl_parameterlist *parameters,
             /* Identification */
             double PACCURACY,
             /* Output */
             cpl_table **efficiency,
             cpl_table **blaze_efficiency);

#endif /* UVES_RESPONSE_EFFICIENCY_H */
