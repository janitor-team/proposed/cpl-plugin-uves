/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2012-03-02 16:38:18 $
 * $Revision: 1.22 $
 * $Name: not supported by cvs2svn $
 */
/*----------------------------------------------------------------------------*/
/**
 * @addtogroup uves_physmod
 */
/*----------------------------------------------------------------------------*/
/**@{*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <uves_physmod_calmap.h>

#include <uves_physmod_regress_echelle.h>
#include <uves_wavecal_utils.h>
#include <uves_utils_wrappers.h>
#include <uves_msg.h>
#include <uves_error.h>

/**
@brief This procedure makes the order definitions and wavelength 
       calibration from the wavelength projection map, and saves the 
       data in tables and global keywords in echelle context.  
       It generates order and line tables.

@param raw_header   header of raw frame
@param chip         UVES chip ID
@param recipe_id    name of calling recipe
@param parameters   input recipe parameters
@param npline_tbl   input table
@param ord_tbl      output order table
@param lin_tbl      output line table
@param w_tbl        output table with results from regression
@param s_tbl        output table with results from regression (same as w one?)
@param abs_ord_min  min absolute order
@param abs_ord_max  max absolute order
@param poly2d       output polynomial with coefficients from regression: 
                    Order=poly2d(X,YNEW) fit is stored in column RORD

@Note: this has been ported from UVES MIDAS pipeline
 
*/
int uves_physmod_calmap(const uves_propertylist *raw_header, 
            enum uves_chip chip,
            const char *recipe_id,
            const cpl_parameterlist* parameters, 
            cpl_table* npline_tbl,
            cpl_table** ord_tbl,
            cpl_table** lin_tbl,
            cpl_table** w_tbl,
            cpl_table** s_tbl,
            int* abs_ord_min,
            int* abs_ord_max,
            polynomial** poly2d)
{
  int rel_ord=0;
  int ord_min=0;
  int ord_max=0;
  int ord_ech=0;
  double dis_avg=0;

  uves_propertylist* plist=NULL;
  polynomial* poly1d=NULL;

  double mean_err_aux=0;
  double mean_err_order=0;

  int num_outliers=0;
  double tol=0;
  double kappa=0.;
  double outputd[3];
  uves_msg_debug("start calmap");



  check(uves_get_parameter(parameters,NULL,recipe_id,"kappa",CPL_TYPE_DOUBLE,&kappa), 
	"Could not read parameter");
  check(uves_get_parameter(parameters,NULL,recipe_id,"tol",CPL_TYPE_DOUBLE,&tol), 
	"Could not read parameter");

  cpl_table_erase_invalid_rows(npline_tbl);
  uves_msg_debug("nraw=%" CPL_SIZE_FORMAT "",cpl_table_get_nrow(npline_tbl));
  *ord_tbl=cpl_table_new(cpl_table_get_nrow(npline_tbl));
    
  cpl_table_duplicate_column(*ord_tbl,"ABS_ORDER",npline_tbl,"ORDER");
  cpl_table_duplicate_column(*ord_tbl,"ORDER",npline_tbl,"ORDER");
  cpl_table_duplicate_column(*ord_tbl,"X",npline_tbl,"XMOD");
  cpl_table_duplicate_column(*ord_tbl,"Y",npline_tbl,"YMOD");
 
  /* 
  cpl_table_dump(*ord_tbl,0,20,stdout);
  OLD VERSION: (WRONG)
  rel_ord = cpl_table_get_int(*ord_tbl,"ORDER",0,&status);
  */
  rel_ord = cpl_table_get_column_max(*ord_tbl,"ORDER");

  uves_msg_debug("relative order=%d",rel_ord);
  cpl_table_multiply_scalar(*ord_tbl,"ORDER",-1);
  cpl_table_add_scalar(*ord_tbl,"ORDER",rel_ord);
  cpl_table_add_scalar(*ord_tbl,"ORDER",+1);


  plist=uves_propertylist_new();
  uves_propertylist_append_bool(plist,"ORDER",0);  /* 0 for ascending order */
  uves_propertylist_append_bool(plist,"X",0);      /* 1 for descending order */
  uves_table_sort(*ord_tbl,plist);
  uves_free_propertylist(&plist);
  *abs_ord_min=cpl_table_get_column_min(*ord_tbl,"ORDER");
  *abs_ord_max=cpl_table_get_column_max(*ord_tbl,"ORDER");

  ord_ech=*abs_ord_max-*abs_ord_min+1;
  uves_msg_debug("Orders: max %d min %d  No %d",
           *abs_ord_max,*abs_ord_min,(*abs_ord_max-*abs_ord_min+1));



  uves_physmod_regress_echelle(raw_header,chip,
                   recipe_id,parameters,
                               ord_tbl,num_outliers, 
                               tol, kappa,s_tbl,w_tbl);


  /* we skip  prepare/background */



  uves_free_table(lin_tbl);
  *lin_tbl=cpl_table_new(cpl_table_get_nrow(npline_tbl));


  cpl_table_duplicate_column(*lin_tbl,"X",npline_tbl,"XMOD");
  cpl_table_duplicate_column(*lin_tbl,"Y",npline_tbl,"ORDER");
  cpl_table_duplicate_column(*lin_tbl,"PEAK",npline_tbl,"ORDER");
  cpl_table_duplicate_column(*lin_tbl,"Ident",npline_tbl,"IDENT");
  cpl_table_duplicate_column(*lin_tbl,"YNEW",npline_tbl,"YMOD");
  cpl_table_duplicate_column(*lin_tbl,"Order",npline_tbl,"ORDER");
  cpl_table_duplicate_column(*lin_tbl,"WAVEC",npline_tbl,"IDENT");

  cpl_table_duplicate_column(*lin_tbl,"AUX",npline_tbl,"ORDER");
  cpl_table_cast_column(*lin_tbl,"AUX","Aux",CPL_TYPE_DOUBLE);
  cpl_table_multiply_columns(*lin_tbl,"Aux","Ident");
  cpl_table_erase_column(*lin_tbl,"AUX");

  cpl_table_set_column_unit(*lin_tbl,"WAVEC","Ang");
  cpl_table_set_column_unit(*lin_tbl,"YNEW","pix");
  cpl_table_set_column_unit(*lin_tbl,"Ident","Ang");
  /* we check that the product Aux=ORDER*Ident=m*lambda=d*sin(alpha) is a
     constant as is expected in a grating. A non constant product is an 
     indication of detector rotation (this is the case: the product is 
     not exacly constant).
  */
  check(poly1d=uves_polynomial_regression_1d(*lin_tbl,"X","Aux",NULL,
                         3,"XREG",NULL,
                         &mean_err_aux,-1),
                                         "Fitting YDIF failed");

  cpl_table_set_column_unit(*lin_tbl,"XREG","pix");
  cpl_table_set_column_unit(*lin_tbl,"X","pix");

  /* NOTE THAT XREG is NOT an output column from MIDAS: should we remove it? */

  outputd[0]=uves_polynomial_get_coeff_1d(poly1d,0);
  outputd[1]=uves_polynomial_get_coeff_1d(poly1d,1);
  outputd[2]=uves_polynomial_get_coeff_1d(poly1d,2);

  ord_min=cpl_table_get_column_min(*lin_tbl,"Order");
  ord_max=cpl_table_get_column_max(*lin_tbl,"Order");

  uves_msg_debug("output0=%f output1=%f output2=%f",
           outputd[0],outputd[1],outputd[2]);

  uves_msg_debug("ord_max=%d ord_min=%d",ord_max,ord_min);

  cpl_table_duplicate_column(*lin_tbl,LINETAB_PIXELSIZE,*lin_tbl,"X");
  cpl_table_multiply_scalar(*lin_tbl,LINETAB_PIXELSIZE,2*outputd[2]);
  cpl_table_add_scalar(*lin_tbl,LINETAB_PIXELSIZE,outputd[1]);
  cpl_table_divide_columns(*lin_tbl,LINETAB_PIXELSIZE,"Order");
 

   dis_avg=cpl_table_get_column_mean(*lin_tbl,LINETAB_PIXELSIZE);
   uves_msg("Average pixel size: %f wav. units",dis_avg);
 
 
  check(*poly2d=uves_polynomial_regression_2d(*lin_tbl,"X","YNEW","Order",
                         NULL,4,4,"RORD",NULL,NULL,
                         &mean_err_order,NULL,NULL,-1,-1),
                                         "Fitting Order failed");



  /* NOTE THAT RORD is NOT an output column from MIDAS: should we remove it? */


  /*
save/regr    {LINTAB} RORD KEYLONG
  */

  /* SHOULD WE SAVE RORD somewhere? */

  /* This saves the current section and parameters and tables: generates
     {P3}LINE.tbl {P3}ORDE.tbl {P3}back.tbl

save/echelle  {P3} >Null
  */

  /*
set/echelle   wlcmtd=guess guess={P3} 
  */


  uves_msg_debug("Line Table: ncol=%" CPL_SIZE_FORMAT "",cpl_table_get_ncol(*lin_tbl));
  cleanup:
  uves_free_propertylist(&plist);
  uves_polynomial_delete(&poly1d);
  
    uves_msg_debug("end calmap");
  return 0;
}
/**@}*/
