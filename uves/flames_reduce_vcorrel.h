/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_REDUCE_VCORREL_H
#define FLAMES_REDUCE_VCORREL_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <cpl.h>

// Simultaneous corvel simcal data reduction
extern double DRS_CVEL_MIN;
extern double DRS_CVEL_MAX;
extern double DRS_CVEL_STEP;

int
flames_reduce_vcorrel(const char* sci_pfx, 
                      const char* cvel_pfx, 
                      const char* ccd_id,
                      const int ord_max,
                      const cpl_frame* cvel_tab, 
                      const char* xxima_pfx, 
                      const char* xwcal_pfx,
                      const double drs_cvel_min,
                      const double drs_cvel_max,
                      double* zero_point,
                      double* avg_cnt,
                      double* sig_cnt,
		      cpl_table* qclog);

#endif
