/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:08 $
 * $Revision: 1.44 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.42  2010/02/13 12:22:31  amodigli
 * removed inlines (let's do work to compiler)
 *
 * Revision 1.41  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.40  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.39  2007/02/27 14:08:46  jmlarsen
 * Extended interface of uves_find_property
 *
 * Revision 1.38  2007/01/29 12:14:51  jmlarsen
 * Added uves_find_property()
 *
 * Revision 1.37  2007/01/15 08:48:26  jmlarsen
 * Exported get_kth function
 *
 * Revision 1.36  2006/11/06 15:19:42  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.35  2006/09/19 07:17:08  jmlarsen
 * Reformatted line
 *
 * Revision 1.34  2006/09/11 08:19:10  jmlarsen
 * Renamed identifier reserved by POSIX
 *
 * Revision 1.33  2006/09/08 14:06:03  jmlarsen
 * Added uves_tools_get_median()
 *
 * Revision 1.32  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.31  2006/06/13 12:02:21  jmlarsen
 * Renamed y0 -> y_0
 *
 * Revision 1.30  2006/02/21 14:24:45  jmlarsen
 * Parameterized behaviour of median filter near image border
 *
 * Revision 1.29  2006/01/31 08:25:50  jmlarsen
 * Renamed uves_fit_gaussian_2d -> uves_fit_gaussian_2d_image
 *
 * Revision 1.28  2006/01/25 16:13:20  jmlarsen
 * Changed interface of gauss.fitting routine
 *
 * Revision 1.27  2006/01/12 15:41:14  jmlarsen
 * Moved gauss. fitting to irplib
 *
 * Revision 1.26  2005/12/20 08:11:44  jmlarsen
 * Added CVS  entry
 *
 */
#ifndef UVES_UTILS_CPL_H
#define UVES_UTILS_CPL_H

/*-----------------------------------------------------------------------------
                    Includes
 -----------------------------------------------------------------------------*/

#include <uves_propertylist.h>
#include <cpl.h>
#include <limits.h>
#include <float.h>
#include <stdbool.h>

/*-----------------------------------------------------------------------------
                             Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                   Prototypes
 -----------------------------------------------------------------------------*/
const cpl_property * uves_find_property_const(const uves_propertylist *plist, 
                    const char *name,
                    int number);
cpl_property * uves_find_property(uves_propertylist *plist, 
                  const char *name,
                  int number);

cpl_error_code uves_filter_image_median(cpl_image **image, int xwindow, 
                    int ywindow, bool extrapolate_border);
cpl_error_code uves_filter_image_average(cpl_image *image, int radius_x, 
                     int radius_y);
cpl_error_code uves_fit_gaussian_2d_image(const cpl_image *image, 
                      const cpl_image *noise,
                      int x1, int y_1,
                      int x2, int y2,
                      double *x0, double *y_0,
                      double *sigmax, double *sigmay,
                      double *amplitude,
                      double *dx0, double *dy0);

double uves_tools_get_median(double *a, int n);
double uves_utils_get_kth_double(double  *   a, 
                    int         n, 
                    int         k);


#endif












