/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 14:06:15 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.8  2010/09/24 09:32:07  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.6  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.5  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.4  2006/04/06 08:49:23  jmlarsen
 * Propagate errors when normalizing spectrum
 *
 * Revision 1.3  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 * Revision 1.2  2005/12/16 14:22:23  jmlarsen
 * Removed midas test data; Added sof files
 *
 * Revision 1.1  2005/11/11 13:18:54  jmlarsen
 * Reorganized code, renamed source files
 *
 */
#ifndef UVES_REDUCE_UTILS_H
#define UVES_REDUCE_UTILS_H
#include <uves_cpl_size.h>

#include <uves_propertylist.h>
#include <uves_chip.h>
#include <cpl.h>
#include <stdbool.h>

cpl_image *
uves_normalize_spectrum(const cpl_image *spectrum, const cpl_image *spectrum_error,
            const uves_propertylist *spectrum_header,
            const uves_propertylist *raw_header,
            int n_traces,
            enum uves_chip chip,
            const cpl_table *atm_extinction,
            bool correct_binning,
            cpl_image **scaled_error);

#endif
