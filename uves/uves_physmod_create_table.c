/*                                                                           *
 *   This file is part of the ESO UVES Pipeline                              *
 *   Copyright (C) 2004,2005 European Southern Observatory                   *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.37 $
 * $Name: not supported by cvs2svn $
 */

/*---------------------------------------------------------------------------*/
/**
 * @addtogroup uves_physmod
 */
/*---------------------------------------------------------------------------*/
/**@{*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/
#include <uves_physmod_create_table.h>

#include <uves_physmod_utils.h>
#include <uves_utils_wrappers.h>
#include <uves_pfits.h>
#include <uves_msg.h>
#include <uves_error.h>
#include <string.h>
/*-----------------------------------------------------------------------------
                                Defines
 ----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/

static int
flames_get_trans(const int plt_no,
                 enum uves_chip chip, 
                 const double wlen, 
                 double * TX,
                 double * TY);

static int
flames_get_physmod_offset(const int plate_no,
                         const int wavec,
                         enum uves_chip chip,
			 double* physmod_shift_x,
                         double* physmod_shift_y,
			 double* rot_1,
                         double* rot_2,
                         double* rot_3);


static int
uves_get_physmod_offset(const int wavec,
                        enum uves_chip chip,
			  const int binx,
			  const int biny,
                         double* trans_x,
                         double* trans_y,
                         double* rot_1,
                         double* rot_2,
			double* rot_3);




/*-----------------------------------------------------------------------------
                            Static variables
 ----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions code
 ----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/**
  @brief    Generates two tables with results of the UVES physical model.
  @param    raw_header  An array containing the input image headers. 
                         The ordering must be the same as the ordering 
                         of images in the input image list
  @param    chip         CCD chip
  @param    recipe_id    name of calling recipe
  @param    parameters   The recipe parameter list
  @param    line_refer   The reference arc lamp line list
  @param    physmod_shift_x Correction to physical model
  @param    physmod_shift_y Correction to physical model
  @param    mod_tbl      The output table with results from the UVES physical 
                         model
  @param    fsr_tbl      Output table with predicted Free Spectral Range 

  @return   0 if everything is ok, -1 otherwise

  This function is used to run the UVES physical model and predict the
  spectral format (order trace, line positions, blaze, etc..)


 */
/*---------------------------------------------------------------------------*/




int 
uves_physmod_create_table(const uves_propertylist *raw_header, 
                          enum uves_chip chip, 
			  bool flames,
			  const char *recipe_id,
                          const cpl_parameterlist* parameters, 
                          cpl_table* line_refer,
                          const double physmod_shift_x,
                          const double physmod_shift_y,
                          cpl_table** mod_tbl,
                          cpl_table** fsr_tbl)

{

  /* General */
  int      MBOX_X=0;
  int      MBOX_Y=0;
  double   TRANS_X=0;
  double   TRANS_Y=0;

  double      ECH_ANG_OFF=0;
  double      CD_ANG_OFF=0;
  double      CCD_ANG_OFF=0;


  int bin_x=0;
  int bin_y=0;


  int      CMP_REG_SW=0;

  double binx=0;
  double biny=0;

  double pressure=0;
  double temp_cam=0;
  double slit_width=0;
  double slit_length=0;
  double humidity=0;


  double wcent=0;
  double xtrans=0;
  double ytrans=0;
  double ccd_rot[2];
  double offset_x[6];
  double offset_y[6];

  const char*   cd_id;
  int     CDID=0;
  int status=0;


  int aRowNumber=0;
  int xpix=0;
  int ypix=0;
  int i=0;
  int mm=0;
  double xmod=0;
  double ymod=0;
  double blaze=0;
  double binsize=0;
  double pixscale=0;
  double pixscalCD=0;
  double linelenpx=0;
  double linewidth=0;
  double linewidpx=0;
  double resol=0;
  double xreg=0;
  double yreg=0;
  double fsr_min=0;
  double fsr_max=0;
  double xfsr_min=0;
  double xfsr_max=0;
  double yfsr_min=0;
  double yfsr_max=0;
  int m_min=0;
  int m_max=0;
  double lmin=0;
  double lmax=0;
  double dxpix=0;
  double dypix=0;
  double dblwav=0;
  int imod=0;
  double dl=0;
  int mmin=0;
  int mmax=0;
  int ncol=0;
  int order=0;
  double xr=0;
  double yr=0;
  double lcent=0;

  double mbox_x=0;
  double mbox_y=0;

  /* variables which were globals in physmodel */
  double uves_beta_ech=0;
  double uves_beta_cd=0 ;
  double uves_physmod_rcd=0;
  double uves_physmod_rech=0;
  double uves_physmod_x_off=0;
  double uves_physmod_y_off=0;
  double x=0;
  double y=0;
  double fc=0;
  double l=0;
  int m=0;
  int plate_no=0;
  double ech_ang_off=0;
  double cd_ang_off=0;
  double ccd_ang_off=0;
  double physmod_off_x=0;
  double physmod_off_y=0;
  int wavec=0;
 
 
  
  enum uves_arm_ident {UVES_ARM_UNDEF,UVES_ARM_BLUE,UVES_ARM_RED};
  enum uves_arm_ident uves_arm_ident = UVES_ARM_UNDEF;
  int upper;

  const char* ccd_id="";
  const char* dpr_tech="";
  /* initialize the configuration */


    /* Read recipe parameters */
  {
    check( uves_get_parameter(parameters, NULL, recipe_id, 
           "mbox_x", CPL_TYPE_INT, &MBOX_X )  , 
       "Could not read parameter");

    check( uves_get_parameter(parameters, NULL, recipe_id, 
           "mbox_y", CPL_TYPE_INT, &MBOX_Y )  , 
       "Could not read parameter");

    check( uves_get_parameter(parameters, NULL, recipe_id, 
           "trans_x", CPL_TYPE_DOUBLE, &TRANS_X )  , 
       "Could not read parameter");

    check( uves_get_parameter(parameters, NULL, recipe_id, 
           "trans_y", CPL_TYPE_DOUBLE, &TRANS_Y )  ,
       "Could not read parameter");
    xtrans=TRANS_X;
    ytrans=TRANS_Y;

    /*
AMo 02/04/2004
We noticed that the physical model has in some way shifted on MIT, thus we apply corrections
    */

if ((xtrans == 0.) && (ytrans == 0.)) {
  /*
   If the user has not set its own defaults which we assume as the right ones
   In case of MIT chip we apply a shift
  */
     if (chip == UVES_CHIP_REDU) {
       //ytrans = 6.7;
     }
}

 uves_msg_debug("xtrans=%f ytrans=%f",xtrans,ytrans);
    check( uves_get_parameter(parameters, NULL, recipe_id, 
           "ech_angle_off", CPL_TYPE_DOUBLE, &ECH_ANG_OFF )  , 
       "Could not read parameter");

    check( uves_get_parameter(parameters, NULL, recipe_id, 
           "cd_angle_off", CPL_TYPE_DOUBLE, &CD_ANG_OFF )  , 
       "Could not read parameter");

    check( uves_get_parameter(parameters, NULL, recipe_id, 
           "ccd_rot_angle_off", CPL_TYPE_DOUBLE, &CCD_ANG_OFF )  , 
       "Could not read parameter");

    check( uves_get_parameter(parameters, NULL, recipe_id, 
          "compute_regression_sw", CPL_TYPE_BOOL, &CMP_REG_SW )  , 
          "Could not read parameter");



    /* check parameters */
  }
    
  /* get instrument configuration setting keywords */
  check (ccd_id = uves_pfits_get_chipid(raw_header,chip), 
     "Could not read CCD ID from input header");
  check (dpr_tech = uves_pfits_get_dpr_tech(raw_header), 
     "Could not read DPR TECH from input header");
  uves_msg("DPR TECH=%s<",dpr_tech);
  if(strstr(dpr_tech,"MOS")!=NULL) {
      check_nomsg( plate_no = uves_flames_pfits_get_plateid(raw_header) );
      uves_msg("PLATE NO=%d",plate_no);
  }

  if(strcmp(ccd_id,"CCD-44b") == 0) {
    uves_msg("New Spectral Format");
    offset_x[0]=1355.0;
    offset_x[1]=1350.0;
    offset_x[2]=1852.0;
    offset_x[3]=1835.0;
    offset_x[4]=1852.0;
    offset_x[5]=1834.0;

    offset_y[0]=1030.0;
    offset_y[1]=1023.0;
    offset_y[2]=2098.0;
    offset_y[3]=2104.0;
    offset_y[4]=-52.0;
    offset_y[5]=-49.0;
    ccd_rot[0]=0.3;
    ccd_rot[1]=-0.10;

  } else {
    uves_msg("Old Spectral Format");

    offset_x[0]=1391.0;
    offset_x[1]=1385.0;
    offset_x[2]=1852.0;
    offset_x[3]=1835.0;
    offset_x[4]=1852.0;
    offset_x[5]=1834.0;

    offset_y[0]=1030.0;
    offset_y[1]=1025.0;
    offset_y[2]=2098.0;
    offset_y[3]=2104.0;
    offset_y[4]=-52.0;
    offset_y[5]=-49.0;

    ccd_rot[0]=0.3;
    ccd_rot[1]=-0.55;
    
  }


  check (biny = uves_pfits_get_binx(raw_header),
     "Could not read x binning factor from input header");

  check (binx = uves_pfits_get_biny(raw_header), 
     "Could not read x binning factor from input header");

  check (pressure = uves_pfits_get_pressure(raw_header), 
     "Could not read pressure from input header");

  /*
    check (humidity = uves_pfits_get_humidity(raw_header), 
           "Could not read humidity from input header");
  */
  humidity=3.;


/* New code */
  check (slit_width = uves_pfits_get_slitwidth(raw_header, chip), 
     "Could not read slit width input header");
  
  check (slit_length = uves_pfits_get_slitlength(raw_header, chip), 
     "Could not read slit length input header");
  
  check (wcent = uves_pfits_get_gratwlen(raw_header, chip), 
     "Could not read central wavelength setting from input header");
  
  check (cd_id =  uves_pfits_get_gratid(raw_header, chip), 
     "Could not read central CD ID setting from input header");

  check (temp_cam = uves_pfits_get_tempcam(raw_header,chip), 
     "Could not read camera's temperature from input header");

  if (chip == UVES_CHIP_BLUE)
      {
      uves_arm_id='b';
      uves_arm_ident=UVES_ARM_BLUE;

      }
  else
      {
      uves_arm_id='r';
      uves_arm_ident=UVES_ARM_RED;
      }
  upper = (chip == UVES_CHIP_REDU);




         if(strstr(cd_id,"CD#1") != NULL) {
       CDID=1;
  } else if(strstr(cd_id,"CD#2") != NULL) {
       CDID=2;
  } else if(strstr(cd_id,"CD#3") != NULL) {
       CDID=3;
  } else if(strstr(cd_id,"CD#4") != NULL) {
       CDID=4;
  } else {
           CDID=0;
  }
    
     mbox_x=MBOX_X/binx;
     mbox_y=MBOX_Y/biny;
     uves_msg("UVES configuration:");
         uves_msg("Optical path: %s (%s)",
                            UVES_ARM(chip),UVES_ARMID(chip));
       
         uves_msg("Detector: %s Binning: %1.1f, %1.1f pix/bin",
                      UVES_DETECTOR(chip),binx,biny);
         uves_msg("Cross disperser ID, number: %s %d",cd_id,CDID);
         uves_msg("Central wavelength: %4.1f nm",wcent);
         uves_msg("Slit width: %3.1f (arcsecs)",slit_width);
         uves_msg("Slit length: %3.1f (arcsecs)",slit_length);
     uves_msg("Temperature: %3.1f C",temp_cam);
     uves_msg("Air pressure: %4.1f mmHg/cm^2",pressure);
     uves_msg("Humidity: %2.1f %%",humidity);

  uves_msg_debug("chip    =%s", uves_chip_tostring_upper(chip));


  /* read input frame (line table) */
  ncol=cpl_table_get_ncol(line_refer);
 
  aRowNumber = cpl_table_get_nrow(line_refer);
  
  uves_msg_debug("Opened table %s ncol=%d nrow=%d","line_ref_table" ,ncol,aRowNumber);

  uves_air_config(pressure/1.33322, temp_cam, humidity);
  uves_msg("Air Index = %g",uves_air_index(wcent));
  if(strstr(dpr_tech,"MOS")!=NULL) {
    uves_msg("uves chip=%s ",uves_chip_tostring_upper(chip));
     flames_get_trans(plate_no,chip,wcent,&TRANS_X,&TRANS_Y);
     //xtrans=TRANS_X+physmod_shift_x;
     //ytrans=TRANS_Y+physmod_shift_y;

     xtrans=physmod_shift_x;
     ytrans=physmod_shift_y;
  }


  /* check if old or new CCD, and eventually apply shifts */
  check_nomsg(bin_x=uves_pfits_get_binx(raw_header));
  check_nomsg(bin_y=uves_pfits_get_biny(raw_header));
  uves_msg("CHECK: xtrans=%f ytrans=%f,ccdrot[0]=%f,ccdrot[1]=%f",
           xtrans,ytrans,ccd_rot[0],ccd_rot[1]);
  
  if(uves_ccd_is_new(raw_header)) {

    if(chip != UVES_CHIP_BLUE) {
      check_nomsg(wavec=(int)uves_pfits_get_gratwlen(raw_header,chip));
      if(flames) {
	ck0_nomsg(flames_get_physmod_offset(plate_no,wavec,chip,
					    &physmod_off_x,
					    &physmod_off_y,
					    &ech_ang_off,
					    &cd_ang_off,
					    &ccd_ang_off));
      } else {
	ck0_nomsg(uves_get_physmod_offset(wavec,chip,binx,biny,
					  &physmod_off_x,
					  &physmod_off_y,
					  &ech_ang_off,
					  &cd_ang_off,
					  &ccd_ang_off));
      }
      ECH_ANG_OFF  +=ech_ang_off;
      CD_ANG_OFF   +=cd_ang_off;
      CCD_ANG_OFF  +=ccd_ang_off;
      xtrans+=physmod_off_x;
      ytrans+=physmod_off_y;

    }
  }
  
  uves_msg("CHECK: xtrans=%f ytrans=%f,ccdrot[0]=%f,ccdrot[1]=%f",
           xtrans,ytrans,ccd_rot[0],ccd_rot[1]);
  uves_msg("CHECK: ECH_ANG_OFF=%f CD_ANG_OFF=%f,CCD_ANG_OFF=%f",
           ECH_ANG_OFF,CD_ANG_OFF,CCD_ANG_OFF);
 



  uves_msg("Shift in x = %6.3f pix, shift in y = %6.3f pix",TRANS_X,TRANS_Y);
  uves_msg("External Shift in x = %6.3f pix, shift in y = %6.3f pix",
	   physmod_shift_x,physmod_shift_y);
  uves_msg("Offset angle of echelle, CD, CCD rotation (deg): "
       "%4.3f, %4.3f, %4.3f",ECH_ANG_OFF,CD_ANG_OFF,CCD_ANG_OFF);
  
  uves_set_ccd_rot(ccd_rot,CCD_ANG_OFF, CCD_ANG_OFF);
  uves_physmod_set_incidence(ECH_ANG_OFF, ECH_ANG_OFF, CD_ANG_OFF, CD_ANG_OFF);
  uves_msg_debug("ccdbin=%f,%f",binx,biny);
  uves_msg_debug("offset x=%f %f %f %f %f %f",
   offset_x[0],offset_x[1],offset_x[2],offset_x[3],offset_x[4],offset_x[5]);

  uves_msg_debug("offset y=%f %f %f %f %f %f",
   offset_y[0],offset_y[1],offset_y[2],offset_y[3],offset_y[4],offset_y[5]);

  uves_msg("Central wavelength: %4.2f nm",wcent);
  uves_msg_debug("chip    =%s", uves_chip_tostring_upper(chip));
  /* uves_msg("uves_arm=%c",ArmId); */      /* b/r */
  uves_msg_debug("uves_arm_ident=%d",uves_arm_ident);       /* b/r */

  /* uves_msg("chipid=%s",CHIP[0]); */ /* upper/lower chip */
  /* uves_msg("cdid=%s",CDID); */       /* 1-4 for CD#1-4 */
  /* uves_cfg_indx = uves_config(ArmId[0],CHIP[0],CDID,wcent,binx,biny); */
  uves_cfg_indx = uves_config_cpl_new(uves_arm_ident,upper,CDID,wcent,binx,biny);
  uves_msg_debug("uves_cfg_indx=%d",uves_cfg_indx);

  uves_physmod_set_offset(offset_x[uves_cfg_indx-1],offset_y[uves_cfg_indx-1],
                          uves_physmod_x_off,uves_physmod_y_off);

/* =========================================================================
 * Computation of the true pixel-scale along echelle dispersion 
 * scale = (K*FocalRatioNasmyth*PixelSize*ApertureCamera*1e-3)/FocalNasmyth
 * K = 206265 : conversion radian to arcsec (1 radian = 206265 arcsec)
 * scale = (206265.0*15.0*0.015*200*1e-3*CCDBIN[0])/120000; 
 * =========================================================================
 */

/* Creation of modtbl */
  
  *mod_tbl= cpl_table_new(3*aRowNumber);
 
  uves_msg("table created: %s ncol=%d, nrow=%d","mod_tbl",10,3*aRowNumber);  
  cpl_table_new_column(*mod_tbl,"WAVE",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*mod_tbl,"ORDER",CPL_TYPE_INT);
  cpl_table_new_column(*mod_tbl,"XMOD",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*mod_tbl,"YMOD",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*mod_tbl,"BIN_SIZE",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*mod_tbl,"LINEWIDPX",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*mod_tbl,"LINEWIDTH",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*mod_tbl,"RESOL",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*mod_tbl,"LINELENPX",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*mod_tbl,"BLAZE",CPL_TYPE_DOUBLE);
  
  uves_ccd_size(&xpix,&ypix); 
  uves_msg_debug("CCDsize: %d %d",xpix,ypix);

  
  for (i=0; i<aRowNumber; i++) {
    
  //for (i=450; i<451; i++) {
    
  
   
  
    /* why we do access line i+1? */
      dblwav=cpl_table_get_double(line_refer, "Wave",i,&status);
      uves_msg_debug(" i: %d Line: %f",i,dblwav); 
     
      dblwav /= 10.0;     /* (10.0: conversion factor from nm to A)*/
      order = uves_physmod_find_order(dblwav);
      uves_msg_debug("order=%d",order); 
      
      order++;  

/* Computes the positions xmod and ymod for the order-1, order, order+1 */
/* and adds a row to the modtbl only if the position are insight the CCD */
 
      for (mm=0; mm<3; mm++){
        /*uves_physmod_xy_model(dblwav, order, &xmod, &ymod);
        blaze = blz; */
        /* The next line fails */
        uves_physmod_lambda_order2beta(dblwav, order, &uves_beta_ech, &uves_beta_cd, &fc);
    
        uves_msg_debug("uves_beta_ech %f, uves_beta_cd %f, fc %f",
                        uves_beta_ech,    uves_beta_cd,    fc);
        
        uves_beta2xy(uves_beta_cd, uves_beta_ech, fc, &xmod, &ymod);
        uves_msg_debug("xmod=%f ymod=%f",xmod,ymod);
    /* Computes the anamorphic magnification and the blaze function  */    
        uves_physmod_photo_beta(dblwav, uves_beta_ech, uves_beta_cd, 
                      &uves_physmod_rech, &uves_physmod_rcd, &blaze);
    
        uves_msg_debug("uves_physmod_rech %f, uves_physmod_rcd %f, blaze %f",
                        uves_physmod_rech,    uves_physmod_rcd,    blaze); 
    
/* Translation */
        
        uves_msg_debug("Before translation: xmod %f, ymod %f",xmod, ymod); 
        
        xmod += xtrans;
        ymod += ytrans;
        uves_msg_debug("xtrans=%f ytrans=%f xmod=%f ymod=%f",
                        xtrans,   ytrans,   xmod,   ymod);
    
        uves_msg_debug("After translation: xmod %f, ymod %f",xmod, ymod); 
    uves_msg_debug("xpix=%d ypix=%d",xpix,ypix);
    
    uves_msg_debug("binx=%f biny=%f",binx,biny); 
    
        if (xmod > 0 && xmod < xpix && ymod > 0 && ymod < ypix) {     

/* Computes the width (in pixel and A) and resolution lines */
      uves_physmod_pixscl(dblwav, order, uves_physmod_rech, uves_physmod_rcd, 
                 binx, biny, fc, slit_width, slit_length, 
         &binsize, &pixscale, &pixscalCD, &linewidpx, 
                 &linelenpx, &linewidth, &resol);
      
          uves_msg_debug("binsize %f, pixscale %f, pixscalCD %f",
          binsize,pixscale,pixscalCD);
          uves_msg_debug("linewidpx %f, linelenpx %f, linewidth %f, resol %f",
          linewidpx,linelenpx,linewidth,resol);
      
/* Compute/regress */
      if (CMP_REG_SW) {
        uves_physmod_xy_regres(xmod,ymod,&xreg,&yreg);
      } else {
        xreg = xmod;
        yreg = ymod;
      }

     
          uves_msg_debug("Fill tab: i=%d wave=%f order=%d xmod=%f ymod=%f",
             i,dblwav,order,xmod,ymod);

          uves_msg_debug("Fill tab: bin_sixe=%f xreg=%f yreg=%f ",
             binsize,xreg,yreg);

          uves_msg_debug("Fill tab: linewidpx=%f linewidth=%f resol=%f linelenpx=%f blaze=%f",
linewidpx,linewidth,resol,linelenpx,blaze);
      
      
      cpl_table_set_double(*mod_tbl, "WAVE"     ,imod, dblwav);
      cpl_table_set_int   (*mod_tbl, "ORDER"    ,imod, order);
      cpl_table_set_double(*mod_tbl, "XMOD"     ,imod, xreg);
      cpl_table_set_double(*mod_tbl, "YMOD"     ,imod, yreg);
      cpl_table_set_double(*mod_tbl, "BIN_SIZE" ,imod, binsize);
      cpl_table_set_double(*mod_tbl, "LINEWIDPX",imod, linewidpx);
      cpl_table_set_double(*mod_tbl, "LINEWIDTH",imod, linewidth);
      cpl_table_set_double(*mod_tbl, "RESOL"    ,imod, resol);
      cpl_table_set_double(*mod_tbl, "LINELENPX",imod, linelenpx);
      cpl_table_set_double(*mod_tbl, "BLAZE"    ,imod, blaze);
          ++imod;

    }
    order--;
      } /* end loop over mm */
    } /* end loop over i */
 
  cpl_table_duplicate_column(*mod_tbl,"IDENT",*mod_tbl,"WAVE");
  cpl_table_multiply_scalar(*mod_tbl,"IDENT",10.);
  cpl_table_duplicate_column(*mod_tbl,"XSTART",*mod_tbl,"XMOD");
  cpl_table_add_scalar(*mod_tbl,"XSTART",-mbox_x/2.);
  cpl_table_duplicate_column(*mod_tbl,"YSTART",*mod_tbl,"YMOD");
  cpl_table_add_scalar(*mod_tbl,"YSTART",-mbox_y/2.);
  cpl_table_duplicate_column(*mod_tbl,"XEND",*mod_tbl,"XMOD");
  cpl_table_add_scalar(*mod_tbl,"XEND",mbox_x/2.);
  cpl_table_duplicate_column(*mod_tbl,"YEND",*mod_tbl,"YMOD");
  cpl_table_add_scalar(*mod_tbl,"YEND",mbox_y/2.);


/* close everything */



    uves_msg("Predicted number of lines: %d",imod);

/* Compute the free spectral range */

    uves_msg("Determine the Free Spectral Range");
 
    dxpix = (double) xpix;
    dypix = (double) ypix;

    m_min = -1;
    m_max = -1;
/*
   uves_physmod_lambda_order_model(&l,&m_max,dxpix/2,1.0);
   uves_physmod_lambda_order_model(&l,&m_min,dxpix/2,dypix);
*/
    uves_physmod_lambda_order_model(&lmax,&m_min,1.0,dypix);
    uves_physmod_lambda_order_model(&lmin,&m_max,dxpix,1.0);

    uves_physmod_lambda_order_model(&lmax,&m_min,dxpix,dypix);
    uves_physmod_lambda_order_model(&lmin,&m_max,1.0,1.0);

   
   uves_msg_debug("m_min= %d,m_max= %d",m_min,m_max);

   
/* Creation of the modfree_sp_rg.tbl table */

    *fsr_tbl=cpl_table_new(m_max-m_min+1);
  
    uves_msg("FSR tbl created: %s. No of columns: %d, No of rows: %d","free_spectral_range",
             20,m_max-m_min+1);
  cpl_table_new_column(*fsr_tbl,"ORDER",CPL_TYPE_INT);
  cpl_table_new_column(*fsr_tbl,"WVCENTCOL",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"YCENTCOL",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"START",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"END",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"WAVECENT",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"YCENT",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"FSRMIN",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"FSRMAX",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"XFSR_MIN",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"XFSR_MAX",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"YFSR_MIN",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"YFSR_MAX",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"PIXSCALE",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"PIXSCALCD",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"BIN_SIZE",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"LINEWIDPX",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"LINEWIDTH",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"RESOL",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*fsr_tbl,"LINELENPX",CPL_TYPE_DOUBLE);


  imod = 0;
  uves_msg_debug("m, waveCentcol,   Start,     End,       yCent,    FSRmin,   FSRmax,  bin (mA)");

  for (m=m_max; m>=m_min; m--) {
     uves_msg_debug("%d ",m);
     uves_physmod_find_FSR(m,&l, &fsr_min, &fsr_max);
     
     uves_physmod_xy_model(l,m,&x,&y);

/* Translation */
      x += xtrans;
      y += ytrans;
     
     uves_physmod_xy_model(fsr_min,m,&xfsr_min,&yfsr_min);
     
/* Translation */
      xfsr_min += xtrans;
      yfsr_min += ytrans;

     uves_physmod_xy_model(fsr_max,m,&xfsr_max,&yfsr_max);
     
/* Translation */
      xfsr_max += xtrans;
      yfsr_max += ytrans;

/* Computes the anamorphic magnification and the blaze function */      
     uves_physmod_lambda_order2beta(l,m, &uves_beta_ech, &uves_beta_cd, &fc);
     uves_physmod_photo_beta(l, uves_beta_ech, uves_beta_cd, 
                             &uves_physmod_rech, &uves_physmod_rcd, &blaze); 
     


/* Computes the width (in pixel and A) and resolution lines */  
     uves_physmod_pixscl(l, m, uves_physmod_rech, uves_physmod_rcd, 
                         binx, biny, fc, slit_width, slit_length, 
                         &dl, &pixscale, &pixscalCD, 
                     &linewidpx, &linelenpx, &linewidth, &resol);

     mmin  = m;
     mmax = m;

  

/* WARNING! The central column doesn't correspond to the central wavelength position */     
     uves_physmod_lambda_order_model(&lcent,&m,dxpix/2,y);
     uves_physmod_xy_model(lcent,m,&xr,&yr);
     
     uves_physmod_lambda_order_model(&lmax,&mmax,dxpix,y);
     uves_physmod_lambda_order_model(&lmin,&mmin,1.0,y); 

/*     if (y>=0. && y <=dypix) {*/

       uves_msg_debug("m=%d, waveCent (nm)=%f, Start=%f, End=%f",m,l,lmin,lmax);
       uves_msg_debug("yCent=%f,FSRmin=%f,FSRmax=%f,bin (mA)=%f",y,fsr_min,fsr_max,dl);


       uves_msg_debug("%d %f %f %f %f %f %f %f",m,lcent,lmin,lmax,yr,fsr_min,fsr_max,dl);


       uves_msg_debug("pixel-scale = %f, anamorphic corrections = %f, %f", 
                      pixscale, uves_physmod_rech, uves_physmod_rcd);


       cpl_table_set_int(*fsr_tbl, "ORDER",imod,m);
       cpl_table_set_double(*fsr_tbl, "WVCENTCOL",imod,lcent);
       cpl_table_set_double(*fsr_tbl, "YCENTCOL",imod,yr);
       cpl_table_set_double(*fsr_tbl, "START",imod,lmin);
       cpl_table_set_double(*fsr_tbl, "END",imod,lmax);
       cpl_table_set_double(*fsr_tbl, "WAVECENT",imod,l);
       cpl_table_set_double(*fsr_tbl, "YCENT",imod,y);
       cpl_table_set_double(*fsr_tbl, "FSRMIN",imod,fsr_min);
       cpl_table_set_double(*fsr_tbl, "FSRMAX",imod,fsr_max);
       cpl_table_set_double(*fsr_tbl, "XFSR_MIN",imod,xfsr_min);
       cpl_table_set_double(*fsr_tbl, "XFSR_MAX",imod,xfsr_max);
       cpl_table_set_double(*fsr_tbl, "YFSR_MIN",imod,yfsr_min);
       cpl_table_set_double(*fsr_tbl, "YFSR_MAX",imod,yfsr_max);
       cpl_table_set_double(*fsr_tbl, "PIXSCALE",imod,pixscale);
       cpl_table_set_double(*fsr_tbl, "PIXSCALCD",imod,pixscalCD);
       cpl_table_set_double(*fsr_tbl, "BIN_SIZE",imod,dl);
       cpl_table_set_double(*fsr_tbl, "LINEWIDPX",imod,linewidpx);
       cpl_table_set_double(*fsr_tbl, "LINEWIDTH",imod,linewidth);
       cpl_table_set_double(*fsr_tbl, "RESOL",imod,resol);
       cpl_table_set_double(*fsr_tbl, "LINELENPX",imod,linelenpx);
         ++imod;

/*     }*/
  }

  /* To remove possible NULL entries */
  cpl_table_erase_invalid_rows(*mod_tbl);

  /* Now we do the polynomial fit */
  uves_msg_debug("End determination fsr range");


   cleanup:

  return 0;
}


static int
flames_get_trans(const int plt_no,
                 enum uves_chip chip, 
                 const double wlen, 
                 double * TX,
                 double * TY)
{


  if(plt_no==1) {
    if( chip==UVES_CHIP_REDL) {
      if(wlen==520) {
        *TX = -15.330;
        *TY = -40.461; 
      } else if (wlen == 580) {
    *TX = -17.972;
        *TY = -39.200;    
      } else if (wlen == 860) {
        *TX= -12.212;
        *TY= -49.370;
      }
    } else {
      if(wlen==520) {
        *TX = -14.237;
        *TY = -40.337;
      } else if (wlen == 580) {
        *TX= -14.738;
        *TY= -38.831;  
      } else if (wlen == 860) {

        *TX = -8.253;
        *TY = -45.385;
      }
    }
  } else if (plt_no==2) {
    if( chip==UVES_CHIP_REDL) {
      if(wlen==520) {
        *TX = +10.136;
        *TY = -41.420;
      } else if (wlen == 580) {
        *TX = +9.000;
        *TY = -38.289;  
      } else if (wlen == 860) {
        *TX = +16.386;
        *TY = -47.519;
      }
    } else {
      if(wlen==520) {
        *TX = +12.244;
        *TY = -41.970;
      } else if (wlen == 580) {
        *TX = +12.023;
        *TY = -38.165;
      } else if (wlen == 860) {
        *TX = +18.241;
        *TY = -43.889; 
      }
    }

  } else {
    uves_msg_warning("plt_no=%d chip=%d Wlen %g is not standard setting setting defaults",plt_no,chip,wlen); 
    *TX = 0;
    *TY = 0;
  }
  return 0;

}


/*---------------------------------------------------------------------------*/
/**
   @brief    Get the physical model offsets in ECHELLE mode
   @param    wavec     central wavelength
   @param    trans_x   out physical model X shift
   @param    trans_y   out physical model Y shift
   @param    ech_ang_off out ech angle 
   @param    cd_ang_off  out cd angle 
   @param    ccd_ang_off out ccd angle 
   @return   0 if everything is ok
*/
/*---------------------------------------------------------------------------*/


static int
uves_get_physmod_offset(const int wavec,
                        enum uves_chip chip,
                        const int binx,
                        const int biny,
			double* trans_x,
			double* trans_y,
			double* ech_ang_off,
			double* cd_ang_off,
			double* ccd_ang_off)
{

   //For the moment rot angle default is assumed always 0,0,0
   *ech_ang_off=0;
   *cd_ang_off=0;
   *ccd_ang_off=0;

   uves_msg("wavec=%d,chip=%d binx=%d biny=%d",wavec,chip,binx,biny);

   if(binx==1 && biny == 1) {



      if(chip==UVES_CHIP_REDL) {
         switch(wavec){
            case 520:
               *trans_x=3.0703;
               *trans_y=6.8252;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 520 REDL 1x1");
               break;

            case 564:
               *trans_x=2.2151;
               *trans_y=0.1092;
               *ccd_ang_off=-.319;
               uves_msg("case ech 564 REDL 1x1");
               break;

            case 580:
               *trans_x=1.7116;
               *trans_y=-0.4874;
               *ccd_ang_off=-0.321;
               uves_msg("case ech 580 REDL 1x1");
               break;

            case 600:
               *trans_x=0.1581;
               *trans_y=5.3308;
               *ccd_ang_off=-.344;
               uves_msg("case ech 600 REDL 1x1");
               break;

            case 620:
               *trans_x=10.3267;
               *trans_y=2.7942;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 620 REDL 1x1");
               break;


            case 640:
               *trans_x=0.3785;
               *trans_y=-3.8460;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 640 REDL 1x1");
               break;


            case 660:
               *trans_x=9.7465;
               *trans_y=0.7125;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 660 REDL 1x1");
               break;

            case 680:
               *trans_x=9.3649;
               *trans_y=-2.4116;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 680 REDL 1x1");
               break;

            case 700:
               *trans_x=8.9850;
               *trans_y=-5.6122;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 700 REDL 1x1");
               break;

            case 720:
               *trans_x=8.7393;
               *trans_y=-7.0060;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 720 REDL 1x1");
               break;

            case 740:
               *trans_x=8.2659;
               *trans_y=-4.3401;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 740 REDL 1x1");
               break;


            case 760:
               *trans_x=8.6384;
               *trans_y=1.9904;
               *trans_x=8.1006;
               *trans_y=-1.0859;
               *ccd_ang_off=-.335;
               uves_msg("case ech 760 REDL 1x1");
               break;


            case 780:
               *trans_x=7.8761;
               *trans_y=0.5324;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 780 REDL 1x1");
               break;


            case 800:
               *trans_x=7.5052;
               *trans_y=0.7860;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 800 REDL 1x1");
               break;

            case 820:
               *trans_x=7.2100;
               *trans_y=-1.8423;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 820 REDL 1x1");
               break;

            case 840:
               *trans_x=6.8437;
               *trans_y=-4.9477;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 820 REDL 1x1");
               break;

            case 860:
               *trans_x=6.8385;
               *trans_y=-3.0247;
               *ccd_ang_off=-.350;
               uves_msg("case ech 860 REDL 1x1");
               break;

            case 880:
               *trans_x=6.4751;
               *trans_y=-8.2393;
               *ccd_ang_off=-.350;
               uves_msg("case ech 880 REDL 1x1");
               break;

            case 900:
               *trans_x=5.9872;
               *trans_y=-7.5682;
               *ccd_ang_off=-.350;
               uves_msg("case ech 900 REDL 1x1");
               break;

            default:
               *ccd_ang_off=-0.338;
               break;

         }
      } else {

         switch(wavec){
            case 520:
               *trans_x=3.5471;
               *trans_y=14.6995;
               *ccd_ang_off=-.131;
               uves_msg("case ech 520 REDU 1x1");
               break;

            case 564:
               *trans_x=3.8134;
               *trans_y=8.0660;
               *ccd_ang_off=-0.161;
               uves_msg("case ech 564 REDU 1x1");
               break;

            case 580:
               *trans_x=2.2649;
               *trans_y=7.5163;
               *ccd_ang_off=-.106;
               uves_msg("case ech 580 REDU 1x1");
               break;

            case 600:
               *trans_x=2.3765;
               *trans_y=12.9146;
               *ccd_ang_off=-.112;
               uves_msg("case ech 600 REDU 1x1");
               break;

            case 620:
               *trans_x=10.7635;
               *trans_y=13.2254;
               *ccd_ang_off=-.131;
               uves_msg("case ech 620 REDL 1x1");
               break;

            case 640:
               *trans_x=2.6177;
               *trans_y=10.0995;
               *ccd_ang_off=-.131;
               uves_msg("case ech 640 REDL 1x1");
               break;

            case 660:
               *trans_x=10.4909;
               *trans_y=11.3694;
               *ccd_ang_off=-.131;
               uves_msg("case ech 660 REDL 1x1");
               break;

            case 680:
               *trans_x=10.4899;
               *trans_y=8.4049;
               *ccd_ang_off=-.131;
               uves_msg("case ech 680 REDL 1x1");
               break;

            case 700:
               *trans_x=10.2939;
               *trans_y=5.3955;
               *ccd_ang_off=-.131;
               uves_msg("case ech 700 REDL 1x1");
               break;


            case 720:
               *trans_x=9.9791;
               *trans_y=4.0860;
               *ccd_ang_off=-.131;
               uves_msg("case ech 720 REDL 1x1");
               break;

            case 740:
               *trans_x=9.8533;
               *trans_y=6.9165;
               *ccd_ang_off=-.131;
               uves_msg("case ech 740 REDL 1x1");
               break;


            case 760:
               *trans_x=11.0378;
               *trans_y=12.9732;
               *trans_x=10.5357;
               *trans_y=10.1393;
               *ccd_ang_off=-.160;
               uves_msg("case ech 760 REDU 1x1");
               break;

            case 780:
               *trans_x=9.5521;
               *trans_y=11.7901;
               *ccd_ang_off=-.131;
               uves_msg("case ech 780 REDL 1x1");
               break;

            case 800:
               *trans_x=9.3306;
               *trans_y=17.4402;
               *ccd_ang_off=-.131;
               uves_msg("case ech 800 REDL 1x1");
               break;

            case 820:
               *trans_x=9.1435;
               *trans_y=14.8772;
               *ccd_ang_off=-.131;
               uves_msg("case ech 820 REDL 1x1");
               break;

            case 840:
               *trans_x=8.8927;
               *trans_y=11.8143;
               *ccd_ang_off=-.131;
               uves_msg("case ech 840 REDL 1x1");
               break;



            case 860:
               *trans_x=10.9306;
               *trans_y=7.8155;
               *ccd_ang_off=-.176;
               uves_msg("case ech 860 REDU 1x1");
               break;

            case 880:
               *trans_x=9.0960;
               *trans_y=3.2741;
               *ccd_ang_off=-.131;
               uves_msg("case ech 880 REDL 1x1");
               break;


            case 900:
               *trans_x=8.8903;
               *trans_y=8.9738;
               *ccd_ang_off=-.131;
               uves_msg("case ech 900 REDL 1x1");
               break;


            default:
               *ccd_ang_off=-.131;
               break;

         }
  
      }
   } else if(binx==2 && biny == 2) {



      if(chip==UVES_CHIP_REDL) {
         switch(wavec){


            case 500:
               *trans_x=1.6127;
               *trans_y=0.3725;
               *ccd_ang_off=-.335;
               uves_msg("case ech 500 REDL 2x2");
               break;


            case 520:
               *trans_x=1.0304;
               *trans_y=4.4927;
               *ccd_ang_off=-0.338;
               uves_msg("case ech 520 REDL 2x2");
               break;


            case 540:
               *trans_x=0.9221;
               *trans_y=2.6768;
               *ccd_ang_off=-.335;
               uves_msg("case ech 540 REDL 2x2");
               break;

            case 560:
               *trans_x=0.7170;
               *trans_y=-0.0175;
               *ccd_ang_off=-.335;
               uves_msg("case ech 560 REDL 2x2");
               break;


            case 564:
               *trans_x=0.5659;
               *trans_y=1.2151;
               *ccd_ang_off=-.319;
               uves_msg("case ech 564 REDL 2x2");
               break;

            case 580:
               *trans_x=0.4406;
               *trans_y=0.7901;
               *ccd_ang_off=-0.321;
               uves_msg("case ech 580 REDL 2x2");
               break;

            case 600:
               *trans_x=0.2550;
               *trans_y=3.7476;
               *ccd_ang_off=-0.315;
               uves_msg("case ech 600 REDL 2x2");
               break;

            case 620:
               *trans_x=0.1970;
               *trans_y=3.0182;
               *ccd_ang_off=-.335;
               uves_msg("case ech 620 REDL 2x2");
               break;

            case 640:
               *trans_x=0.0203;
               *trans_y=-0.0638;
               *ccd_ang_off=-.335;
               uves_msg("case ech 640 REDL 2x2");
               break;

            case 660:
               *trans_x=5.2122;
               *trans_y=1.5986;
               *ccd_ang_off=-.335;
               uves_msg("case ech 660 REDL 2x2");
               break;

            case 680:
               *trans_x=5.0355;
               *trans_y=-0.1277;
               *ccd_ang_off=-.335;
               uves_msg("case ech 680 REDL 2x2");
               break;

            case 700:
               *trans_x=4.8466;
               *trans_y=-1.8237;
               *ccd_ang_off=-.335;
               uves_msg("case ech 700 REDL 2x2");
               break;

            case 720:
               *trans_x=4.7320;
               *trans_y=-2.4860;
               *ccd_ang_off=-.335;
               uves_msg("case ech 720 REDL 2x2");
               break;

            case 740:
               *trans_x=4.5968;
               *trans_y=-1.1956;
               *ccd_ang_off=-.335;
               uves_msg("case ech 740 REDL 2x2");
               break;

            case 760:
               *trans_x=3.7298;
               *trans_y=2.0585;
               *trans_x=4.5392;
               *trans_y=0.4414;
               *ccd_ang_off=-.335;
               uves_msg("case ech 760 REDL 2x2");
               break;

            case 780:
               *trans_x=4.4156;
               *trans_y=1.2900;
               *ccd_ang_off=-.335;
               uves_msg("case ech 780 REDL 2x2");
               break;

            case 800:
               *trans_x=4.2877;
               *trans_y=2.2643;
               *ccd_ang_off=-.335;
               uves_msg("case ech 800 REDL 2x2");
               break;


            case 820:
               *trans_x=4.1695;
               *trans_y=1.0259;
               *ccd_ang_off=-.335;
               uves_msg("case ech 820 REDL 2x2");
               break;

            case 840:
               *trans_x=4.1004;
               *trans_y=-0.6081;
               *ccd_ang_off=-.335;
               uves_msg("case ech 840 REDL 2x2");
               break;

            case 860:
               *trans_x=3.1362;
               *trans_y=-.6278;
               *ccd_ang_off=-.350;
               uves_msg("case ech 860 REDL 2x2");
               break;

            case 880:
               *trans_x=3.8903;
               *trans_y=-3.1012;
               *ccd_ang_off=-.335;
               uves_msg("case ech 880 REDL 2x2");
               break;

            case 900:
               *trans_x=3.7873;
               *trans_y=-2.0264;
               *ccd_ang_off=-.335;
               uves_msg("case ech 900 REDL 2x2");
               break;



            default:
               *ccd_ang_off=-.335;
               break;
         }
      } else {

         switch(wavec){


            case 500:
               *trans_x=1.6644;
               *trans_y=2.1540;
               *ccd_ang_off=-.131;
               uves_msg("case ech 500 REDU 2x2");
               break;



            case 520:
               *trans_x=1.3965;
               *trans_y=6.2855;
               *ccd_ang_off=-.131;
               uves_msg("case ech 520 REDU 2x2");
               break;

            case 540:
               *trans_x=1.3951;
               *trans_y=4.7774;
               *ccd_ang_off=-.131;
               uves_msg("case ech 540 REDU 2x2");
               break;

            case 560:
               *trans_x=1.3307;
               *trans_y=2.0433;
               *ccd_ang_off=-.131;
               uves_msg("case ech 560 REDU 2x2");
               break;


            case 564:
               *trans_x=1.3329;
               *trans_y=2.9888;
               *ccd_ang_off=-0.161;
               uves_msg("case ech 564 REDU 2x2");
               break;

            case 580:
               *trans_x=0.9856;
               *trans_y=2.5873;
               *ccd_ang_off=-.106;
               uves_msg("case ech 580 REDU 2x2");
               break;

            case 600:
               *trans_x=0.9223;
               *trans_y=5.5208;
               *ccd_ang_off=-.112;
               uves_msg("case ech 600 REDU 2x2");
               break;


            case 620:
               *trans_x=1.1483;
               *trans_y=5.2252;
               *ccd_ang_off=-.131;
               uves_msg("case ech 620 REDU 2x2");
               break;



            case 640:
               *trans_x=1.0534;
               *trans_y=2.1526;
               *ccd_ang_off=-.131;
               uves_msg("case ech 640 REDU 2x2");
               break;

            case 660:
               *trans_x=5.8447;
               *trans_y=4.8530;
               *ccd_ang_off=-.131;
               uves_msg("case ech 660 REDU 2x2");
               break;


            case 680:
               *trans_x=5.7227;
               *trans_y=3.2191;
               *ccd_ang_off=-.131;
               uves_msg("case ech 680 REDU 2x2");
               break;

            case 700:
               *trans_x=5.6851;
               *trans_y=1.6912;
               *ccd_ang_off=-.131;
               uves_msg("case ech 700 REDU 2x2");
               break;

            case 720:
               *trans_x=5.5375;
               *trans_y=1.0217;
               *ccd_ang_off=-.131;
               uves_msg("case ech 720 REDU 2x2");
               break;

            case 740:
               *trans_x=5.4927;
               *trans_y=2.3500;
               *ccd_ang_off=-.131;
               uves_msg("case ech 740 REDU 2x2");
               break;



            case 760:
               *trans_x=4.8082;
               *trans_y=5.3052;
               *trans_x=5.6659;
               *trans_y=3.9673;
               *ccd_ang_off=-.160;
               uves_msg("case ech 760 REDU 2x2");
               break;


            case 780:
               *trans_x=5.3406;
               *trans_y=4.7919;
               *ccd_ang_off=-.131;
               uves_msg("case ech 780 REDU 2x2");
               break;


            case 800:
               *trans_x=5.2926;
               *trans_y=5.8133;
               *ccd_ang_off=-.131;
               uves_msg("case ech 800 REDU 2x2");
               break;

            case 820:
               *trans_x=5.1947;
               *trans_y=4.5974;
               *ccd_ang_off=-.131;
               uves_msg("case ech 820 REDU 2x2");
               break;


            case 840:
               *trans_x=5.1033;
               *trans_y=2.9638;
               *ccd_ang_off=-.131;
               uves_msg("case ech 840 REDU 2x2");
               break;

            case 860:
               *trans_x=4.5923;
               *trans_y=2.6809;
               *ccd_ang_off=-.176;
               uves_msg("case ech 860 REDU 2x2");
               break;


            case 880:
               *trans_x=4.9641;
               *trans_y=0.3260;
               *ccd_ang_off=-.131;
               uves_msg("case ech 880 REDU 2x2");
               break;

            case 900:
               *trans_x=4.9610;
               *trans_y=1.4586;
               *ccd_ang_off=-.131;
               uves_msg("case ech 900 REDU 2x2");
               break;


            default:
               *ccd_ang_off=-.131;
               break;

         }
  
      }

   } else if(binx==2 && biny == 1) {



      if(chip==UVES_CHIP_REDL) {

         switch(wavec){
            case 520:
               *trans_x=-0.5469;
               *trans_y=3.1225;
               *ccd_ang_off=-.400;
               uves_msg("case ech 520 REDL 2x1");
               break;

            case 564:
               *trans_x=-1.3918;
               *trans_y=-2.9967;

               *ccd_ang_off=-.400;
               uves_msg("case ech 564 REDL 2x1");
               break;

            case 580:
               *trans_x=-1.4333;
               *trans_y=-4.3223;

               *ccd_ang_off=-.400;
               uves_msg("case ech 580 REDL 2x1");
               break;

            case 600:
               *trans_x=-1.6725;
               *trans_y=1.5057;


               *ccd_ang_off=-.400;
               uves_msg("case ech 600 REDL 2x1");
               break;


            case 760:
               *trans_x=1.5073;
               *trans_y=-1.6050;
               *ccd_ang_off=-.400;
               uves_msg("case ech 760 REDL 2x1");
               break;

            case 860:
               *trans_x=0.9818;
               *trans_y=-6.9373;
               *ccd_ang_off=-.400;
               uves_msg("case ech 860 REDL 2x1");
               break;

            default:
               *ccd_ang_off=-.400;
               break;

         }
      } else {
         switch(wavec){
            case 520:
               *trans_x=-1.0242;
               *trans_y=11.4442;
               *ccd_ang_off=-.100;
               uves_msg("case ech 520 REDU 2x1");
               break;

            case 564:
               *trans_x=-1.2162;
               *trans_y=5.7253;
               *ccd_ang_off=-.100;
               uves_msg("case ech 564 REDU 2x1");
               break;

            case 580:
               *trans_x=-1.0591;
               *trans_y=4.5918;
               *ccd_ang_off=-.100;
               uves_msg("case ech 580 REDU 2x1");
               break;

            case 600:
               *trans_x=-1.1089;
               *trans_y=10.0907;
               *ccd_ang_off=-.100;
               uves_msg("case ech 600 REDU 2x1");
               break;

            case 760:
               *trans_x=1.9719;
               *trans_y=9.6496;

               *ccd_ang_off=-.100;
               uves_msg("case ech 760 REDU 2x1");
               break;

            case 860:
               *trans_x=1.9402;
               *trans_y=4.2234;
               *ccd_ang_off=-.100;
               uves_msg("case ech 860 REDU 2x1");
               break;

            default:
               *ccd_ang_off=-.100;
               break;
         }
      }
   } else if(binx==3 && biny == 2) {



      if(chip==UVES_CHIP_REDL) {
         switch(wavec){
            case 520:
               *trans_x=2.0359;
               *trans_y=2.9332;
               *ccd_ang_off=-.190;
               uves_msg("case ech 520 REDL 3x2");
               break;

            case 564:
               *trans_x=1.5143;
               *trans_y=-0.2408;
               *ccd_ang_off=-.190;
               uves_msg("case ech 564 REDL 3x2");
               break;

            case 580:
               *trans_x=1.2014;
               *trans_y=-0.9518;
               *ccd_ang_off=-.190;
               uves_msg("case ech 580 REDL 3x2");
               break;

            case 600:
               *trans_x=1.2753;
               *trans_y=1.8916;
               *ccd_ang_off=-.190;
               uves_msg("case ech 600 REDL 3x2");
               break;


            case 760:
               *trans_x=3.5390;
               *trans_y=0.3579;
               *ccd_ang_off=-.190;
               uves_msg("case ech 760 REDL 3x2");
               break;

            case 860:
               *trans_x=3.0845;
               *trans_y=-2.2232;
               *ccd_ang_off=-.190;
               uves_msg("case ech 860 REDL 3x2");
               break;

            default:
               *ccd_ang_off=-.190;
               break;

         }
      } else {
         switch(wavec){

            case 520:
               *trans_x=0.4483;
               *trans_y=4.5161;
               *ccd_ang_off=-.100;
               uves_msg("case ech 520 REDU 3x2");
               break;

            case 564:
               *trans_x=0.0711;
               *trans_y=1.5479;
               *ccd_ang_off=-.100;
               uves_msg("case ech 564 REDU 3x2");
               break;

            case 580:
               *trans_x=0.2785;
               *trans_y=0.9470;
               *ccd_ang_off=-.100;
               uves_msg("case ech 580 REDU 3x2");
               break;

            case 600:
               *trans_x=0.1448;
               *trans_y=3.7903;
               *ccd_ang_off=-.100;
               uves_msg("case ech 600 REDU 3x2");
               break;

            case 760:
               *trans_x=2.1704;
               *trans_y=3.6996;
               *ccd_ang_off=-.100;
               uves_msg("case ech 760 REDU 3x2");
               break;

            case 860:
               *trans_x=1.9817;
               *trans_y=1.0266;
               *ccd_ang_off=-.100;
               uves_msg("case ech 860 REDU 3x2");
               break;

            default:
               *ccd_ang_off=-.100;
               break;

         }
  
      }
 
   } else {

      uves_msg_warning("New CCD, frame is unbinned."); 
      uves_msg_warning("You may have to compute proper CCD rotation angle and X, Y offsets");


   }

   uves_msg("NEW CCD Physical Model shifts trans=%f,%f rot=%f,%f,%f",
            *trans_x,*trans_y,*ech_ang_off,*cd_ang_off,*ccd_ang_off);
   return 0;
}


/*---------------------------------------------------------------------------*/
/**
   @brief    Get the physical model shifts in FIBER mode
   @param    plate_no  plate number
   @param    wavec     central wavelength
   @param    trans_x   out physical model X shift
   @param    trans_y   out physical model Y shift
   @param    ech_ang_off  out ech angle 
   @param    cd_ang_off   out cd angle
   @param    ccd_ang_off  out ccd angle
   @return   0 if everything is ok
*/
/*---------------------------------------------------------------------------*/


static int
flames_get_physmod_offset(const int plate_no,
			  const int wavec,
			  enum uves_chip chip,
			  double* trans_x,
			  double* trans_y,
			  double* ech_ang_off,
			  double* cd_ang_off,
			  double* ccd_ang_off)
{

   //For the moment rot angle default is assumed always 0,0,0
   *ech_ang_off=0;
   *cd_ang_off=0;
   *ccd_ang_off=0;
   uves_msg_warning("New CCD, frame is unbinned, FIBER mode");;
   uves_msg_warning("You may have to compute proper CCD rotation angle and X, Y offsets");

   uves_msg("plate_no=%d,wavec=%d,chip=%d",plate_no,wavec,chip);
   switch(plate_no){

      case 1:

         if(chip==UVES_CHIP_REDL) {
            switch(wavec){

               case 520:
                  *trans_x=4.5211;
                  *trans_y=0.2826;
                  *ccd_ang_off=-0.338;
                  uves_msg("case FIBRE 520 REDL plt1");
                  break;

               case 580:
                  *trans_x=6.0388;
                  *trans_y=-7.4226;
                  *ccd_ang_off=-0.321;
                  uves_msg("case FIBRE 580 REDL plt1");
                  break;

               case 860:
                  *trans_x=4.1211;
                  *trans_y=-2.3926;
                  *ccd_ang_off=-.350;
                  uves_msg("case FIBRE 860 REDL plt1");
                  break;

            }
         } else {

            switch(wavec){

               case 520:
                  *trans_x=2.1001;
                  *trans_y=8.0998;
                  *ccd_ang_off=-0.131;
                  uves_msg("case FIBRE 520 REDU plt1");
                  break;

               case 580:
                  *trans_x=2.1388;
                  *trans_y=0.1226;
                  *ccd_ang_off=-.106;
                  uves_msg("case FIBRE 580 REDU plt1");
                  break;

               case 860:
                  *trans_x=3.7411;
                  *trans_y=5.1006;
                  *ccd_ang_off=-.176;
                  uves_msg("case FIBRE 860 REDU plt1");
                  break;

            }  
         }
         break;

      case 2:

         if(chip==UVES_CHIP_REDL) {
            switch(wavec){

               case 520:
                  *trans_x=4.0311;
                  *trans_y=3.2253;
                  *ccd_ang_off=-0.338;
                  uves_msg("case FIBER 520 REDL plt2");
                  break;

               case 580:
                  *trans_x=4.1158;
                  *trans_y=-7.2539;
                  *ccd_ang_off=-.321;
                  uves_msg("case FIBER 580 REDL plt2");
                  break;

               case 860:
                  *trans_x=1.5000;
                  *trans_y=-2.4624;
                  *ccd_ang_off=-.343;
                  uves_msg("case FIBER 860 REDL plt2");
                  break;

            }
         } else {

            switch(wavec){

               case 520:
                  *trans_x=2.3911;
                  *trans_y=11.9726;
                  *ccd_ang_off=-0.131;
                  uves_msg("case FIBER 520 REDU plt2");
                  break;

               case 580:
                  *trans_x=1.5658;
                  *trans_y=0.6239;
                  *ccd_ang_off=-.106;
                  uves_msg("case FIBER 580 REDU plt2");
                  break;

               case 860:
                  *trans_x=3.1174;
                  *trans_y=4.7984;
                  *ccd_ang_off=-.176;
                  uves_msg("case FIBER 860 REDU plt2");
                  break;

            }  
         }
         break;

  default:
    *trans_x=0;
    *trans_y=0;
  
  }


  uves_msg("NEW CCD Physical Model shifts trans=%f,%f rot=%f,%f,%f",
	   *trans_x,*trans_y,*ech_ang_off,*cd_ang_off,*ccd_ang_off);
  return 0;
}






/**@}*/

