/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2002,2003 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 13:54:03 $
 * $Revision: 1.12 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.11  2011/03/23 10:08:10  amodigli
 * changed uves_wavecal_search() API to allow QC log
 *
 * Revision 1.10  2010/09/24 09:32:09  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.8  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.7  2007/05/22 11:46:15  jmlarsen
 * Removed 1d wavecal mode which was not supported
 *
 * Revision 1.6  2007/05/07 14:25:59  jmlarsen
 * Changed formatting
 *
 * Revision 1.5  2007/03/15 12:36:46  jmlarsen
 * Added experimental ppm code
 *
 * Revision 1.4  2007/03/05 10:24:40  jmlarsen
 * Parametrized kappa parameter
 *
 * Revision 1.3  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.2  2006/02/15 13:19:15  jmlarsen
 * Reduced source code max. line length
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.6  2005/12/20 08:11:44  jmlarsen
 * Added CVS  entry
 *
 */
#ifndef UVES_WAVECAL_IDENTIFY_H
#define UVES_WAVECAL_IDENTIFY_H
#include <uves_cpl_size.h>

#include <uves_utils_polynomial.h>
#include <cpl.h>
#include <stdbool.h>

polynomial *
uves_wavecal_identify(cpl_table *linetable, 
              const cpl_table *line_refer, 
              const polynomial *guess_dispersion, 
              int DEGREE, double TOLERANCE, 
              double ALPHA, double MAXERROR,
                      double kappa,
                      const int trace, const int window,cpl_table* qclog);

int
uves_wavecal_identify_lines_ppm(cpl_table *linetable, const cpl_table *line_refer);

#endif
