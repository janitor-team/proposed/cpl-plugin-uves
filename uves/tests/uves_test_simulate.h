/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jmlarsen $
 * $Date: 2007-06-26 14:51:01 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.4  2007/06/20 15:55:37  jmlarsen
 * Parametrized assumption about MIDAS format
 *
 * Revision 1.3  2007/06/20 15:49:31  jmlarsen
 * Support creating first/last abs. order number
 *
 * Revision 1.2  2007/05/22 11:28:14  jmlarsen
 * Removed unused variables
 *
 * Revision 1.1  2007/03/15 12:47:34  jmlarsen
 * Imported sources
 *
 */
#ifndef UVES_TEST_SIMULATE_H
#define UVES_TEST_SIMULATE_H

#include <uves_utils_polynomial.h>
#include <cpl.h>
#include <stdbool.h>

void
create_order_table(cpl_table **ordertable, polynomial **order_locations, cpl_table **tracetable,
                   int minorder, int maxorder, int nx);


void
create_line_table(cpl_table **linetable, polynomial **dispersion, polynomial **abs_orders,
                  int *firstabs, int *lastabs,
                  int minorder, int maxorder, int nx);

#endif
