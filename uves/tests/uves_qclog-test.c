/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2009-06-05 05:49:02 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2008/09/29 07:01:54  amodigli
 * add #include <string.h>
 *
 * Revision 1.2  2007/05/23 06:43:23  jmlarsen
 * Removed unused variables
 *
 * Revision 1.1  2007/03/15 12:27:18  jmlarsen
 * Moved unit tests to ./uves/tests and ./flames/tests
 *
 * Revision 1.3  2007/02/27 14:04:14  jmlarsen
 * Move unit test infrastructure to IRPLIB
 *
 * Revision 1.2  2007/01/29 12:17:54  jmlarsen
 * Support setting verbosity from command line
 *
 * Revision 1.1  2006/11/28 08:26:35  jmlarsen
 * Added QC log unit test
 *
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <uves_qclog.h>
#include <uves_utils_wrappers.h>
#include <uves_error.h>
#include <cpl_test.h>
#include <string.h>
#include <cpl.h>
/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_qclog_test  UVES qclog unit tests
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*----------------------------------------------------------------------------*/
/**
   @brief test QC name
 */
/*----------------------------------------------------------------------------*/
static void
test_qc_name(void)
{
    const char *qc_name = NULL;

    const char *name = "SOMETHING";
    int trace_number = 2;
    bool flames = false;

    /* UVES */
    qc_name = uves_qclog_get_qc_name(name, flames, trace_number);
    
    assure( strcmp(qc_name, "QC SOMETHING") == 0,
	    CPL_ERROR_ILLEGAL_OUTPUT, "%s != %s",
	    qc_name, "QC SOMETHING");

    /* FLAMES */
    flames = true;

    uves_free_string_const(&qc_name);
    qc_name = uves_qclog_get_qc_name(name, flames, trace_number);
    
    assure( strcmp(qc_name, "QC FIB3 SOMETHING") == 0,
	    CPL_ERROR_ILLEGAL_OUTPUT, "%s != %s",
	    qc_name, "QC FIB3 SOMETHING");

  cleanup:
    uves_free_string_const(&qc_name);
    return;
}

   
/*----------------------------------------------------------------------------*/
/**
  @brief   Test of uves_qclog
**/
/*----------------------------------------------------------------------------*/

int main(void)
{
    /* Initialize CPL + UVES messaging */
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    check( test_qc_name(),
	   "Test of QC names failed");

  cleanup:
    return cpl_test_end(0);
}


/**@}*/
