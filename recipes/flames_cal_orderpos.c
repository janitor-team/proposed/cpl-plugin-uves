/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston MA 02110-1301 USA          *
 */
 
/*
 * $Author: amodigli $
 * $Date: 2007-06-06 08:17:33 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup flames_cal_orderpos
 */
/*----------------------------------------------------------------------------*/

#include <flames.h>
#include <flames_utils.h>
#include <uves_utils.h>

int cpl_plugin_get_info(cpl_pluginlist *list)
{
    if (UVES_BINARY_VERSION != uves_get_version_binary())
    {
        cpl_msg_error(__func__, 
              "I am flames_cal_orderpos version %d, but I am linking "
              "against UVES library version %d. "
              "This will not work. "
              "Please remove all previous installations "
              "of the " PACKAGE_NAME " and try again.",
              UVES_BINARY_VERSION, uves_get_version_binary());
        return 1;
    }

    if (UVES_BINARY_VERSION != flames_get_version_binary())
    {
        cpl_msg_error(__func__, 
              "I am flames_cal_orderpos version %d, but I am linking "
              "against FLAMES library version %d. "
              "This will not work. "
              "Please remove all previous installations "
              "of the " PACKAGE_NAME " and try again.",
              UVES_BINARY_VERSION, flames_get_version_binary());
        return 1;
    }

    return flames_cal_orderpos_get_info(list);
}
