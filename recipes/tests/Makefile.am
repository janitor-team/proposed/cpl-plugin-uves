## Process this file with automake to produce Makefile.in

##   This file is part of the UVES Pipeline Library
##   Copyright (C) 2002,2003 European Southern Observatory

##   This library is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation; either version 2 of the License, or
##   (at your option) any later version.

##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.

##   You should have received a copy of the GNU General Public License
##   along with this program; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA

AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~


if MAINTAINER_MODE

MAINTAINERCLEANFILES = Makefile.in

endif
LIBUVES = $(top_builddir)/uves/libuves.la 
LIBFLAMES = $(top_builddir)/flames/libflames.la 
LIBIRPLIB = $(top_builddir)/irplib/libirplib.la

OBJDIR = $(top_builddir)/recipes/.libs

AM_CPPFLAGS = $(all_includes)
LDADD = $(LIBFLAMES)  $(LIBUVES) $(LIBIRPLIB)

check_PROGRAMS = uves_cal_mbias-test \
		uves_cal_mdark-test \
		uves_cal_mflat-test \
		uves_cal_orderpos-test \
		uves_cal_predict-test \
		uves_cal_wavecal-test \
		uves_cal_response-test \
		uves_obs_scired-test \
		uves_obs_redchain-test \
		uves_cal_tflat-test \
		uves_cal_cd_align-test \
		uves_cal_mkmaster-test \
		flames_cal_orderpos-test \
		flames_cal_predict-test \
		flames_cal_mkmaster-test \
		flames_cal_wavecal-test \
		flames_obs_scired-test \
		flames_cal_prep_sff_ofpos-test \
		flames_obs_redchain-test 
#		uves_cal_lingain-test\
#		uves_cal_ronbias-test \
#		flames_utl_unpack-test     

#recipe_main-test
#recipe_main_test_SOURCES = recipe_main.c
#recipe_main_test_LDADD   = $(LDADD) $(LIBALL)
#recipe_main_test_CPPFLAGS = -DUVES_TEST_NAME=recipe_main $(AM_CPPFLAGS)


#uves_cal_lingain_test_SOURCES = recipe_main.c
#uves_cal_lingain_test_LDADD   = $(OBJDIR)/uves_cal_lingain.o  $(LDADD) $(LIBALL)
#uves_cal_lingain_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_lingain $(AM_CPPFLAGS)
#uves_cal_ronbias_test_SOURCES = recipe_main.c
#uves_cal_ronbias_test_LDADD   = $(OBJDIR)/uves_cal_ronbias.o  $(LDADD) $(LIBALL)
#uves_cal_ronbias_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_ronbias $(AM_CPPFLAGS)
#flames_utl_unpack_test_SOURCES = recipe_main.c
#flames_utl_unpack_test_LDADD   = $(OBJDIR)/flames_utl_unpack.o  $(LDADD) $(LIBALL)
#flames_utl_unpack_test_CPPFLAGS = -DUVES_TEST_NAME=flames_utl_unpack $(AM_CPPFLAGS)

uves_cal_mbias_test_SOURCES = recipe_main.c
uves_cal_mbias_test_LDADD   = $(OBJDIR)/uves_mbias.o  $(LDADD) $(LIBALL)
uves_cal_mbias_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_mbias $(AM_CPPFLAGS)

uves_cal_mdark_test_SOURCES = recipe_main.c
uves_cal_mdark_test_LDADD   = $(OBJDIR)/uves_mdark.o  $(LDADD) $(LIBALL)
uves_cal_mdark_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_mdark $(AM_CPPFLAGS)

uves_cal_mflat_test_SOURCES = recipe_main.c
uves_cal_mflat_test_LDADD   = $(OBJDIR)/uves_mflat.o  $(LDADD) $(LIBALL)
uves_cal_mflat_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_mflat $(AM_CPPFLAGS)

uves_cal_orderpos_test_SOURCES = recipe_main.c
uves_cal_orderpos_test_LDADD   = $(OBJDIR)/uves_orderpos.o  $(LDADD) $(LIBALL)
uves_cal_orderpos_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_orderpos $(AM_CPPFLAGS)

uves_cal_predict_test_SOURCES = recipe_main.c
uves_cal_predict_test_LDADD   = $(OBJDIR)/uves_physmod.o  $(LDADD) $(LIBALL)
uves_cal_predict_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_predict $(AM_CPPFLAGS)

uves_cal_wavecal_test_SOURCES = recipe_main.c
uves_cal_wavecal_test_LDADD   = $(OBJDIR)/uves_wavecal.o  $(LDADD) $(LIBALL)
uves_cal_wavecal_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_wavecal $(AM_CPPFLAGS)

uves_cal_response_test_SOURCES = recipe_main.c
uves_cal_response_test_LDADD   = $(OBJDIR)/uves_response.o  $(LDADD) $(LIBALL)
uves_cal_response_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_response $(AM_CPPFLAGS)

uves_obs_scired_test_SOURCES = recipe_main.c
uves_obs_scired_test_LDADD   = $(OBJDIR)/uves_scired.o  $(LDADD) $(LIBALL)
uves_obs_scired_test_CPPFLAGS = -DUVES_TEST_NAME=uves_obs_scired $(AM_CPPFLAGS)

uves_obs_redchain_test_SOURCES = recipe_main.c
uves_obs_redchain_test_LDADD   = $(OBJDIR)/uves_redchain.o  $(LDADD) $(LIBALL)
uves_obs_redchain_test_CPPFLAGS = -DUVES_TEST_NAME=uves_obs_redchain $(AM_CPPFLAGS)

uves_cal_tflat_test_SOURCES = recipe_main.c
uves_cal_tflat_test_LDADD   = $(OBJDIR)/uves_tflat.o  $(LDADD) $(LIBALL)
uves_cal_tflat_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_tflat $(AM_CPPFLAGS)

uves_cal_cd_align_test_SOURCES = recipe_main.c
uves_cal_cd_align_test_LDADD   = $(OBJDIR)/uves_cal_cd_align.o  $(LDADD) $(LIBALL)
uves_cal_cd_align_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_cd_align $(AM_CPPFLAGS)

uves_cal_mkmaster_test_SOURCES = recipe_main.c
uves_cal_mkmaster_test_LDADD   = $(OBJDIR)/uves_cal_mkmaster.o  $(LDADD) $(LIBALL)
uves_cal_mkmaster_test_CPPFLAGS = -DUVES_TEST_NAME=uves_cal_mkmaster $(AM_CPPFLAGS)

flames_cal_orderpos_test_SOURCES = recipe_main.c
flames_cal_orderpos_test_LDADD   = $(OBJDIR)/flames_cal_orderpos.o  $(LDADD) $(LIBALL)
flames_cal_orderpos_test_CPPFLAGS = -DUVES_TEST_NAME=flames_cal_orderpos $(AM_CPPFLAGS)

flames_cal_predict_test_SOURCES = recipe_main.c
flames_cal_predict_test_LDADD   = $(OBJDIR)/flames_cal_predict.o  $(LDADD) $(LIBALL)
flames_cal_predict_test_CPPFLAGS = -DUVES_TEST_NAME=flames_cal_predict $(AM_CPPFLAGS)

flames_cal_mkmaster_test_SOURCES = recipe_main.c
flames_cal_mkmaster_test_LDADD   = $(OBJDIR)/flames_cal_mkmaster.o  $(LDADD) $(LIBALL)
flames_cal_mkmaster_test_CPPFLAGS = -DUVES_TEST_NAME=flames_cal_mkmaster $(AM_CPPFLAGS)

flames_cal_wavecal_test_SOURCES = recipe_main.c
flames_cal_wavecal_test_LDADD   = $(OBJDIR)/flames_cal_wavecal.o  $(LDADD) $(LIBALL)
flames_cal_wavecal_test_CPPFLAGS = -DUVES_TEST_NAME=flames_cal_wavecal $(AM_CPPFLAGS)

flames_obs_scired_test_SOURCES = recipe_main.c
flames_obs_scired_test_LDADD   = $(OBJDIR)/flames_obs_scired.o  $(LDADD) $(LIBALL)
flames_obs_scired_test_CPPFLAGS = -DUVES_TEST_NAME=flames_obs_scired $(AM_CPPFLAGS)

flames_cal_prep_sff_ofpos_test_SOURCES = recipe_main.c
flames_cal_prep_sff_ofpos_test_LDADD   = $(OBJDIR)/flames_cal_prep_sff_ofpos.o  $(LDADD) $(LIBALL)
flames_cal_prep_sff_ofpos_test_CPPFLAGS = -DUVES_TEST_NAME=flames_cal_prep_sff_ofpos $(AM_CPPFLAGS)

flames_obs_redchain_test_SOURCES = recipe_main.c
flames_obs_redchain_test_LDADD   = $(OBJDIR)/flames_obs_scired.o  $(LDADD) $(LIBALL)
flames_obs_redchain_test_CPPFLAGS = -DUVES_TEST_NAME=flames_obs_redchain $(AM_CPPFLAGS)



# Be sure to reexport important environment variables.
# - And be sure the find the dynamic libraries
TESTS_ENVIRONMENT = MAKE="$(MAKE)" CC="$(CC)" CFLAGS="$(CFLAGS)" \
        CPPFLAGS="$(CPPFLAGS)" LD="$(LD)" LDFLAGS="$(LDFLAGS)" \
        LIBS="$(LIBS)" LN_S="$(LN_S)" NM="$(NM)" RANLIB="$(RANLIB)" \
        LD_LIBRARY_PATH="$(top_builddir)/flames/.libs:$(top_builddir)/uves/.libs:$(top_builddir)/irplib/.libs:$(LD_LIBRARY_PATH)" \
        OBJEXT="$(OBJEXT)" EXEEXT="$(EXEEXT)" \
        MALLOC_PERTURB_=227 MALLOC_CHECK_=2

TESTS = $(check_PROGRAMS)

# We need to remove any files that the above tests created.
clean-local:
	$(RM) *.paf *.fits *.log

