/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston MA 02110-1301 USA          *
 */
 
/*
 * $Author: amodigli $
 * $Date: 2007-06-06 08:17:33 $
 * $Revision: 1.76 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.75  2006/10/26 14:01:55  jmlarsen
 * Replaced hard-coded package name with PACKAGE_NAME
 *
 * Revision 1.6  2006/10/19 08:19:10  jmlarsen
 * Doc. update
 *
 * Revision 1.5  2006/08/11 11:26:20  jmlarsen
 * Added runtime check of UVES library version number
 *
 * Revision 1.4  2006/05/05 13:52:54  jmlarsen
 * Added doxygen header, include config.h
 *
 * Revision 1.3  2006/04/24 09:17:19  jmlarsen
 * Removed explicit uves_-prefix'
 *
 * Revision 1.2  2006/02/03 08:55:42  jmlarsen
 * Removed stray doxygen marker
 *
 * Revision 1.1  2006/02/03 07:55:40  jmlarsen
 * Added plugin template
 *
 * Revision 1.42  2006/01/19 08:47:24  jmlarsen
 * Inserted missing doxygen end tag
 *
 * Revision 1.41  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup uves_physmod
 */
/*----------------------------------------------------------------------------*/

#include <uves.h>
#include <uves_utils.h>

int cpl_plugin_get_info(cpl_pluginlist *list)
{
    if (UVES_BINARY_VERSION != uves_get_version_binary())
    {
        cpl_msg_error(__func__, 
              "I am uves_physmod version %d, but I am linking "
              "against UVES library version %d. "
              "This will not work. "
              "Please remove all previous installations "
              "of the " PACKAGE_NAME " and try again.",
              UVES_BINARY_VERSION, uves_get_version_binary());
        return 1;
    }
    return uves_physmod_get_info(list);
}
