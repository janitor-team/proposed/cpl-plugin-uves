# import the needed modules
from __future__ import absolute_import
from __future__ import print_function
try:
  import reflex
  import_sucess = True

#NOTE for developers: 
# -If you want to modify the current script to cope
#  with different parameters, this is the function to modify:
#  setInteractiveParameters()
# -If you want to modify the current script to read different data from
#  the input FITS, this is the function to modify:
#  readFitsData()                  (from class DataPlotterManager) 
# -If you want to modify the current script to modify the plots (using the same
#  data),  this is the function to modify:
#  plotProductsGraphics()          (from class DataPlotterManager)
# -If you want to modify the text that appears in the "Help" button,
#  this is the function to modify:
#  setWindowHelp()
# -If you want to modify the title of the window, modify this function:
#  setWindowTitle()


  #This class deals with the specific details of data reading and final plotting.
  class DataPlotterManager:
    # This function will read all the columns, images and whatever is needed
    # from the products. The variables , self.plot_x, self.plot_y, etc...
    # are used later in function plotProductsGraphics().
    # Add/delete these variables as you need (only that plotProductsGraphics()
    # has to use the same names).
    # You can also create some additional variables (like statistics) after
    # reading the files.
    # If you use a control variable (self.xxx_found), you can modify 
    # later on the layout of the plotting window based on the presence of 
    # given input files. 
    # sof contains all the set of frames
    def readFitsData(self, fitsFiles):
      #Control variable to check if the interesting files where at the input
      self.line_table_blue_found = False
      self.line_table_red_found = False
      #Read all the products
      frames = dict()
      for frame in fitsFiles:
        if frame == '' :
          continue
        category = frame.category
        frames[category] = frame

      if "LINE_TABLE_BLUE" in frames : 
        self.line_table_blue_found = True

      if "LINE_TABLE_REDL" in frames and \
         "LINE_TABLE_REDU" in frames :  
        self.line_table_red_found = True  

      if self.line_table_blue_found == True :
        frame = frames["LINE_TABLE_BLUE"]
        self.line_tab        = uves_plot_common.PlotableBlueLineTable(frame)
        
      if self.line_table_red_found == True :
        frame_redl = frames["LINE_TABLE_REDL"]
        frame_redu = frames["LINE_TABLE_REDU"]
        self.line_tab        = uves_plot_common.PlotableRedLineTable(frame_redl, frame_redu)        

    # This function creates all the subplots. It is responsible for the plotting 
    # layouts. 
    # There can different layouts, depending on the availability of data
    # Note that subplot(I,J,K) means the Kth plot in a IxJ grid 
    # Note also that the last one is actually a box with text, no graphs.
    def addSubplots(self, figure):
      if self.line_table_blue_found == True or self.line_table_red_found == True:
        self.subplot_res_wave  = figure.add_subplot(3,2,1)
        self.subplot_res_order = figure.add_subplot(3,2,2)
        self.subplot_fwhm_wave = figure.add_subplot(3,2,3)
        self.subplot_y_x       = figure.add_subplot(3,2,4)
        self.subtext_lineinfo  = figure.add_subplot(3,1,3)
      else : 
        self.subtext_nodata    = figure.add_subplot(1,1,1)
          
    # This is the function that makes the plots.
    # Add new plots or delete them using the given scheme.
    # The data has been already stored in self.plot_x, self.plot_xdif, etc ...
    # It is mandatory to add a tooltip variable to each subplot.
    # One might be tempted to merge addSubplots() and plotProductsGraphics().
    # There is a reason not to do it: addSubplots() is called only once at
    # startup, while plotProductsGraphics() is called always there is a resize.
    def plotProductsGraphics(self):
      if self.line_table_blue_found == True or self.line_table_red_found == True :
        #Residual vs wavelength
        if self.line_table_blue_found == True:
          #There is a discrepancy in self.line_tab.mean_residual,self.line_tab.median_residual when compared to reflex 1.0. It might be because of changes in the pipelines 
          title_res_wave   = 'Wave Res: Mean=%#.3g Med=%#.3g RMS=%#.3g'% (self.line_tab.mean_residual,self.line_tab.median_residual,self.line_tab.std_residual) 
        elif self.line_table_red_found == True :
          title_res_wave   = 'Wave Res: Mean=%#.3g Med=%#.3g RMS=%#.3g'% (self.line_tab.mean_all_residual,self.line_tab.median_all_residual,self.line_tab.std_all_residual) 
        tooltip_res_wave ="""Plot of the wavelength residual [Ang] as a function of wavelength [Ang] for the wavelength calibration model. 
Blue points represent line identifications which were used in the wavelength solution, and red points represent line identifications that were clipped from the wavelength solution. 
Lines that were detected in the arc frame but not identified in the line catalogue are not plotted. 
The solid and dashed horizontal lines represent the median value and the +-1 sigma range respectively."""
        self.line_tab.plotRESvsWAVE(self.subplot_res_wave, title_res_wave, tooltip_res_wave)

        #Residual vs order
        if self.line_table_blue_found == True:
          title_res_order   = 'Wave Res: Mean=%#.3g Med=%#.3g RMS=%#.3g'% (self.line_tab.mean_residual,self.line_tab.median_residual,self.line_tab.std_residual) 
        elif self.line_table_red_found == True :
          title_res_order   = 'Wave Res: Mean=%#.3g Med=%#.3g RMS=%#.3g'% (self.line_tab.mean_all_residual,self.line_tab.median_all_residual,self.line_tab.std_all_residual) 
        tooltip_res_order ="""Plot of the wavelength residual [Ang] as a function of order for the wavelength calibration model. 
Blue points represent line identifications which were used in the wavelength solution, and red points represent line identifications that were clipped from the wavelength solution. 
Lines that were detected in the arc frame but not identified in the line catalogue are not plotted. 
The solid and dashed horizontal lines represent the median value and the +-1 sigma range respectively."""
        self.line_tab.plotRESvsORDER(self.subplot_res_order, title_res_order, tooltip_res_order)
      
        #TFWHM vs wavelenth
        if self.line_table_blue_found == True:
          title_fwhm_wave   = 'FWHM: Mean=%#.3g Med=%#.3g RMS=%#.3g'% (self.line_tab.mean_fwhm,self.line_tab.median_fwhm,self.line_tab.std_fwhm) 
        elif self.line_table_red_found == True :
          title_fwhm_wave   = 'FWHM: Mean=%#.3g Med=%#.3g RMS=%#.3g'% (self.line_tab.mean_all_fwhm,self.line_tab.median_all_fwhm,self.line_tab.std_all_fwhm) 
        tooltip_fwhm_wave ="""Plot of the line FWHM (pix) as a function of wavelength [Ang] for identified lines that were used in the wavelength solution (blue points) and all remaining detected lines (red points). 
The solid and dashed horizontal lines represent the median value and the +-1 sigma range respectively."""
        self.line_tab.plotFWHMvsWAVE(self.subplot_fwhm_wave, title_fwhm_wave, tooltip_fwhm_wave)

        #Y vs X
        title_y_x   = 'Arcline Positions' 
        tooltip_y_x ="""Plot of the measured line Y coordinate (pix) versus the measured line X coordinate (pix) for identified lines that were used in the wavelength solution (blue points) and all remaining detected lines (red points).
This plot shows the distribution of detected arc lines on the wavelength calibration arc frame."""
        self.line_tab.plotYvsX(self.subplot_y_x, title_y_x, tooltip_y_x)

        #Fith subpanel: a text box
        self.line_tab.plotLinesText(self.subtext_lineinfo)
        
      else :
        #Data not found info
        self.subtext_nodata.set_axis_off()
        self.text_nodata = 'Line prediction not found in the products (PRO.CATG=LINE_GUESS_TAB_BLUE)' 
        self.subtext_nodata.text(0.1, 0.6, self.text_nodata, color='#11557c', fontsize=18,
                                 ha='left', va='center', alpha=1.0)
        self.subtext_nodata.tooltip='Line prediction not found in the products'
 
  
    # This function specifies which are the parameters that should be presented
    # in the window to be edited.
    # Note that the parameter has to be also in the in_sop port (otherwise it 
    # won't appear in the window) 
    # The descriptions are used to show a tooltip. They should match one to one
    # with the parameter list 
    # Note also that parameters have to be prefixed by the 'recipe name:'
    def setInteractiveParameters(self):
      paramList = list()
      paramList.append(reflex.RecipeParameter(recipe='uves_cal_wavecal',displayName='range',group='wavecal',description='Width (pix) of search window is 2*range + 1. This parameter is automatically adjusted according to binning'))
      paramList.append(reflex.RecipeParameter(recipe='uves_cal_wavecal',displayName='minlines',group='wavecal',description='Minimum number of lines to detect. If zero, the default value (1100 for BLUE/REDL chips; 1000 for REDU chip) is used'))
      paramList.append(reflex.RecipeParameter(recipe='uves_cal_wavecal',displayName='maxlines',group='wavecal',description='Maximum number of lines to detect. If zero, the default value (1600 for BLUE/REDL chip; 1400 for REDU chip) is used'))
      paramList.append(reflex.RecipeParameter(recipe='uves_cal_wavecal',displayName='alpha',group='wavecal',description='The parameter that controls the distance to the nearest neighbours'))
      paramList.append(reflex.RecipeParameter(recipe='uves_cal_wavecal',displayName='degree',group='wavecal',description='Degrees of the global 2d dispersion polynomial. If a negative number is specified, the polynomial degrees are automatically selected by starting from (1, 1) and inreasing the degrees as long as the RMS residual decreases significantly'))
      paramList.append(reflex.RecipeParameter(recipe='uves_cal_wavecal',displayName='tolerance',group='wavecal',description='Tolerance of fit. If positive, the tolerance is in pixel units. If negative, abs(tolerance) is in wavelength units. Lines with residuals worse than the tolerance are excluded from the final fit. Unlike in previous versions, this parameter is not corrected for CCD binning. This rejection based on the absolute residual in pixel can be effectively disabled by setting the tolerance to a very large number (e.g. 9999). In that case outliers will be rejected using only kappa sigma clipping'))
      paramList.append(reflex.RecipeParameter(recipe='uves_cal_wavecal',displayName='kappa',group='wavecal',description='Lines with residuals more then kappa stdev are rejected from the final fit'))

      return paramList

    def setWindowHelp(self):
      help_text = """
In this window, the user should aim to improve the wavelength calibration solution by minimising the RMS statistic for the wavelength residuals (shown in the titles of the upper two plots) while keeping a reasonable number of emission lines used in the final solution (reported as "Final no. of fitted lines")."""
      return help_text

    def setWindowTitle(self):
      title = 'Uves Interactive Wavelength Calibration'
      return title

except ImportError:
  import_sucess = 'false'
  print("Error importing modules pyfits, wx, matplotlib, numpy")

#This is the 'main' function
if __name__ == '__main__':

  # import reflex modules
  import reflex_interactive_app
  import sys

  # import UVES reflex modules
  import uves_plot_common

  # Create interactive application
  interactive_app = reflex_interactive_app.PipelineInteractiveApp(enable_init_sop=True)

  #Check if import failed or not
  if import_sucess == 'false' :
    interactive_app.setEnableGUI('false')

  #Open the interactive window if enabled
  if interactive_app.isGUIEnabled() :
    #Get the specific functions for this window
    dataPlotManager = DataPlotterManager()
    interactive_app.setPlotManager(dataPlotManager)
    interactive_app.showGUI()
  else :
    interactive_app.passProductsThrough()

  # print outputs
  interactive_app.print_outputs()

  sys.exit()
