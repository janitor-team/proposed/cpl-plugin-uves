cpl-plugin-uves (6.1.3+dfsg-5) unstable; urgency=medium

  * Use sha1sum to check the checksum (Closes: #984860)

 -- Ole Streicher <olebole@debian.org>  Tue, 09 Mar 2021 13:59:42 +0100

cpl-plugin-uves (6.1.3+dfsg-4) unstable; urgency=medium

  * Check SHA sum for downloaded calibration file (Closes: #984508)

 -- Ole Streicher <olebole@debian.org>  Sat, 06 Mar 2021 17:30:58 +0100

cpl-plugin-uves (6.1.3+dfsg-3) unstable; urgency=medium

  [ Helmut Grohne ]
  * Request sphinxdoc addon via B-D-I (Closes: #983283)

 -- Ole Streicher <olebole@debian.org>  Mon, 22 Feb 2021 14:28:18 +0100

cpl-plugin-uves (6.1.3+dfsg-2) unstable; urgency=low

  * Increase tolerance in hdrl_fpn_tests

 -- Ole Streicher <olebole@debian.org>  Thu, 02 Jul 2020 09:39:10 +0200

cpl-plugin-uves (6.1.3+dfsg-1) unstable; urgency=low

  * New upstream version 6.1.3+dfsg. Rediff patches
  * Add libgsl build dependency
  * Push Standards-Version to 4.5.0. No changes needed
  * Push dh-compat to 13. Don't fail on missing binaries
  * Add "Rules-Requires-Root: no" to d/control
  * Remove obsolete build dependency versions

 -- Ole Streicher <olebole@debian.org>  Thu, 02 Jul 2020 08:43:38 +0200

cpl-plugin-uves (5.10.4+dfsg-1) unstable; urgency=low

  [ Chris Lamb ]
  * Make the build reproducible (Closes: #901481)

  [ Ole Streicher ]
  * New upstream version 5.10.4+dfsg. Rediff patches
  * Push Standards-Version to 4.4.0. No changes required.
  * Push compat to 12. Remove d/compat
  * Add tests on salsa
  * Mark test as superficial
  * Add overrides for plugin *.so not linked to libc

 -- Ole Streicher <olebole@debian.org>  Sun, 25 Aug 2019 15:48:32 +0200

cpl-plugin-uves (5.9.1+dfsg-1) unstable; urgency=low

  * Update VCS fields to use salsa.d.o
  * Use name-based sorting for the recipes to ensure reproducible build
  * New upstream version 5.9.1+dfsg. Rediff patches
  * Push Standards-Version to 4.1.4. Change remaining URLs to https
  * Push compat to 11
  * Switch to Python 3
  * Adjust documentation path

 -- Ole Streicher <olebole@debian.org>  Fri, 27 Apr 2018 15:52:31 +0200

cpl-plugin-uves (5.8.2+dfsg-1) unstable; urgency=low

  * New upstream version 5.8.2+dfsg
  * Rediff patches
  * Push Standards-Version to 4.0.0. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Sat, 01 Jul 2017 20:54:42 +0200

cpl-plugin-uves (5.7.0+dfsg-1) unstable; urgency=low

  * New upstream version
  * Bump Standards-Version to 3.9.8. Use https for VCS URLs

 -- Ole Streicher <olebole@debian.org>  Fri, 22 Apr 2016 09:25:05 +0200

cpl-plugin-uves (5.5.7+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Wed, 23 Sep 2015 11:32:58 +0200

cpl-plugin-uves (5.5.5+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Wed, 02 Sep 2015 16:04:25 +0200

cpl-plugin-uves (5.5.2+dfsg-4) unstable; urgency=low

  * Reproducibly sort recipes in sphinx output. Closes: #792940

 -- Ole Streicher <olebole@debian.org>  Mon, 20 Jul 2015 15:22:43 +0200

cpl-plugin-uves (5.5.2+dfsg-3) unstable; urgency=low

  * Limit CI test dependency to plugin

 -- Ole Streicher <olebole@debian.org>  Wed, 20 May 2015 09:28:17 +0200

cpl-plugin-uves (5.5.2+dfsg-2) unstable; urgency=low

  * Loop over all possible upstream package names when downloading
    calibration data
  * Add astropy dependency to CI test to make it work

 -- Ole Streicher <olebole@debian.org>  Sat, 16 May 2015 15:21:25 +0200

cpl-plugin-uves (5.5.2+dfsg-1) unstable; urgency=low

  * New upstream version
  * Build-depend on libcfitsio-dev instead of libcfitsio3-dev. Closes: #761705
  * Increase tolerance to fix FTBS on mips64el. Closes:  #774239
  * Update uploader e-mail
  * Update standards version to 3.9.6. No changes needed.
  * Add simple CI test

 -- Ole Streicher <olebole@debian.org>  Wed, 29 Apr 2015 15:59:13 +0200

cpl-plugin-uves (5.4.3+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Wed, 25 Jun 2014 16:50:04 +0200

cpl-plugin-uves (5.4.0+dfsg-1) unstable; urgency=low

  * New upstream version
  * Change maintainer and VCS location to debian-astro

 -- Ole Streicher <olebole@debian.org>  Fri, 11 Apr 2014 14:47:48 +0200

cpl-plugin-uves (5.3.0+dfsg1-1) unstable; urgency=low

  * Remove libqfits from +dfsg
  * Change maintainer to debian-astro

 -- Ole Streicher <olebole@debian.org>  Sun, 30 Mar 2014 14:28:05 +0200

cpl-plugin-uves (5.3.0+dfsg-2) unstable; urgency=low

  * Fix FTBS on ia64 (irqlib)
  * Fix missing declaration in cpl_propertylist_test.c

 -- Ole Streicher <olebole@debian.org>  Wed, 29 Jan 2014 12:01:28 +0100

cpl-plugin-uves (5.3.0+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #731630)

 -- Ole Streicher <olebole@debian.org>  Tue, 31 Dec 2013 15:15:10 +0100
