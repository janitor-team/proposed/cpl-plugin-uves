/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/


/*--------------------------------------------------------------------------*/
/**
 * @defgroup flames_Standard   Substep: Flames Standard Extraction
 *
 */
/*--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
  Includes
  ---------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <flames_Stand_Extract.h>
#include <flames_standard.h>
/* MIDAS include files */
#include <flames_midas_def.h>
/* FLAMES-UVES include files */ 
#include <flames_uves.h>
#include <flames_newmatrix.h>

/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/


/**
   @name  flames_Std_Extract()          
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani  

   @param ScienceFrame input science frame to be extracted
   @param SingleFF     input all flat field frame base structure
   @param Order        input order traces structure
   @param ordsta       input order start
   @param ordend       input order end
   @param mask         input mask
   @param pfibrecentre
   @param phalfwinsize 

   Purpose:
   DRS function called:                              
   Pseudocode:                       

   @note
 */


flames_err 
standard(flames_frame *ScienceFrame, 
         allflats *SingleFF, 
         orderpos *Order, 
         int32_t ordsta, 
         int32_t ordend, 
         frame_mask **mask,  
         double ***pfibrecentre, 
         double phalfwinsize)
{

    int32_t j=0, nf=0, no=0;
    double **aa=0, **varaa=0, **xx=0, *xx2, **covarxx=0, **covariance=0;
    double *pylow, *pyup;
    int32_t *ylow, *yup;
    int32_t *fibrestosolve=0;
    int32_t *orderstosolve=0;
    int32_t numslices=0;
    int32_t numorders=0, ifibre=0;
    int32_t arraysize=0;
    int32_t jnoifibreoffsetstep=0;
    int32_t jnoifibreoffset=0;
    int32_t noifibreoffset=0;
    int32_t jnoifibreindex=0;
    frame_mask *fmvecbuf1=0;

    /*
    char output[100];
     */
    numorders = 1+ordend-ordsta;
    arraysize = numorders*ScienceFrame->maxfibres;
    fibrestosolve = lvector(1, arraysize);
    orderstosolve = lvector(1, arraysize);
    /* allocate aa, xx and covariances once and for all, as large as they may be
     possibly needed */
    aa = dmatrix(1, arraysize, 1, arraysize);
    varaa = dmatrix(1, arraysize, 1, arraysize);
    covarxx = dmatrix(1, arraysize, 1, arraysize);
    covariance = dmatrix(1, arraysize, 1, arraysize);
    xx = dmatrix(1, arraysize, 1, 1);
    xx2 = dvector(1, arraysize);

    pylow = dvector(1, arraysize);
    pyup = dvector(1, arraysize);
    ylow = lvector(1, arraysize);
    yup = lvector(1, arraysize);

    jnoifibreoffsetstep = (ScienceFrame->lastorder-ScienceFrame->firstorder+1)*
                    ScienceFrame->maxfibres;
    fmvecbuf1 = ScienceFrame->specmask[0][0];

    for (j=0; j<=(ScienceFrame->subcols-1); j++) {
        if (Std_Extract(ScienceFrame, SingleFF, Order, ordsta, ordend, j,
                        mask, aa, varaa, xx, xx2, covarxx, covariance,
                        fibrestosolve, orderstosolve, &numslices, pfibrecentre,
                        phalfwinsize, pylow, pyup, ylow, yup, arraysize))
            return 1;
        if (numslices==0) {
            jnoifibreoffset = j*jnoifibreoffsetstep;
            for (no=ordsta-Order->firstorder;no<=ordend-Order->firstorder;no++){
                noifibreoffset = no*ScienceFrame->maxfibres;
                for (nf=0; nf<=(ScienceFrame->num_lit_fibres-1); nf++) {
                    ifibre = ScienceFrame->ind_lit_fibres[nf];
                    jnoifibreindex = jnoifibreoffset+noifibreoffset+ifibre;
                    fmvecbuf1[jnoifibreindex]=0;
                }
            }
        }
    }

    free_lvector(fibrestosolve, 1, arraysize);
    free_lvector(orderstosolve, 1, arraysize);
    free_dmatrix(aa, 1, arraysize, 1, arraysize);
    free_dmatrix(varaa, 1, arraysize, 1, arraysize);
    free_dmatrix(covarxx, 1, arraysize, 1, arraysize);
    free_dmatrix(covariance, 1, arraysize, 1, arraysize);
    free_dmatrix(xx, 1, arraysize, 1, 1);
    free_dvector(xx2, 1, arraysize);
    free_dvector(pylow, 1, arraysize);
    free_dvector(pyup, 1, arraysize);
    free_lvector(ylow, 1, arraysize);
    free_lvector(yup, 1, arraysize);

    return NOERR;

}
/**@}*/
