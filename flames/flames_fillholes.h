/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program; if not, write to the Free
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge,
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
      Internet e-mail: midas@eso.org
      Postal address: European Southern Observatory
                      Data Management Division
                      Karl-Schwarzschild-Strasse 2
                       D 85748 Garching bei Muenchen
                       GERMANY
 ===========================================================================*/
#ifndef FLAMES_FILLHOLES_H
#define FLAMES_FILLHOLES_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


#include<flames_shiftcommon.h>

flames_err calcfillshifts(allflats *allflatsin, shiftstruct *shiftdata, 
                          int32_t ix);
flames_err locatefillfibre(allflats *allflatsin, orderpos* ordpos, 
                           shiftstruct *shiftdata, int32_t iorder,
                           int32_t ifibre, int32_t ix);
flames_err initfillfibre(allflats *allflatsin, int32_t iorder, 
                         int32_t iframe, int32_t ifibre, int32_t ix,
                         badifibrestruct *badifibre, int32_t *badtotal);
flames_err fillnormfactors(allflats *allflatsin, shiftstruct *shiftdata, 
                           badifibrestruct *badifibre, int32_t iorder,
                           int32_t iframe, int32_t ifibre, int32_t ix,
                           int32_t ixindex, normstruct* normdata);
flames_err selectfillavail(allflats *allflatsin, shiftstruct *shiftdata, 
                           normstruct *normdata, fitstruct *fitdata,
                           int32_t iorder, int32_t iframe, int32_t ix,
                           int32_t iy);

flames_err  cloneallflats(allflats *allflatsin, allflats *allflatsout);



/* The following function takes a pointer to an allflats structure (all memory
   allocations are assumed to have been done already) and tries to fill
   missing (bad) pixels with interpolated values. Moreover, in the 2D frames
   each pixel is normalised so that for any given x, and order the integrated
   value over one fibre equals 1, and the normalisation factor is stored in
   appropriate structure members, to make shifting more efficient later. */


flames_err
fillholes(allflats *allflatsin,
          orderpos *ordpos,
          const cpl_frameset *slitsname,
          char *backname,
          int bxdegree,
          int bydegree,
          scatterswitch bkgswitch,
          scatterswitch2 bkgswitch2,
          int badwinxsize,
          int badwinysize,
          double badfracthres,
          int badtotthres,
          double kappa2,
          double decentSNR,
          int32_t maxbackiters,
          double maxdiscardfract,
          int32_t maxcleaniters,
          double maxsinglepixelfrac,
          double fracslicesthres,
          int *OUTPUTI);

#endif
