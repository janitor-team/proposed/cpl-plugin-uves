/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : readordpos.c                                                 */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <uves_msg.h>
#include <flames_allocordpos.h>
#include <flames_readordpos.h>


flames_err readordpos(const char *orderfile, orderpos *ordpos)
{
    int tid=0;
    int actvals=0;
    int unit=0;
    int null=0;
    int status=0;


    /* open the table */
    if ((status = TCTOPN(orderfile, F_D_MODE, &tid)) != 0) {
        /* I could not open the dummy table: protest... */
    	char output[100];
        sprintf(output, "Error: I couldn't open the %s table\n", orderfile);
        SCTPUT(output);
        return flames_midas_fail();
    }

    /* initialise the ordpos structure from the dummy table descriptors */

    /* all scalars first */

    /* read the degrees of the order polynomial */
    if ((status = SCDRDI(tid, "COEFFI", 6, 1, &actvals, &ordpos->xdegree, &unit,
                    &null)) != 0) {
        /* I could not get the x degree of the polynomial: protest... */
        return(MAREMMA);
    }

    if ((status = SCDRDI(tid, "COEFFI", 7, 1, &actvals, &ordpos->mdegree, &unit,
                    &null)) != 0) {
        /* I could not get the m degree of the polynomial: protest... */
        return(MAREMMA);
    }

    /* read the maximum number of fibres that we plan to support */
    if ((status = SCDRDI(tid, "MAXFIBRES", 1, 1, &actvals, &ordpos->maxfibres,
                    &unit, &null)) != 0) {
        /* I could not get the coefficients of the polynomial: protest... */
        return(MAREMMA);
    }

    /* read the fibre half width */
    if ((status = SCDRDD(tid, "HALFIBREWIDTH", 1, 1, &actvals,
                    &ordpos->halfibrewidth, &unit, &null)) != 0) {
        /* I could not get the fibre half width: protest... */
        return(MAREMMA);
    }

    /* read the gaussian pseudo-fibre FWHM for gausscorrel and friends */
    if ((status = SCDRDD(tid, "GAUSSFIBRESIGMA", 1, 1, &actvals,
                    &ordpos->pgausssigma, &unit, &null)) != 0) {
        /* I could not get the gaussian pseudo-fibre FWHM: protest... */
        return(MAREMMA);
    }

    /* read the gaussian pseudo-fibre half width for gausscorrel and friends */
    if ((status = SCDRDD(tid, "GAUSSHALFWIDTH", 1, 1, &actvals,
                    &ordpos->pgausshalfwidth, &unit, &null)) != 0) {
        /* I could not get the gaussian pseudo-fibre half width: protest... */
        return(MAREMMA);
    }

    /* read firstorder */
    if ((status = SCDRDI(tid, "ORDERLIM", 1, 1, &actvals, &ordpos->firstorder,
                    &unit, &null)) != 0) {
        /* I could not get firstorder: protest... */
        return(MAREMMA);
    }

    /* read lastorder */
    if ((status = SCDRDI(tid, "ORDERLIM", 2, 1, &actvals, &ordpos->lastorder,
                    &unit, &null)) != 0) {
        /* I could not get lastorder: protest... */
        return(MAREMMA);
    }

    /* read tab_io_oshift */
    if ((status = SCDRDI(tid, "TAB_IN_OUT_OSHIFT", 1, 1, &actvals,
                    &ordpos->tab_io_oshift, &unit, &null)) != 0) {
        /* I could not get tab_io_oshift: protest... */
        return(MAREMMA);
    }

    /* read tab_io_yshift */
    if ((status = SCDRDD(tid, "TAB_IN_OUT_YSHIFT", 1, 1, &actvals,
                    &ordpos->tab_io_yshift, &unit, &null)) != 0) {
        /* I could not get tab_io_yshift: protest... */
        return(MAREMMA);
    }

    /* read corrected */
    if ((status = SCDRDC(tid, "CORRECTED", 1, 1, 1, &actvals,
                    &ordpos->corrected, &unit, &null)) != 0) {
        /* I could not read corrected flag, complain */
        return MAREMMA;
    }

    /* read ycorrection */
    if ((status = SCDRDD(tid, "YCORRECTION", 1, 1, &actvals,
                    &ordpos->ycorrection, &unit, &null)) != 0) {
        /* I could not read the ycorrection, complain */
        return MAREMMA;
    }

    /* read chipchoice */
    if ((status = SCDRDC(tid, "CHIPCHOICE", 1, 1, 1, &actvals,
                    &ordpos->chipchoice, &unit, &null)) != 0) {
        /* I could not read chipchoice flag, complain */
        return MAREMMA;
    }

    /* allocate the internal arrays */
    if ((status = allocordpos(ordpos)) != NOERR) {
        /* error allocating ordpos arrays */
        return(MAREMMA);
    }

    /* now read the arrays */

    /* read start */
    if ((status = SCDRDD(tid, "REFSTART", 1, 2, &actvals, ordpos->start,
                    &unit, &null)) != 0) {
        /* I could not read START, complain */
        return MAREMMA;
    }

    /* read step */
    if ((status = SCDRDD(tid, "REFSTEP", 1, 2, &actvals, ordpos->step,
                    &unit, &null)) != 0) {
        /* I could not read STEP, complain */
        return MAREMMA;
    }

    /* read npix */
    if ((status = SCDRDI(tid, "REFNPIX", 1, 2, &actvals, ordpos->npix,
                    &unit, &null)) != 0) {
        /* I could not read NPIX, complain */
        return MAREMMA;
    }

    /* read the coefficients of the order polynomial */
    if ((status = SCDRDD(tid, "COEFFD", 1,
                    (ordpos->xdegree+1)*(ordpos->mdegree+1), &actvals,
                    ordpos->orderpol[0], &unit, &null)) != 0) {
        /* I could not get the coefficients of the polynomial: protest... */
        return(MAREMMA);
    }

    /* check that the correct number of values was actually read in */
    if (actvals != (ordpos->xdegree+1)*(ordpos->mdegree+1)) {
        /* help! SCDRDI read the wrong number of coefficients... */
        return(MAREMMA);
    }

    /* read the fibrepos vector */
    if ((status = SCDRDD(tid, "FIBREPOS", 1, ordpos->maxfibres, &actvals,
                    ordpos->fibrepos, &unit, &null)) != 0) {
        /* I could not get the fibre offsets: protest... */
        return(MAREMMA);
    }

    /* check that the correct number of values was actually read in */
    if (actvals != ordpos->maxfibres) {
        /* help! SCDRDD read the wrong number of coefficients... */
        return(MAREMMA);
    }

    /* read the fibremask vector */
    if ((status = SCDRDI(tid, "FIBREMASK", 1, ordpos->maxfibres, &actvals,
                    ordpos->fibremask, &unit, &null)) != 0) {
        /* I could not get the fibre mask: protest... */
        return(MAREMMA);
    }

    /* check that the correct number of values was actually read in */
    if (actvals != ordpos->maxfibres) {
        /* help! SCDRDI read the wrong number of coefficients... */
        return(MAREMMA);
    }

    /* read the gaussselfshift vector */
    if ((status = SCDRDD(tid, "GAUSSSELFSHIFT", 1, ordpos->maxfibres, &actvals,
                    ordpos->gaussselfshift, &unit, &null)) != 0) {
        /* I could not get the fibre self-shifts: protest... */
        return(MAREMMA);
    }

    /* check that the correct number of values was actually read in */
    if (actvals != ordpos->maxfibres) {
        /* help! SCDRDD read the wrong number of coefficients... */
        return(MAREMMA);
    }

    /* close the table */
    if ((status=TCTCLO(tid))!=0) SCTPUT("Warning: problems closing order table");

    return(NOERR);

}
