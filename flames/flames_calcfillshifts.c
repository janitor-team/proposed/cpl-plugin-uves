/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_calcfillshift   Substep: ??? 
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                          Includes
 --------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_shiftcommon.h>
#include <flames_fillholes.h>
/**@{*/

/*---------------------------------------------------------------------------
                            Implementation
 ---------------------------------------------------------------------------*/
/**
 @name  flames_calcfillshift()  
 @short   ???
 @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

 @param allflatsin pointer to allflat frame 
 @param shiftdata  pointer to shift structure
 @param ix         


 @return initialized frame

 DRS Functions called:                                                   
                                                                         
 Pseudocode:                                                             

@note
*/

flames_err 
calcfillshifts(
               allflats *allflatsin, 
               shiftstruct *shiftdata, 
               int32_t ix)
{
  int32_t i=0, pyshift=0, numoffsets=0;

  shiftstruct *myshiftdata=0;

  myshiftdata = shiftdata+ix;

  numoffsets = 0;
  /* the first ix shifts to check are +-1, with 0 integer yshift */
  for (i=-1; i<=1 ; i+=2){
    if ((ix+i)>=0 && (ix+i)<=allflatsin->subcols-1) {
      /* the shifted ix is within frame boundaries */
      (myshiftdata->ixoffsets)[numoffsets] = (ix+i);
      (myshiftdata->yintoffsets)[numoffsets] = 0;
      (myshiftdata->yfracoffsets)[numoffsets] = 
    ((myshiftdata+i)->ordercentre)-(myshiftdata->ordercentre);
      numoffsets++;
    }
  }
  /* now check the ix shifts corresponding to +-1 in yshift */
  for (pyshift=-1; pyshift<=1; pyshift+=2) {
    /* find the ix shift which corresponds to a shift exactly opposite
       to pyshift so that their sum is close to zero */
    double pxshift = -((double)pyshift*(allflatsin->substepy))/
      ((allflatsin->substepx)*(myshiftdata->orderslope));
    /* find the range in integer ix shifts corresponding to the above 
       pxshift */
    /* this is a bit subtle: if pxshift is not integer, i will range from 
       floor(pxshift) to ceil(pxshift); otherwise, if pxshift is integer, i 
       will range from pxshift-1 to pxshift+1 */
    for (i = (int32_t)(ceil(pxshift))-1; i <= (int32_t)(floor(pxshift))+1; 
     i++) {
      if ((ix+i)>=0 && (ix+i)<=allflatsin->subcols-1) {
    /* the shifted ix is within frame boundaries */
    /* save the absolute shifted ix for later use (not the offset!) */
    (myshiftdata->ixoffsets)[numoffsets] = (ix+i);
    (myshiftdata->yintoffsets)[numoffsets] = pyshift;
    (myshiftdata->yfracoffsets)[numoffsets] = 
      ((myshiftdata+i)->ordercentre)-(myshiftdata->ordercentre)+pyshift;
    numoffsets++;
      }
    }
  }

  myshiftdata->numoffsets = numoffsets;

  return(NOERR);

}
/**@}*/

