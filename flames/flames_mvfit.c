/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : mvfit.c                                                      */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


//#include <flames_lfit.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_mvfit.h>
#include <flames_lsfit.h>

double **q;

int mvfit(fit_info *fitpar)
{
    int i=0;
    int j=0;
    int *ia=0;
    double chisq=0;
    double *x=0;
    double **covar=0;
    int32_t ndata=0;
    int npar=0;

    ndata=fitpar->n_xy-1;
    npar= fitpar->n_par;

    x=dvector(1,ndata);
    for (i=1;i<=ndata;i++) x[i] = (double)i;

    ia=ivector(1, npar);
    for (i=1;i<=npar;i++) ia[i]=1;

    covar=dmatrix(1,npar,1,npar);
    /* not needed as we are using cxalloc */
    for (i=1; i<=npar; i++){
        for (j=1; j<=npar; j++) {
            covar[i][j] = 0;
        }
    }
    /*q = dmatrix(1,ndata,1,npar);*/
    q = fitpar->deriv;

    cpl_vector* vx=cpl_vector_wrap(ndata,&x[0]);
    cpl_vector* vy=cpl_vector_wrap(ndata,&(fitpar->f_yvalue)[0]);
    cpl_vector* vs=cpl_vector_wrap(ndata,&(fitpar->f_sigma)[0]);
    flames_lfit(vx,vy,vs,ndata,fitpar->par,ia,npar,covar,&chisq,funcs);
    cpl_vector_unwrap(vx);
    cpl_vector_unwrap(vy);
    cpl_vector_unwrap(vs);


    free_ivector(ia,1,npar);
    free_dmatrix(covar,1,npar,1,npar);
    free_dvector(x,1,ndata);
    return(0);
}

void funcs(double x,double afunk[],int ma)
{
    int i=0;
    for (i=1;i<=ma;i++) afunk[i]=q[(int)x][i];

}
