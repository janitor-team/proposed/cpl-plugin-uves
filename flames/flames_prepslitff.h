/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:31:29 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.5  2007/06/04 11:45:28  jmlarsen
 * Added header files
 *
 * Revision 1.2  2007/01/29 13:09:57  jmlarsen
 * Work on conversion to CPL
 *
 * Revision 1.1  2007/01/10 08:06:10  jmlarsen
 * Added source files
 *
 * Revision 1.1  2006/10/20 06:42:09  jmlarsen
 * Moved FLAMES source to flames directory
 *
 * Revision 1.78  2006/10/12 11:37:28  jmlarsen
 * Temporarily disabled FLAMES code generation
 *
 */
#ifndef FLAMES_PREPSLITFF_H
#define FLAMES_PREPSLITFF_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <cpl.h>

int flames_prepslitff(const cpl_frameset *SLITCAT,
                      cpl_frameset **OUTCAT,
                      const char *MYORDER,
                      const char *BASENAME,
                      const double *DECENTSNR);

#endif
