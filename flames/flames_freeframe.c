/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_freeframe  Substep: Free memory allocated by a frame
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_freeframe.h>
#include <flames_newmatrix.h>

/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_freeframe()  
   @short Free memory allocated by a frame
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param myframe

   @return success or failure code

   DRS Functions called:          
   free_dmatrix                                                         
   free_mmatrix                                                          
   free_cvector                                                          
   free_mvector                                                          
   free_lvector                                                          

   Pseudocode:                                                             
   Call DRS routines to free memory                                       

   @note
 */
flames_err freeframe(flames_frame *myframe)
{

    free_fdmatrix(myframe->frame_array, 0, myframe->subrows-1,
                    0, myframe->subcols-1);
    free_fdmatrix(myframe->frame_sigma, 0, myframe->subrows-1,
                  0, myframe->subcols-1);
    free_fmmatrix(myframe->badpixel, 0, myframe->subrows-1,
                  0, myframe->subcols-1);
    free_cvector(myframe->framename, 0, CATREC_LEN+1);
    free_cvector(myframe->sigmaname, 0, CATREC_LEN+1);
    free_cvector(myframe->badname, 0, CATREC_LEN+1);
    if (myframe->maxfibres>0) {
        free_cvector(myframe->fibremask, 0, myframe->maxfibres-1);
        free_lvector(myframe->ind_lit_fibres, 0, myframe->maxfibres-1);
    }
    if (myframe->back.Window_Number > 0) {
        free_dvector(myframe->back.x, 1, myframe->back.Window_Number);
        free_dvector(myframe->back.y, 1, myframe->back.Window_Number);
        free_dmatrix(myframe->back.window, 1, myframe->back.Window_Number, 1, 5);
        myframe->back.Window_Number = 0;
    }
    if (myframe->back.coeff != 0) {
        free_dvector(myframe->back.coeff, 1,
                        (int32_t)((myframe->back.xdegree+1)*
                                        (myframe->back.ydegree+1)));
        myframe->back.coeff = 0;
    }
    if (myframe->back.expon != 0) {
        free_dmatrix(myframe->back.expon, 1, 2, 1,
                        (int32_t)((myframe->back.xdegree+1)*
                                        (myframe->back.ydegree+1)));
        myframe->back.expon = 0;
    }
    if (myframe->nflats > 0) {
        free_dvector(myframe->yshift, 0, myframe->nflats-1);
    }

    return(NOERR);

}
/**@}*/
