/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : writeback.c                                                  */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_writeback.h>
#include <string.h>

flames_err writeback(flames_frame *ScienceFrame, const char *backname, 
                     frame_data **backframe)
{

    int naxis=0;
    int npix[2]={0,0};
    int unit=0;
    int backid=0;

    float cuts[4]={0,0,0,0};
    frame_data minimum=0, maximum=0;

    double start[2]={0,0};
    double step[2]={0,0};

    char ident[73];
    char cunit[3][16];

    frame_data *fdvecbuf1=0;
    int32_t iyixend=0;
    int32_t iyixindex=0;

    fdvecbuf1 = backframe[0];
    iyixend = (ScienceFrame->subrows*ScienceFrame->subcols)-1;

    memset(ident, '\0', 73);
    memset(cunit[0], '\0', 48);

    npix[0] = ScienceFrame->subcols;
    npix[1] = ScienceFrame->subrows;
    naxis = 2;
    cuts[0] = 0;
    cuts[1] = 0;
    start[0] = ScienceFrame->substartx;
    start[1] = ScienceFrame->substarty;
    step[0] = ScienceFrame->substepx;
    step[1] = ScienceFrame->substepy;

    strncpy(cunit[0], "                ", 16);
    strncpy(cunit[1], "PIXEL           ", 16);
    strncpy(cunit[2], "PIXEL           ", 16);

    if(SCFCRE(backname, FLAMESDATATYPE, F_O_MODE, F_IMA_TYPE,
                    ScienceFrame->subcols*ScienceFrame->subrows,
                    &backid))
    {
        /* could not create the data file */
        SCFCLO(backid);
        return MAREMMA;
    }

    /* write standard descriptors of file */
    if (SCDWRC(backid, "IDENT", 1, ident, 1, 72, &unit))
    {
        SCFCLO(backid);
        return MAREMMA;
    }
    if (SCDWRI(backid, "NAXIS", &naxis, 1, 1, &unit))
    {
        SCFCLO(backid);
        return MAREMMA;
    }
    if (SCDWRI(backid, "NPIX", npix, 1, 2, &unit))
    {
        SCFCLO(backid);
        return MAREMMA;
    }
    if (SCDWRD(backid, "START", start, 1, 2, &unit))
    {
        /* error writing descriptor */
        SCFCLO(backid);
        return MAREMMA;
    }
    if (SCDWRD(backid, "STEP", step, 1, 2, &unit))
    {
        SCFCLO(backid);
        return MAREMMA;
    }
    if (SCDWRC(backid, "CUNIT", 1, cunit[0], 1, 48, &unit))
    {
        SCFCLO(backid);
        return MAREMMA;
    }
    minimum = maximum = fdvecbuf1[0];
    for (iyixindex=1; iyixindex<=iyixend; iyixindex++) {
        if (fdvecbuf1[iyixindex]>maximum) maximum = fdvecbuf1[iyixindex];
        if (fdvecbuf1[iyixindex]<minimum) minimum = fdvecbuf1[iyixindex];
    }
    cuts[2] = (float) minimum;
    cuts[3] = (float) maximum;
    if (SCDWRR(backid, "LHCUTS", cuts, 1, 4, &unit))
    {
        SCFCLO(backid);
        return MAREMMA;
    }

    if(SCFPUT(backid, 1, ScienceFrame->subcols*ScienceFrame->subrows,
                    (char *) fdvecbuf1))
    {
        SCFCLO(backid);
        return MAREMMA;
    }
    SCFCLO(backid);

    return(NOERR);

}

