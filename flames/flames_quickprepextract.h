/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */


#ifndef FLAMES_QUICK_PREP_EXTRACT_H
#define FLAMES_QUICK_PREP_EXTRACT_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>
/* the following funtion initialises the global, merged bad
   pixel mask to be used in the subsequent optimal extraction, creates
   the lookup tables for lit fibres in the ScienceFrame and allocates the
   arrays to store the allocated spectra again in ScienceFrame, in the case
   of "quick and dirty" extraction without y shift correction */

flames_err 
quickprepextract(flames_frame *ScienceFrame, 
                 allflats *Shifted_FF, 
                 orderpos *Order,
                 frame_mask **mask);


#endif
