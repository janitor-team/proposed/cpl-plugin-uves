/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2013-07-22 07:27:35 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_cal_mkmaster_sflat  Recipe: SFLAT reduction
 *
 * See man-page for details.
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_reduce_mflat.h>
#include <uves_recipe.h>
#include <flames.h>
#include <flames.h>

#include <cpl.h>


const char * const flames_cal_mkmaster_desc =
"This recipe creates a composite master flat frame to reduce FIBER mode data by:\n"
"1) Group each input raw flat frame per grating setting. Then for each set:\n"
"1a) subtracts the master bias frame from each flat field frame, \n"
"1b) divides each flat field frame by the exposure time for that frame, \n"
"1c) takes the median of all bias subtracted, normalized raw flat frames,\n"
"1d) optionally subtracts the master dark frame, and \n"
"1e) subtracts the background to get the bias subtracted, \n"
"optionally dark subtracted, normalized, background subtracted master \n"
"flat-field frame.\n"
"2) Creates a synthetic master frame resulting from the coaddition of\n"
"each master flat frame obtained by each set\n"
"Symbolically,\n"
" masterflat = median( (flat_i - masterbias)/exptime_i ) - masterdark/exptime\n"
"            - background.\n"
"\n"
"The input flat field frames must have same tag which must match\n"
"(SFLAT_(BLUE|RED), for example SFLAT_BLUE or FLAT_RED. Also, a\n"
"master bias (MASTER_BIAS_xxxx) and ordertable (ORDER_TABLE_xxxx) must be\n"
"provided for each chip (xxxx = BLUE, REDL, REDU). A master dark frame\n"
"(MASTER_(P)DARK_xxxx) may optionally be provided. On blue input the recipe\n"
"computes one master flat field frame; on red input the recipe produces a\n"
"master flat field frame for each chip (MASTER_SFLAT_xxxx).";





/*-----------------------------------------------------------------------------
                            Forward declarations
 -----------------------------------------------------------------------------*/
static int flames_cal_mkmaster_define_parameters(cpl_parameterlist *parameters);

/*-----------------------------------------------------------------------------
                            Recipe standard code
 -----------------------------------------------------------------------------*/
#define cpl_plugin_get_info flames_cal_mkmaster_get_info
UVES_RECIPE_DEFINE(
    FLAMES_MKMASTER_ID, FLAMES_MKMASTER_DOM, 
    flames_cal_mkmaster_define_parameters,
    "Andrea Modigliani", "cpl@eso.org",
    "Creates a master flat frame to support FIBER mode data reduction ",
    flames_cal_mkmaster_desc);

/*-----------------------------------------------------------------------------
                              Functions code
 -----------------------------------------------------------------------------*/
/**@{*/
/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    parameters        the parameterlist to fill
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int
flames_cal_mkmaster_define_parameters(cpl_parameterlist *parameters)
{
    return uves_mflat_define_parameters_body(parameters, 
                         make_str(FLAMES_MKMASTER_ID));
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Get the command line options and execute the data reduction
  @param    parameters  the parameters list
  @param    frames      the frames list
  @return   CPL_ERROR_NONE if everything is ok

 */
/*----------------------------------------------------------------------------*/
static void
UVES_CONCAT2X(FLAMES_MKMASTER_ID,exe)(cpl_frameset *frames,
                      const cpl_parameterlist *parameters,
                      const char *starttime)
{
    uves_mflat_exe_body(frames, parameters, starttime,
            make_str(FLAMES_MKMASTER_ID));
    return;
}
/**@}*/
