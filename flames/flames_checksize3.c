/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_checksize3   Substep: check input frame size descriptors
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                          Includes
 --------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <uves_msg.h>
#include <flames_checksize3.h>
#include <flames_newmatrix.h>
/**@{*/

/*---------------------------------------------------------------------------
                            Implementation
 ---------------------------------------------------------------------------*/
/**
 @name  flames_checksize3()  
 @short  check input frame size descriptors
 @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

 @param frameid  input frame id
 @param slitflats input allslitflats structure


 @return success or failure code

 DRS Functions called:          
       none                                         
                                                                         
 Pseudocode:                                                             
     check several MIDAS descriptors                                     

@note
*/

flames_err checksize3(int frameid, allflats *myflats)
{
  int actvals=0, unit=0, null=0, naxis=0;
  int npix[2]={0,0};
  double start[2]={0,0}, step[2]={0,0};



  if (0 != SCDRDI(frameid, "NAXIS", 1, 1, &actvals, &naxis, &unit, &null)) {
    /* problems reading NAXIS */
    return(MAREMMA);
  }

  if (naxis != 2) {
    /* wrong file dimensions: wrong frame name? */
    return(MAREMMA);
  }

  if (0!= SCDRDI(frameid, "NPIX", 1, 2, &actvals, npix, &unit, &null)) {
    /* problems reading NPIX */
    return(MAREMMA);
  }

  if (npix[0] != myflats->subcols || npix[1] != myflats->subrows) {
    /* wrong file dimensions: wrong frame name? */
    return(MAREMMA);
  }

  if ( SCDRDD(frameid, "START", 1, 2, &actvals, start, &unit, &null) != 0) {
    /* problems reading START */
    return(MAREMMA);
  }

  uves_msg_debug("start=%f %f",start[0],start[1]);
  uves_msg_debug("check=%f %f",myflats->substartx,myflats->substarty);
  if (start[0] != myflats->substartx || start[1] != myflats->substarty) {
    /* wrong file dimensions: wrong frame name? */
    return(MAREMMA);
  }

  if (SCDRDD(frameid, "STEP", 1, 2, &actvals, step,
               &unit, &null) != 0) {
    /* problems reading START */
    return(MAREMMA);
  }
  uves_msg_debug("step= %f %f",step[0],step[1]);
  uves_msg_debug("check=%f %f",myflats->substepx,myflats->substepy);
  if (step[0] != myflats->substepx || step[1] != myflats->substepy) {
    /* wrong file dimensions: wrong frame name? */
    return(MAREMMA);
  }
  /* the file dimensions match, go ahead */


  return(NOERR);

}
/**@}*/
