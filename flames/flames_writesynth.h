/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_WRITE_SYNTH_H
#define FLAMES_WRITE_SYNTH_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>

/* the following function writes to disk the fitted frame, the fitted sigma 
   and the overall mask resulting from an optimal extraction, using the 
   specified filenames */
flames_err 
writesynth(flames_frame *ScienceFrame, 
           const char *synthname, 
           const char *sigmaname, 
           const char *badname);

flames_err write_flames_frame_data(flames_frame *ScienceFrame,const char *data_name);
flames_err write_flames_frame_sigma(flames_frame *ScienceFrame,const char *sigma_name);
flames_err write_flames_frame_mask(flames_frame *ScienceFrame,const char *mask_name);


/* The following function finds the optimal shift for the FF 
   in order to use them as a physical model for the perpendicular
   shape of the PSF on the CCD by calculating the correlation 
   between Science Frame and FF. The correlation takes place between 
   a synthetic frame with gaussian-shaped fibres centered on 
   shifted fibre positions and the actual, real frame on which the shift is
   to be found. In this version, the shift is searched separately for
   the fibres present in each of the frames composing the SingleFF structure, 
   with respect to what is supposed to be an all fibres flat field frame.
   The y shifts are returned in the ydelta vector, and the deltamask vector
   is set to TRUE for the frames for which a well defined shift was found, 
   FALSE for the frames for which no correlation maximum could be found 
   for whatever reason.
 */

#endif
