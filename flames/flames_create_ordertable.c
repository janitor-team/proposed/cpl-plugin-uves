/* @(#)create_ordertable.c */
/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_create_ordertable   Follow orders. Create and fill in table OTAB
 *
 */
/*-------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <flames_create_ordertable.h>

#include <flames_midas_tblsys.h>
#include <flames_midas_tblerr.h>
#include <flames_midas_tbldef.h>
#include <flames_midas_macrogen.h>
#include <flames_midas_atype.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_create_ordertable()  
   @short Follow orders. Create and fill in table OTAB            
   @author I. Porceddu  -  ITAL_FLAMES Consortium Ported to CPL by A. Modigliani                    
   @param OTAB    User-defined name of the order table. Empty order table. 
   @param MAXROWS Maximum rows. 
   @param MAXCOLS Maximum columns. 
   Columns are created 
                                                                
   @return 0 if success

                                                                         
   DRS Functions called: none                                             
                                                                        
   Pseudocode:                                                            
   Initialize an order table                                           

   @note
*/

int 
flames_create_ordertable(
                         const char *OTAB,
			 const int *MAXROWS,
			 const int *MAXCOLS)
{
  char ordtab[TEXT_LEN+1];

  int unit=0;
  int null=0;
  int ordcol=0;
  int xcol=0;
  int ycol=0;
  int yfitcol=0;
  int deltacol=0;
  int fibrecol=0; 
  int ordfibcol=0;
  int tid=0;
  int actvals=0;
  int maxrows=0;
  int maxcols=0;

  memset(ordtab, '\0', TEXT_LEN+1);
  
  SCSPRO("create_ordertable");

  SCKGETC(OTAB, 1, 60, &actvals, ordtab); /* User defined */
     
  SCKRDI(MAXROWS, 1, 1, &actvals, &maxrows, &unit, &null);
  SCKRDI(MAXCOLS, 1, 1, &actvals, &maxcols, &unit, &null);

  TCTINI (ordtab, F_O_MODE, maxrows, &tid);
  TCCINI (tid, D_R4_FORMAT, 1, "I6", "  ", "ORDER", &ordcol);
  TCCINI (tid, D_R4_FORMAT, 1, "I6", "  ", "X",     &xcol);
  TCCINI (tid, D_R4_FORMAT, 1, "I6", "  ", "Y",     &ycol);

  TCCINI(tid, D_R4_FORMAT, 1, "f7.2", " ", "YFIT", &yfitcol);
  TCCINI(tid, D_R4_FORMAT, 1, "f7.2", " ", "RESIDUAL", &deltacol);

  TCCINI (tid, D_R4_FORMAT, 1, "I6", "  ", "FIBRE", &fibrecol);

  TCCINI (tid, D_C_FORMAT, 21, "A", "  ", "ORDERFIB", &ordfibcol);
  TCTCLO(tid);

  SCSEPI();
  return(0);
}

/**@}*/



