/*
 * This file is part of the UVES Pipeline
 * Copyright (C) 2002, 2003, 2004, 2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:31:29 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2007/06/22 11:28:42  jmlarsen
 * Exported function for unit testing
 *
 * Revision 1.10  2007/06/20 11:11:57  jmlarsen
 * Moved flames_reset_crval_to_one()
 *
 * Revision 1.9  2007/06/06 08:17:32  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.8  2007/06/04 06:25:03  amodigli
 * added flames_fix_estention() & flames_reset_crval_to_one()
 *
 * Revision 1.7  2007/05/31 16:06:43  jmlarsen
 * Exported flames_write_descr
 *
 * Revision 1.6  2007/04/24 12:49:34  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.5  2007/04/10 07:27:46  jmlarsen
 * Updated flames_dfs_set_history_val()
 *
 * Revision 1.4  2007/04/03 11:07:52  jmlarsen
 * Merged table FITS headers when loading
 *
 * Revision 1.3  2007/03/15 15:46:29  jmlarsen
 * Export set_history_val
 *
 * Revision 1.2  2007/01/10 11:08:34  jmlarsen
 * Don't define DRS_USE_ORDEF
 *
 * Revision 1.1  2006/11/07 14:01:04  jmlarsen
 * Moved flames_load_ functions to separate source file
 *
 * Revision 1.83  2006/10/24 14:05:25  jmlarsen
 * Generalized load functions to support FLAMES
 *
 * Revision 1.82  2006/10/19 13:53:25  jmlarsen
 * Changed guess line table tag to LINE_GUESS_TAB
 *
 * Revision 1.81  2006/10/12 11:37:28  jmlarsen
 * Temporarily disabled FLAMES code generation
 *
 * Revision 1.80  2006/10/02 08:34:06  jmlarsen
 * Added REF_TFLAT
 *
 * Revision 1.79  2006/09/27 13:13:24  jmlarsen
 * Use dynamic memory allocation to store bad pixels
 *
 * Revision 1.78  2006/09/20 15:42:18  jmlarsen
 * Implemented MASTER_RESPONSE support
 *
 * Revision 1.77  2006/09/20 10:57:19  jmlarsen
 * Fixed typo
 *
 * Revision 1.76  2006/09/20 07:26:54  jmlarsen
 * Shortened max line length
 *
 * Revision 1.75  2006/09/19 14:25:32  jmlarsen
 * Propagate FITS keywords from master flat, not science, to WCALIB_FLAT_OBJ
 *
 * Revision 1.74  2006/09/19 07:12:58  jmlarsen
 * Exported function uves_write_statistics
 *
 * Revision 1.73  2006/09/14 08:46:51  jmlarsen
 * Added support for TFLAT, SCREEN_FLAT frames
 *
 * Revision 1.72  2006/09/11 08:19:10  jmlarsen
 * Renamed identifier reserved by POSIX
 *
 * Revision 1.71  2006/08/24 11:36:58  jmlarsen
 * Write recipe start/stop time to header
 *
 * Revision 1.70  2006/08/17 13:56:52  jmlarsen
 * Reduced max line length
 *
 * Revision 1.69  2006/08/11 08:59:41  jmlarsen
 * Take into account the different meanings of line table 'Y' column
 *
 * Revision 1.68  2006/08/07 14:42:02  jmlarsen
 * Implemented on-the-fly correction of a line table when its order
 * numbering is inconsistent with the order table (DFS02694)
 *
 * Revision 1.67  2006/08/07 12:14:19  jmlarsen
 * Removed unused function
 *
 * Revision 1.66  2006/08/01 14:43:36  amodigli
 * fixed bug loading fitsheader in uves_load_masterformatcheck
 *
 * Revision 1.65  2006/07/31 06:29:05  amodigli
 * added flames_load_frame_index
 *
 * Revision 1.64  2006/07/14 12:20:10  jmlarsen
 * Added LINE_INTMON_TABLE
 *
 * Revision 1.63  2006/06/26 07:54:15  amodigli
 * flames_load_image flames_load_table
 *
 * Revision 1.62  2006/06/23 15:31:32  amodigli
 * added useful stuff for flames
 *
 * Revision 1.61  2006/06/22 15:25:35  amodigli
 * changes for flames_cal_prep_sff_ofpos
 *
 * Revision 1.60  2006/06/19 06:51:14  amodigli
 * added support flames-old format
 *
 * Revision 1.59  2006/06/16 08:22:01  jmlarsen
 * Manually propagate ESO.DET. keywords from 1st/2nd input header
 *
 * Revision 1.58  2006/06/13 11:55:06  jmlarsen
 * Shortened max line length
 *
 * Revision 1.57  2006/06/06 09:04:14  jmlarsen
 * Bugfix: UVES_CHIP_REDU -> UVES_CHIP_REDL
 *
 * Revision 1.56  2006/06/06 08:40:10  jmlarsen
 * Shortened max line length
 *
 * Revision 1.55  2006/05/22 06:47:15  amodigli
 * fixed some bugs on msflat
 *
 * Revision 1.54  2006/05/19 13:07:52  amodigli
 * modified to support SFLATs
 *
 * Revision 1.53  2006/05/17 09:54:55  amodigli
 * added supposr SFLATs
 *
 * Revision 1.52  2006/05/15 06:09:52  amodigli
 * added support for some FLAMES input frames
 *
 * Revision 1.51  2006/04/25 14:58:48  amodigli
 * added paf creation functionalities
 *
 * Revision 1.50  2006/04/24 09:19:01  jmlarsen
 * Simplified code
 *
 * Revision 1.49  2006/04/06 13:13:04  jmlarsen
 * Added const modifier
 *
 * Revision 1.48  2006/04/06 12:56:50  jmlarsen
 * Added support for PDARK, IFLAT, DLFAT frames
 *
 * Revision 1.47  2006/04/06 11:48:17  jmlarsen
 * Support for SCI_POINT_-, SCI_EXTND_- and SCI_SLICER-frames
 *
 * Revision 1.46  2006/04/06 09:48:15  amodigli
 * changed uves_frameset_insert interface to have QC log
 *
 * Revision 1.45  2006/04/06 08:31:15  jmlarsen
 * Added support for reading MASTER_DFLAT, MASTER_IFLAT, MASTER_PDARK
 *
 * Revision 1.44  2006/03/24 14:19:19  jmlarsen
 * Recognize background tables
 *
 * Revision 1.43  2006/03/06 09:22:43  jmlarsen
 * Added support for reading MIDAS line tables with MIDAS tags
 *
 * Revision 1.42  2006/02/15 13:19:15  jmlarsen
 * Reduced source code max. line length
 *
 * Revision 1.41  2006/01/09 15:22:55  jmlarsen
 * Removed some warnings
 *
 * Revision 1.40  2006/01/03 16:56:53  amodigli
 * Added MASTER_ARC_FORM
 *
 * Revision 1.39  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */

#ifndef FLAMES_CAL_PREP_SFF_OFPOS_IMPL_H
#define FLAMES_CAL_PREP_SFF_OFPOS_IMPL_H

/*-----------------------------------------------------------------------------
                                Includes
  -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                Functions prototypes
 -----------------------------------------------------------------------------*/

void
flames_ex_bin_tab(const cpl_frame *in_ima_frm,
                  const cpl_frame *in_tab_frm,
                  const char *out_tab);

#endif
