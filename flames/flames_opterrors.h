/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_OPT_ERRORS_H
#define FLAMES_OPT_ERRORS_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include<flames_uves.h>
/* 
   ------------------------------------------------
   The following function is used in the optimal extraction procedure
   ------------------------------------------------
 */


/* 
   The following function computes the covariances following invocation of
   Opt_Extract() above
 */

flames_err 
opterrors(flames_frame *, 
          allflats *, 
          orderpos *, 
          int32_t, 
          frame_mask **, 
          double **, 
          double **,
          int32_t *, 
          int32_t *, 
          int32_t, 
          int32_t);

#endif
