/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */


#ifndef FLAMES_STRIP_TBL_EXT_H
#define FLAMES_STRIP_TBL_EXT_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>

/* this function takes an input string and a pointer to an output string;
   if the input string has a .tbl extension, it is stripped and the 
   result put in the output string; if it contains no MIDAS style extension,
   the input string is copied unchanged; if it contains an extension 
   other than .tbl, an error is issued. */

flames_err 
striptblext(char *tablename, char *basename);

#endif
