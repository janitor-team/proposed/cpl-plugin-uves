/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_H
#define FLAMES_H

#include <cpl.h>
#include <stdint.h>
/*-----------------------------------------------------------------------------
                                   Defines
 ----------------------------------------------------------------------------*/
#define FLAMES_DEBUG 0

#define FLAMES_CAL_ORDERPOS_ID flames_cal_orderpos
#define FLAMES_CAL_ORDERPOS_DOM "OrderPos"

#define FLAMES_MKMASTER_ID flames_cal_mkmaster
#define FLAMES_MKMASTER_DOM "mkmaster"

#define FLAMES_CAL_PREP_SFF_OFPOS_ID flames_cal_prep_sff_ofpos
#define FLAMES_CAL_PREP_SFF_OFPOS_DOM "OFPos"

#define FLAMES_CAL_PREDICT_ID flames_cal_predict
#define FLAMES_CAL_PREDICT_DOM "Predict"

#define FLAMES_CAL_WAVECAL_ID flames_cal_wavecal
#define FLAMES_CAL_WAVECAL_DOM "Wavecal"

#define FLAMES_OBS_SCIRED_ID flames_obs_scired
#define FLAMES_OBS_SCIRED_DOM "SciRed"

#define FLAMES_OBS_REDCHAIN_ID flames_obs_redchain
#define FLAMES_OBS_REDCHAIN_DOM "RedChain"

#define FLAMES_EXTRACT_ID flames_extract
#define FLAMES_EXTRACT_DOM "extract"

#define FLAMES_FILLORDTAB_ID flames_fillordtab
#define FLAMES_FILLORDTAB_DOM "fillordtab"

#define FLAMES_MES_SLITFF_SIZE_ID flames_mes_slitff_size
#define FLAMES_MES_SLITFF_SIZE_DOM "mes_slitff_size"

#define FLAMES_PREPBKG_ID flames_prepbkg
#define FLAMES_PREPBKG_DOM "prepbkg"

#define FLAMES_PREPFIBREFF_ID flames_prepfibreff
#define FLAMES_PREPFIBREFF_DOM "prepfibreff"

#define FLAMES_PREPNORM_ID flames_prepnorm
#define FLAMES_PREPNORM_DOM "prepnorm"

#define FLAMES_PREPPA_ID flames_preppa
#define FLAMES_PREPPA_DOM "preppa"


#define FLAMES_PREPSLITFF_ID flames_prepslitff
#define FLAMES_PREPSLITFF_DOM "prepslitff"

#define FLAMES_UTL_UNPACK_ID flames_utl_unpack
#define FLAMES_UTL_UNPACK_DOM "unpack"


int flames_utl_unpack_get_info(cpl_pluginlist *);
int flames_cal_orderpos_get_info(cpl_pluginlist *);
int flames_cal_predict_get_info(cpl_pluginlist *);
int flames_cal_prep_sff_ofpos_get_info(cpl_pluginlist *);
int flames_cal_wavecal_get_info(cpl_pluginlist *);
int flames_cal_mkmaster_get_info(cpl_pluginlist *);
int flames_obs_scired_get_info(cpl_pluginlist *);
int flames_obs_redchain_get_info(cpl_pluginlist *);

int flames_extract_get_info(cpl_pluginlist *);
int flames_fillordtab_get_info(cpl_pluginlist *);
int flames_mes_slitff_size_get_info(cpl_pluginlist *);
int flames_prepbkg_get_info(cpl_pluginlist *);
int flames_prepfibreff_get_info(cpl_pluginlist *);
int flames_prepnorm_get_info(cpl_pluginlist *);
int flames_preppa_get_info(cpl_pluginlist *);
int flames_prepslitff_get_info(cpl_pluginlist *);
#endif /* FLAMES_H */


