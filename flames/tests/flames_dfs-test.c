/*                                                                            *
 *   This file is part of the ESO UVES Pipeline                               *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:31:05 $
 * $Revision: 1.13 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.11  2009/06/05 05:55:26  amodigli
 * updated init/end to cpl5
 *
 * Revision 1.10  2007/08/13 12:11:32  amodigli
 * added support CPL4
 *
 * Revision 1.9  2007/06/26 13:38:54  jmlarsen
 * Fixed statistics
 *
 * Revision 1.8  2007/06/25 15:46:44  jmlarsen
 * Added unit test
 *
 * Revision 1.7  2007/06/22 15:28:01  jmlarsen
 * Support read/write of D_I2_FORMAT
 *
 * Revision 1.6  2007/06/22 14:53:26  jmlarsen
 * Expanded, again, interface of uves_save_image()
 *
 * Revision 1.5  2007/06/22 07:10:43  amodigli
 * shorten lines length
 *
 * Revision 1.4  2007/06/15 14:00:43  jmlarsen
 * Changed flames_fix_estension
 *
 * Revision 1.3  2007/04/24 12:49:34  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.2  2007/04/10 07:04:44  jmlarsen
 * Fixed memory leak
 *
 * Revision 1.1  2007/03/15 15:07:20  jmlarsen
 * Added flames_dfs-test
 *
 * Revision 1.1  2007/03/15 12:27:23  jmlarsen
 * Moved unit tests to ./uves/tests and ./flames/tests
 *
 * Revision 1.3  2007/03/05 10:11:52  jmlarsen
 * Added test
 *
 * Revision 1.2  2007/02/27 14:04:14  jmlarsen
 * Move unit test infrastructure to IRPLIB
 *
 * Revision 1.1  2007/01/29 12:17:26  jmlarsen
 * Added
 *
 * Revision 1.2  2006/11/16 09:49:25  jmlarsen
 * Fixed doxygen bug
 *
 * Revision 1.1  2006/11/07 14:02:20  jmlarsen
 * Added FLAMES library unit test
 *
 * Revision 1.18  2006/11/06 15:30:54  jmlarsen
 * Added missing includes
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/

#include <flames_dfs.h>

#include <uves_utils_wrappers.h>
#include <uves_dfs.h>
#include <uves_error.h>
#include <uves_parameters.h>
#include <cpl_test.h>

#include <cpl.h>

/*----------------------------------------------------------------------------
                                Defines
 ----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/**
 * @defgroup flames_dfs_test  FLAMES DFS unit tests
 */
/*---------------------------------------------------------------------------*/
/**@{*/
static void
test_set_history_val(void)
{
    uves_propertylist *header = uves_propertylist_new();
    char type = 'I';
    const char *value = "9";
    const char *name = "MAXFIBRES";

    check_nomsg( flames_dfs_set_history_val(header, type,
                                            name, value));

    cpl_test_eq( uves_propertylist_get_size(header), 3 );

    value = "15";

    check_nomsg( flames_dfs_set_history_val(header, type,
                                            name, value));

    cpl_test_eq( uves_propertylist_get_size(header), 3 );

  cleanup:
    uves_free_propertylist(&header);
    return;
}

/*---------------------------------------------------------------------------*/
/**
 * @brief   test filename
 */
/*---------------------------------------------------------------------------*/
static void
test_filename(void)
{
    const char *filename1 = "pippo.bdf";
    const char *filename2 = "pappo.fits";
    const char *out1 = flames_fix_estention(filename1);
    const char *out2 = flames_fix_estention(filename2);

    cpl_test_eq_string(out1, "pippo.fits");
    cpl_test_eq_string(out2, "pappo.fits");

    uves_free_string_const(&out1);
    uves_free_string_const(&out2);

    return;
}

/*----------------------------------------------------------------------------*/
/**
   @brief test pipeline product creation
 */
/*----------------------------------------------------------------------------*/
static void
test_save_frame(void)
{
    cpl_frameset *frames = cpl_frameset_new();
    cpl_parameterlist *parameters = cpl_parameterlist_new();
    int nx = 1500;
    int ny = 1;
    int nkey = 360;
    cpl_image *image = cpl_image_new(nx, ny, CPL_TYPE_INT);
    uves_propertylist *raw_header = uves_propertylist_new();
    uves_propertylist *product_header = uves_propertylist_new();
    const char *starttime;
    const char *recipe_id = "uves_cal_phony";
    const char *tag = "PHONY_TAG";
    const char *raw_filename = "raw_file.fits";

    uves_define_global_parameters(parameters);
    cpl_test_eq( cpl_error_get_code(), CPL_ERROR_NONE );

    /* Create raw image */
    {
        cpl_image *raw_image = cpl_image_new(nx, ny, CPL_TYPE_DOUBLE);
        cpl_frame *raw_frame = cpl_frame_new();

        {
            int i;
            for (i = 0; i < nkey; i++)
                {
                    const char *key_name = uves_sprintf("KEY%d", i);
                    uves_propertylist_append_int(raw_header, key_name, i);
                    uves_free_string_const(&key_name);
                }
            uves_propertylist_append_string(raw_header, "ORIGIN", "unknown...");
        }

        uves_image_save(raw_image, 
                        raw_filename,
                        CPL_BPP_IEEE_FLOAT,
                        raw_header,
                        CPL_IO_DEFAULT);
        cpl_test_eq( cpl_error_get_code(), CPL_ERROR_NONE );
        
        uves_free_image(&raw_image);

        /* Wrap frame */
        cpl_frame_set_tag(raw_frame, "BIAS_BLUE"); /* Use recognized tag,
                                                      so that FRAME_TYPE
                                                      is set to RAW */
        cpl_frame_set_filename(raw_frame, raw_filename);
        cpl_frameset_insert(frames, raw_frame);
    }

    starttime = uves_initialize(frames, parameters, recipe_id,
                                "This recipe does not do anything");
    cpl_test_eq( cpl_error_get_code(), CPL_ERROR_NONE );

    /* Create product file */
    uves_propertylist_append_string(product_header, "ESO DPR TYPE", "Echelle");
    check_nomsg( uves_save_image(image, "dfs_product.fits",
                                 product_header, true, true));

    check_nomsg( 
        flames_frameset_insert(frames, 
                               CPL_FRAME_GROUP_PRODUCT,
                               CPL_FRAME_TYPE_IMAGE,
                               CPL_FRAME_LEVEL_INTERMEDIATE,
                               "dfs_product.fits",
                               tag,
                               raw_header,
                               parameters, 
                               recipe_id,
                               PACKAGE "/" PACKAGE_VERSION,
                               NULL, /* qc table */
                               starttime,
                               false,   /* dump PAF */
                               CPL_STATS_MAX | CPL_STATS_MIN));
    
    cpl_test_eq( cpl_error_get_code(), CPL_ERROR_NONE );
    cpl_test( cpl_frameset_find(frames, tag) != NULL);

  cleanup:
    uves_free_frameset(&frames);
    uves_free_parameterlist(&parameters);
    uves_free_image(&image);
    uves_free_propertylist(&raw_header);
    uves_free_propertylist(&product_header);
    uves_free_string_const(&starttime);
    return;
}
    
/*---------------------------------------------------------------------------*/
/**
  @brief   Test of dfs module
**/
/*---------------------------------------------------------------------------*/

int main(void)
{
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);
    cpl_errorstate initial_errorstate = cpl_errorstate_get();
    check_nomsg( test_set_history_val());
    check_nomsg( test_filename() );
    check_nomsg( test_save_frame());

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE)
        {
	  cpl_errorstate_dump(initial_errorstate,CPL_FALSE,NULL);
        }
    return cpl_test_end(0);
}

/**@}*/
