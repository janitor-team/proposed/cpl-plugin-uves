/*                                                                            *
 *   This file is part of the ESO UVES Pipeline                               *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */
 
/*
 * $Author: amodigli $
 * $Date: 2012-10-13 14:22:25 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.2  2012/10/10 06:40:00  amodigli
 * using flames_set_fibremask from flames_dfs.c and replaced sprintf by snprintf for portability
 *
 * Revision 1.1  2012/10/08 11:36:53  amodigli
 * added to CVS
 *
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/

#include <flames_utils.h>

#include <uves_error.h>
#include <uves_utils_wrappers.h>
#include <cpl_test.h>

#include <cpl.h>
#include <string.h>

#include <float.h>
/*----------------------------------------------------------------------------
                                Defines
 ----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/**
 * @defgroup flames_test  FLAMES unit tests
 */
/*---------------------------------------------------------------------------*/
/**@{*/
/**
          @brief test version function
*/
static cpl_table*
flames_crea_ozpoz_table(const char* type, const char* mode, const int wave,const int plate,const int new)
{
  cpl_table* result;
  int maxfibres=9;
  int* pbut=0;
  const char *ozpoz_tab_filename = "ozpoz.fits";
  char filename[80];
  uves_msg("test type %s mode %s wave %d plate %d new %d",
	   type,mode,wave,plate,new);

  int b37=37;
  if(plate==2 && new ==1) b37=0;

  result=cpl_table_new(maxfibres);
  cpl_table_new_column(result,"BUTTON",CPL_TYPE_INT);
  cpl_table_fill_column_window_int(result,"BUTTON",0,9,0);
  pbut=cpl_table_get_data_int(result,"BUTTON");

  if(wave == 520 ) {
    if (strcmp(type,"ODD") == 0 ) {
      pbut[0]=3;
      pbut[1]=b37;
      pbut[2]=69;
    } else if (strcmp(type,"EVEN") == 0 ) {
      pbut[0]=135;
      pbut[1]=169;
      pbut[2]=201;
    } else if (strcmp(type,"ALL") == 0 ) {
      pbut[0]=3;
      pbut[1]=b37;
      pbut[2]=69;
      pbut[3]=135;
      pbut[4]=169;
      pbut[5]=201;
    } else if (strcmp(type,"WAVE") == 0 ) {
      pbut[0]=3;
      pbut[1]=b37;
      pbut[2]=69;
      pbut[3]=135;
      pbut[4]=169;
      pbut[5]=201;
    } else if (strcmp(type,"OBJECT") == 0 ) {
      pbut[0]=3;
      pbut[1]=b37;
      pbut[2]=69;
      pbut[3]=135;
      pbut[4]=169;
      pbut[5]=201;
    }
  } else if (wave == 580 ) {
    if (strcmp(mode,"OzPoz") == 0 ) {
 
      if (strcmp(type,"ODD") == 0 ) {
	pbut[0]=3;
	pbut[1]=b37;
	pbut[2]=69;
	pbut[3]=103;
      } else if (strcmp(type,"EVEN") == 0 ) {
	pbut[0]=135;
	pbut[1]=169;
	pbut[2]=201;
	pbut[3]=235;
      } else if (strcmp(type,"ALL") == 0 ) {
	pbut[0]=3;
	pbut[1]=b37;
	pbut[2]=69;
	pbut[3]=103;
	pbut[4]=135;
	pbut[5]=169;
	pbut[6]=201;
	pbut[7]=235;
      } else if (strcmp(type,"WAVE") == 0 ) {
	pbut[0]=3;
	pbut[1]=b37;
	pbut[2]=69;
	pbut[3]=103;
	pbut[4]=135;
	pbut[5]=169;
	pbut[6]=201;
	pbut[7]=235;
      } else if (strcmp(type,"OBJECT") == 0 ) {
	pbut[0]=3;
	pbut[1]=b37;
	pbut[2]=69;
	pbut[3]=103;
	pbut[4]=135;
	pbut[5]=169;
	pbut[6]=201;
	pbut[7]=235;
      }

    } else if (strcmp(mode,"SimCal") == 0 ) {

      if (strcmp(type,"ODD") == 0 ) {
	pbut[0]=3;
	pbut[1]=b37;
	pbut[2]=69;
	pbut[3]=103;
      } else if (strcmp(type,"EVEN") == 0 ) {
	pbut[0]=135;
	pbut[1]=169;
	pbut[2]=201;
      } else if (strcmp(type,"ALL") == 0 ) {
	pbut[0]=3;
	pbut[1]=b37;
	pbut[2]=69;
	pbut[3]=103;
	pbut[4]=135;
	pbut[5]=169;
	pbut[6]=201;
      } else if (strcmp(type,"WAVE") == 0 ) {
	pbut[0]=3;
	pbut[1]=b37;
	pbut[2]=69;
	pbut[3]=103;
	pbut[4]=135;
	pbut[5]=169;
	pbut[6]=201;
      } else if (strcmp(type,"OBJECT") == 0 ) {
	pbut[0]=3;
	pbut[1]=b37;
	pbut[2]=69;
	pbut[3]=103;
	pbut[4]=135;
	pbut[5]=169;
	pbut[6]=201;
      }

    }

  } else if (wave == 860 ) {

    if (strcmp(type,"ODD") == 0 ) {
      pbut[0]=3;
      pbut[1]=b37;
      pbut[2]=69;
      pbut[3]=103;
    } else if (strcmp(type,"EVEN") == 0 ) {
      pbut[0]=135;
      pbut[1]=169;
      pbut[2]=201;
      pbut[3]=235;
    } else if (strcmp(type,"ALL") == 0 ) {
      pbut[0]=3;
      pbut[1]=b37;
      pbut[2]=69;
      pbut[3]=103;
      pbut[4]=135;
      pbut[5]=169;
      pbut[6]=201;
      pbut[7]=235;
    } else if (strcmp(type,"WAVE") == 0 ) {
      pbut[0]=3;
      pbut[1]=b37;
      pbut[2]=69;
      pbut[3]=103;
      pbut[4]=135;
      pbut[5]=169;
      pbut[6]=201;
      pbut[7]=235;
    } else if (strcmp(type,"OBJECT") == 0 ) {
      pbut[0]=3;
      pbut[1]=b37;
      pbut[2]=69;
      pbut[3]=103;
      pbut[4]=135;
      pbut[5]=169;
      pbut[6]=201;
      pbut[7]=235;
    }

  }
  /*
  cpl_table_and_selected_int(result,"BUTTON",CPL_EQUAL_TO,0);
  cpl_table_erase_selected(result);
  */
  //cpl_table_dump(result,0,8,stdout);
  sprintf(filename,"%s_%s_%s",type,mode,ozpoz_tab_filename);
  cpl_table_save(result, NULL,NULL,filename,CPL_IO_DEFAULT );

  return result;
}

static void
test_fibremask(void)
{
    cpl_table *table = NULL;
    const int size=40;
    char expected[size];
    int status=0;
    char fibremask[size];



    table=flames_crea_ozpoz_table("ODD","OzPoz",520,1,0);
    snprintf(expected,size,"%s","0,1,0,1,0,1,0,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    uves_free_table(&table);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ODD","OzPoz",520);
    
    table=flames_crea_ozpoz_table("EVEN","OzPoz",520,1,0);
    snprintf(expected,size,"%s","0,0,1,0,1,0,1,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","EVEN","OzPoz",520);
    uves_free_table(&table);

    
    table=flames_crea_ozpoz_table("ALL","OzPoz",520,1,0);
    snprintf(expected,size,"%s","0,1,1,1,1,1,1,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ALL","OzPoz",520);
    uves_free_table(&table);
    
    table=flames_crea_ozpoz_table("WAVE","OzPoz",520,1,0);
    snprintf(expected,size,"%s", "0,1,1,1,1,1,1,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","WAVE","OzPoz",520);
    uves_free_table(&table);

    

    table=flames_crea_ozpoz_table("OBJECT","OzPoz",520,1,0);
    snprintf(expected,size,"%s", "0,1,1,1,1,1,1,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",520);
    uves_free_table(&table);

    

    table=flames_crea_ozpoz_table("ODD","OzPoz",580,1,0);
    snprintf(expected,size,"%s", "0,1,0,1,0,1,0,1,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ODD","OzPoz",520);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("EVEN","OzPoz",580,1,0);
    snprintf(expected,size,"%s", "0,0,1,0,1,0,1,0,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","EVEN","OzPoz",520);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("ALL","OzPoz",580,1,0);
    snprintf(expected,size,"%s", "0,1,1,1,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ALL","OzPoz",520);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("WAVE","OzPoz",580,1,0);
    snprintf(expected,size,"%s", "0,1,1,1,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","WAVE","OzPoz",520);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("OBJECT","OzPoz",580,1,0);
    snprintf(expected, size,"%s","0,1,1,1,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",520);
    uves_free_table(&table);


    

    table=flames_crea_ozpoz_table("ODD","SimCal",580,1,0);
    snprintf(expected, size,"%s", "0,1,0,1,0,1,0,1,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ODD","SimCal",580);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("EVEN","SimCal",580,1,0);
    snprintf(expected, size,"%s","0,0,1,0,1,0,1,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","EVEN","SimCal",580);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("ALL","SimCal",580,1,0);
    snprintf(expected,size,"%s", "1,1,1,1,1,1,1,1,0");
    flames_set_fibremask(table,fibremask,size,1);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ALL","SimCal",580);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("WAVE","SimCal",580,1,0);
    snprintf(expected, size,"%s","1,1,1,1,1,1,1,1,0");
    flames_set_fibremask(table,fibremask,size,1);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","WAVE","SimCal",580);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("OBJECT","SimCal",580,1,0);
    snprintf(expected, size,"%s","1,1,1,1,1,1,1,1,0");
    flames_set_fibremask(table,fibremask,size,1);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","SimCal",580);
    uves_free_table(&table);


    

    table=flames_crea_ozpoz_table("ODD","OzPoz",860,1,0);
    snprintf(expected, size,"%s","0,1,0,1,0,1,0,1,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",860);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("EVEN","OzPoz",860,1,0);
    snprintf(expected, size,"%s","0,0,1,0,1,0,1,0,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",860);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("ALL","OzPoz",860,1,0);
    snprintf(expected,size,"%s", "0,1,1,1,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",860);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("WAVE","OzPoz",860,1,0);
    snprintf(expected, size,"%s","0,1,1,1,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",860);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("OBJECT","OzPoz",860,1,0);
    snprintf(expected, size,"%s","0,1,1,1,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",860);
    uves_free_table(&table);
    



    /* new setting: plate=2 button 37 is off */
    table=flames_crea_ozpoz_table("ODD","OzPoz",520,2,1);
    snprintf(expected,size,"%s","0,1,0,0,0,1,0,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    uves_free_table(&table);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ODD","OzPoz",520);
    
    table=flames_crea_ozpoz_table("EVEN","OzPoz",520,2,1);
    snprintf(expected, size,"%s","0,0,1,0,1,0,1,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","EVEN","OzPoz",520);
    uves_free_table(&table);

    
    table=flames_crea_ozpoz_table("ALL","OzPoz",520,2,1);
    snprintf(expected, size,"%s","0,1,1,0,1,1,1,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ALL","OzPoz",520);
    uves_free_table(&table);
    
    table=flames_crea_ozpoz_table("WAVE","OzPoz",520,2,1);
    snprintf(expected, size,"%s","0,1,1,0,1,1,1,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","WAVE","OzPoz",520);
    uves_free_table(&table);

    

    table=flames_crea_ozpoz_table("OBJECT","OzPoz",520,2,1);
    snprintf(expected, size,"%s","0,1,1,0,1,1,1,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",520);
    uves_free_table(&table);

    

    table=flames_crea_ozpoz_table("ODD","OzPoz",580,2,1);
    snprintf(expected, size,"%s","0,1,0,0,0,1,0,1,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ODD","OzPoz",520);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("EVEN","OzPoz",580,2,1);
    snprintf(expected, size,"%s","0,0,1,0,1,0,1,0,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","EVEN","OzPoz",520);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("ALL","OzPoz",580,2,1);
    snprintf(expected,size,"%s", "0,1,1,0,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ALL","OzPoz",520);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("WAVE","OzPoz",580,2,1);
    snprintf(expected, size,"%s","0,1,1,0,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","WAVE","OzPoz",520);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("OBJECT","OzPoz",580,2,1);
    snprintf(expected, size,"%s","0,1,1,0,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",520);
    uves_free_table(&table);


    

    table=flames_crea_ozpoz_table("ODD","SimCal",580,2,1);
    snprintf(expected,  size,"%s","0,1,0,0,0,1,0,1,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ODD","SimCal",580);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("EVEN","SimCal",580,2,1);
    snprintf(expected, size,"%s","0,0,1,0,1,0,1,0,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","EVEN","SimCal",580);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("ALL","SimCal",580,2,1);
    snprintf(expected, size,"%s","0,1,1,0,1,1,1,1,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","ALL","SimCal",580);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("WAVE","SimCal",580,2,1);
    snprintf(expected, size,"%s","0,1,1,0,1,1,1,1,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","WAVE","SimCal",580);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("OBJECT","SimCal",580,2,1);
    snprintf(expected, size,"%s","0,1,1,0,1,1,1,1,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","SimCal",580);
    uves_free_table(&table);


    

    table=flames_crea_ozpoz_table("ODD","OzPoz",860,2,1);
    snprintf(expected, size,"%s","0,1,0,0,0,1,0,1,0");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",860);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("EVEN","OzPoz",860,2,1);
    snprintf(expected, size,"%s","0,0,1,0,1,0,1,0,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",860);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("ALL","OzPoz",860,2,1);
    snprintf(expected, size,"%s","0,1,1,0,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",860);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("WAVE","OzPoz",860,2,1);
    snprintf(expected, size,"%s","0,1,1,0,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",860);
    uves_free_table(&table);

    table=flames_crea_ozpoz_table("OBJECT","OzPoz",860,2,1);
    snprintf(expected, size,"%s","0,1,1,0,1,1,1,1,1");
    flames_set_fibremask(table,fibremask,size,0);
    uves_msg("expected fibremask='%s'",expected);
    uves_msg("computed fibremask='%s'",fibremask);
    status=strcmp(expected,fibremask);
    assure(status == 0,CPL_ERROR_ILLEGAL_OUTPUT,"fail case %s %s %d","OBJECT","OzPoz",860);
    uves_free_table(&table);
    


 cleanup:
    return;
}
    
/*---------------------------------------------------------------------------*/
/**
  @brief   Test of FLAMES library

  This test exists just to verify that the flames library can be loaded/run
**/
/*---------------------------------------------------------------------------*/

int main(void)
{
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    check( test_fibremask(),
	   "Fibremask test failed");

  cleanup:
    return cpl_test_end(0);
}

/**@}*/
