/*                                                                            *
 *   This file is part of the ESO UVES Pipeline                               *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                           */
 
/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:31:05 $
 * $Revision: 1.20 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.18  2009/08/04 09:14:50  amodigli
 * swapped tests of msffsz
 *
 * Revision 1.17  2009/06/05 05:55:26  amodigli
 * updated init/end to cpl5
 *
 * Revision 1.16  2008/12/23 08:32:43  amodigli
 * replace // with proper C comments
 *
 * Revision 1.15  2008/09/29 06:53:43  amodigli
 * add #include <string.h>
 *
 * Revision 1.14  2008/02/25 07:21:58  amodigli
 * fix interface msffsz
 *
 * Revision 1.13  2008/01/30 17:12:55  amodigli
 * change test case for test_flames_msffsz2
 *
 * Revision 1.12  2008/01/27 13:49:46  amodigli
 * added test_flames_msffsz2
 *
 * Revision 1.11  2008/01/25 12:23:04  amodigli
 * added test_flames_msffsz
 *
 * Revision 1.10  2008/01/22 07:56:32  amodigli
 * fixed warnings and disactivated tests requiring external input
 *
 * Revision 1.9  2007/10/01 17:10:30  amodigli
 * added test_cubify
 *
 * Revision 1.8  2007/09/17 16:05:32  amodigli
 * fixed some bugs, added messaging
 *
 * Revision 1.7  2007/09/17 08:29:47  amodigli
 * added test cubify
 *
 * Revision 1.6  2007/08/30 08:18:47  amodigli
 * fixed some doxygen warnings
 *
 * Revision 1.5  2007/08/13 12:11:32  amodigli
 * added support CPL4
 *
 * Revision 1.4  2007/07/04 06:40:48  jmlarsen
 * Replaced CPL_BPP_DEFAULT with CPL_BPP_IEEE_FLOAT
 *
 * Revision 1.3  2007/07/03 14:02:35  jmlarsen
 * Fixed memory leak
 *
 * Revision 1.2  2007/06/25 16:00:06  jmlarsen
 * Added doc.
 *
 * Revision 1.1  2007/06/25 15:46:40  jmlarsen
 * Added unit test
 *
 * Revision 1.4  2007/06/15 14:00:43  jmlarsen
 * Changed flames_fix_estension
 *
 * Revision 1.3  2007/04/24 12:49:34  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.2  2007/04/10 07:04:44  jmlarsen
 * Fixed memory leak
 *
 * Revision 1.1  2007/03/15 15:07:20  jmlarsen
 * Added flames_dfs-test
 *
 * Revision 1.1  2007/03/15 12:27:23  jmlarsen
 * Moved unit tests to ./uves/tests and ./flames/tests
 *
 * Revision 1.3  2007/03/05 10:11:52  jmlarsen
 * Added test
 *
 * Revision 1.2  2007/02/27 14:04:14  jmlarsen
 * Move unit test infrastructure to IRPLIB
 *
 * Revision 1.1  2007/01/29 12:17:26  jmlarsen
 * Added
 *
 * Revision 1.2  2006/11/16 09:49:25  jmlarsen
 * Fixed doxygen bug
 *
 * Revision 1.1  2006/11/07 14:02:20  jmlarsen
 * Added FLAMES library unit test
 *
 * Revision 1.18  2006/11/06 15:30:54  jmlarsen
 * Added missing includes
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <flames_cal_prep_sff_ofpos_impl.h>
#include <uves_utils_wrappers.h>
#include <uves_error.h>
#include <uves_globals.h>
#include <uves_pfits.h>
#include <uves_dfs.h>
#include <uves_dump.h>
#include <flames_utils_science.h>
#include <flames_utils.h>
#include <cpl_test.h>
#include <string.h>
#include <cpl.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup flames_prep_sff_ofpos_test  FLAMES unit tests for multi-fibre tracing recipe
 */
/*----------------------------------------------------------------------------*/
/**@{*/


/*---------------------------------------------------------------------------*/
/**
  @brief   Test flames_msffsz
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/
static int
test_flames_msffsz2(void)
{

  cpl_frame* frm=NULL;

  const char* file="msff1_redl.fits";
  const char* table="rofl.fits";
  enum uves_chip chip=UVES_CHIP_REDL;
  const int flat_save_size=2;

  check_nomsg(frm=cpl_frame_new());
  check_nomsg(cpl_frame_set_filename(frm,file));
  check_nomsg(cpl_frame_set_type(frm,CPL_FRAME_TYPE_IMAGE));
  check_nomsg( msffsz_flames2(frm, flat_save_size,table, chip) );

 cleanup:
  uves_free_frame(&frm);

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }



}

/*---------------------------------------------------------------------------*/
/**
  @brief   Test flames_msffsz
  This test exists just to verify that function works
**/
static int
test_flames_msffsz(void)
{

  cpl_frame* frm=NULL;

  const char* file="msff3_redu.fits";
  const char* table="rofu.fits";
  enum uves_chip chip=UVES_CHIP_REDL;
  const int flat_save_size=-2;

  check_nomsg(frm=cpl_frame_new());
  check_nomsg(cpl_frame_set_filename(frm,file));
  check_nomsg(cpl_frame_set_type(frm,CPL_FRAME_TYPE_IMAGE));
  check_nomsg(flames_get_mff_hw_and_yshift(frm, flat_save_size,table) );

 cleanup:
  uves_free_frame(&frm);

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }



}





static int
test_flames_my_cubify(void)
{
  char tag[MED_NAME_SIZE];
  char ibase[MED_NAME_SIZE];
  char file_com[MED_NAME_SIZE];
  char file_out[MED_NAME_SIZE];
  char file_inp[MED_NAME_SIZE];
  char prefix[MED_NAME_SIZE];
  int nflats=0;
  uves_propertylist* plist=NULL; 
  enum uves_chip chip = UVES_CHIP_REDU;
  cpl_frame* frm=NULL;
  cpl_frameset* set=NULL;
  cpl_frameset* set_out=NULL;
  cpl_image* img=NULL;
  cpl_imagelist* iml=NULL;
  int i=0;
  int k=0;

  sprintf(prefix,"%s%c","slitff_",uves_chip_tochar(chip));
  sprintf(file_out,"%s%s",prefix,"_dtc.fits");
  sprintf(file_com,"%s%s",prefix,"_common.fits");
  sprintf(ibase,"%s%s",prefix,"_data");

  check_nomsg(plist=uves_propertylist_load(file_com,0));
  check_nomsg(nflats=uves_flames_pfits_get_nflats(plist));
  uves_free_propertylist(&plist);


  /* SLITFF DATA */
  strcpy(tag,FLAMES_SLIT_FF_DTC(chip));
  check_nomsg(frm=cpl_frame_new());
  check_nomsg(cpl_frame_set_tag(frm,tag));
  check_nomsg(cpl_frame_set_filename(frm,file_out));
  check_nomsg(cpl_frame_set_group(frm,CPL_FRAME_GROUP_CALIB));

  check_nomsg(iml=cpl_imagelist_new());
  for(i=0;i<nflats;i++){
    k=i+1;
    sprintf(file_inp,"%s%2.2d%s",ibase,k,".fits");
    uves_msg_warning("file=%s",file_inp);
    check_nomsg(img=cpl_image_load(file_inp,CPL_TYPE_FLOAT,0,0));
    cpl_imagelist_set(iml,img,i);
  }

  check_nomsg(uves_save_imagelist(iml,file_out,plist));

  check_nomsg(set=cpl_frameset_new());
  check_nomsg(set_out=cpl_frameset_new());
  cpl_frameset_insert(set,frm);


  uves_free_frameset(&set);
  check_nomsg(flames_add_desc_data(ibase,file_out,nflats,1));


  /* SLITFF SIGMA */
  sprintf(ibase,"%s%s",prefix,"_sigma");
  sprintf(file_out,"%s%s",prefix,"_sgc.fits");

  strcpy(tag,FLAMES_SLIT_FF_SGC(chip));
  check_nomsg(frm=cpl_frame_new());
  check_nomsg(cpl_frame_set_tag(frm,tag));
  check_nomsg(cpl_frame_set_filename(frm,file_out));
  check_nomsg(cpl_frame_set_group(frm,CPL_FRAME_GROUP_CALIB));

  check_nomsg(iml=cpl_imagelist_new());
  for(i=0;i<nflats;i++){
    k=i+1;
    sprintf(file_inp,"%s%2.2d%s",ibase,k,".fits");
    uves_msg_warning("file=%s",file_inp);
    check_nomsg(img=cpl_image_load(file_inp,CPL_TYPE_FLOAT,0,0));
    cpl_imagelist_set(iml,img,i);
  }

  check_nomsg(uves_save_imagelist(iml,file_out,plist));

  check_nomsg(set=cpl_frameset_new());
  check_nomsg(set_out=cpl_frameset_new());
  cpl_frameset_insert(set,frm);


  uves_free_frameset(&set);
  check_nomsg(flames_add_desc_sigma(ibase,file_out,nflats,1));


  /* SLITFF BADPIXEL */
  sprintf(ibase,"%s%s",prefix,"_badpixel");
  sprintf(file_out,"%s%s",prefix,"_bpc.fits");

  strcpy(tag,FLAMES_SLIT_FF_BPC(chip));
  check_nomsg(frm=cpl_frame_new());
  check_nomsg(cpl_frame_set_tag(frm,tag));
  check_nomsg(cpl_frame_set_filename(frm,file_out));
  check_nomsg(cpl_frame_set_group(frm,CPL_FRAME_GROUP_CALIB));

  check_nomsg(iml=cpl_imagelist_new());
  for(i=0;i<nflats;i++){
    k=i+1;
    sprintf(file_inp,"%s%2.2d%s",ibase,k,".fits");
    uves_msg_warning("file=%s",file_inp);
    check_nomsg(img=cpl_image_load(file_inp,CPL_TYPE_FLOAT,0,0));
    cpl_imagelist_set(iml,img,i);
  }

  check_nomsg(uves_save_imagelist(iml,file_out,plist));

  check_nomsg(set=cpl_frameset_new());
  check_nomsg(set_out=cpl_frameset_new());
  cpl_frameset_insert(set,frm);


  uves_free_frameset(&set);
  check_nomsg(flames_add_desc_bpmap(ibase,file_out,nflats,1));


  /* SLITFF BOUND */
  sprintf(ibase,"%s%s",prefix,"_bound");
  sprintf(file_out,"%s%s",prefix,"_bnc.fits");

  strcpy(tag,FLAMES_SLIT_FF_BPC(chip));
  check_nomsg(frm=cpl_frame_new());
  check_nomsg(cpl_frame_set_tag(frm,tag));
  check_nomsg(cpl_frame_set_filename(frm,file_out));
  check_nomsg(cpl_frame_set_group(frm,CPL_FRAME_GROUP_CALIB));

  check_nomsg(iml=cpl_imagelist_new());
  for(i=0;i<nflats;i++){
    k=i+1;
    sprintf(file_inp,"%s%2.2d%s",ibase,k,".fits");
    uves_msg_warning("file=%s",file_inp);
    check_nomsg(img=cpl_image_load(file_inp,CPL_TYPE_FLOAT,0,0));
    cpl_imagelist_set(iml,img,i);
  }

  check_nomsg(uves_save_imagelist(iml,file_out,plist));

  check_nomsg(set=cpl_frameset_new());
  check_nomsg(set_out=cpl_frameset_new());
  cpl_frameset_insert(set,frm);


  uves_free_frameset(&set);
  check_nomsg(flames_add_desc_bound(ibase,file_out,nflats,1));




  /* FIBFF DATA */
  sprintf(prefix,"%s%c","fibreff_",uves_chip_tochar(chip));
  sprintf(file_com,"%s%s",prefix,"_common.fits");
  sprintf(ibase,"%s%s",prefix,"_data");
  sprintf(file_out,"%s%s",prefix,"_dtc.fits");


  check_nomsg(plist=uves_propertylist_load(file_com,0));
  check_nomsg(nflats=uves_flames_pfits_get_nflats(plist));
  uves_free_propertylist(&plist);

  strcpy(tag,FLAMES_FIB_FF_DTC(chip));
  check_nomsg(frm=cpl_frame_new());
  check_nomsg(cpl_frame_set_tag(frm,tag));
  check_nomsg(cpl_frame_set_filename(frm,file_out));
  check_nomsg(cpl_frame_set_group(frm,CPL_FRAME_GROUP_CALIB));

  check_nomsg(iml=cpl_imagelist_new());
  for(i=0;i<nflats;i++){
    k=i+1;
    sprintf(file_inp,"%s%2.2d%s",ibase,k,".fits");
    uves_msg_warning("file=%s",file_inp);
    check_nomsg(img=cpl_image_load(file_inp,CPL_TYPE_FLOAT,0,0));
    cpl_imagelist_set(iml,img,i);
  }

  check_nomsg(uves_save_imagelist(iml,file_out,plist));

  check_nomsg(set=cpl_frameset_new());
  check_nomsg(set_out=cpl_frameset_new());
  cpl_frameset_insert(set,frm);


  uves_free_frameset(&set);
  check_nomsg(flames_add_desc_data(ibase,file_out,nflats,2));


  /* FIBFF SIGMA */
  sprintf(ibase,"%s%s",prefix,"_sigma");
  sprintf(file_out,"%s%s",prefix,"_sgc.fits");

  strcpy(tag,FLAMES_FIB_FF_SGC(chip));
  check_nomsg(frm=cpl_frame_new());
  check_nomsg(cpl_frame_set_tag(frm,tag));
  check_nomsg(cpl_frame_set_filename(frm,file_out));
  check_nomsg(cpl_frame_set_group(frm,CPL_FRAME_GROUP_CALIB));

  check_nomsg(iml=cpl_imagelist_new());
  for(i=0;i<nflats;i++){
    k=i+1;
    sprintf(file_inp,"%s%2.2d%s",ibase,k,".fits");
    uves_msg_warning("file=%s",file_inp);
    check_nomsg(img=cpl_image_load(file_inp,CPL_TYPE_FLOAT,0,0));
    cpl_imagelist_set(iml,img,i);
  }

  check_nomsg(uves_save_imagelist(iml,file_out,plist));

  check_nomsg(set=cpl_frameset_new());
  check_nomsg(set_out=cpl_frameset_new());
  cpl_frameset_insert(set,frm);


  uves_free_frameset(&set);
  check_nomsg(flames_add_desc_sigma(ibase,file_out,nflats,2));


  /* FIBFF BADPIXEL */
  sprintf(ibase,"%s%s",prefix,"_badpixel");
  sprintf(file_out,"%s%s",prefix,"_bpc.fits");


  strcpy(tag,FLAMES_FIB_FF_BPC(chip));
  check_nomsg(frm=cpl_frame_new());
  check_nomsg(cpl_frame_set_tag(frm,tag));
  check_nomsg(cpl_frame_set_filename(frm,file_out));
  check_nomsg(cpl_frame_set_group(frm,CPL_FRAME_GROUP_CALIB));

  check_nomsg(iml=cpl_imagelist_new());
  for(i=0;i<nflats;i++){
    k=i+1;
    sprintf(file_inp,"%s%2.2d%s",ibase,k,".fits");
    uves_msg_warning("file=%s",file_inp);
    check_nomsg(img=cpl_image_load(file_inp,CPL_TYPE_FLOAT,0,0));
    cpl_imagelist_set(iml,img,i);
  }

  check_nomsg(uves_save_imagelist(iml,file_out,plist));

  check_nomsg(set=cpl_frameset_new());
  check_nomsg(set_out=cpl_frameset_new());
  cpl_frameset_insert(set,frm);


  uves_free_frameset(&set);
  check_nomsg(flames_add_desc_bpmap(ibase,file_out,nflats,2));


   
  cleanup:

  uves_free_propertylist(&plist);
  uves_free_frameset(&set);
  uves_free_frameset(&set_out);

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }
}



/*----------------------------------------------------------------------------*/
/**
 * @brief   test filename
 */
/*----------------------------------------------------------------------------*/
static void
test_ex_bin_tab(void)
{
    const char *in_ima = "odd_l.fits";
    const char *in_ima_x1 = "odd.fits";
    const char *in_ima_x2 = "odda.fits";
    cpl_frame *in_image = cpl_frame_new();
    cpl_frame_set_filename(in_image, in_ima);
    cpl_table *x1 = cpl_table_new(5);
    cpl_table *x2 = cpl_table_new(8);
    cpl_table_new_column(x1, "OBJECT", CPL_TYPE_STRING);
    cpl_table_new_column(x1, "RA", CPL_TYPE_DOUBLE);
    cpl_table_new_column(x1, "DEC", CPL_TYPE_DOUBLE);
    cpl_table_new_column(x1, "R", CPL_TYPE_DOUBLE);
    cpl_table_new_column(x1, "R_ERROR", CPL_TYPE_DOUBLE);
    cpl_table_new_column(x1, "THETA", CPL_TYPE_DOUBLE);
    cpl_table_new_column(x1, "THETA_ERROR", CPL_TYPE_DOUBLE);
    cpl_table_new_column(x1, "TYPE", CPL_TYPE_STRING);
    cpl_table_new_column(x1, "BUTTON", CPL_TYPE_INT);
    cpl_table_new_column(x1, "PRIORITY", CPL_TYPE_INT);
    cpl_table_new_column(x1, "ORIENT", CPL_TYPE_DOUBLE);
    cpl_table_new_column(x1, "IN_TOL", CPL_TYPE_STRING);

    cpl_table_new_column(x2, "Slit", CPL_TYPE_STRING);
    cpl_table_new_column(x2, "FPS", CPL_TYPE_STRING);
    cpl_table_new_column(x2, "Retractor", CPL_TYPE_STRING);
    cpl_table_new_column(x2, "BN", CPL_TYPE_STRING);
    cpl_table_new_column(x2, "FBN", CPL_TYPE_STRING);
    cpl_table_new_column(x2, "RP", CPL_TYPE_STRING);
    cpl_table_new_column(x2, "_400", CPL_TYPE_STRING);
    cpl_table_new_column(x2, "_420", CPL_TYPE_STRING);
    cpl_table_new_column(x2, "_500", CPL_TYPE_STRING);
    cpl_table_new_column(x2, "_700", CPL_TYPE_STRING);

    cpl_table_save(x1, NULL, NULL, in_ima_x1, CPL_IO_DEFAULT);
    cpl_table_save(x2, NULL, NULL, in_ima_x2, CPL_IO_DEFAULT);

    uves_propertylist *in_image_header = uves_propertylist_new();
    uves_propertylist_append_string(in_image_header, "HISTORY", "'FIBREMASK','I*4'");
    uves_propertylist_append_string(in_image_header, "HISTORY", " 1 1 1 1 1 1 1 1 1");
    uves_propertylist_append_string(in_image_header, "HISTORY", "");
    uves_image_save(NULL, in_ima, CPL_BPP_IEEE_FLOAT, in_image_header, CPL_IO_DEFAULT);

    
    cpl_frame *in_table = cpl_frame_new();

    /* Need only header with FIBREPOS */
    cpl_frame_set_filename(in_table, "in_tab.fits"); 
    uves_propertylist *in_table_header = uves_propertylist_new();
    uves_propertylist_append_string(in_table_header, "HISTORY", "'FIBREPOS','R*8'");
    uves_propertylist_append_string(in_table_header, "HISTORY", " 1 2 3 4 5 6 7 8 9.345");
    uves_propertylist_append_string(in_table_header, "HISTORY", "");
    uves_image_save(NULL, "in_tab.fits", CPL_BPP_IEEE_FLOAT, in_table_header, CPL_IO_DEFAULT);
    

    flames_ex_bin_tab(in_image,
                      in_table,
                      "out_fits.fits");


    uves_free_frame(&in_image);
    uves_free_frame(&in_table);
    uves_free_table(&x1);
    uves_free_table(&x2);
    uves_free_propertylist(&in_image_header);
    uves_free_propertylist(&in_table_header);
    return;
}
    
/*----------------------------------------------------------------------------*/
/**
  @brief   Test transmission computation
**/
/*----------------------------------------------------------------------------*/
static void
test_trans(void)
{
    /* This code is duplicated from flames_cal_prep_sff_ofpos_impl.c.
       Not a proper unit test */
    const char *parFibFFN = "/home/jmlarsen/fibreff_l_norm.fits";
    int MAXFIBRES = 9;
    int fib_msk[9] = {1, 1, 1, 1, 1, 1, 1, 1, 0};
    double abs_fib_transm[9];
    double tot_transm = 0;
    int it;
    cpl_imagelist *ilist;

    for (it = 1; it <= MAXFIBRES; it++) {
/*
        DRS_NLIT_FIBRES = DRS_NLIT_FIBRES + {fib_msk({it})}
        DRS_NLIT_FIBRES += fib_msk[it-1];
*/        
        uves_msg_debug("fibre mask(%d) = %d", it, fib_msk[it-1]);
        
/*   if  {fib_msk({it})} .eq. 1 then */
        if (fib_msk[it-1] == 1) {
/*
        extract/ima &tmp = {parFibFFN} [<,@{it},<:>,@{it},>]
        statistic/image &tmp {SESSOUTV}
        abs_fib_transm({it}) = {outputr(7)}
        tot_transm = tot_transm + abs_fib_transm({it}) 
*/
            

            /* Computes total intensity in plane defined by y == fibre */
            int extension = 0;
            int nx, ny, nz;
            int x, y, z;
            int dummy;

            uves_free_imagelist(&ilist);
            check( ilist = cpl_imagelist_load(parFibFFN, CPL_TYPE_FLOAT,
                                              extension),
                   "Failed to load image cube %s", parFibFFN);
            
            nx = cpl_image_get_size_x(cpl_imagelist_get(ilist, 0));
            ny = cpl_image_get_size_y(cpl_imagelist_get(ilist, 0));
            nz = cpl_imagelist_get_size(ilist);

            uves_msg_error("nx, ny, nz = %d %d %d", nx, ny, nz);
            
            /* typical size is 4096*9*21; therefore efficiency is not too 
               important here */
            abs_fib_transm[it-1] = 0;
            for (z = 0; z < nz; z++) {
                for (y = it-1; y <= it-1; y++) {
                    for (x = 0; x < nx; x++) {
                        abs_fib_transm[it-1] += 
                            cpl_image_get(cpl_imagelist_get(ilist, z),
                                          x+1, y+1, &dummy);
                    }
                }
            }
            tot_transm += abs_fib_transm[it-1];
        }
    }
    
    fprintf(stderr, "%f %f %f %f %f %f %f %f %f\n",
            abs_fib_transm[0],
            abs_fib_transm[1],
            abs_fib_transm[2],
            abs_fib_transm[3],
            abs_fib_transm[4],
            abs_fib_transm[5],
            abs_fib_transm[6],
            abs_fib_transm[7],
            abs_fib_transm[8]);            
    
  cleanup:
    uves_free_imagelist(&ilist);
    return;            
}

/*----------------------------------------------------------------------------*/
/**
  @brief   main
**/
/*----------------------------------------------------------------------------*/

int main(int args, char *argv[])
{
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    cpl_errorstate initial_errorstate = cpl_errorstate_get();

    if (0) { /* requires FITS file as input */
        test_trans();
    }
    /* test_flames_msffsz2(); */

    /* test_flames_my_cubify(); */

    test_ex_bin_tab();

    if (cpl_error_get_code() != CPL_ERROR_NONE)
        {

	  cpl_errorstate_dump(initial_errorstate,CPL_FALSE,NULL);

        }
    return cpl_test_end(0);
}

/**@}*/
