/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
#ifndef FLAMES_MVFIT_H
#define FLAMES_MVFIT_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <flames_midas_def.h>
#define NRANSI

typedef struct _fit_info
{
    double *f_yvalue;      /* data vector (to be fitted!) */
    double *f_xvalue;    /* data vector with x values  */
    double *f_sigma;   /* variance of each vector element */
    int32_t n_par;    /* n. of fit parameters */
    int32_t n_xy;     /* n. of points in data vector */
    double **deriv;     /* derivatives n_xy*n_par */
    int **seq_order; /* data array for orders / fibres */
    double *par;            /* fit parameters */
    int nfibres; /* no. of fibres actually found */
    int32_t xdegree;  /* degree of the x variable as from the bivariate polynomial fitting */
    int32_t mdegree;  /* degree of the m (order) variable as from the bivariate polynomial fitting */

} fit_info;

void funcs(double ,double *,int);
int mvfit(fit_info *);
#endif
