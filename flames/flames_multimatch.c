/* @(#)multimatch    */
/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : multimatch.c                                                 */
/* Author   : I. Porceddu  -  ITAL_FLAMES Consortium                       */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  :                                                              */
/*                                                                         */
/* Input    :                                                              */
/*      ORDTAB                                                             */
/*      Table originated by HOUGH/ECHELLE. Threee further columns have     */
/*      been added                                                         */
/*    Now rmiddummr.tbl                                                  */
/*      DEFPOL     Degree in x and m domain as computed during format      */
/*                 check loop                                              */
/*                                                                         */
/*      COEFFD     A descriptor containing the coeffs of the bivariate     */
/*                 poly as extracted from the format check application     */
/*                                                                         */
/*      ECHORD   Number of the first guess detected orders                 */
/*                                                                         */
/* Output:                                                           */
/*      OUTTAB     rmiddummr.tbl with :NEWORD and :FIBRE filled            */ 
/*                                                                         */
/* SEQUENCE:
   - flames_preordpos (preordpos.c) 
       creates rmiddumm table 
   - flames_ordpos (hough/echelle - matchorders.c - tracing.c - fitting.c )
       hough/echelle: creates middummr table and clone it into rmiddummr table
       matchorders: adds :NEWORD and :FIBRE values to rmiddummr table
 */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_multimatch.h>

#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_midas_tblsys.h>
#include <flames_midas_tblerr.h>
#include <flames_midas_tbldef.h>
#include <flames_midas_macrogen.h>
#include <flames_midas_atype.h>
#include <flames_midas_def.h>
#include <flames_def_drs_par.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int flames_multimatch(cpl_frameset *CATNAME,
                      char *ALLFRAME,
                      int *LENGTH)                      
{
    int null=0;
    int actvals=0;
    int unit=0;
    int entries=0;
    int i=0;
    int last_entry=0;
    int seq=0;
    int status=0;
    int *length=0;
    int total_length=0;
    int maxfibres=0;
    //char ffcat[60];
    cpl_frameset *ffcat;
    char text[60];
    char frame_name[CATREC_LEN];
    char *frames;

    /* let's make sure frames is a null string, otherwise this may wreak havoc
     at concatenation time */
    //  memset(ffcat, '\0', 60);
    memset(text, '\0', 60);
    memset(frame_name, '\0', CATREC_LEN);

    /* === interface to MIDAS ================================================= */

    /*Let's initialize the MIDAS environment */
    SCSPRO("multimatch");

    /* initialise MAXFIBRES from keyword */
    if (SCKRDI(&MAXFIBRES, 1, 1, &actvals, &maxfibres, &unit, &null) != 0) {
        /* problems reading MAXFIBRES */
        SCTPUT("Error reading the MAXFIBRES keyword");
        return flames_midas_fail();
    }
    frames = calloc(CATREC_LEN*maxfibres, sizeof(char));
    memset(frames, '\0', CATREC_LEN*maxfibres);

    /* Let's open the catalog containing the frames to be processed
     The keyword CATNAME does contain the catalog's name */

    /* Catalog name */
    //  SCKGETC(CATNAME,1,60,&actvals,ffcat);
    ffcat = CATNAME;
    /* Display information line */
    //  sprintf (text,"Catalog file is %s\n",ffcat);
    //  SCTPUT  (text);
    /* Catalog info */
    SCCSHO(ffcat,&entries,&last_entry);
    length = ivector(1, entries);
    for (i=1; i<=entries; i++) {
        length[i] = 0;
    }
    for( seq = 1; seq <= entries; seq++) {
        if ((status = SCCFND(ffcat, seq, frame_name)) != 0) {
            /* no catalogue entry */
            /* Display information line */
            //      sprintf (text,"Catalog file %s does not contain more frames \n", ffcat);
            sprintf (text,"Catalog does not contain more frames \n");
        }
        strncat(&frames[0],frame_name,CATREC_LEN);
        length[seq] = strlen(frame_name);
        total_length += length[seq];
    }
    length[0] = entries;
    SCKWRC(ALLFRAME,1,frames,1,total_length,&null);
    SCKWRI(LENGTH,length,1,entries+1,&null);
    /*printf("%s %d \n ",frames,strlen(frames)); */

    free(frames);
    free_ivector(length,1,entries);

    return SCSEPI();

}
