/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_GET_ORDPOS_H
#define FLAMES_GET_ORDPOS_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>


/* the following function calculates the y position of the 
   center of a given order "m" at a given "x", 
   and returns it in the double variable to which "y" points. 
   Beware: both "m" and "n" are double! */
flames_err 
get_ordpos(orderpos *ordpos, double m, double x, double *y);


#endif
