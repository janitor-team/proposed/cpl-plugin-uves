/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_checksize   Substep: check input frame size descriptors
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                          Includes
 --------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_checksize.h>
#include <uves_msg.h>
#include <flames_newmatrix.h>
/**@{*/

/*---------------------------------------------------------------------------
                            Implementation
 ---------------------------------------------------------------------------*/
/**
 @name  flames_checksize()  
 @short  check input frame size descriptors
 @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

 @param frameid   input frame id
 @param slitflats input all slit flats structure

 @return success or failure code

 DRS Functions called:          
       none                                         
                                                                         
 Pseudocode:                                                             
     check several MIDAS descriptors                                     

@note
*/

flames_err checksize(int frameid, allslitflats *slitflats)
{
  int actvals=0, unit=0, null=0, naxis=0;
  int npix[2]={0,0};
  double start[2]={0,0}, step[2]={0,0};


  
  if (0 != SCDRDI(frameid, "NAXIS", 1, 1, &actvals, &naxis, &unit, &null)) {
    /* problems reading NAXIS */
    return flames_midas_error(MAREMMA);
  }

  if (naxis != 2) {
    /* wrong file dimensions: wrong frame name? */
    return flames_midas_error(MAREMMA);
  }

  if (0 != SCDRDI(frameid, "NPIX", 1, 2, &actvals, npix, &unit, &null)) {
    /* problems reading NPIX */
    return flames_midas_error(MAREMMA);
  }
  uves_msg_debug("npix=%d %d",npix[0],npix[1]);
  uves_msg_debug("subcols=%d subrows=%d",slitflats->subcols,slitflats->subrows);
  if (npix[0] != slitflats->subcols || npix[1] != slitflats->subrows) {
    /* wrong file dimensions: wrong frame name? */
    return flames_midas_error(MAREMMA);
  }

  if (0 != SCDRDD(frameid, "START", 1, 2, &actvals, start, &unit, &null)) {
    /* problems reading START */
    return flames_midas_error(MAREMMA);
  }
  uves_msg_debug("start[0]=%f start[1]=%f",start[0],start[1]);
  uves_msg_debug("substartx=%f substarty=%f",slitflats->substartx,slitflats->substarty);
  if (start[0] != slitflats->substartx || 
      start[1] != slitflats->substarty) {
    /* wrong file dimensions: wrong frame name? */
    return flames_midas_error(MAREMMA);
  }

  if (0 != SCDRDD(frameid, "STEP", 1, 2, &actvals, step, &unit, &null)) {
    /* problems reading START */
    return flames_midas_error(MAREMMA);
  }

  if (step[0] != slitflats->substepx || step[1] != slitflats->substepy) {
    /* wrong file dimensions: wrong frame name? */
    return flames_midas_error(MAREMMA);
  }
  /* the file dimensions match, go ahead */
  uves_msg_debug("step[0]=%f step[1]=%f",step[0],step[1]);
  uves_msg_debug("substepx=%f substepy=%f",slitflats->substepx,slitflats->substepy);


  return(NOERR);

}
/**@}*/
