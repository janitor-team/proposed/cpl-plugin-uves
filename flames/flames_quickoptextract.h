/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */


#ifndef FLAMES_QUICK_OPT_EXTRACT_H
#define FLAMES_QUICK_OPT_EXTRACT_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include<flames_uves.h>
/* 
   ------------------------------------------------
   The following function is used in the optimal extraction procedure
   ------------------------------------------------
 */

/* 
   The following function does the raw optimal extraction giving as its 
   result the optimally extracted spectrum of all fibres for one order at time
   no y shift correction flavour
 */
flames_err 
quickoptextract(flames_frame *ScienceFrame, 
                allflats *SingleFF, 
                orderpos *Order,
                int32_t ordsta, 
                int32_t ordend, 
                int32_t j,
                frame_mask **mask, 
                double **aa, 
                double **xx,
                int32_t arraysize, 
                int32_t *fibrestosolve,
                int32_t *orderstosolve, 
                int32_t *numslices);


#endif
