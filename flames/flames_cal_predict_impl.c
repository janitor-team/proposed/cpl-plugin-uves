/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2013-07-22 07:28:04 $
 * $Revision: 1.13 $
 * $Name: not supported by cvs2svn $
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup flames_cal_predict  Recipe: Guess order & table generation
 *
 * This recipe determines the echelle spectral format using a physical model
 * See man-page for details.
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <flames.h>

#include <uves_recipe.h>
#include <uves_physmod_body.h>
#include <uves_error.h>

#include <cpl.h>

/**@{*/
/*-----------------------------------------------------------------------------
                            Forward declarations
 -----------------------------------------------------------------------------*/
static int flames_cal_predict_define_parameters(cpl_parameterlist *parameters);


const char * const flames_physmod_desc_short = "Implements the UVES physical model";
const char * const flames_physmod_desc =
"This recipe implements the UVES physical model\n"
"Input files are flames-uves formatcheck frames identified by the tag\n"
"FIB_ARC_LAMP_FORM_RED and a ThAr line reference table\n"
"identified by the tag LINE_REFER_TABLE.\n"
"Optional input are master bias frames identified by the tag \n"
"MASTER_BIAS_(REDL|REDU).\n"
"The recipe extracts from the input files FITS header data indicating the\n"
"instrument setting and ambiental atmospheric conditions, then using the\n"
"model predicts X,Y position of the lines listed in the LINE_REFER_TABLE\n"
"table which are imaging on the detector and stores this information in an\n"
"guess order and a guess line table.\n"
"Output are a guess line table, FIB_LIN_GUE_(REDL|REDU), and a guess order \n"
"table, FIB_ORD_GUE_(REDL|REDU).\n"
"If the user provides in input also master format checks having tag\n"
"MASTER_FORM_(REDL|REDU), the recipe performs also a stability check\n";




/*-----------------------------------------------------------------------------
                            Recipe standard code
 -----------------------------------------------------------------------------*/
#define cpl_plugin_get_info flames_cal_predict_get_info
UVES_RECIPE_DEFINE(
    FLAMES_CAL_PREDICT_ID, FLAMES_CAL_PREDICT_DOM, 
    flames_cal_predict_define_parameters,
    "Andrea Modigliani", "cpl@eso.org",
    flames_physmod_desc_short,
    flames_physmod_desc);

/*-----------------------------------------------------------------------------
                              Functions code
 -----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    parameters        the parameterlist to fill
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int
flames_cal_predict_define_parameters(cpl_parameterlist *parameters)
{
    return uves_physmod_define_parameters_body(parameters, make_str(FLAMES_CAL_PREDICT_ID));
}
/*----------------------------------------------------------------------------*/
/**
  @brief    Get the command line options and execute the data reduction
  @param    parameters  the parameters list
  @param    frames      the frames list
  @return   CPL_ERROR_NONE if everything is ok
 */
/*----------------------------------------------------------------------------*/
static void
flames_cal_predict_exe(cpl_frameset *frames, const cpl_parameterlist *parameters,
            const char *starttime)
{
    bool flames = true;
    uves_physmod_exe_body(frames, flames, make_str(FLAMES_CAL_PREDICT_ID),
              parameters, starttime);
    return;
}
/**@}*/


