/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : optsynth.c                                                   */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/
/* C functions include files */ 

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* MIDAS include files */
#include <flames_midas_def.h>
/* FLAMES-UVES include files */ 
#include <flames_optsynth.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
/**
 @brief computes chi-2 to evaluate extraction quality

  @param ScienceFrame input science frame
  @param SingleFF  single fibre FF
  @param Order     order table
  @param backframe background frame
  @param chisquare chi2 value
  @param chisqpixels  data points contributing to chi2 computation
  @param nfittedparams number of fitted parameters

  @doc
      - initialise frame to inter-order background
      - build fitted frame
      - compute frame array as sum of good contributes
      - computes chi2 as difference between frame array and frame background 
        divided by frame sigma.

 */
flames_err optsynth(flames_frame *ScienceFrame, allflats *SingleFF, 
                    orderpos *Order, frame_data ***backframe,
                    double *chisquare, int *chisqpixels,
                    int *nfittedparams)
{

    int32_t lastorder, ix, iy, lfibre, ifibre, iframe, iorder;
    frame_data **framebuffer=0;
    frame_mask **chisqmask=0;
    frame_data chisqbuffer=0;
    char output[160];

    frame_data *fdvecbuf1=0;
    frame_data *fdvecbuf2=0;

    frame_data *fdvecbuf4=0;
    frame_data *fdvecbuf5=0;
    frame_mask *fmvecbuf1=0;
    frame_mask *fmvecbuf2=0;
    frame_mask *fmvecbuf3=0;
    int32_t *lvecbuf1=0;
    int32_t *lvecbuf2=0;
    int32_t iyixindex=0;
    int32_t iyixuplimit=0;
    int32_t numorders=0;
    int32_t ixiorderifibrestep=0;
    int32_t iorderifibreindex=0;
    int32_t iorderifibreixoffset=0;
    int32_t ixiorderifibreindex=0;
    int32_t iorderifibreixindex=0;

    /* initialise output */
    memset(output, 0, 160);

    /* first, set the frame equal to the background (actually
     just swap pointers) */
    framebuffer = ScienceFrame->frame_array;
    ScienceFrame->frame_array = *backframe;
    *backframe = framebuffer;

    /* Allocate and initialise the chisqmask */
    chisqmask = fmmatrix(0, ScienceFrame->subrows-1, 0,
                    ScienceFrame->subcols-1);
    fmvecbuf1 = chisqmask[0];
    iyixuplimit = (ScienceFrame->subrows*ScienceFrame->subcols)-1;
    for (iyixindex=0; iyixindex<=iyixuplimit; iyixindex++) {
        fmvecbuf1[iyixindex] = 0;
    }

    /* initialise the diagnostics as well */
    *chisquare = 0;
    *chisqpixels = 0;
    *nfittedparams = 0;

    /* now, begin building the fitted frame */
    lastorder = Order->lastorder-Order->firstorder;
    numorders = lastorder+1;
    ixiorderifibrestep = numorders*ScienceFrame->maxfibres;
    fmvecbuf2 = ScienceFrame->specmask[0][0];
    lvecbuf1 = SingleFF->lowfibrebounds[0][0];
    lvecbuf2 = SingleFF->highfibrebounds[0][0];
    fdvecbuf1 = ScienceFrame->frame_array[0];
    fdvecbuf2 = ScienceFrame->spectrum[0][0];
    fdvecbuf4 = (*backframe)[0];
    fdvecbuf5 = ScienceFrame->frame_sigma[0];
    fmvecbuf3 = ScienceFrame->badpixel[0];
    for (lfibre=0; lfibre<=(ScienceFrame->num_lit_fibres-1); lfibre++) {
        ifibre = ScienceFrame->ind_lit_fibres[lfibre];
        iframe = SingleFF->fibre2frame[ifibre];
        frame_data* fdvecbuf3 = SingleFF->flatdata[iframe].data[0];
        for (iorder=0; iorder<=lastorder; iorder++) {
            iorderifibreindex = (iorder*ScienceFrame->maxfibres)+ifibre;
            iorderifibreixoffset = iorderifibreindex*ScienceFrame->subcols;
            for (ix=0; ix<=(ScienceFrame->subcols-1); ix++) {
                ixiorderifibreindex = (ix*ixiorderifibrestep)+iorderifibreindex;
                iorderifibreixindex = iorderifibreixoffset+ix;
                /* was this fibre extracted at this ix and iorder? */
                if (fmvecbuf2[ixiorderifibreindex] == 1) {
                    /* yes, it was extracted, add its contribution */
                    /* since it was extracted, one more parameter was fitted */
                    (*nfittedparams)++;
                    for (iy=lvecbuf1[iorderifibreixindex];
                                    iy<=lvecbuf2[iorderifibreixindex];
                                    iy++) {
                        iyixindex = (iy*ScienceFrame->subcols)+ix;
                        fdvecbuf1[iyixindex] +=
                                        fdvecbuf2[ixiorderifibreindex]*fdvecbuf3[iyixindex];
                        /* mark this pixel in the frame as a "fitted" one */
                        fmvecbuf1[iyixindex] = 1;
                    }
                }
            }
        }
    }

    /* now compute the chisquare */
    for (iyixindex=0; iyixindex<=iyixuplimit; iyixindex++) {
        if ((fmvecbuf1[iyixindex]==1) && (fmvecbuf3[iyixindex]==0)) {
            chisqbuffer = fdvecbuf1[iyixindex]-fdvecbuf4[iyixindex];
            *chisquare += (double) (chisqbuffer*chisqbuffer/fdvecbuf5[iyixindex]);
            (*chisqpixels)++;
        }
    }

    /* speak about the results */
    sprintf(output, "Measured Chi square %g on %d free parameters", *chisquare,
            *chisqpixels-*nfittedparams);
    SCTPUT(output);
    sprintf(output, "resulting from %d used pixels and %d fitted params",
            *chisqpixels, *nfittedparams);
    SCTPUT(output);

    /* free the locally allocated array */
    free_fmmatrix(chisqmask, 0, ScienceFrame->subrows-1, 0,
                  ScienceFrame->subcols-1);

    return NOERR;

}
