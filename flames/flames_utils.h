/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_UTILS_H
#define FLAMES_UTILS_H



#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-----------------------------------------------------------------------------
                    Includes
 -----------------------------------------------------------------------------*/
#include <uves_propertylist.h>
#include <uves_chip.h>
#include <flames_dfs.h>
#include <flames_get_flat_size.h>
#include <cpl.h>

/*-----------------------------------------------------------------------------
                             Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                   Prototypes
 -----------------------------------------------------------------------------*/
char * 
flames_get_basename(const char *filename);

cpl_error_code
flames_get_mff_hw_and_yshift(cpl_frame* frm, const int save_flat_size, const char* name_tbl);

int flames_get_version_binary(void);

const char *
flames_get_frmid(const cpl_frame *frame, enum uves_chip chip,
                 int *wc);
cpl_frame *
flames_new_table(const char *filename, 
                 const cpl_table *table,
                 const uves_propertylist *header);

cpl_frame *
flames_new_frame(const char *filename, 
                 const cpl_image *image,
                 const uves_propertylist *header);

cpl_frame *
flames_new_framelist(const char *filename, 
                     const cpl_imagelist *imagelist,
                     const uves_propertylist *header);

cpl_frame *
flames_new_frame_table(const char *filename, 
                       const cpl_table *table,
                       const uves_propertylist *header,
                       const uves_propertylist *theader);

cpl_frame *
flames_image_duplicate(const char *filename_prefix,
                       const cpl_frame *frame,
                       bool prefix,
                       bool reset_crval);

cpl_frame *
flames_image_subtract_scalar_create(const char *filename_prefix,
                                    const cpl_frame *f,
                                    double value);
cpl_frame *
flames_image_subtract_create(const char *filename_prefix,
                             const cpl_frame *f1,
                             const cpl_frame *f2);

void flames_merge_table(const char *n1, const char *n2);
void flames_rename_table(const char *n1, const char *n2);
void flames_sort_table_2(const char *filename, const char *c1, const char *c2);
void flames_sort_table(const char *filename, 
                       const char *c1,
                       const char *c2,
                       const char *c3);

void
flames_reset_crval_to_one(uves_propertylist** h);

void
flames_select_non_null(const char *filename, const char *column);

void
flames_select_all(const char *filename);

double
flames_image_get_median(const cpl_frame *f);

cpl_error_code
msffsz_flames(cpl_frame *in_ima,
              const int save_flat_size,
              const char *in_tab,
              enum uves_chip chip);
void
msffsz_flames2(cpl_frame *in_ima,
               const int save_flat_size,
               const char *in_tab,
               enum uves_chip chip);

#endif
