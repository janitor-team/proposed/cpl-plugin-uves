/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_allocspectrum   Substep: Initialize a science frame
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* MIDAS include files */
#include <flames_midas_def.h>
/* FLAMES-UVES include files */ 
#include <flames_uves.h>
#include <flames_allocspectrum.h>
#include <flames_newmatrix.h>
/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_allocspectrum()  
   @short   Initialize a science frame
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param ScienceFrame pointer to a science frame

   @return initialized frame

   DRS Functions called:                                                  
   fd3tensor                                                   
                                                                        
   Pseudocode:                                                            
   use fd3tensor                                              
   to initialize to NULL data                                 
   @note
*/

flames_err alloc_spectrum(flames_frame *ScienceFrame)
{ 
 
  ScienceFrame->spectrum = 
    fd3tensor(0, ScienceFrame->subcols-1, 
	      0, ScienceFrame->lastorder-ScienceFrame->firstorder, 
	      0, ScienceFrame->maxfibres-1);
  memset(&ScienceFrame->spectrum[0][0][0], 0, 
	 ScienceFrame->subcols*
	 (ScienceFrame->lastorder+1-ScienceFrame->firstorder)*
	 ScienceFrame->maxfibres*sizeof(frame_data));

  ScienceFrame->specsigma = 
    fd3tensor(0, ScienceFrame->subcols-1, 
	      0, ScienceFrame->lastorder-ScienceFrame->firstorder, 
	      0, ScienceFrame->maxfibres-1);
  memset(&ScienceFrame->specsigma[0][0][0], 0, 
	 ScienceFrame->subcols*
	 (ScienceFrame->lastorder+1-ScienceFrame->firstorder)*
	 ScienceFrame->maxfibres*sizeof(frame_data));

  ScienceFrame->normspec = 
    fd3tensor(0, ScienceFrame->subcols-1, 
	      0, ScienceFrame->lastorder-ScienceFrame->firstorder, 
	      0, ScienceFrame->maxfibres-1);
  memset(&ScienceFrame->normspec[0][0][0], 0, 
	 ScienceFrame->subcols*
	 (ScienceFrame->lastorder+1-ScienceFrame->firstorder)*
	 ScienceFrame->maxfibres*sizeof(frame_data));

  ScienceFrame->normsigma = 
    fd3tensor(0, ScienceFrame->subcols-1, 
	      0, ScienceFrame->lastorder-ScienceFrame->firstorder, 
	      0, ScienceFrame->maxfibres-1);
  memset(&ScienceFrame->normsigma[0][0][0], 0, 
	 ScienceFrame->subcols*
	 (ScienceFrame->lastorder+1-ScienceFrame->firstorder)*
	 ScienceFrame->maxfibres*sizeof(frame_data));

  ScienceFrame->speccovar = 
    fd3tensor(0, ScienceFrame->subcols-1, 
	      0, ScienceFrame->lastorder-ScienceFrame->firstorder, 
	      0, ScienceFrame->maxfibres-1);
  memset(&ScienceFrame->speccovar[0][0][0], 0, 
	 ScienceFrame->subcols*
	 (ScienceFrame->lastorder+1-ScienceFrame->firstorder)*
	 ScienceFrame->maxfibres*sizeof(frame_data));

  ScienceFrame->specmask = 
    fm3tensor(0, ScienceFrame->subcols-1, 
	      0, ScienceFrame->lastorder-ScienceFrame->firstorder, 
	      0, ScienceFrame->maxfibres-1);
  memset(&ScienceFrame->specmask[0][0][0], 0, 
	 ScienceFrame->subcols*
	 (ScienceFrame->lastorder+1-ScienceFrame->firstorder)*
	 ScienceFrame->maxfibres*sizeof(frame_mask));

  ScienceFrame->normmask = 
    fm3tensor(0, ScienceFrame->subcols-1, 
	      0, ScienceFrame->lastorder-ScienceFrame->firstorder, 
	      0, ScienceFrame->maxfibres-1);
  memset(&ScienceFrame->normmask[0][0][0], 0, 
	 ScienceFrame->subcols*
	 (ScienceFrame->lastorder+1-ScienceFrame->firstorder)*
	 ScienceFrame->maxfibres*sizeof(frame_mask));


  return NOERR;

}
/**@}*/








