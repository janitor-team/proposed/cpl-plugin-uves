/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_gaussselfcorrel 
 The following function finds the peak of the correlation between 
 a synthetic frame with gaussian-shaped fibres centered on 
 shifted fibre positions and the actual, real frames in the allflats
 structure, to be stored as a correction for subsequent correlations.

 *
 */
/*-------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <flames_getordpos.h>
#include <flames_gausscorrel.h>
#include <flames_gaussselfcorrel.h>
#include <flames_singlecorrel.h>
#include <flames_correl.h>
#include <flames_midas_tblsys.h>
#include <flames_midas_tbldef.h>
#include <flames_midas_tblerr.h>
#include <flames_midas_atype.h>
#include <flames_midas_macrogen.h>
#include <flames_midas_def.h>
#include <flames_def_drs_par.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>

/* this is the numeric value of the golden section, used in the brent 
   algorithm */
#define CGOLD 0.3819660
/**@{*/




/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_gaussselfcorrel()  
   @short 
   The following function finds the peak of the correlation between 
   a synthetic frame with gaussian-shaped fibres centered on 
   shifted fibre positions and the actual, real frames in the allflats
   structure, to be stored as a correction for subsequent correlations.


   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param myflats 
   @param Order
   @param maxcorriters
   @param shiftwindow
   @param shifttol
   @param correlxstep 
   @param outtabname


   @return success or failure code

   DRS Functions called:                                              
   singlecorrel                                                       
   cvector                                                           
   lvector                                                                
   lmatrix                                                                
   dmatrix                                                                
   free_cvector                                                           
   free_lvector                                                           
   free_dmatrix                                                           
   free_lmatrix                                                           

   Pseudocode:                                                        
   Missing 

   @doc 

   The following function finds the peak of the correlation between 
   a synthetic frame with gaussian-shaped fibres centered on 
   shifted fibre positions and the actual, real frames in the allflats
   structure, to be stored as a correction for subsequent correlations.

   @note
 */

flames_err 
gaussselfcorrel(
		allflats *myflats,
		orderpos *Order,
		int32_t maxcorriters,
		double shiftwindow,
		double shifttol,
		int32_t correlxstep,
		const char *outtabname)
{
	char qc_y_self_shift_key[CATREC_LEN+1];
	char frm_id[CATREC_LEN+1];

	int32_t *fibrelist=0;
	int32_t nlitfibres=0;
	int outtid=0;
	int fibrecol=0;
	int yshiftcol=0;
	int correlcol=0;
	int row=0;
	double highshift=0;
	double lowshift=0;
	int32_t ifibre=0;
	double plowshift=0;
	double phighshift=0;
	int32_t iorder=0;
	int32_t iframe=0;
	int32_t lfibre=0;
	int ibuf=0;
	double ydelta=0;

	double **ordercentres=0;
	double *ordercentresiorder=0;
	double *y_shift_self=0;
	int32_t **ilowlimits=0;
	int32_t *ilowlimitsiorder=0;
	int32_t **iuplimits=0;
	int32_t *iuplimitsiorder=0;
	int32_t ix=0;
	double x=0;
	double pordercentre=0;
	double tol2=0;

	double ashift=0;
	double bshift=0;
	double ushift=0;
	double vshift=0;
	double wshift=0;
	double xshift=0;
	double xmshift=0;
	double dshift=0;
	double eshift=0;
	double fashift=0;
	double fbshift=0;
	double fushift=0;
	double fvshift=0;
	double fwshift=0;
	double fxshift=0;
	double p=0;
	double q=0;
	double r=0;
	double s=0;
	double t=0;
	double etemp=0;
	flames_err done=0;
	int32_t iter=0;
	char output[CATREC_LEN+1];
	flames_frame *framebuffer=0;


	int actvals=0;
	char drs_verbosity[10];
	int mid_stat=0;
	int* mid_unit=0;

	memset(output, 0, CATREC_LEN+1);
	memset(qc_y_self_shift_key, '\0', CATREC_LEN+1);
	memset(frm_id, '\0', CATREC_LEN+1);

	/* allocate local arrays */
	fibrelist = lvector(0, myflats->maxfibres-1);
	y_shift_self = dvector(0, N_FIBRES_MAX);

	nlitfibres = 1;
	ordercentres = dmatrix(0, Order->lastorder-Order->firstorder,
			0, myflats->subcols-1);
	ilowlimits = lmatrix(0, Order->lastorder-Order->firstorder,
			0, myflats->subcols-1);
	iuplimits = lmatrix(0, Order->lastorder-Order->firstorder,
			0, myflats->subcols-1);


	memset(drs_verbosity, 0, 10);
	if ((mid_stat=SCKGETC(DRS_VERBOSITY, 1, 3, &actvals, drs_verbosity))
			!= 0) {
		/* the keyword seems undefined, protest... */
		return(MAREMMA);
	}


	/* We want to be able to output the correlation scans in a MIDAS table,
     for debugging purposes */
	/* we only create the output table if we were given a name for it, so that
     it can be easily skipped if unnecessary */
	if (outtabname != NULL) {
		/* we have a name */
		if (TCTINI(outtabname, F_O_MODE, maxcorriters+3, &outtid)
				!= 0) {
			SCTPUT("Warning: Error opening output MIDAS table for correlations");
			outtabname=NULL;
		}
		else {
			if ((TCCINI(outtid,D_I4_FORMAT,1,"I8","  ","FIBRE",&fibrecol)!=0)||
					(TCCINI(outtid,D_R8_FORMAT,1,"G8.5","  ","YSHIFT",&yshiftcol)!=0)||
					(TCCINI(outtid,D_R8_FORMAT,1,"G8.5","  ","CORRELATION",&correlcol)
							!=0)) {
				SCTPUT("Warning: Error creating colums in MIDAS table for \
correlations");
				TCTCLO(outtid);
				outtabname=NULL;
			}
			else row=0;
		}
	}

	for (iorder=0; iorder<=Order->lastorder-Order->firstorder; iorder++) {
		double dorder = (double)(iorder+Order->firstorder);
		/* find the y positions of the order centres once and for all */
		ordercentresiorder = ordercentres[iorder];
		for (ix=0; ix<=(myflats->subcols-1); ix+=correlxstep) {
			x = ((double)ix+myflats->substartx)*myflats->substepx;
			if (get_ordpos(Order, dorder, x, ordercentresiorder+ix)!=NOERR) {
				SCTPUT("Error in get_ordpos() called by gausscorrel()");
				return flames_midas_fail();
			}
		}
	}

	/* allocate the framebuffer */
	framebuffer = calloc(1, sizeof(flames_frame));
	/* copy some scalars from framebuffer to myflats */
	framebuffer->subrows = myflats->subrows;
	framebuffer->subcols = myflats->subcols;
	framebuffer->substepx = myflats->substepx;
	framebuffer->substepy = myflats->substepy;
	framebuffer->substartx = myflats->substartx;
	framebuffer->substarty = myflats->substarty;
	framebuffer->maxfibres = myflats->maxfibres;
	framebuffer->fibremask = cvector(0, framebuffer->maxfibres-1);

	/* run over the frames in the allflats structure */
	for (iframe=0; iframe<=(myflats->nflats-1); iframe++) {
		for (lfibre=0;lfibre<N_FIBRES_MAX;lfibre++){
			y_shift_self[lfibre]=0.;
		}


		/* set up the frame buffer for this frame */
		framebuffer->frame_array = myflats->flatdata[iframe].data;
		framebuffer->frame_sigma = myflats->flatdata[iframe].sigma;
		framebuffer->badpixel = myflats->flatdata[iframe].badpixel;

		/* now run over fibres in this frame */
		for (lfibre=0;
				lfibre<=((myflats->flatdata)[iframe]).numfibres-1; lfibre++) {
			/* set up the mask so that only this fibre is lit */
			for (ifibre=0; ifibre<=(myflats->maxfibres-1); ifibre++)
				framebuffer->fibremask[ifibre] = FALSE;
			ifibre = myflats->flatdata[iframe].fibres[lfibre];
			fibrelist[0] = ifibre;
			framebuffer->fibremask[ifibre] = TRUE;
			lowshift = highshift = Order->fibrepos[ifibre];
			highshift += (Order->halfibrewidth+shiftwindow);
			lowshift -= (Order->halfibrewidth+shiftwindow);
			if (myflats->substepy>0) {
				plowshift = lowshift/myflats->substepy;
				phighshift = highshift/myflats->substepy;
			}
			else {
				plowshift = highshift/myflats->substepy;
				phighshift = lowshift/myflats->substepy;
			}

			for (iorder=0; iorder<=Order->lastorder-Order->firstorder; iorder++) {
				ordercentresiorder = ordercentres[iorder];
				ilowlimitsiorder = ilowlimits[iorder];
				iuplimitsiorder = iuplimits[iorder];
				for (ix=0; ix<=(myflats->subcols-1); ix+=correlxstep) {
					/* convert the order centre to pixel coordinates */
					pordercentre = (ordercentresiorder[ix]-myflats->substarty)/
					myflats->substepy;
					/* here find order limits in pixel coordinates */
					ilowlimitsiorder[ix] = (int32_t) floor(pordercentre+plowshift);
					iuplimitsiorder[ix] = (int32_t) ceil(pordercentre+phighshift);
					if (ilowlimitsiorder[ix]<0) ilowlimitsiorder[ix]=0;
					if (iuplimitsiorder[ix]>(int32_t)(myflats->subrows-1))
						iuplimitsiorder[ix]=(int32_t)(myflats->subrows-1);
				}
			}

			/* Ok, first of all let's see whether we can bracket the correlation
	 maximum within the limits we set for searching it */
			ashift = -shiftwindow;
			xshift = 0;
			bshift = shiftwindow;
			fashift = singlecorrel(framebuffer, Order, fibrelist, nlitfibres,
					ordercentres, ilowlimits, iuplimits,
					correlxstep, ashift);
			fxshift = singlecorrel(framebuffer, Order, fibrelist, nlitfibres,
					ordercentres, ilowlimits, iuplimits,
					correlxstep, xshift);
			fbshift = singlecorrel(framebuffer, Order, fibrelist, nlitfibres,
					ordercentres, ilowlimits, iuplimits,
					correlxstep, bshift);
			/* be somewhat verbose */

			sprintf(output, "Self-correlation step for frame %d, fibre %d",
					iframe+1, ifibre+1);
			SCTPUT(output);

			if ( strcmp(drs_verbosity,"LOW") == 0 ){
			} else {
				sprintf(output, "searching correlation maximum for shifts \
between %gx and %g",
ashift, bshift);
				SCTPUT(output);
			}

			sprintf(output, "correl(%g)=%g, correl(%g)=%g, correl(%g)=%g", ashift,
					fashift, xshift, fxshift, bshift, fbshift);
			SCTPUT(output);



			/* should we output something to the MIDAS table? */
			ibuf = (int) ifibre;
			if (outtabname != NULL) {
				TCEWRD(outtid, 1, yshiftcol, &ashift);
				TCEWRD(outtid, 1, correlcol, &fashift);
				TCEWRI(outtid, 1, fibrecol, &ibuf);
				TCEWRD(outtid, 2, yshiftcol, &bshift);
				TCEWRD(outtid, 2, correlcol, &fbshift);
				TCEWRI(outtid, 2, fibrecol, &ibuf);
				TCEWRD(outtid, 3, yshiftcol, &xshift);
				TCEWRD(outtid, 3, correlcol, &fxshift);
				TCEWRI(outtid, 3, fibrecol, &ibuf);
				row=3;
			}
			/* make sure that we are bracketing the maximum correlation between
	 ashift and cshift */
	 if ((fxshift<=fashift) || (fxshift<=fbshift)) {
		 /* the maximum correlation is probably outside this interval, report
	   with an error and exit */
		 sprintf(output,
				 "Error in gausscorrel: no maximum for yshift \
between %g and %g", 
ashift, bshift);
		 SCTPUT(output);
		 if (outtabname != NULL) TCTCLO(outtid);
		 return flames_midas_fail();
	 }

	 /* begin searching, here follows an inlined, adapted version of
	 the brent function */

	 /* give sensible values to the variables (added by Giacomo) so that we
	 can try the parabolic approximation immediately */
	 if (fbshift<fashift) {
		 wshift = ashift;
		 fwshift = fashift;
		 vshift = bshift;
		 fvshift = fbshift;
	 }
	 else {
		 wshift = bshift;
		 fwshift = fbshift;
		 vshift = ashift;
		 fvshift = fashift;
	 }
	 dshift = xshift-wshift;
	 eshift = dshift/CGOLD;

	 done = FALSE;

	 for (iter=1; (iter<=maxcorriters) && (done==FALSE); iter++) {

		 xmshift = 0.5*(ashift+bshift);
		 /* differently from the NR implementation, we here use absolute
	   tolerances, since we know that the solution, if any, must be 
	   in an interval of order unity, centered around zero, and we
	   need an accuracy better than 0.1 pixels, but probably not 
	   better than 0.01 pixels. This means that rounding errors are
	   not a problem, regardless that we compute the correlation values
	   in single or double precision */
		 tol2 = 2*shifttol;

		 if (fabs(xshift-xmshift)+0.5*(bshift-ashift) <= tol2) {
			 /* did we get close enough to the maximum? */
			 Order->gaussselfshift[ifibre] = ydelta = xshift;
			 double maxcorrel = fxshift;
			 done = TRUE;
		 }
		 else {
			 if (fabs(eshift) > shifttol) {
				 /* compute a trial parabolic shift */
				 s = xshift-wshift;
				 t = xshift-vshift;
				 r = s*(fxshift-fvshift);
				 q = t*(fxshift-fwshift);
				 p = t*q-s*r;
				 q = 2*(q-r);
				 if (q > 0) p = -p;
				 q = fabs(q);
				 etemp = eshift;
				 eshift = dshift;
				 /* if the parabolic shift is not acceptable, switch to a golden
	       section shift */
		   if (fabs(p) >= fabs(0.5*q*etemp) || p <= q*(ashift-xshift) ||
				   p >= q*(bshift-xshift))
			   dshift =
					   CGOLD*(eshift = (xshift >= xmshift ? ashift-xshift :
							   bshift-xshift));
		   else {
			   dshift = p/q;
			   ushift = xshift+dshift;
			   if (ushift-ashift < tol2 || bshift-ushift < tol2)
				   dshift = xmshift>=xshift ? shifttol : -shifttol;
		   }
			 }
			 else {
				 /* compute the golden section shift */
				 dshift =
						 CGOLD*(eshift = (xshift >= xmshift ? ashift-xshift :
								 bshift-xshift));
			 }
			 ushift = (fabs(dshift) >= shifttol ? xshift+dshift :
					 xshift+(dshift >= 0 ? shifttol : -shifttol));
			 /* here we evaluate the correlation at the new shift value */
			 fushift = singlecorrel(framebuffer, Order, fibrelist, nlitfibres,
					 ordercentres, ilowlimits, iuplimits,
					 correlxstep, ushift);
			 if ( strcmp(drs_verbosity,"LOW") == 0 ){
			 } else {
				 sprintf(output, "iteration %d, correl(%g)=%g", iter, ushift,
						 fushift);
				 SCTPUT(output);

			 }

			 if (outtabname != NULL) {
				 row++;
				 TCEWRD(outtid, row, yshiftcol, &ushift);
				 TCEWRD(outtid, row, correlcol, &fushift);
				 TCEWRI(outtid, row, fibrecol, &ibuf);
			 }

			 /* decide what to do with the new point: is it closer than xshift
	     to the maximum? */
			 if (fushift >= fxshift) {
				 /* yes, ushift is at least as good as xshift */
				 /* on which side of xshift is ushift? */
				 if (ushift >= xshift) ashift=xshift; else bshift=xshift;
				 vshift=wshift;
				 wshift=xshift;
				 xshift=ushift;
				 fvshift=fwshift;
				 fwshift=fxshift;
				 fxshift=fushift;
			 }
			 else {
				 if (ushift < xshift) ashift=ushift; else bshift=ushift;
				 if (fushift >= fwshift || wshift == xshift) {
					 vshift=wshift;
					 wshift=ushift;
					 fvshift=fwshift;
					 fwshift=fushift;
				 }
				 else if (fushift >= fvshift || vshift == xshift ||
						 vshift == wshift) {
					 vshift=ushift;
					 fvshift=fushift;
				 }
			 }
		 }
	 }

	 /* did we get a solution or did we run out of iterations? */
	 if (done == FALSE) {
		 sprintf(output,
				 "Error: gausscorrel did not converge within %d iterations",
				 maxcorriters);
		 SCTPUT(output);
		 return flames_midas_fail();
	 }

	 y_shift_self[ifibre]=ydelta;

	 /* since we did find a solution, brag about it */
	 sprintf(output, "y shift for fibre %d = %g", ifibre+1, ydelta);
	 SCTPUT(output);

		}
		sprintf(frm_id, "%1d",(iframe+1));
		sprintf(qc_y_self_shift_key,"%14s", "DRS_Y_SELF_SHF");
		strcat( qc_y_self_shift_key,frm_id);
		SCDWRD(outtid,qc_y_self_shift_key,y_shift_self,1,N_FIBRES_MAX,mid_unit);
	}


	/* close the output table */
	if (outtabname != NULL) TCTCLO(outtid);

	/* we can free local arrays now */
	free_lvector(fibrelist, 0, myflats->maxfibres-1);
	free_dvector(y_shift_self, 0, N_FIBRES_MAX);

	free_cvector(framebuffer->fibremask, 0, myflats->maxfibres-1);
	free(framebuffer);
	free_dmatrix(ordercentres, 0, Order->lastorder-Order->firstorder,
			0, myflats->subcols-1);
	free_lmatrix(ilowlimits, 0, Order->lastorder-Order->firstorder,
			0, myflats->subcols-1);
	free_lmatrix(iuplimits, 0, Order->lastorder-Order->firstorder,
			0, myflats->subcols-1);

	return NOERR;

}

/**@}*/




