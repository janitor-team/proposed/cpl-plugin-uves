/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/

/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_freeoneflats  this function frees memory for the internal 
 *            array members of a dummy allflats structure
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

/* Program  : freeoneflats.c                                               */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : free allocated memory for flat                               */
/*                                                                         */
/*                                                                         */
/* Input:  pointer to allflats                                             */ 
/*                                                                      */
/* Output:     none                                                       */
/*                                                                         */
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_freeoneflats.h>
#include <flames_newmatrix.h>

/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_freeoneflats()  
   @short  free allocated memory for flat
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param myflats

   @return success or failure code

   DRS Functions called:                                                  
   free_fdmatrix                                                          
   free_fmmatrix                                                          
   free_cvector                                                           
   free_lvector                                                           
   free_ivector                                                           
   free_fm3tensor                                                         
   free_fd3tensor                                                         
   free_l3tensor                                                          

   Pseudocode:                                                           
   free allocated memory                                                  

   @note
 */

flames_err 
freeoneflats(allflats *myflats)
{

    /* free submember of flatdata */
    free_fdmatrix(myflats->flatdata[0].data,
                    0, myflats->subrows-1, 0, myflats->subcols-1);
    free_fdmatrix(myflats->flatdata[0].sigma,
                  0, myflats->subrows-1, 0, myflats->subcols-1);
    free_fmmatrix(myflats->flatdata[0].badpixel,
                  0, myflats->subrows-1, 0, myflats->subcols-1);
    free_cvector(myflats->flatdata[0].framename, 0, CATREC_LEN);
    free_cvector(myflats->flatdata[0].sigmaname, 0, CATREC_LEN);
    free_cvector(myflats->flatdata[0].badname, 0, CATREC_LEN);
    free_lvector(myflats->flatdata[0].fibres,
                 0, myflats->flatdata[0].numfibres-1);
    /* free flatdata array itself */
    free(myflats->flatdata);

    /* free other dynamic allflats members */
    free_ivector(myflats->fibremask, 0,myflats->maxfibres-1);
    free_ivector(myflats->fibre2frame, 0,myflats->maxfibres-1);
    free_fd3tensor(myflats->normfactors,
                   0, myflats->lastorder-myflats->firstorder,
                   0, myflats->maxfibres-1, 0, myflats->subcols-1);
    free_fd3tensor(myflats->normsigmas,
                   0, myflats->lastorder-myflats->firstorder,
                   0, myflats->maxfibres-1, 0, myflats->subcols-1);
    free_fm3tensor(myflats->goodfibres,
                   0, myflats->lastorder-myflats->firstorder,
                   0, myflats->maxfibres-1, 0, myflats->subcols-1);
    free_l3tensor(myflats->lowfibrebounds,
                  0, myflats->lastorder-myflats->firstorder,
                  0, myflats->maxfibres-1, 0, myflats->subcols-1);
    free_l3tensor(myflats->highfibrebounds,
                  0, myflats->lastorder-myflats->firstorder,
                  0, myflats->maxfibres-1, 0, myflats->subcols-1);

    /* ok, finished */

    return(NOERR);

}
/**@}*/
