/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_SINGLE_CORREL_H
#define FLAMES_SINGLE_CORREL_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>

/* The following function computes the correlation between a synthetic 
   frame with gaussian-shaped fibres listed in fibrelist, centered on fibre 
   positions (as given in the Order structure), shifted by
   yshift and an actual, real frame ScienceFrame.
 */

double 
singlecorrel(flames_frame *ScienceFrame, 
             orderpos *Order, 
             int32_t *fibrelist,
             int32_t nlitfibres,
             double **ordercentres, 
             int32_t **ilowlimits,
             int32_t **iuplimits,
             int32_t correlxstep,
             double yshift);

#endif
