/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2013-07-22 07:28:53 $
 * $Revision: 1.18 $
 * $Name: not supported by cvs2svn $
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup flames_cal_wavecal  Recipe: FLAMES wavelength calibration
 *
 * See man-page for details.
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <flames.h>

#include <uves_wavecal_body.h>
#include <uves_recipe.h>
#include <uves.h>
#include <uves_error.h>

#include <cpl.h>

/*-----------------------------------------------------------------------------
                            Forward declarations
 -----------------------------------------------------------------------------*/
static int flames_cal_wavecal_define_parameters(cpl_parameterlist *parameters);
const char * const flames_wavecal_desc_short = "Performs the wavelength calibration";

const char * const flames_wavecal_desc =
"The recipe performs a wavelength calibration for each extraction window.\n"
"Conceptually, each chip contains a number of order lines, each of which\n"
"contains a number of fibre traces, each of which contains a number of\n"
"extraction windows. For FLAMES/UVES data there\n"
"are multiple traces per order but only one extraction window per trace.\n"
"The number of traces is defined in the order table while the geometry of\n"
"the extraction windows is specified by recipe parameters (see below).\n"
"\n"
"Expected input for this recipe is an arc lamp frame, FIB_ARC_LAMP_RED,\n"
"FIB_ORDEF_TABLE_(REDL|REDU), 'guess' line table(s)\n"
"for each chip, FIB_LIN_GUE_(REDL|REDU), a wavelength catalogue table, \n"
"LINE_REFER_TABLE, and optionally a wavelength table of bright lines,\n"
"LINE_INTMON_TABLE, used only for computing Quality Control parameters.\n"
"\n"
"The output line table(s), FIB_LINE_TABLE_(REDL|REDU), contains the columns\n"
"X            : Horizontal position (from Gaussian fit) of detected line\n"
"dX           : Uncertainty (one sigma) of X\n"
"Ynew         : Vertical position of detected line\n"
"XWidth       : Width (in pixels) of detected line from Gaussian fit\n"
"Peak         : Intensity of detected line\n"
"Background   : Fitted background (ADU) of detected line\n"
"Slope        : Linear background slope (ADU/pixel) of detected line\n"
"               from Gaussian fit\n"
"Intensity    : Intensity of detected line scaled to unit exposure\n"
"               time. (This column only present if a LINE_INTMON_TABLE\n"
"               is provided.)\n"
"Order        : Absolute order number of detected line\n"
"Y            : Relative order number of detected line\n"
"               (it's not a very descriptive column name)\n"
"WaveC        : Wavelength of this line (computed using the resulting\n"
"               dispersion relation)\n"
"dLambdaC     : Uncertainty (one sigma) of 'WaveC'.\n"
"Pixel        : The width in w.l.u. of a pixel (computed locally).\n"
"Residual     : Residual (in w.l.u.) of this line\n"
"Residual_pix : Residual (in pixels) of this line\n"
"Lambda_candidate : Nearest line in catalogue\n"
"dLambda_cat_sq   : Squared distance to nearest catalogue line\n"
"dLambda_nn_sq    : Squared distance to nearest neighbour multiplied by ALPHA\n"
"Ident        : The wavelength associated with this emission line,\n"
"               or invalid if this line was not identified\n"
"dIdent       : Uncertainty of catalogue wavelength\n"
"Select       : 1 if the line was identified, 0 otherwise\n"
"NLinSol      : 1 if the line was identified and accepted for the\n"
"               polynomial fit, 0 otherwise\n"
"Intensity    : Intensity of detected line scaled to unit exposure\n"
"               time. (This column is present only if a LINE_INTMON_TABLE\n"
"               is provided.)\n"
"\n"
"The 2nd table extension contains the dispersion relation (a 2d polynomial).\n"
"The 3rd table extension contains the map from (pixel, pixel)-space to\n"
" physical order numbers (used internally by the calibration recipe; \n"
"another 2d polynomial).\n"
"\n"
"If there is more than one extraction window, the results of each calibration\n"
"is stored in subsequent table extensions of the same FITS file. For \n"
"example, extensions 4, 5 and 6 would contain the resulting line table \n"
"(and its two associated polynomials) for the second extraction window. \n"
"The results for the calibration of the n'th extraction window is stored \n"
"in extensions (3*n - 2) to 3*n.\n";

/*-----------------------------------------------------------------------------
                            Recipe standard code
 -----------------------------------------------------------------------------*/
#define cpl_plugin_get_info flames_cal_wavecal_get_info
UVES_RECIPE_DEFINE(
    FLAMES_CAL_WAVECAL_ID, FLAMES_CAL_WAVECAL_DOM, flames_cal_wavecal_define_parameters,
    "Jonas M. Larsen", "cpl@eso.org",
    flames_wavecal_desc_short,
    flames_wavecal_desc);

/*-----------------------------------------------------------------------------
                              Functions code
 -----------------------------------------------------------------------------*/
/**@{*/
/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    parameters        the parameterlist to fill
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int
flames_cal_wavecal_define_parameters(cpl_parameterlist *parameters)
{
    double slit_length_in_pixels = 7;
    return uves_wavecal_define_parameters_body(parameters, 
                                               make_str(FLAMES_CAL_WAVECAL_ID),
                           slit_length_in_pixels);
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Get the command line options and execute the data reduction
  @param    parameters  the parameters list
  @param    frames      the frames list
  @return   CPL_ERROR_NONE if everything is ok

 */
/*----------------------------------------------------------------------------*/
static void
flames_cal_wavecal_exe(cpl_frameset *frames, const cpl_parameterlist *parameters,
               const char *starttime)
{
    bool flames = true;

    uves_wavecal_exe_body(frames, flames, make_str(FLAMES_CAL_WAVECAL_ID),
              parameters, starttime);
}
/**@}*/


