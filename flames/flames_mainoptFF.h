/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_MAINOPTFF_H
#define FLAMES_MAINOPTFF_H

#include <cpl.h>

int flames_mainoptFF(const char *IN_A,
                     const cpl_frameset* IN_B,
                     const cpl_frameset* IN_C,
                     const char *IN_D,
                     const char *IN_E,
                     const char *IN_F,
                     const double *DECENTSNR,
                     const double *MAX_DISCARD_FRACT,
                     const int *MAX_BACK_ITERS,
                     const int *MAX_CORR_ITERS,
                     const int *MIN_OPT_ITERS_INT,
                     const int *MAX_OPT_ITERS_INT,
                     const int *X_KILL_SIZE,
                     const int *Y_KILL_SIZE,
                     const int *BKGPOL,
                     const char *BKGFITINLINE,
                     const char *BKGFITMETHOD,
                     const char *BKGBADSCAN,
                     const int *BKGBADWIN,
                     const double *BKGBADMAXFRAC,
                     const int *BKGBADMAXTOT,
                     const double *SIGMA,
                     const double *MAXYSHIFT,
                     const double *CORRELTOL,
                     const int *CORRELXSTEP,
                     double *OUTPUTD,
                     int *OUTPUTI);

#endif
