/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_checksize2   Substep: check input frame size descriptors
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                          Includes
 --------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_checksize2.h>
#include <uves_msg.h>
#include <flames_newmatrix.h>
/**@{*/

/*---------------------------------------------------------------------------
                            Implementation
 ---------------------------------------------------------------------------*/
/**
 @name  flames_checksize2()  
 @short  check input frame size descriptors
 @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

 @param frameid   input frame id
 @param slitflats input al slit flat structure


 @return success or failure code

 DRS Functions called:          
       none                                         
                                                                         
 Pseudocode:                                                             
     check several MIDAS descriptors                                     

@note
*/

flames_err checksize2(int frameid, allslitflats *slitflats)
{
  int actvals=0, unit=0, null=0, naxis=0;
  int npix[3]={0,0,0};
  double start[3]={0,0,0}, step[3]={0,0,0};

  if (0 != SCDRDI(frameid, "NAXIS", 1, 1, &actvals, &naxis, &unit, &null)) {
    /* problems reading NAXIS */
    return(MAREMMA);
  }
 
  uves_msg_debug("naxis=%d",naxis);

  if (naxis != 3) {
    /* wrong file dimensions: wrong frame name? */
    return(MAREMMA);
  }
 
   if (0 != SCDRDI(frameid, "NPIX", 1, 3, &actvals, npix, &unit, &null)) {
    /* problems reading NPIX */
    return(MAREMMA);
  }

 uves_msg_debug("npix=%d %d %d",npix[0],npix[1],npix[2]);
 uves_msg_debug("versus %d %d %d",
           slitflats->subcols, 
           1+slitflats->lastorder-slitflats->firstorder,2);
   if (npix[2] != 2 || 
      npix[1] != 1+slitflats->lastorder-slitflats->firstorder || 
      npix[0] != slitflats->subcols) {
    /* wrong file dimensions: wrong frame name? */
    return(MAREMMA);
  }


   if ( 0 != SCDRDD(frameid, "START", 1, 3, &actvals, start, &unit, &null) ) {
    /* problems reading START */
    return(MAREMMA);
  }
 

 uves_msg_debug("start= %f %f %f",start[0],start[1],start[2]);
 uves_msg_debug("check= %f %d %d",
          slitflats->substartx,slitflats->firstorder,0);
  if (start[2] != 0 ||
      start[1] != slitflats->firstorder || 
      start[0] != slitflats->substartx) {
    /* wrong file dimensions: wrong frame name? */
    return(MAREMMA);
  }



   if (SCDRDD(frameid, "STEP", 1, 3, &actvals, step, &unit, &null) != 0) {
    /* problems reading START */
    return(MAREMMA);
  }



   if (step[2] != 1 || step[1] != 1 || step[0] != slitflats->substepx) {
    /* wrong file dimensions: wrong frame name? */
    return(MAREMMA);
  }
 uves_msg_debug("step= %f %f %f",step[0],step[1],step[2]);
 uves_msg_debug("check= %f %d %d",slitflats->substepx,1,1);



   /* the file dimensions match, go ahead */

  return(NOERR);

}
/**@}*/









