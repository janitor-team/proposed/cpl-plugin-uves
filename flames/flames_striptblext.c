/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : striptblext.c                                                */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <flames_striptblext.h>
#include <flames_midas_def.h>
#include <flames_uves.h>


flames_err striptblext(char *tablename, char *basename)
/* jmlarsen: changed to strip .fits, not .tbl, extension */
{
    int namelength=0;
    int baselength=0;
    int i=0;
    char sbuffer[6];
    char message[CATREC_LEN+1];

    memset(sbuffer, '\0', 6);
    memset(message, '\0', CATREC_LEN+1);

    /* find out whether the tablename includes the .fits extension or not */
    namelength = strlen(tablename);
    if (namelength >= 5) {
        /* it may contain an extension */
        baselength = namelength-5;
        if (tablename[baselength] == '.') {
            /* yes, it contains an extension */
            /* convert the extension to lowercase */
            for (i=0; i<=4; i++)
                sbuffer[i] = (char) tolower((int) tablename[baselength+i]);
            sbuffer[5] = '\0';
            if (strncmp(sbuffer, ".fits", 5) != 0) {
                sprintf(message, "Warning: unrecognised %s extension.\n",
                                tablename+baselength);
                SCTPUT(message);
                SCTPUT("It will be stripped and substituted with the default (.fits)");
            }
            /* copy the stripped tablename to basename */
            strncpy(basename, tablename, baselength);
        }
        else {
            /* no extension present, take the whole name */
            baselength = namelength;
        }
    }
    else {
        /* no extension present, take the whole name */
        baselength = namelength;
    }
    if (baselength == 0) {
        /* there is no name left: complain and exit */
        sprintf(message, "Invalid output file name %s", tablename);
        SCTPUT(message);
        return(MAREMMA);
    }
    /* copy the tablename without extension to basename */
    strncpy(basename, tablename, baselength);
    /* add a null character to always properly terminate the string */
    basename[baselength] = '\0';

    return(NOERR);
}

