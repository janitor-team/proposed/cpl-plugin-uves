/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : mainopt.c                                                    */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/
/* C functions include files */ 
/* 020716    KB */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* MIDAS include files */
#include <flames_midas_def.h>
/* FLAMES-UVES include files */ 
#include <flames_readallff.h>
#include <flames_optsynth.h>
#include <flames_writesynth.h>
#include <flames_optimal.h>
#include <flames_quickoptimal.h>
#include <flames_scatter.h>
#include <flames_write_spectra.h>
#include <flames_prepextract.h>
#include <flames_quickprepextract.h>
#include <flames_readback.h>
#include <flames_readframe.h>
#include <flames_doquickoptimal.h>
#include <flames_freeordpos.h>
#include <flames_freeframe.h>
#include <flames_writeback.h>
#include <flames_freeallflats.h>
#include <flames_newmatrix.h>
#include <flames_computeback.h>
#include <flames_mainoptquick.h>
#include <flames_freespectrum.h>
#include <uves_msg.h>
#include <flames_readordpos.h>


int flames_mainoptquick(const char *IN_A,
                        const cpl_frameset *fibff_set,
                        const char *IN_D,
                        const char *IN_E,
                        const char *BASENAME,
                        const double *MAXDISCARDFRACT,
                        const int *MAXBACKITERS,
                        const int *MINOPTITERSINT,
                        const int *MAXOPTITERSINT,
                        const int *XKILLSIZE,
                        const int *YKILLSIZE,
                        const int *BKGPOL,  
                        const char *BKGFITINLINE, //was int*
                        const char *BKGFITMETHOD,
                        const char *BKGBADSCAN,  //was int*
                        const int *BKGBADWIN, 
                        const double *BKGBADMAXFRAC,
                        const int *BKGBADMAXTOT,
                        const double *SIGMA,
                        double *OUTPUTD,
                        int *OUTPUTI)

{ 

    char output[200];
    int i=0;
    int unit=0;
    int actvals=0;
    frame_mask **mask=0;
    int bxydegrees[2], bxdegree=0, bydegree=0;
    double kappa=0;
    double kappa2=0;
    int chisqpixels=0, nfittedparams=0;
    double chisquare=0;
    frame_data **backframe;
    flames_err status=0;
    flames_frame *ScienceFrame=0;

    allflats *Shifted_FF=0;

    orderpos *Order=0;

    //char allfile[CATREC_LEN+2];
    const cpl_frameset* allfile=fibff_set;
    char backfile[CATREC_LEN+2];
    char infile[CATREC_LEN+2];
    char catfile[CATREC_LEN+2];
    char orderfile[CATREC_LEN+2];
    int32_t n=0;
    int nval=0;
    int null=0;
    int32_t iorder=0, ifibre=0, ix=0;
    scatterswitch bkgswitch=USEALL;
    scatterswitch2 bkgswitch2=NOBADSCAN;
    int badwinsize[2], badwinxsize=0;
    int badwinysize=0;
    double badfracthres=0;
    int badtotthres=0;
    char keytype=0;
    int noelem=0;
    int bytelem=0;
    int maxbackiters=0;
    double maxdiscardfract=0;
    int ibuf=0;
    int32_t minoptitersint=0;
    int32_t maxoptitersint=0;
    int32_t xkillsize=0;
    int32_t ykillsize=0;
    char bkgfitmethod[CATREC_LEN+1];
    int32_t negativepixels=0;
    char bkginline=TRUE;

    frame_data *fdvecbuf1=0;
    frame_data *fdvecbuf2=0;
    frame_data *fdvecbuf3=0;



    frame_mask *fmvecbuf1=0;


    frame_data pixelvalue=0;


    int32_t lastiyixindex=0;
    int32_t iorderifibreindex=0;
    int32_t iorderifibreixoffset=0;
    int32_t iorderifibreixindex=0;
    int32_t ixiorderifibreindex=0;

    memset(output, 0, 200);
    memset(bkgfitmethod, 0, CATREC_LEN+1);

    bkgswitch = USEALL;

    SCSPRO("optimalquick"); /* Get into MIDAS Env.+enable automatic error
                 handling */

    /* by default, the application aborts, if an SC call fails,
     to make the code cleaner, 
     SCECNT has to be used, if you want to handle errors from the 
     SC routines yourself (see the Midas environment doc.)  */

    /* Read once and for all, here at the beginning, what MIDAS keywords we
     need */

    /* get input frame name from MIDAS env.*/
    SCKGETC(IN_A,1,CATREC_LEN+1,&nval,infile);


    /* read input table name */
    SCKGETC(IN_D,1,160,&nval,backfile);

    /* read order table name */
    SCKGETC(IN_E,1,160,&nval,orderfile);

    /* read output base name */
    SCKGETC(BASENAME,1,160,&nval,catfile);

    /* initialise MAXDISCARDFRACT from keyword */
    SCKRDD(MAXDISCARDFRACT, 1, 1, &actvals, &maxdiscardfract, &unit,&null);


    /* initialise MAXBACKITERS from keyword */
    SCKRDI(MAXBACKITERS, 1, 1, &actvals, &maxbackiters, &unit,&null);


    /* read the MINOPTITERSINT keyword */
    SCKRDI(MINOPTITERSINT, 1, 1, &actvals, &ibuf, &unit, &null);
    if (ibuf<1) {
        SCTPUT("Warning: illegal (<1) value for MINOPTITERSINT, falling back to \
1");
        ibuf=1;
    }
    minoptitersint = (int32_t) ibuf;
    /* read the MAXOPTITERSINT keyword */
    SCKRDI(MAXOPTITERSINT, 1, 1, &actvals, &ibuf, &unit, &null);

    maxoptitersint = (int32_t) ibuf;
    if (maxoptitersint<minoptitersint) {
        SCTPUT("Warning: illegal value (too low) for MAXOPTITERSINT, \
falling back to MINOPTITERSINT");
        maxoptitersint=minoptitersint;
    }
    /* read the XKILLSIZE keyword */
    SCKRDI(XKILLSIZE, 1, 1, &actvals, &ibuf, &unit, &null);
    if (ibuf<0) {
        SCTPUT("Warning: illegal (negative) value for XKILLSIZE, falling \
back to 0");
        ibuf=0;
    }
    xkillsize = (int32_t) ibuf;
    /* read the YKILLSIZE keyword */
    SCKRDI(YKILLSIZE, 1, 1, &actvals, &ibuf, &unit, &null);
    if (ibuf<0) {
        SCTPUT("Warning: illegal (negative) value for YKILLSIZE, falling \
back to 0");
        ibuf=0;
    }
    ykillsize = (int32_t) ibuf;
    /* initialise background fitting scalars */
    SCKRDI(BKGPOL, 1, 2, &actvals, bxydegrees, &unit, &null);
    //SCKRDI(BKGPOLX, 1, 1, &actvals, bxdegree, &unit, &null);
    //SCKRDI(BKGPOLY, 1, 1, &actvals, bydegree, &unit, &null);
    bxdegree = bxydegrees[0];
    bydegree = bxydegrees[1];

    /* Is inline background fitting and subtraction required? */
    SCKFND_string(BKGFITINLINE, &keytype, &noelem, &bytelem);
    switch(keytype) {
    case 'C':
        /* it exists and it is a character keyword, do read it */
        SCKGETC(BKGFITINLINE, 1, CATREC_LEN, &nval, bkgfitmethod);
        if (nval<1) {
            SCTPUT("Warning: BKGFITINLINE is ambiguous, falling back to default");
        }
        else {
            /* convert bkgfitmethod to upper case, to ease the subsequent check */
            for (i=0; i<=(nval-1); i++) bkgfitmethod[i] = toupper(bkgfitmethod[i]);
            /* compare as many letters as we have with the expected values */
            if (strncmp("YES", bkgfitmethod, (size_t) nval) == 0)
                bkginline = TRUE;
            else if (strncmp("NO", bkgfitmethod, (size_t) nval) == 0)
                bkginline = FALSE;
            else
                SCTPUT("Warning: unsupported BKGFITINLINE value, falling back to \
default");
        }
        break;
    case ' ':
        /* the keyword does not exist at all */
        SCTPUT("Warning: BKGFITINLINE is undefined, falling back to default");
        break;
    default:
        /* the keyword is of the wrong type */
        SCTPUT("Warning: BKGFITINLINE is not a string, falling back to default");
        break;
    }

    /* before trying to read it, make sure that BKGFITMETHOD exists and it is
     of the appropriate type */
    SCKFND_string(BKGFITMETHOD, &keytype, &noelem, &bytelem);
    switch(keytype) {
    case 'C':
        /* it exists and it is a character keyword, go ahead and read it */
        SCKGETC(BKGFITMETHOD, 1, CATREC_LEN, &nval, bkgfitmethod) ;
        /* is the string long enough to be unambiguous? */
        if (nval<2) {
            /* no, fall back to the default */
            SCTPUT("Warning: BKGFITMETHOD is ambiguous, falling back to default");
        }
        else {
            /* convert bkgfitmethod to upper case, to ease the subsequent check */
            for (i=0; i<=(nval-1); i++) bkgfitmethod[i] = toupper(bkgfitmethod[i]);
            /* compare as many letters as we have with the expected values */
            if (strncmp("ALL", bkgfitmethod, (size_t) nval) == 0)
                bkgswitch = USEALL;
            else if (strncmp("MEDIAN", bkgfitmethod, (size_t) nval)
                            == 0) bkgswitch = USEMEDIAN;
            else if (strncmp("MINIMUM", bkgfitmethod, (size_t) nval)
                            == 0) bkgswitch = USEMINIMUM;
            else if (strncmp("AVERAGE", bkgfitmethod, (size_t) nval)
                            == 0) bkgswitch = USEAVERAGE;
            else
                SCTPUT("Warning: unsupported BKGFITMETHOD value, falling back to \
default");
        }
        break;
    case ' ':
        /* the keyword does not exist at all */
        SCTPUT("Warning: BKGFITMETHOD is undefined, falling back to default");
        break;
    default:
        /* the keyword is of the wrong type */
        SCTPUT("Warning: BKGFITMETHOD is not a string, falling back to default");
        break;
    }

    /* before trying to read it, make sure that BKGBADSCAN exists and it is
     of the appropriate type */
    SCKFND_string(BKGBADSCAN, &keytype, &noelem, &bytelem);
    switch(keytype) {
    case 'C':
        /* it exists and it is a character keyword, go ahead and read it */
        SCKGETC(BKGBADSCAN, 1, CATREC_LEN, &nval, bkgfitmethod) ;
        /* is the string long enough to be unambiguous? */
        if (nval<1) {
            /* no, fall back to the default */
            SCTPUT("Warning: BKGBADSCAN is ambiguous, falling back to default");
        }
        else {
            /* convert bkgfitmethod to upper case, to ease the subsequent check */
            for (i=0; i<=(nval-1); i++) bkgfitmethod[i] = toupper(bkgfitmethod[i]);
            /* compare as many letters as we have with the expected values */
            if (strncmp("NONE", bkgfitmethod, (size_t) nval) == 0)
                bkgswitch2 = NOBADSCAN;
            else if (strncmp("FRACTION", bkgfitmethod, (size_t) nval)
                            == 0) bkgswitch2 = FRACBADSCAN;
            else if (strncmp("ABSOLUTE", bkgfitmethod, (size_t) nval)
                            == 0) bkgswitch2 = ABSBADSCAN;
            else
                SCTPUT("Warning: unsupported BKGBADSCAN value, falling back to \
default");
        }
        break;
    case ' ':
        /* the keyword does not exist at all */
        SCTPUT("Warning: BKGBADSCAN is undefined, falling back to default");
        break;
    default:
        /* the keyword is of the wrong type */
        SCTPUT("Warning: BKGBADSCAN is not a string, falling back to default");
        break;
    }

    /* if neighborhood bad pixel scanning was requested, read the other
     keywords needed */
    if (bkgswitch2 == FRACBADSCAN) {
        SCKRDI(BKGBADWIN, 1, 2, &actvals, badwinsize, &unit, &null);
        //status = SCKRDI(BKGBADWINX, 1, 1, &actvals, &badwinxsize, &unit, &null);
        //status = SCKRDI(BKGBADWINY, 1, 1, &actvals, &badwinysize, &unit, &null);
        badwinxsize = badwinsize[0];
        badwinysize = badwinsize[1];
        SCKRDD(BKGBADMAXFRAC, 1, 1, &actvals, &badfracthres, &unit,&null);

        /* check the values read for consistence */
        if ((badwinxsize < 0) || (badwinysize < 0)) {
            SCTPUT("Warning: BKGBADWIN values must be non negative, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
        if (badfracthres < 0) {
            SCTPUT("Warning: BKGBADMAXFRAC value must be non negative, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
    }
    else if (bkgswitch2 == ABSBADSCAN) {
        SCKRDI(BKGBADWIN, 1, 2, &actvals, badwinsize, &unit, &null);
        //SCKRDI(BKGBADWINX, 1, 1, &actvals, &badwinxsize, &unit, &null);
        //SCKRDI(BKGBADWINY, 1, 1, &actvals, &badwinysize, &unit, &null);
        badwinxsize = badwinsize[0];
        badwinysize = badwinsize[1];
        SCKRDI(BKGBADMAXTOT, 1, 1, &actvals, &badtotthres, &unit,&null);


        /* check the values read for consistence */
        if ((badwinxsize < 0) || (badwinysize < 0)) {
            SCTPUT("Warning: BKGBADWIN values must be non negative, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
        if (badtotthres < 0) {
            SCTPUT("Warning: BKGBADMAXTOT value must be non negative, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
    }

    /* read the kappa factor to be used later in kappa-sigma clipping */
    SCKRDD(SIGMA, 1, 1, &actvals, &kappa, &unit, &null);
    /* compute once and for all the square of kappa, as we will be using that */
    kappa2 = kappa*kappa;


    /* Link the MIDAS names of the frames to the physical ones */

    if(!(ScienceFrame = calloc(1, sizeof(flames_frame)))) {
        SCTPUT("Allocation error during ScienceFrame memory allocation");
        return flames_midas_fail();
    }

    /* let's read the Science Frame */
    sprintf(output, "I'm reading the frame %s", infile);
    SCTPUT(output);

    if (readframe(ScienceFrame, infile) != NOERR) {
        SCTPUT("Error while reading the frame");
        return flames_midas_fail();
    }

    /* Read the table data and then put them in the C structures */
    sprintf(output,"Reading the order/fibre table...");
    SCTPUT(output);
    if(!(Order = calloc(1, sizeof(orderpos)))) {
        SCTPUT("Allocation error during the allocation of Order structure");
        return flames_midas_fail();
    }
    /* use the readordpos function to read the descriptors */
    if (readordpos(orderfile, Order) != NOERR) {
        SCTPUT("Error while reading the order table");
        return flames_midas_fail();
    }
    /* does the order table match the Science frame chip? */
    if(Order->chipchoice != ScienceFrame->chipchoice) {
        /* no, it doesn't */
        SCTPUT("Error: chip mismatch between Science frame and order table");
        return flames_midas_fail();
    }

    /* initialise firstorder and lastorder in ScienceFrame from Order */
    ScienceFrame->firstorder = Order->firstorder;
    ScienceFrame->lastorder = Order->lastorder;
    ScienceFrame->tab_io_oshift = Order->tab_io_oshift;


    /* allocate and initialise the frame which will contain the
     estimated background. This will remain zero if no background fitting 
     was required*/
    lastiyixindex = (ScienceFrame->subrows*ScienceFrame->subcols)-1;
    backframe = fdmatrix(0, ScienceFrame->subrows-1, 0,
                    ScienceFrame->subcols-1);
    memset(&backframe[0][0], 0,
           ScienceFrame->subrows*ScienceFrame->subcols*sizeof(frame_data));

    if (bkginline==TRUE) {
        /* Inline background fitting and subtraction required */

        SCTPUT("*** Step 1: Background fitting and subtraction ***\n");

        /* Read the data of the frames and put them in the right C structures */

        /* Get the Midas name of the frame to be read */

        sprintf(output, "I'm reading the background table %s", backfile);
        SCTPUT(output); /* Display text w/o storing it into MIDAS logfile */

        /* read in the background table */
        if ((status=readback(&(ScienceFrame->back), backfile, bxdegree, bydegree))
                        != NOERR) {
            /* something went wrong while reading the background table */
            sprintf(output, "Error %d while reading the background table", status);
            SCTPUT(output);
            return flames_midas_fail();
        }


        /* Calculate the fit model of the background */

        SCTPUT("Start the background fitting procedure");

        if (ScienceFrame->back.Window_Number > 0) {
            if (scatter(ScienceFrame, Order, bkgswitch, bkgswitch2, badwinxsize,
                            badwinysize, badfracthres, badtotthres, kappa2,
                            (int32_t) maxbackiters, maxdiscardfract, OUTPUTI)) {
                SCTPUT("Error executing the scatter function");
                return flames_midas_fail();
            }
            /* compute the estimated background */
            if (computeback(ScienceFrame, backframe) != NOERR) {
                SCTPUT("Error computing fitted background");
                return flames_midas_fail();
            }
            /* subtract the estimated background from the data frame */
            /* the error of the estimated background is assumed to be negligible */
            SCTPUT("Subtracting fitted background from Science Frame");
            fdvecbuf1 = ScienceFrame->frame_array[0];
            fdvecbuf2 = backframe[0];
            for (ix=0; ix<=lastiyixindex; ix++) {
                fdvecbuf1[ix] -= fdvecbuf2[ix];
            }
            /* some more black magic to make the thing more robust: scan the frame
     for any negative pixel values; if any are found, compare them to the
     standard deviation: if the negative value is not compatible with 
     zero, mark that pixel as bad */
            negativepixels=0;
            fdvecbuf1 = ScienceFrame->frame_array[0];
            fdvecbuf2 = ScienceFrame->frame_sigma[0];
            fmvecbuf1 = ScienceFrame->badpixel[0];
            for (ix=0; ix<=lastiyixindex; ix++) {
                if (fmvecbuf1[ix]==0 && (pixelvalue=fdvecbuf1[ix])<0) {
                    if ((pixelvalue*pixelvalue)>4*fdvecbuf2[ix]) {
                        fmvecbuf1[ix]=1;
                        negativepixels++;
                    }
                }
            }
            if (negativepixels!=0) {
                sprintf(output, "Warning: %d pixels result lower than fitted \
background", negativepixels);
                SCTPUT(output);
                SCTPUT("either they are unmasked bad pixels or some contamination is");
                SCTPUT("skewing background determination");
            }
            SCTPUT("Writing fitted background frame to middumma.bdf");
            if (writeback(ScienceFrame, "middumma.bdf", backframe)!=NOERR)
                SCTPUT("Warning: error writing background frame to disk");
        }
        else {
            SCTPUT("Error: no regions available for background estimation");
            return flames_midas_fail();
        }

    }


    /* Allocate memory for the Shifted_FF structure */
    if (!(Shifted_FF=calloc(1, sizeof(allflats)))) {
        SCTPUT("Allocation while allocating memory for the shifted FF");
        return flames_midas_fail();
    }

    /* let's read the fibre FFs */
    sprintf(output, "I'm reading the fibre FF frames");
    SCTPUT(output);
    if (readallff(allfile, Shifted_FF) != NOERR) {
        SCTPUT("Error while reading the fibre Flat Field frames");
        return flames_midas_fail();
    }

    /* is this fibre FF frames set shiftable? */
    if (Shifted_FF->shiftable == 'y') {
        sprintf(output, "The fibre FF set is slit-flatfielded");
        SCTPUT(output);
        return flames_midas_fail();
    }


    SCTPUT("\n*** Step 3: prepare merged bad pixel mask and final \
initialisations ***\n");
    /* allocate the global, merged bad pixel mask */
    mask=fmmatrix(0,ScienceFrame->subrows-1,0,ScienceFrame->subcols-1);
    if(!mask) {
        SCTPUT("Error in allocating mask matrix");
        return flames_midas_fail();
    }

    /* initialise the global, merged bad pixel mask to be used
     in the subsequent optimal extraction, initialise the lookup tables for 
     lit fibres in the ScienceFrame and allocate the arrays to store the 
     allocated spectra again in ScienceFrame */
    if (quickprepextract(ScienceFrame, Shifted_FF, Order, mask)!=NOERR) {
        /* something went wrong in quickprepextract */
        SCTPUT("Error in quickprepextract");
        return flames_midas_fail();
    }

    /* start from first order, then divide orders in non-overlapping subsets */
    /* Going to do the real spectrum extraction */
    SCTPUT("\n*** Step 4: optimal extraction proper ***\n");
    if (doquickoptimal(ScienceFrame, Order, Shifted_FF, kappa2, mask, backframe,
                    minoptitersint, maxoptitersint, xkillsize, ykillsize)
                    !=NOERR) {
        SCTPUT("Error in doquickoptimal");
        return flames_midas_fail();
    }

    /* free the old bad pixel mask, replace it with the global mask */
    free_fmmatrix(ScienceFrame->badpixel, 0, ScienceFrame->subrows-1, 0,
                  ScienceFrame->subcols-1);
    ScienceFrame->badpixel = mask;

    /* build the fitted Science Frame, do it in place to save memory */
    if (optsynth(ScienceFrame, Shifted_FF, Order, &backframe, &chisquare,
                    &chisqpixels, &nfittedparams)!=NOERR){
        SCTPUT("Error computing the fitted frame");
        return flames_midas_fail();
    }

    SCKWRD(OUTPUTD, &chisquare, 1, 1, &unit);
    SCKWRI(OUTPUTI, &chisqpixels, 1, 1, &unit);
    SCKWRI(OUTPUTI, &nfittedparams, 2, 1, &unit);
    SCTPUT("Writing the synthesized data frame to middummb.bdf");
    SCTPUT("Writing the synthesized sigma frame to middummc.bdf");
    SCTPUT("Writing the overall mask to middummd.bdf");
    if (writesynth(ScienceFrame, "middummb.bdf", "middummc.bdf", "middummd.bdf")
                    != NOERR)
        SCTPUT("Warning: error writing dummy debugging frames");

    SCTPUT("\n*** Step 5: write extrated spectra to disk, clean up and \
exit ***\n");

    /* let's free a bit of memory */

    /* free the estimated background frame, we don't need it any more */
    free_fdmatrix(backframe, 0, ScienceFrame->subrows-1, 0,
                  ScienceFrame->subcols-1);

    /* Creating the output catalog of the spectra extracted */
    //Fixme: the next 3 lines are useless as that catalog is not used elsewhere
    //SCTPUT("Creating catalog of the spectra extracted");
    //SCCCRE("spectra.cat",F_IMA_TYPE,0);
    //SCTPUT("Created catalog of Spectra");

    /* can these spectra be normalised? */
    if (Shifted_FF->normalised == 'y') {
        /* yes, compensation was performed already */
        SCTPUT("Correcting for normalisation factors");
        fdvecbuf1 = Shifted_FF->normfactors[0][0];
        fdvecbuf2 = Shifted_FF->normsigmas[0][0];
        fmvecbuf1 = Shifted_FF->goodfibres[0][0];
        fdvecbuf3 = ScienceFrame->spectrum[0][0];
        frame_data* fdvecbuf4 = ScienceFrame->normsigma[0][0];
        frame_data* fdvecbuf5 = ScienceFrame->specsigma[0][0];
        frame_data* fdvecbuf6 = ScienceFrame->normspec[0][0];
        frame_mask* fmvecbuf2 = ScienceFrame->specmask[0][0];
        frame_mask* fmvecbuf3 = ScienceFrame->normmask[0][0];
        for (ifibre=0; ifibre<=(ScienceFrame->maxfibres-1); ifibre++) {
            /* is this fibre lit? */
            if ((ScienceFrame->fibremask[ifibre] == TRUE) &&
                            (Shifted_FF->fibremask[ifibre] == TRUE)) {
                for (iorder=0; iorder<=(Order->lastorder-Order->firstorder); iorder++){
                    iorderifibreindex = (iorder*ScienceFrame->maxfibres)+ifibre;
                    iorderifibreixoffset = iorderifibreindex*ScienceFrame->subcols;
                    for (ix=0; ix<=(ScienceFrame->subcols-1); ix++) {
                        iorderifibreixindex = iorderifibreixoffset+ix;
                        ixiorderifibreindex = (ix*(1+Order->lastorder-Order->firstorder)*
                                        ScienceFrame->maxfibres)+iorderifibreindex;
                        /* can this fibre be corrected here? */
                        if ((fmvecbuf2[ixiorderifibreindex]==1) &&
                                        (fmvecbuf1[iorderifibreixindex]==GOODSLICE)) {
                            /* yes, therefore correct and propagate errors */
                            frame_data normvalue = fdvecbuf1[iorderifibreixindex];
                            frame_data normvalue2 = normvalue*normvalue;
                            pixelvalue = fdvecbuf3[ixiorderifibreindex];
                            fdvecbuf4[ixiorderifibreindex] =
                                            (fdvecbuf5[ixiorderifibreindex]/normvalue2) +
                                            ((pixelvalue*pixelvalue)*fdvecbuf2[iorderifibreixindex]/
                                                            (normvalue2*normvalue2));
                            fdvecbuf6[ixiorderifibreindex] =
                                            fdvecbuf3[ixiorderifibreindex]/normvalue;
                            fmvecbuf3[ixiorderifibreindex] = 1;
                        }
                        /* no, mark this slice as bad and zero normalised spectra */
                        else {
                            fmvecbuf3[ixiorderifibreindex] = 0;
                            fdvecbuf6[ixiorderifibreindex] = 0;
                            fdvecbuf4[ixiorderifibreindex] = 0;
                        }
                    }
                }
            }
        }
    }
    else {
        SCTPUT("Warning: no correction factors available");
        fdvecbuf1 = ScienceFrame->normspec[0][0];
        fdvecbuf2 = ScienceFrame->normsigma[0][0];
        fmvecbuf1 = ScienceFrame->normmask[0][0];
        for (ifibre=0; ifibre<=(ScienceFrame->maxfibres-1); ifibre++) {
            /* is this fibre lit? */
            if ((ScienceFrame->fibremask[ifibre] == TRUE) &&
                            (Shifted_FF->fibremask[ifibre] == TRUE)) {
                for (iorder=0; iorder<=(Order->lastorder-Order->firstorder); iorder++){
                    iorderifibreindex = (iorder*ScienceFrame->maxfibres)+ifibre;
                    for (ix=0; ix<=(ScienceFrame->subcols-1); ix++) {
                        ixiorderifibreindex = (ix*(1+Order->lastorder-Order->firstorder)*
                                        ScienceFrame->maxfibres)+iorderifibreindex;
                        fmvecbuf1[ixiorderifibreindex] = 0;
                        fdvecbuf1[ixiorderifibreindex] = 0;
                        fdvecbuf2[ixiorderifibreindex] = 0;
                    }
                }
            }
        }
    }

    for (n=0; n<ScienceFrame->maxfibres; n++) {
        if(ScienceFrame->fibremask[n] == TRUE) {
            if (n==0) sprintf(output,"Writing spectrum for the first fibre...");
            else if (n==1)
                sprintf(output,"Writing spectrum for the second fibre...");
            else if (n==2) sprintf(output,"Writing spectrum for the third fibre...");
            else sprintf(output,"Writing spectrum for the %d-th fibre...", n+1);
            SCTPUT(output);
            status=Write_Spectra(ScienceFrame, n, catfile);
            if (status) SCTPUT("Error during the writing on disk");
            else if (n==0) sprintf(output,"Spectrum for the first fibre written");
            else if (n==1) sprintf(output,"Spectrum for the second fibre written");
            else if (n==2) sprintf(output,"Spectrum for the third fibre written");
            else sprintf(output,"Spectrum for the %d-th fibre written", n+1);
            SCTPUT(output);
        }
    }

    /* finally free for good the ScienceFrame (including the spectrum),
     Shifted_FF and Order */
    if (free_spectrum(ScienceFrame)!=NOERR) {
        SCTPUT("Error while freeing the spectrum in the ScienceFrame structure");
        return flames_midas_fail();
    }
    if (freeframe(ScienceFrame)!=NOERR) {
        SCTPUT("Error while freeing the memory of the ScienceFrame structure");
        return flames_midas_fail();
    }
    free(ScienceFrame);
    if (freeallflats(Shifted_FF)!=NOERR) {
        SCTPUT("Error while freeing the memory of the Shifted_FF structure");
        return flames_midas_fail();
    }
    free(Shifted_FF);
    if (freeordpos(Order)!=NOERR) {
        SCTPUT("Error while freeing the memory of the Order structure");
        return flames_midas_fail();
    }
    free(Order);

    SCTPUT("\n*** Optimal extraction complete ***\n");

    return SCSEPI();

}
