/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_PREPFIBREFF_H
#define FLAMES_PREPFIBREFF_H

int flames_prepfibreff(const cpl_frameset  * INFIBREFFCAT,
                       cpl_frameset  ** OUTFIBREFFCAT,
                       const char *MYORDER,
                       const cpl_frameset *SLITFFCAT,
                       const char *BACKTABLE,
                       const double *MAXSINGLEPXFRC,
                       const int *MAXCLEANITERS,
                       const double *FRACSLICESTHRES,
                       const double *GAUSSCORRELSCL,
                       const double *GAUSSCORRELWND,
                       const double *GAUSSFIBRESIGMA,                       
                       const double *GAUSSHALFWIDTH,                       
                       const double *MINFIBREFRAC,
                       const char *BASENAME,
                       const double *DECENTSNR,
                       const double *MAXDISCARDFRACT,
                       const int *MAXBACKITERS,
                       const int *MAXCORRITERS,
                       const int *BKGPOL,
                       const char *BKGFITMETHOD,
                       const char *BKGBADSCAN,
                       const int *BKGBADWIN,
                       const double *BKGBADMAXFRAC,
                       const int *BKGBADMAXTOT,
                       const double *SIGMA,
                       const double *MAXYSHIFT,
                       const double *CORRELTOL,
                       const int *CORRELXSTEP,
                       int *OUTPUTI);

#endif
