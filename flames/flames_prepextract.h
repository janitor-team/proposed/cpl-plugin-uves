/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: jtaylor $
 * $Date: 2012-11-19 09:16:16 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2010/09/24 09:31:29  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.1  2007/07/30 06:49:58  amodigli
 * added to repository (removed from flames_uves.h)
 *
 * Revision 1.1  2007/07/27 06:44:09  amodigli
 * added to repository
 *
 */

#ifndef FLAMES_PREP_EXTRACT_H
#define FLAMES_PREP_EXTRACT_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>


/* the following funtion initialises the global, merged bad
   pixel mask to be used in the subsequent optimal extraction, creates
   the lookup tables for lit fibres in the ScienceFrame and allocates the
   arrays to store the allocated spectra again in ScienceFrame */
flames_err 
prepextract(flames_frame *ScienceFrame, 
            allflats *Shifted_FF, 
        orderpos *Order, 
            frame_data **normcover, 
        int32_t orderoffset, 
            int32_t realfirstorder, 
        int32_t reallastorder, 
            frame_mask **mask);

#endif
