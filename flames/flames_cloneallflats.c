/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/

/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_cloneallflats   Substep: copy in flat into out flats
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                          Includes
 --------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_shiftcommon.h>
#include <flames_fillholes.h>
/**@{*/

/*---------------------------------------------------------------------------
                            Implementation
 ---------------------------------------------------------------------------*/
/**
 @name  flames_cloneallflats()  
 @short copy in flat into out flats
 @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

 @param frameid        input frame id
 @param slitflats      input allstlitflats structure


 @return success or failure code

 DRS Functions called:          
       none                                         
                                                                         
 Pseudocode:                                                             
     copy all structure elements from in to out                               

@note
*/

flames_err  cloneallflats(allflats *allflatsin, allflats *allflatsout)
{
  /* TODO: check the proper order of the dimensions in the allocation of the 
     matrix */
  /* BEWARE: the matrix is allocated with the indices starting from 1 and not 
     zero */
  /* BEWARE2: I changed my mind, the matrix is allocated with the indices 
     starting from zero */
  int32_t iframe=0, ix=0,  totpixlimit=0;
  frame_mask *fmvecbuf1=0;

  allflatsout->flatdata = 
    (singleflat *) calloc((size_t)allflatsin->nflats, sizeof(singleflat));
  totpixlimit = (allflatsin->subrows*allflatsin->subcols)-1;
  /* copy all the pointers from allflatsin, except badpixel */
  for(iframe=0;iframe<=(allflatsin->nflats-1);iframe++){
    ((allflatsout->flatdata)[iframe]).data = 
      ((allflatsin->flatdata)[iframe]).data;
    ((allflatsout->flatdata)[iframe]).sigma = 
      ((allflatsin->flatdata)[iframe]).sigma;
    ((allflatsout->flatdata)[iframe]).badpixel = 
      fmmatrix(0,(allflatsin->subrows)-1, 0, 
           (allflatsin->subcols)-1);
    fmvecbuf1 = allflatsin->flatdata[iframe].badpixel[0];
    frame_mask* fmvecbuf2 = allflatsout->flatdata[iframe].badpixel[0];
    for (ix=0; ix<=totpixlimit; ix++) fmvecbuf2[ix] = fmvecbuf1[ix];
    ((allflatsout->flatdata)[iframe]).framename = 
      ((allflatsin->flatdata)[iframe]).framename;
    ((allflatsout->flatdata)[iframe]).sigmaname = 
      ((allflatsin->flatdata)[iframe]).sigmaname;
    ((allflatsout->flatdata)[iframe]).badname = 
      ((allflatsin->flatdata)[iframe]).badname;
    ((allflatsout->flatdata)[iframe]).numfibres = 
      ((allflatsin->flatdata)[iframe]).numfibres;
    ((allflatsout->flatdata)[iframe]).fibres = 
      ((allflatsin->flatdata)[iframe]).fibres;
    ((allflatsout->flatdata)[iframe]).yshift = 
      ((allflatsin->flatdata)[iframe]).yshift;
  }
  allflatsout->nflats = allflatsin->nflats;
  allflatsout->subrows = allflatsin->subrows;
  allflatsout->subcols = allflatsin->subcols;
  allflatsout->chiprows = allflatsin->chiprows;
  allflatsout->chipcols = allflatsin->chipcols;
  allflatsout->subfirstrow = allflatsin->subfirstrow;
  allflatsout->subfirstcol = allflatsin->subfirstcol;
  allflatsout->substartx = allflatsin->substartx;
  allflatsout->substarty = allflatsin->substarty;
  allflatsout->substepx = allflatsin->substepx;
  allflatsout->substepy = allflatsin->substepy;
  allflatsout->chipstartx = allflatsin->chipstartx;
  allflatsout->chipstarty = allflatsin->chipstarty;
  allflatsout->chipstepx = allflatsin->chipstepx;
  allflatsout->chipstepy = allflatsin->chipstepy;
  allflatsout->ron = allflatsin->ron;
  allflatsout->fibremask = allflatsin->fibremask;
  allflatsout->fibre2frame = allflatsin->fibre2frame;
  allflatsout->maxfibres = allflatsin->maxfibres;
  allflatsout->halfibrewidth = allflatsin->halfibrewidth;
  allflatsout->minfibrefrac = allflatsin->minfibrefrac;
  allflatsout->firstorder = allflatsin->firstorder;
  allflatsout->lastorder = allflatsin->lastorder;
  allflatsout->normfactors = allflatsin->normfactors;
  allflatsout->normsigmas = allflatsin->normsigmas;
  allflatsout->goodfibres = allflatsin->goodfibres;
  allflatsout->lowfibrebounds = allflatsin->lowfibrebounds;
  allflatsout->highfibrebounds = allflatsin->highfibrebounds;
  
  return(NOERR);

}

/**@}*/
