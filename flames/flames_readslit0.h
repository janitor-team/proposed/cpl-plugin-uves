/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_READ_SLIT0_H
#define FLAMES_READ_SLIT0_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>

/* this function takes a pointer to an empty allslitflats structure, a frame 
   index and a frame filename; it then initialises scalars in the allslitflats
   structure from the frame descriptors, allocates array members of the 
   structure and finally reads the frame to the slit specified by the index */

flames_err 
readslit0(allslitflats *slitflats, 
          int32_t iframe, 
          char *filename);

#endif


