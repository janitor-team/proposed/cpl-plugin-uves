/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_covariance_reorder   Substep: Compute covariance 
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


#include <flames_midas_def.h>
#include <flames_covariance_reorder.h>
/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_correl()  
   @short Compute correlation 
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param covar covaraiance matrix
   @param ma    total number of parameters
   @param ia    order of parameters
   @param mfit  order of fit

   @return void


   Purpose  : Given the covariance matrix covar[1.. ma][1..ma] of a fit for
   mfit of ma total parameters, and their ordering ia[1..ma]    
   repack the covariance matrix to the true order of the        
   parameters. Elements associated with fixed parameters will   
   zero.                                                        
                                                                         
   Input:  covar[][] covariance matrix                                      
   ma        matrix dimension                                        
   ia[]      parameter ordering                                      
   mfit      number of parameters to fit                             
                                                                      
   Output:  covar[][] repacked                                          
                                                                         
   DRS Functions called:                                                   
   none                                                                    
                                                                         
   Pseudocode:                                                             
   see code: nested loops with if statements to repack covar[][]       
 
   @note
*/


void flames_covariance_reorder(double **covar, int ma, int ia[], int mfit)
{
    int i=0,j=0,k=0;
    double swap=0;



    for (i=mfit+1;i<=ma;i++)
        for (j=1;j<=i;j++) covar[i][j]=covar[j][i]=0.0;
    k=mfit;
    for (j=ma;j>=1;j--) {
        if (ia[j]) {
            for (i=1;i<=ma;i++) {
                swap = covar[i][k];
                covar[i][k] = covar[i][j];
                covar[i][j] = swap;
            }
            for (i=1;i<=ma;i++) {
                swap = covar[k][i];
                covar[k][i] = covar[j][i];
                covar[j][i] = swap;
            }
            k--;
        }
    }
}



/**@}*/  



