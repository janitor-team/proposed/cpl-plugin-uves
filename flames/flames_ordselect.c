/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/* Program  : ordselect.c                                                  */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif



/* MIDAS include files */
#include <flames_midas_def.h>
/* FLAMES-UVES include files */ 
#include <flames_ordselect.h>
#include <flames_uves.h>
#include <uves_msg.h>

flames_err 
ordselect(
                orderpos *Order,
                flames_frame *ScienceFrame,
                allflats *Shifted_FF,
                int32_t *ordend)
{

    int32_t j=0;
    int32_t myorder=0;
    int32_t n=0;
    int32_t lowlimglobal=0;
    int32_t highlimglobal=0;
    int32_t lowlimthis=0;
    int32_t highlimthis=0;
    char overlap=0;

    frame_mask *fmvecbuf1=0;
    int32_t *lvecbuf1=0;
    int32_t *lvecbuf2=0;
    int32_t myorderifibreoffset=0;
    int32_t ifibre=0;
    int32_t myorderifibrejindex=0;

    fmvecbuf1 = Shifted_FF->goodfibres[0][0];
    lvecbuf1 = Shifted_FF->lowfibrebounds[0][0];
    lvecbuf2 = Shifted_FF->highfibrebounds[0][0];

    for (j=0; j<=(ScienceFrame->subcols-1); j++) {
        /* find the upper and lower limits of the set of slices to be
       extracted at this x and order */
        myorder = *ordend-Order->firstorder;
        myorderifibreoffset = myorder*Shifted_FF->maxfibres;
        for (n=0; (n<=(ScienceFrame->num_lit_fibres-1)) &&
        fmvecbuf1[myorderifibrejindex=
                        ((myorderifibreoffset+
                                        (ifibre=ScienceFrame->ind_lit_fibres[n]))*
                                        ScienceFrame->subcols)+j]==BADSLICE;
        n++);
        if (n<=(ScienceFrame->num_lit_fibres-1)) {
            /* I found the first extractable one */
            lowlimglobal = lvecbuf1[myorderifibrejindex];
            highlimglobal = lvecbuf2[myorderifibrejindex];
            /* find any other extractable slices */
            for (n++; n<=(ScienceFrame->num_lit_fibres-1); n++) {
                ifibre=ScienceFrame->ind_lit_fibres[n];
                myorderifibrejindex=
                                ((myorderifibreoffset+ifibre)*ScienceFrame->subcols)+j;
                if (fmvecbuf1[myorderifibrejindex]!=BADSLICE) {
                    if (lowlimglobal > lvecbuf1[myorderifibrejindex])
                        lowlimglobal = lvecbuf1[myorderifibrejindex];
                    if (highlimglobal < lvecbuf2[myorderifibrejindex])
                        highlimglobal = lvecbuf2[myorderifibrejindex];
                }
            }

            /* are there any more overlapping orders? */
            overlap=1;
            while (*ordend<Order->lastorder && overlap==1) {
                overlap=0;
                myorder++;
                myorderifibreoffset = myorder*Shifted_FF->maxfibres;
                /* find the upper and lower limits of the set of slices to be
	   extracted at this x and order */
                for (n=0; (n<=(ScienceFrame->num_lit_fibres-1)) &&
                fmvecbuf1[myorderifibrejindex=
                                ((myorderifibreoffset+
                                                (ifibre=ScienceFrame->ind_lit_fibres[n]))*
                                                ScienceFrame->subcols)+j]==BADSLICE;
                n++);
                if (n<=(ScienceFrame->num_lit_fibres-1)) {
                    /* I found the first extractable one */
                    lowlimthis = lvecbuf1[myorderifibrejindex];
                    highlimthis = lvecbuf2[myorderifibrejindex];
                    for (n++; n<=(ScienceFrame->num_lit_fibres-1); n++) {
                        ifibre=ScienceFrame->ind_lit_fibres[n];
                        myorderifibrejindex=
                                        ((myorderifibreoffset+ifibre)*ScienceFrame->subcols)+j;
                        if (fmvecbuf1[myorderifibrejindex]!=BADSLICE) {
                            if (lowlimthis > lvecbuf1[myorderifibrejindex])
                                lowlimthis = lvecbuf1[myorderifibrejindex];
                            if (highlimthis < lvecbuf2[myorderifibrejindex])
                                highlimthis = lvecbuf2[myorderifibrejindex];
                        }
                    }
                    /* do the new orders overlap the global ones? */

                    /*
          uves_msg_warning("low=%d high=%d lowlimglobal=%d highlimglobal=%d",
			    lowlimthis,highlimthis,lowlimglobal,highlimglobal);
                     */
                    if ((lowlimthis>=lowlimglobal && lowlimthis<=highlimglobal) ||
                                    (highlimthis>=lowlimglobal && highlimthis<=highlimglobal)) {
                        /* yes, they do */
                        (*ordend)++;
                        overlap=1;
                        if (lowlimthis<lowlimglobal) lowlimglobal=lowlimthis;
                        if (highlimthis>highlimglobal) highlimglobal=highlimthis;
                    }
                }
            }
        }
    }

    return NOERR;

}

