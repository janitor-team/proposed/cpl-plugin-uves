/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_freeslitsflats Free memory allocated by slit flat fields 
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_freeslitflats.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>



/* Input:  pointer to allslitflats                                         */ 
/*                                                                      */
/* Output:  none                                                            */
/*                                                                         */
/*                                                                         */ 

/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_freeslitflats()  
   @short Free memory allocated by slit flat fields
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param frameid
   @param slitflats

   @return success or failure code

   DRS Functions called:                                                 
   free_fdmatrix                                                          
   free_fmmatrix                                                         
   free_lmatrix                                                           
   free_cvector                                                           

   Pseudocode:                                                           
   Call DRS functions to free allocated memory                            

   @note
 */
flames_err 
freeslitflats(allslitflats *slitflats)
{
    int32_t iframe=0;
    int32_t slitflats_subcols_1 = slitflats->subcols-1;
    int32_t slitflats_subrows_1 = slitflats->subrows-1;

    int32_t slitflats_n_orders = slitflats->lastorder-slitflats->firstorder;


    /* free submembers of slit */
    for (iframe=0; iframe<=slitflats->nflats-1; iframe++) {
        free_fdmatrix(slitflats->slit[iframe].data,
                        0, slitflats_subrows_1, 0, slitflats_subcols_1);
        free_fdmatrix(slitflats->slit[iframe].sigma,
                      0, slitflats_subrows_1, 0, slitflats_subcols_1);
        free_fmmatrix(slitflats->slit[iframe].badpixel,
                      0, slitflats_subrows_1, 0, slitflats_subcols_1);
        free_cvector(slitflats->slit[iframe].framename, 0, CATREC_LEN);
        free_cvector(slitflats->slit[iframe].sigmaname, 0, CATREC_LEN);
        free_cvector(slitflats->slit[iframe].badname, 0, CATREC_LEN);
        free_cvector(slitflats->slit[iframe].boundname, 0, CATREC_LEN);
        free_lmatrix(slitflats->slit[iframe].lowbound,
                     0, slitflats_n_orders,
                     0, slitflats_subcols_1);
        free_lmatrix(slitflats->slit[iframe].highbound,
                     0, slitflats_n_orders,
                     0, slitflats_subcols_1);
    }
    /* free slit member array itself */
    free(slitflats->slit);
    /* free other dynamic allslitflats members */
    free_fdmatrix(slitflats->normfactor,
                  0, slitflats_n_orders,
                  0, slitflats_subcols_1);
    free_lmatrix(slitflats->lowbound,
                 0, slitflats_n_orders,
                 0, slitflats_subcols_1);
    free_lmatrix(slitflats->highbound,
                 0, slitflats_n_orders,
                 0, slitflats_subcols_1);
    free_fmmatrix(slitflats->goodx,
                  0, slitflats_n_orders,
                  0, slitflats_subcols_1);
    /* ok, finished */
    return(NOERR);
}
/**@}*/









