/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_initshiftedff   Substep: initialize shifted ff frm
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/



#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_initshiftedff.h>
#include <flames_newmatrix.h>
#include <flames_shiftcommon.h>
#include <flames_shiftall.h>
#include <flames_alloconeflats.h>



/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_checksize()  
   @short  check input frame size descriptors
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param allflatsin 
   @param allflatsout

   @return success or failure code

   DRS Functions called:          
   none                                         

   Pseudocode:                                                             

   @note
 */
flames_err  
initshiftedff(
                allflats *allflatsin,
                allflats *allflatsout)
{
    /* TODO: check the proper order of the dimensions in the allocation of the
     matrix */
    /* BEWARE: the matrix is allocated with the indices starting from 1 and
     not zero */
    /* BEWARE2: I changed my mind, the matrix is allocated with the indices
     starting from zero */
    int32_t iframe=0;
    int32_t ifibre=0;
    int32_t iorderifibreixtotal=0;
    //int status=0;

    iorderifibreixtotal = ((1+allflatsin->lastorder-allflatsin->firstorder)*
                    allflatsin->maxfibres*allflatsin->subcols);

    /* copy all relevant scalars from allflatsin */
    allflatsout->nflats = allflatsin->nflats;
    allflatsout->subrows = allflatsin->subrows;
    allflatsout->subcols = allflatsin->subcols;
    /*
    allflatsout->chiprows = allflatsin->chiprows;
    allflatsout->chipcols = allflatsin->chipcols;
    allflatsout->subfirstrow = allflatsin->subfirstrow;
    allflatsout->subfirstcol = allflatsin->subfirstcol;
     */
    allflatsout->substartx = allflatsin->substartx;
    allflatsout->substarty = allflatsin->substarty;
    allflatsout->substepx = allflatsin->substepx;
    allflatsout->substepy = allflatsin->substepy;
    /*
    allflatsout->chipstartx = allflatsin->chipstartx;
    allflatsout->chipstarty = allflatsin->chipstarty;
    allflatsout->chipstepx = allflatsin->chipstepx;
    allflatsout->chipstepy = allflatsin->chipstepy;
     */
    allflatsout->chipchoice = allflatsin->chipchoice;
    allflatsout->ron = allflatsin->ron;
    allflatsout->gain = allflatsin->gain;
    allflatsout->maxfibres = allflatsin->maxfibres;
    allflatsout->pixmax = allflatsin->pixmax;
    allflatsout->halfibrewidth = allflatsin->halfibrewidth;
    allflatsout->minfibrefrac = allflatsin->minfibrefrac;
    allflatsout->firstorder = allflatsin->firstorder;
    allflatsout->lastorder = allflatsin->lastorder;
    allflatsout->tab_io_oshift = allflatsin->tab_io_oshift;
    allflatsout->numfibres = allflatsin->numfibres;
    allflatsout->shiftable = allflatsin->shiftable;
    allflatsout->normalised = allflatsin->normalised;

    /* allocate array members of allflatsout */
    if (NOERR != alloconeflats(allflatsout)) {
        /* error allocating allflatsout */
        return flames_midas_fail();
    }
    if (allflatsin->nflats>0) {
        for (iframe=0; iframe<=(allflatsin->nflats-1); iframe++) {
            /* copy only the scalar members of flatdata here, the
	 unchanged arrays will be copied by shiftffn */
            /* lit fibres are the same as in the unshifted frames */
            ((allflatsout->flatdata)[iframe]).numfibres =
                            ((allflatsin->flatdata)[iframe]).numfibres;
            /* the yshift is set to 0 here, as it will be compensated in the shift */
            ((allflatsout->flatdata)[iframe]).yshift = 0;
        }
    }
    else {
        /* nflats == 0, something is very wrong here */
        return flames_midas_fail();
    }
    for (ifibre=0; ifibre<=allflatsin->maxfibres-1; ifibre++) {
        allflatsout->fibremask[ifibre] = allflatsin->fibremask[ifibre];
        allflatsout->fibre2frame[ifibre] = allflatsin->fibre2frame[ifibre];
    }
    /*the normfactors and normsigmas must not be affected by shifting
    (that's what the "shifting" problem is all about!) so they are just 
    copied over from allflatsin. I might even copy just the pointer, 
    but this would be begging for trouble right now... */
    memcpy(allflatsout->normfactors[0][0], allflatsin->normfactors[0][0],
           (size_t)(iorderifibreixtotal)*sizeof(frame_data));
    memcpy(allflatsout->normsigmas[0][0], allflatsin->normsigmas[0][0],
           (size_t)(iorderifibreixtotal)*sizeof(frame_data));
    /* copy over from allflatsin also goodfibres: bad ix/fibre/order
     points are typically the ones that were not normalisable, hence 
     unusable at all */
    memcpy(allflatsout->goodfibres[0][0], allflatsin->goodfibres[0][0],
           (size_t)(iorderifibreixtotal)*sizeof(frame_mask));
    /* lowfibrebounds and highfibrebounds do not need to be initialised,
     they will get the right values in the main loop while shifting, 
     therefore there is nothing to be done here for them */

    return(NOERR);

}

/**@}*/







